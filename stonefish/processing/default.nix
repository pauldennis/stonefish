{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, stdenv ? pkgs.stdenv

, wabt ? pkgs.wabt
, binaryen ? pkgs.binaryen
, tree ? pkgs.tree
, hexyl ? pkgs.hexyl
, gpp ? pkgs.gpp

, ghc ? import ./glasgowHaskellCompiler.nix {}

}:


stdenv.mkDerivation {

  name = "stonefish";
  builder = ./builder.sh;

  source = ./source;

  buildInputs = [
    wabt
    binaryen
    tree
    ghc
    hexyl
    gpp
  ];

#   TURN_OFF_TESTSUITE = "no-testsuite";

}
