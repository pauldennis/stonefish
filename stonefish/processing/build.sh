set -e

NO_INTERNET_NIX_FUCKUP=$1

if [[ "$NO_INTERNET_NIX_FUCKUP" == "no-internet" ]] ; then
  NIX_BUILD_NO_INTERNET_MITIGATION="--no-substitute"
fi

nix-build --pure --show-trace $NIX_BUILD_NO_INTERNET_MITIGATION
