{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, ghc ? pkgs.haskellPackages.ghcWithPackages (pkgs: with pkgs; [
    utf8-string
    safe
    byteable
    split
    either
    hashing
    memory
    extra
    containers
    transformers
    utility-ht
    composition-prelude
  ])

}:

ghc
