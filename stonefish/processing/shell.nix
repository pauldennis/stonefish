{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
, glibcLocales ? pkgs.glibcLocales

, eza ? pkgs.eza
, wabt ? pkgs.wabt
, binaryen ? pkgs.binaryen

}:

pkgs.mkShell {
  buildInputs = [
    eza
    wabt
    binaryen
  ];

  shellHook = ''
    echo this is a nix shell; alias l="ls -al"
  '';

  # excruciating unicode and whatnot
  LOCALE_ARCHIVE="${glibcLocales}/lib/locale/locale-archive";
  LANG="en_GB.UTF8";


}
