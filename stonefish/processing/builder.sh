echo "START###############################################"

set -e

source $stdenv/setup

# copy everything to temporary folder where we have write permission
cp -r $source/* $TMPDIR



ghc \
  -main-is Inject_Profiling_Info.inject_profiling_info \
  Inject_Profiling_Info.hs



pushd $TMPDIR

  echo "#### begin gpp precompiler"

  precompiler()
  {
    echo preprocessor
    echo "  " for $1
    echo "  " from $2
    echo "  " to $3
    echo "  " with seed $4
    echo -n "  ..."

    gpp \
      $2 \
      -o $3 \
      --nostdinc \
      --nocurinc \
      -I . \
      --includemarker ";; line %1 (%, %)" \
      -U "" "" "(" "," ")" "(" ")" "#" "\\" \
      -M "#" "\n" " " " " "\n" "(" ")" \
      -n \
      -DBUILD_NAME=$1 \
      -DSTONEFISH_SEED=$4 \
      -x \
      2> /dev/stdout \
      | (grep \
          ': warning: the defined(...) macro is already defined' \
          --invert-match \
        || true \
        )

      # TODO upstream BUG https://github.com/logological/gpp/issues/55

    echo "done"
  }

  precompiler \
    debug \
    $TMPDIR/stonefish_debug.wat \
    $TMPDIR/build_debug.wat

  precompiler \
    noBreakpointTriggered \
    $TMPDIR/stonefish_noBreakpointTriggered.wat \
    $TMPDIR/build_noBreakpointTriggered.wat

  precompiler \
    noAssertions \
    $TMPDIR/stonefish_noAssertions.wat \
    $TMPDIR/build_noAssertions.wat

  precompiler \
    noFluff \
    $TMPDIR/stonefish_noFluff.wat \
    $TMPDIR/build_noFluff.wat

  precompiler \
    release \
    $TMPDIR/stonefish_release.wat \
    $TMPDIR/build_release.wat

  precompiler \
    profiled \
    $TMPDIR/stonefish_release_profiled.wat \
    $TMPDIR/build_release_profiled.wat

#   # debug shortcut output
#   mkdir $out
#   cp debug.assert_index $out/
#   cp build_debug.wat $out/
#   wat2wasm --version
#   echo "debug inspection"
#   exit 0

  DETERMINISTIC_RANDOM_VALUE=$(sha512sum $TMPDIR/build_debug.wat | head -c 128)

  precompiler \
    someAssertions \
    $TMPDIR/stonefish_someAssertions.wat \
    $TMPDIR/build_someAssertions.wat \
    "${DETERMINISTIC_RANDOM_VALUE}_someAssertions"

  precompiler \
    someOtherAssertions \
    $TMPDIR/stonefish_someOtherAssertions.wat \
    $TMPDIR/build_someOtherAssertions.wat \
    "${DETERMINISTIC_RANDOM_VALUE}_someOtherAssertions"



  echo "#### begin wat2wasm"

  translate()
  {
    echo " * $1:"

    invokation()
    {
      wat2wasm \
        build_$1.wat \
        -o stonefishRuntime_$1.wasm \
        --debug-names \
        --enable-multi-memory \
        --disable-simd
    }

    set +e
      invokation $1 2> wat2wasm_invokation_file_output
      errorCode=$?
    set -e

    if [ $errorCode -eq 0 ]; then
      :
    else
      head --lines $(runhaskell ExtractLineNumberForUsabilityHack.hs) build_$1.wat | tail --lines 100
      invokation $1
      exit 10
    fi

  }

  translate "debug"
  translate "noBreakpointTriggered"
  translate "noAssertions"
  translate "noFluff"
  translate "someAssertions"
  translate "someOtherAssertions"
  translate "release"
  translate "release_profiled"

  commen_optimize()
  {
    wasm-opt \
      \
      --disable-simd \
      \
      --enable-multivalue \
      --enable-bulk-memory \
      --enable-reference-types \
      --enable-multimemory \
      \
      --debuginfo \
      \
      -O4 \
      --inline-functions-with-loops \
      --flexible-inline-max-function-size 10000000000 \
      --partial-inlining-ifs 1000000000 \
      --converge \
      --generate-global-effects \
      --ignore-implicit-traps \
      \
      $TMPDIR/stonefishRuntime_$1.wasm \
      --output "$TMPDIR/stonefishRuntime_$2.$3" \
      $4 \
      $5 \
      $6
  }

  optimize()
  {
    commen_optimize $1 $2 "wat" $3 --emit-text
    commen_optimize $1 $2 "wasm" $3
  }

  echo "#### begin wasm-opt"

  # TODO currently there are traps happening on purpose so --traps-never-happen is incorrect
  optimize debug debug_optimized

  optimize noFluff noFluff_untrustworthyOptimizations --traps-never-happen
  optimize noFluff noFluff

  optimize release release_untrustworthyOptimizations --traps-never-happen
  optimize release release

  optimize release_profiled release_profiled_untrustworthyOptimizations --traps-never-happen

  echo "#### begin wasm2c $(wasm2c --version)"

  wasm2c \
    stonefishRuntime_noAssertions.wasm \
    -o stonefishRuntime_noAssertions.c \
    --enable-multi-memory \
    --disable-simd

  wasm2c \
    stonefishRuntime_debug.wasm \
    -o stonefishRuntime_debug.c \
    --enable-multi-memory \
    --disable-simd


  if [[ "$TURN_OFF_TESTSUITE" == "no-testsuite" ]] ; then
    : # no operation
  else
    runhaskell \
      --ghc-arg="-main-is" \
      --ghc-arg="normal_testsuit" \
      CheckTestCaseOutput.hs
  fi

popd

# create the output folder
mkdir -p $out


# copy to output directory
folder_WASM=$out/stonefishRuntime_WASM
mkdir $folder_WASM/
cp -r $TMPDIR/stonefishRuntime_debug.wasm $folder_WASM/stonefishRuntime_debug.wasm
# cp -r $TMPDIR/stonefishRuntime_debug_optimized.wasm $folder_WASM/stonefishRuntime_debug_optimized.wasm
# cp -r $TMPDIR/stonefishRuntime_noAssertions.wasm $folder_WASM/stonefishRuntime_noAssertions.wasm
# cp -r $TMPDIR/stonefishRuntime_someAssertions.wasm $folder_WASM/stonefishRuntime_someAssertions.wasm
# cp -r $TMPDIR/stonefishRuntime_someOtherAssertions.wasm $folder_WASM/stonefishRuntime_someOtherAssertions.wasm
# cp -r $TMPDIR/stonefishRuntime_noFluff_untrustworthyOptimizations.wasm $folder_WASM/stonefishRuntime_noFluff_untrustworthyOptimizations.wasm
# cp -r $TMPDIR/stonefishRuntime_noFluff.wasm $folder_WASM/stonefishRuntime_noFluff.wasm
# cp -r $TMPDIR/stonefishRuntime_release_untrustworthyOptimizations.wasm $folder_WASM/stonefishRuntime_release_untrustworthyOptimizations.wasm
cp -r $TMPDIR/stonefishRuntime_release.wasm $folder_WASM/stonefishRuntime_release.wasm
cp -r $TMPDIR/stonefishRuntime_release_profiled_untrustworthyOptimizations.wasm $folder_WASM/stonefishRuntime_release_profiled_untrustworthyOptimizations.wasm




folder_Explore=$out/stonefishRuntime_Explore
mkdir $folder_Explore
cp -r $TMPDIR/build_debug.wat $folder_Explore/stonefishRuntime_debug.wat
cp -r $TMPDIR/build_noAssertions.wat $folder_Explore/stonefishRuntime_noAssertions.wat
cp -r $TMPDIR/build_someAssertions.wat $folder_Explore/stonefishRuntime_someAssertions.wat
cp -r $TMPDIR/build_someOtherAssertions.wat $folder_Explore/stonefishRuntime_someOtherAssertions.wat

# cp -r $TMPDIR/stonefishRuntime_debug_optimized.wat $folder_Explore/stonefishRuntime_debug_optimized.wat
# cp -r $TMPDIR/stonefishRuntime_noFluff_untrustworthyOptimizations.wat $folder_Explore/stonefishRuntime_noFluff_untrustworthyOptimizations.wat
# cp -r $TMPDIR/stonefishRuntime_noFluff.wat $folder_Explore/stonefishRuntime_noFluff.wat
# cp -r $TMPDIR/stonefishRuntime_release_untrustworthyOptimizations.wat $folder_Explore/stonefishRuntime_release_untrustworthyOptimizations.wat
# cp -r $TMPDIR/stonefishRuntime_release.wat $folder_Explore/stonefishRuntime_release.wat
cp -r $TMPDIR/stonefishRuntime_release_profiled_untrustworthyOptimizations.wat $folder_Explore/stonefishRuntime_release_profiled_untrustworthyOptimizations.wat
cp $TMPDIR/number_of_performance_knobs $folder_Explore/number_of_performance_knobs

cp debug.assert_index $folder_Explore/




folder_C=$out/stonefishRuntime_C/
mkdir $folder_C
cp -r $TMPDIR/stonefishRuntime_noAssertions.c $folder_C/stonefishRuntime_noAssertions.c
cp -r $TMPDIR/stonefishRuntime_noAssertions.h $folder_C/stonefishRuntime_noAssertions.h
cp -r $TMPDIR/stonefishRuntime_debug.c $folder_C/stonefishRuntime_debug.c
cp -r $TMPDIR/stonefishRuntime_debug.h $folder_C/stonefishRuntime_debug.h




pushd $out
  tree
popd

echo "STOP################################################"
