
;;;;;;::::::::::::::::::::::::::::::
;; uncomment to test by hand that
;; non trapping NEGATIVE_TEST
;; cases are actually detected
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (func
;;   $TRAPPING_TEST_this_test_case_should_prevent_the_build_process
;;   (export "TRAPPING_TEST_this_test_case_should_prevent_the_build_process")
;;   ;; no trap here, now the build system chould reject building
;; )



;; these tests are supposed to allways trap even when a subset of individual asserts are turned off
#ifdef STONEFISH_EXPORT_TRAPPING_TESTS
  (export "TRAPPING_TEST_unreachable" (func $TRAPPING_TEST_unreachable))
#endif


;; these tests are trapping when the assertions are turned on
#ifdef STONEFISH_EXPORT_TRIGGER_ASSERT_TEST
  (export "TRIGGER_ASSERT_TEST_pop_from_empty_stack" (func $TRIGGER_ASSERT_TEST_pop_from_empty_stack))
  (export "TRIGGER_ASSERT_TEST_no_double_free" (func $TRIGGER_ASSERT_TEST_no_double_free))
#endif

