
;; that way one needs only one break point for breaking on asserts
(func
  $try_trigger_undefined_behaviour

  (param $assertIdentification i64)

  ;; TODO should have a ifdef for every occurence of $breakpoint? or like this
  ;; TODO how to do things with breakpoints?
    ;; calling break points in debug mode is allowed
    ;; calling break points while doing tests in debug mode means test failure

  ;; NOTE when debugging mechanism are disabled we are calling several times unreachable which should be ok
  (call $unconditional_breakpoint
    (local.get $assertIdentification)
  )
  (call $heapdump)

  unreachable
)

;; Every occurence of the $assert function could be removed form the source code without changing the semantics of the program.
;; A smart compiler might interpret the occurence of the unreachable in the implementation as undefined behaviour.
;; One could make unpure reads. In this case the smart compiler might have less possibilies for optimisation.
;; Although possible but probably discouraged one can make unpure writes. But every assert must be removable individually.
;; In case of some unpure assertions that are intertwined $assert is not appropriate to use. In this case use the assert groups.
#if STONEFISH_SHOULD_HAVE_ASSERT_FUNCTION_AT_ALL
  (func
    $\assert

    (param $supposedToBeTrue i32)

    (if
      (local.get $supposedToBeTrue)
      (then
      )
      (else
        (call $try_trigger_undefined_behaviour
          (i64.const 1) ;;TODO
        )
      )
    )
  )
  (func
    $identified_assert

    (param $assertNumber i64)

    (param $supposedToBeTrue i32)

    (if
      (local.get $supposedToBeTrue)
      (then
      )
      (else
        (call $try_trigger_undefined_behaviour
          (local.get $assertNumber)
        )
      )
    )
  )
#endif





;; impure assert groups have intertwined unpure behaviour. Occurences are to be removed in RELESE mode as it is considered too complicated for a compiler to detect that they could be optimised out. Althought it clould be interesting if one could have a compiler that takes a set of programms as input and the user promises that they are all equivalent.

(func
  $assert_for_assert_groups
  (param $supposedToBeTrue i32)

  (if
    (local.get $supposedToBeTrue)
    (then
    )
    (else
      (call $try_trigger_undefined_behaviour
        (i64.const 2) ;;TODO
      )
    )
  )
)

#ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILSTACKELEMENTS
  (func
    $impure_assert_group.StackDebug
    (param $supposedToBeTrue i32)

    (call $assert_for_assert_groups
      (local.get $supposedToBeTrue)
    )
  )
#endif

#ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
  (func
    $impure_assert_group.HeapDebug
    (param $supposedToBeTrue i32)

    (call $assert_for_assert_groups
      (local.get $supposedToBeTrue)
    )
  )
#endif


;; TODO remove typo invarian
#ifdef STONEFISH__INVARIANT_VARIATION__SHALLOW_INLINE_CONSTRUCTOR_SWAP
  (func
    $impure_assert_group.ShallowInlineConstructor_Invarian_Variation
    (param $supposedToBeTrue i32)

    (call $assert_for_assert_groups
      (local.get $supposedToBeTrue)
    )
  )
#endif


#ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILLEASHES_FOR_FORGOTTEN_BACKREFERENCES_DETECTION
  (func
    $impure_assert_group.SpoilLeashes_For_Forgotten_Backreference_detection
    (param $supposedToBeTrue i32)

    (call $assert_for_assert_groups
      (local.get $supposedToBeTrue)
    )
  )
#endif


#ifdef STONEFISH_DEBUG_DETECT_UNALLOWED_MULTITHREADING_USAGE
  (func
    $impure_assert_group.DetectUnallowedMultithreadingUsage
    (param $supposedToBeTrue i32)

    (call $assert_for_assert_groups
      (local.get $supposedToBeTrue)
    )
  )
#endif


#ifdef STONEFISH_DEBUG_WIGGLE_FIRST_ALLOCATED_DUPLICATION_IDENTIFICATION
  (func
    $impure_assert_group.reserve_some_unused_duplication_identifications
    (param $supposedToBeTrue i32)

    (call $assert_for_assert_groups
      (local.get $supposedToBeTrue)
    )
  )
#endif

