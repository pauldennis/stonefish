(func
  $TRAPPING_TEST_unreachable
  unreachable
)

(func
  $TESTING
  (result i64)

  (call $TEST_last_page_is_not_available)
  (call $TEST_understanding_the_language)
  (call $TEST_languageHelpers)
  (call $TEST_commen_library)
  (call $TEST_not)
  (call $TEST_devisibility)
  (call $TEST_overlows)
  (call $TEST_isInBetween)


  (call $TEST_Finger)
  (call $TEST_isDevisibleByFingerByteSize)


  (call $TEST_subtraction)
  (call $TEST_isDevisibleByElementSize)

  (call $TEST_assumtion_about_pointer_arithmetic)

  (call $TEST_little_stack_test)
  (call $TEST_STACK_UNUSED_MARKING_not_a_valid_heap_index)

  (call $TEST_RUNGS)

  (i64.const 0)
)


