
(func
  $unconditional_breakpoint

  (param $id i64)

  ;; TODO do dead end!
  #ifdef STONEFISH_DEBUG_TOOLS__BREAKPOINT
    (call $debug.breakpoint
      (local.get $id)
    )
  #else
    unreachable
  #endif
)

(func
  $identified_conditional_breakpoint
  (param $id i64)
  (param $supposedToBeTrue i32)

  (if
    (local.get $supposedToBeTrue)
    (then
    )
    (else
      (call $unconditional_breakpoint
        (local.get $id)
      )
    )
  )
)

(func
  $breakpoint_on_false
  (param $id i64)
  (param $supposedToBeTrue i32)

  (if
    (local.get $supposedToBeTrue)
    (then
    )
    (else
      (call $unconditional_breakpoint
        (local.get $id)
      )
    )
  )
)

(func
  $breakpoint_on_true
  (param $id i64)
  (param $supposedToBeTrue i32)

  (if
    (local.get $supposedToBeTrue)
    (then
      (call $unconditional_breakpoint
        (local.get $id)
      )
    )
    (else
    )
  )
)
