

#ifdef STONEFISH_EXPORT_LINKING
  (import
    "link"
    "dereferenceSubNodeLocation"
    (func $link.dereferenceSubNodeLocation
      (param $array externref)
      (param $location i64)

      (result i64)
    )
  )

  (import
    "link"
    "memorizeCOPYLocation"
    (func $link.memorizeCOPYLocation
      (param $blueprint externref)
      (param $ORIGINAL_location i64)
      (param $COPY_location i64)
    )
  )

  (import
    "link"
    "isCOPYLocationSet"
    (func $link.isCOPYLocationSet
      (param $blueprint externref)
      (param $ORIGINAL_location i64)

      (result i32)
    )
  )

  (import
    "link"
    "recallCOPYLocation"
    (func $link.recallCOPYLocation
      (param $blueprint externref)
      (param $ORIGINAL_location i64)

      (result i64)
    )
  )

  (import
    "link"
    "constructArchetype"
    (func $link.constructArchetype
      (param $melee externref)

      (result externref) ;; archetype
    )
  )


  ;; TODO rename install_fragments to link?
  (import
    "install_fragments"
    "extractMyRuleRightSide"
    (func $install_fragments.extractMyRuleRightSide
      (param $ruleBluePrints externref)
      (param $ruleIndentification i64)
      (param $matchedConstructor i64)

      (result i64) ;; mapping
      (result i64) ;; root
      (result externref) ;; melee
    )
  )
  (import
    "install_fragments"
    "check_for_rule_existence"
    (func $install_fragments.check_for_rule_existence
      (param $ruleBluePrints externref)
      (param $ruleIndentification i64)

      (result i32)
    )
  )


  (import
    "link"
    "get_entry_fragment_duplication_identification_count"
    (func $link.get_entry_fragment_duplication_identification_count
      (param $rulebook externref)
      (result i64)
    )
  )
#endif
