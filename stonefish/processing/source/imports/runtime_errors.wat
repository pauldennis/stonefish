
;; TODO wrap the imports and put an unreachable at the end
;; TODO rename to runtime_error?

(import
  "runtime_errors"
  "cannot_apply_x"
  (func $runtime_errors.cannot_apply_x
    (param $finger i64)
  )
)

(import
  "runtime_errors"
  "left_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number_but_x"
  (func $runtime_errors.left_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number
    (param $finger i64)
  )
)

(import
  "runtime_errors"
  "right_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number"
  (func $runtime_errors.right_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number
    (param $finger i64)
  )
)

(import
  "runtime_errors"
  "addition_cannot_be_represented"
  (func $runtime_errors.addition_cannot_be_represented
    (param $left i32)
    (param $right i32)
  )
)

(import
  "runtime_errors"
  "ran_out_of_duplication_labels"
  (func $runtime_errors.ran_out_of_duplication_labels
  )
)

(import
  "runtime_errors"
  "expected_from_interface_to_return_a_constructor_but_got_x"
  (func $runtime_errors.expected_from_interface_to_return_a_constructor_but_got_x
  )
)

(import
  "runtime_errors"
  "expected_from_interface_to_return_a_constructor_called_ReturnNewPrivateDataAndSystemRequest_but_got_x"
  (func $runtime_errors.expected_from_interface_to_return_a_constructor_called_ReturnNewPrivateDataAndSystemRequest_but_got_x
  )
)

(import
  "runtime_errors"
  "expected_from_interface_to_return_a_constructor_with_arity_of_two_but_got_x"
  (func $runtime_errors.expected_from_interface_to_return_a_constructor_with_arity_of_two_but_got_x
  )
)

(import
  "runtime_errors"
  "to_do__not_implemented"
  (func $runtime_errors.to_do__not_implemented
  )
)

;; TODO there is a place where this import is missused. another version of of this one is
(import
  "runtime_errors"
  "expected_type_x_and_id_x_or_Superposition_but_got_x"
  (func $runtime_errors.expected_type_x_and_id_x_or_Superposition_but_got_x
    (param $type i64)
    (param $id i64)
    (param $offendingTerm i64)
  )
)

(import
  "runtime_errors"
  "expected_type_x_or_Superposition_but_got_x"
  (func $runtime_errors.expected_type_x_or_Superposition_but_got_x
    (param $type i64)
    (param $offendingTerm i64)
  )
)

(import
  "runtime_errors"
  "expected_type_x_but_got_x"
  (func $runtime_errors.expected_type_x_but_got_x
    (param $type i64)
    (param $offendingTerm i64)
  )
)

(import
  "runtime_errors"
  "expected_constructor_but_got_x"
  (func $runtime_errors.expected_constructor_but_got_x
    (param $offendingTerm i64)
  )
)

(import
  "runtime_errors"
  "expected_constructor_for_pattern_matching_or_Superposition_but_got_x"
  (func $runtime_errors.expected_constructor_for_pattern_matching_or_Superposition_but_got_x
    (param $offendingTerm i64)
  )
)

(import
  "runtime_errors"
  "expected_constructor_leaf_but_got_x"
  (func $runtime_errors.expected_constructor_leaf_but_got_x
    (param $offendingHeader i64)
  )
)




(import
  "runtime_errors"
  "ran_out_of_duplication_identification_numbers"
  (func $runtime_errors.ran_out_of_duplication_identification_numbers
    (param $next_free_duplication_identification i64)
    (param $unsuccessfully_tried_allocate_this_many i64)
  )
)
