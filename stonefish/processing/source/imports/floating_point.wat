
(import
  "rounding_fiasco"
  "f32_multiplicate_floor"
  (func $f32.multiplicate_floor
    (param $left f32)
    (param $right f32)
    (result f32)
  )
)
(import
  "rounding_fiasco"
  "f32_add_floor"
  (func $f32.add_floor
    (param $left f32)
    (param $right f32)
    (result f32)
  )
)
(import
  "rounding_fiasco"
  "f32_squareRoot_floor"
  (func $f32.squareRoot_floor
    (param $radical f32)
    (result f32)
  )
)
(import
  "rounding_fiasco"
  "f32_divide_floor"
  (func $f32.divide_floor
    (param $left f32)
    (param $right f32)
    (result f32)
  )
)
