
#ifdef STONEFISH_DEBUG_TOOLS_HEAPDUMP
  (import
    "debug"
    "heapdump"
    (func $debug.heapdump
    )
  )
  (import
    "debug"
    "register_wrapper"
    (func $debug.register_wrapper
      (param $wrapper i64)
    )
  )
  (import
    "debug"
    "deregister_wrapper"
    (func $debug.deregister_wrapper
      (param $wrapper i64)
    )
  )
#endif
