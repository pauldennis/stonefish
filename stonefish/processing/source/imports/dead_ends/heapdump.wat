
#ifndef STONEFISH_DEBUG_TOOLS_HEAPDUMP
  (func $debug.heapdump
    unreachable
  )
  (func $debug.register_wrapper
    (param $wrapper i64)
    unreachable
  )
  (func $debug.deregister_wrapper
    (param $wrapper i64)
    unreachable
  )
#endif
