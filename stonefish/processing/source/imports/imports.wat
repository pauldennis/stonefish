
;; ;; for use with wasm-interp
;; (import
;;   "host"
;;   "print"
;;   (func $host.print
;;     (param $value i64)
;;   )
;; )


#ifdef STONEFISH_DEBUG_TOOLS
  (import
    "host"
    "i64_print"
    (func $i64.print
      (param $value i64)
    )
  )

  (import
    "host"
    "i32_print"
    (func $i32.print
      (param $value i32)
    )
  )


  (import
    "debug"
    "breakpoint"
    (func $debug.breakpoint
      (param $assertIdentification i64)
    )
  )
#endif







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;








;; TODO rename passes to passing from embedder to user program?
;; TODO move somewhere else
#ifdef STONEFISH_IMPORT_PASSES
  (import
    "stonefish_import_passes"
    "trigger_websocket_message"
    (func $stonefish_import_passes.trigger_websocket_message
      (param $bool_message_ExportWrapper i64)
    )
  )
#endif



