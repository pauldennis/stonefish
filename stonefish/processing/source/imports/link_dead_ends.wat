

#ifndef STONEFISH_EXPORT_LINKING
  (func $link.constructArchetype
    (param $melee externref)

    (result externref)

    unreachable
  )
#endif



#ifndef STONEFISH_EXPORT_LINKING
  (func $install_fragments.check_for_rule_existence
    (param $ruleBluePrints externref)
    (param $ruleIndentification i64)

    (result i32)

    unreachable
  )
#endif



#ifndef STONEFISH_EXPORT_LINKING
  (func
    $install_fragments.extractMyRuleRightSide
    (param $ruleBluePrints externref)
    (param $ruleIndentification i64)
    (param $matchedConstructor i64)

    (result i64) ;; mapping
    (result i64) ;; root
    (result externref) ;; melee

    (unreachable)
  )
#endif




#ifndef STONEFISH_EXPORT_LINKING
  (func $link.get_entry_fragment_duplication_identification_count
    (param $rulebook externref)
    (result i64)

    unreachable
  )
#endif
