

#ifdef STONEFISH_IMPORT_EVENTS
  (import
    "events"
    "lightSwitchNeedsToBeSetToBool"
    (func $events.lightSwitchNeedsToBeSetToBool
      (param $setToThisBool i64)
    )
  )
#endif

