


#ifdef STONEFISH_OPTIMIZATION_EXPLICIT_HEAP_IS_FULL_ERROR_MESSAGE
  (import
    "optimization_versus_ergonomics"
    "not_enough_space_left"
    (func $optimization_versus_ergonomics.not_enough_space_left
      (param $heap_space_left i64)
      (param $requested_size i64)
    )
  )
#endif

