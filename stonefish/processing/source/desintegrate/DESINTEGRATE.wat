;; TODO place?
;; have no return value?
(func
  $evaluate_to_weak_head_normal_form_ExportWrapped
  (param $ExportNode i64)
  (result i64)

  (local $rawHeader i64)
  (local $resultHeader i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $ExportNode)
      )
    )
  #endif

  (local.set $rawHeader
    (call $disaggregate_ExportWrapper
      (local.get $ExportNode)
    )
  )

  (local.set $rawHeader
    (call $evaluate_to_weak_head_normal_form
      (local.get $rawHeader)
    )
  )

  (local.set $resultHeader
    (call $compose_wrappedTerm_Finger
      (local.get $rawHeader)
    )
  )

  (local.get $resultHeader)
)



(func
  $disintegrate_WrappedBool
  (param $ExportNode i64)

  (result i32)

  (local $actual_wannebe_bool i64)

  (local $result_bool i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $ExportNode)
      )
    )
  #endif

  (local.set $actual_wannebe_bool
    (call $disaggregate_ExportWrapper
      (local.get $ExportNode)
    )
  )

  ;; TODO who is responsible for evaluating the header to weak head normal form?
  (if
    (call $isFingerABool
      (local.get $actual_wannebe_bool)
    )
    (then
    )
    (else
      ;; TODO how to report this error case?
      (call $runtime_errors.expected_type_x_but_got_x
        (call $CONSTRUCTOR_ID__False_Arity)
        (local.get $ExportNode)
      )
    )
  )

  (local.set $result_bool
    (call $disaggregate_Bool
      (local.get $actual_wannebe_bool)
    )
  )

  (local.get $result_bool)
)




(func
  $disintegrate_Wrapped_intrinsic_f32
  (param $ExportNode i64)

  (result f32)

  (local $actual_wannebe_f32 i64)

  (local $result_f32 f32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $ExportNode)
      )
    )
  #endif

  (local.set $actual_wannebe_f32
    (call $disaggregate_ExportWrapper
      (local.get $ExportNode)
    )
  )

  (local.set $actual_wannebe_f32
    (call $evaluate_to_weak_head_normal_form
      (local.get $actual_wannebe_f32)
    )
  )

  (if
    (call $isFingerAnIntrinsiscNumber
      (local.get $actual_wannebe_f32)
    )
    (then
    )
    (else
      ;; TODO how to report this error case?
      (call $runtime_errors.expected_type_x_but_got_x
        (call $INTRINSIC_NUMBER_TAG) ;; TODO how to have a better concept where error messages stay helpfull and do not degrade when copy-pasted?
        (local.get $ExportNode)
      )
    )
  )

  (local.set $result_f32
    (call $f32.disaggregate_IntrinsicNumber
      (local.get $actual_wannebe_f32)
    )
  )

  (local.get $result_f32)
)




(func
  $disintegrate_WrappedConstructorLeafHeader
  (param $ExportNode i64)

  (result i64)

  (local $actual_wannebe_constructor i64)

  (local $result_constructorLeaf_id i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $ExportNode)
      )
    )
  #endif

  (local.set $actual_wannebe_constructor
    (call $disaggregate_ExportWrapper
      (local.get $ExportNode)
    )
  )


  ;; TODO who is responsible for evaluating the header to weak head normal form?

  ;; TODO can this be asserted?
  (if
    (call $isFingerAConstructorLeaf
      (local.get $actual_wannebe_constructor)
    )
    (then
    )
    (else
      (call $runtime_errors.expected_constructor_leaf_but_got_x
        (local.get $actual_wannebe_constructor)
      )
    )
  )

  (local.set $result_constructorLeaf_id
    (call $disaggregate_ConstructorLeaf
      (local.get $actual_wannebe_constructor)
    )
  )

  (local.get $result_constructorLeaf_id)
)








(func
  $disintegrate_WrappedConstructorNode
  (param $ExportNode i64)

  (result i64)
  (result i64)

  (result i64)
  (result i64)
  (result i64)
  (result i64)
  (result i64)
  (result i64)
  (result i64)
  (result i64)

  (local $result_constructor_id i64)
  (local $result_constructor_arity i64)

  (local $result_constructor_child_0 i64)
  (local $result_constructor_child_1 i64)
  (local $result_constructor_child_2 i64)
  (local $result_constructor_child_3 i64)
  (local $result_constructor_child_4 i64)
  (local $result_constructor_child_5 i64)
  (local $result_constructor_child_6 i64)
  (local $result_constructor_child_7 i64)


  (local $actual_wannebe_constructor i64)
  (local $constructorNode i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $ExportNode)
      )
    )
  #endif

  (local.set $actual_wannebe_constructor
    (call $disaggregate_ExportWrapper
      (local.get $ExportNode)
    )
  )


  ;; TODO evaluate the expression for the user?
    ;; TODO have concept for who is responsible for evaluating the header to weak head normal form?
  (local.set $actual_wannebe_constructor
    (call $evaluate_to_weak_head_normal_form
      (local.get $actual_wannebe_constructor)
    )
  )


  ;; TODO who is responsible for ensuring that the result is a constructor?
  (if
    (call $isFingerAConstructor
      (local.get $actual_wannebe_constructor)
    )
    (then
    )
    (else
      (call $runtime_errors.expected_constructor_but_got_x
        (local.get $actual_wannebe_constructor)
      )
    )
  )



  ;; TODO have parser like function?
  (local.set $constructorNode
    (local.get $actual_wannebe_constructor)
  )



  (local.set $result_constructor_id
  (local.set $result_constructor_arity
  (local.set $result_constructor_child_0
  (local.set $result_constructor_child_1
  (local.set $result_constructor_child_2
  (local.set $result_constructor_child_3
  (local.set $result_constructor_child_4
  (local.set $result_constructor_child_5
  (local.set $result_constructor_child_6
  (local.set $result_constructor_child_7
    (call $disaggregate_Constructor
      (local.get $actual_wannebe_constructor)
    )
  )
  )
  )
  )
  )
  )
  )
  )
  )
  )

  ;; NOTE by convention the disaggregate function took care of relinquishing the node

  (block $0
  (block $1
  (block $2
  (block $3
  (block $4
  (block $5
  (block $6
  (block $7
  (block $8

    (if (call $i64.isEqual (i64.const 8) (local.get $result_constructor_arity)) (then br $8))
    (local.set $result_constructor_child_7 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 7) (local.get $result_constructor_arity)) (then br $7))
    (local.set $result_constructor_child_6 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 6) (local.get $result_constructor_arity)) (then br $6))
    (local.set $result_constructor_child_5 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 5) (local.get $result_constructor_arity)) (then br $5))
    (local.set $result_constructor_child_4 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 4) (local.get $result_constructor_arity)) (then br $4))
    (local.set $result_constructor_child_3 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 3) (local.get $result_constructor_arity)) (then br $3))
    (local.set $result_constructor_child_2 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 2) (local.get $result_constructor_arity)) (then br $2))
    (local.set $result_constructor_child_1 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 1) (local.get $result_constructor_arity)) (then br $1))
    (local.set $result_constructor_child_0 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 0) (local.get $result_constructor_arity)) (then br $0))

  )
    (local.set $result_constructor_child_7
      (call $compose_wrappedTerm_Finger
        (local.get $result_constructor_child_7)
      )
    )
  )
    (local.set $result_constructor_child_6
      (call $compose_wrappedTerm_Finger
        (local.get $result_constructor_child_6)
      )
    )
  )
    (local.set $result_constructor_child_5
      (call $compose_wrappedTerm_Finger
        (local.get $result_constructor_child_5)
      )
    )
  )
    (local.set $result_constructor_child_4
      (call $compose_wrappedTerm_Finger
        (local.get $result_constructor_child_4)
      )
    )
  )
    (local.set $result_constructor_child_3
      (call $compose_wrappedTerm_Finger
        (local.get $result_constructor_child_3)
      )
    )
  )
    (local.set $result_constructor_child_2
      (call $compose_wrappedTerm_Finger
        (local.get $result_constructor_child_2)
      )
    )
  )
    (local.set $result_constructor_child_1
      (call $compose_wrappedTerm_Finger
        (local.get $result_constructor_child_1)
      )
    )
  )
    (local.set $result_constructor_child_0
      (call $compose_wrappedTerm_Finger
        (local.get $result_constructor_child_0)
      )
    )
  )



  ;; TODO is there a better way of returning? Maybe just having 8 seperated functions and some constructor and arity getter?


  (local.get $result_constructor_id)
  (local.get $result_constructor_arity)
  (local.get $result_constructor_child_0)
  (local.get $result_constructor_child_1)
  (local.get $result_constructor_child_2)
  (local.get $result_constructor_child_3)
  (local.get $result_constructor_child_4)
  (local.get $result_constructor_child_5)
  (local.get $result_constructor_child_6)
  (local.get $result_constructor_child_7)
)


