




(func
  $embedder_apply_and_reduce
  (param $hopefully_a_lambda_WrappedHeader i64)
  (param $argument_WrappedHeader i64)

  (result i64)

  (local $f_Header i64)
  (local $argument_Header i64)
  (local $new_applicated_expression i64)

  (local $newTerm_Export i64)

  (local $freshly_reduced_term i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $hopefully_a_lambda_WrappedHeader)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $argument_WrappedHeader)
      )
    )
  #endif

  (local.set $f_Header
    (call $disaggregate_ExportWrapper
      (local.get $hopefully_a_lambda_WrappedHeader)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $f_Header)
      )
    )
  #endif

  (local.set $argument_Header
    (call $disaggregate_ExportWrapper
      (local.get $argument_WrappedHeader)
    )
  )

  (local.set $new_applicated_expression
    (call $compose_application_node
      (local.get $f_Header)
      (local.get $argument_Header)
    )
  )

  (local.set $freshly_reduced_term
    (call $evaluate_to_weak_head_normal_form
      (local.get $new_applicated_expression)
    )
  )

  (local.set $newTerm_Export
    (call $compose_wrappedTerm_Finger
      (local.get $freshly_reduced_term)
    )
  )

  (local.get $newTerm_Export)

)
