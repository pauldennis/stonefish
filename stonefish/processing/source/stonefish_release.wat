

#define STONEFISH_ALL_ASSERTIONS
#define STONEFISH_DEBUG_TOOLS NO
#define STONEFISH_DEBUG_TOOLS__BREAKPOINT NO
#define STONEFISH_EXPORT_TRAPPING_TESTS NO
#define STONEFISH_EXPORT_TRIGGER_ASSERT_TEST NO
#define STONEFISH_EXPORT_TESTSUITE NO
#define STONEFISH_EXPORT_DEBUG_ENTRANCE NO
#define STONEFISH_DEBUG_SPOILTOOL_YOLOMODE NO
#define STONEFISH_DEBUG_SPOILTOOL_SPOILLEASHES_FOR_FORGOTTEN_BACKREFERENCES_DETECTION NO
#define STONEFISH__INVARIANT_VARIATION__SHALLOW_INLINE_CONSTRUCTOR_SWAP YES
#define STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS NO
#define STONEFISH_DEBUG_SPOILTOOL_SPOILSTACKELEMENTS NO
#define STONEFISH_DEBUG_MODE_ALLOW_FORCING NO
#define STONEFISH_DEBUG_TOOLS_HEAPDUMP NO
#define STONEFISH_EXPORT_LINKING YES
#define STONEFISH_EXPORT_APPLY YES
#define STONEFISH_EXPORT_DESINTEGRATE YES
#define STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS NO
#define STONEFISH_IMPORT_PASSES YES
#define STONEFISH_DEBUG_DETECT_UNALLOWED_MULTITHREADING_USAGE YES
#define STONEFISH_IMPORT_EVENTS YES
#define STONEFISH_OPTIMIZATION_EXPLICIT_HEAP_IS_FULL_ERROR_MESSAGE NO
#define STONEFISH_DEBUG_WIGGLE_FIRST_ALLOCATED_DUPLICATION_IDENTIFICATION NO


#include stonefish.wat
