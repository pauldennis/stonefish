

#ifdef STONEFISH_EXPORT_DEBUG_ENTRANCE
  (func
    (export "DEBUG")

    (call $reentrence_detection_prolog
    )

    (call $DEBUG)

    (call $reentrence_detection_epilog
    )
  )
#endif



#ifdef STONEFISH_EXPORT_TESTSUITE
  (func
    (export "TESTING")

    (result i64)

    (call $reentrence_detection_prolog
    )

    (call $TESTING)

    (call $reentrence_detection_epilog
    )
  )
#endif


;; TODO rename LINK to SYNTHESIZE since it fits better and link is only some old name for some old technique?
#ifdef STONEFISH_EXPORT_LINKING

  ;; TODO remove this export from relase builds since this is a private function and is not sandbox save
  (func
    (export "LINK_FRAGMENT")

    (param $duplication_identification_mapping_offset i64)
    (param $rootFinger i64)
    (param $melee externref)
    (result i64)

    (call $reentrence_detection_prolog
    )

    (call $link_fragment_to_script
      (local.get $duplication_identification_mapping_offset)
      (local.get $rootFinger)
      (local.get $melee)
    )

    (call $reentrence_detection_epilog
    )
  )
  (func
    (export "LINK_MODULE")

    (param $rootFinger i64)
    (param $melee externref)
    (param $rules externref)

    (result i64)

    (call $reentrence_detection_prolog
    )

    (call $link_module
      (local.get $rootFinger)
      (local.get $melee)
      (local.get $rules)
    )

    (call $reentrence_detection_epilog
    )
  )
  (func
    (export "LINK_INTRINSIC_F32")

    (param $number f32)
    (result i64)

    (call $reentrence_detection_prolog
    )

    (call $link_intrinsic_number_f32
      (local.get $number)
    )

    (call $reentrence_detection_epilog
    )
  )
#endif



;; TODO rename macro?
#ifdef STONEFISH_EXPORT_APPLY
  ;; name inspired from:
  ;;   https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/dispatchEvent
  (func
    (export "SYNCHRONOUSLY_DISPATCH_EVENT")

    (param $capsule_WrappedHeader i64)
    (param $eventPayload_WrappedHeader i64)
    (result i64)
    (result i64)

    (call $reentrence_detection_prolog
    )

    (call $host_event
      (local.get $capsule_WrappedHeader)
      (local.get $eventPayload_WrappedHeader)
    )

    (call $reentrence_detection_epilog
    )
  )

  (func
    (export "STONEFISH_APPLY_AND_REDUCE")

    (param $hopefully_a_lambda_WrappedHeader i64)
    (param $argument_WrappedHeader i64)

    (result i64)

    (call $reentrence_detection_prolog
    )

    (call $embedder_apply_and_reduce
      (local.get $hopefully_a_lambda_WrappedHeader)
      (local.get $argument_WrappedHeader)
    )

    (call $reentrence_detection_epilog
    )
  )
#endif




#ifdef STONEFISH_EXPORT_DESINTEGRATE
  ;; TODO reuse more generic functions?

  (func
    (export "EVALUATE_TO_WEAK_HEAD_NORMAL_FORM")

    (param $Header i64)
    (result i64)

    (call $reentrence_detection_prolog
    )

    (call $evaluate_to_weak_head_normal_form_ExportWrapped
      (local.get $Header)
    )

    (call $reentrence_detection_epilog
    )
  )

  (func
    (export "DISINTEGRATE_BOOL")

    (param $Header i64)

    (result i32)

    (call $reentrence_detection_prolog
    )

    (call $disintegrate_WrappedBool
      (local.get $Header)
    )

    (call $reentrence_detection_epilog
    )
  )

  (func
    (export "DISINTEGRATE_f32")

    (param $Header i64)

    (result f32)

    (call $reentrence_detection_prolog
    )

    (call $disintegrate_Wrapped_intrinsic_f32
      (local.get $Header)
    )

    (call $reentrence_detection_epilog
    )
  )

  (func
    (export "DISINTEGRATE_CONSTRUCTOR_LEAF")

    (param $WrappedHeader i64)

    (result i64) ;; constructor id

    (call $reentrence_detection_prolog
    )

    (call $disintegrate_WrappedConstructorLeafHeader
      (local.get $WrappedHeader)
    )

    (call $reentrence_detection_epilog
    )
  )

  (func
    (export "DISINTEGRATE_CONSTRUCTOR")

    (param $WrappedHeader i64)

    (result i64) ;; constructor id
    (result i64) ;; arity of constructor

    (result i64) ;; 0
    (result i64) ;; 1
    (result i64) ;; 2
    (result i64) ;; 3
    (result i64) ;; 4
    (result i64) ;; 5
    (result i64) ;; 6
    (result i64) ;; 7

    (call $reentrence_detection_prolog
    )

    (call $disintegrate_WrappedConstructorNode
      (local.get $WrappedHeader)
    )

    (call $reentrence_detection_epilog
    )
  )
#endif





;; TODO have macros arounit! (Or maybe not?)
;; NOTE webassembly does an "export"
;;   an embedder "imports" the "exports"
;;   the embedder can test the webassembly integration with some test functions
(func
  (export "WEBASSEMBLY_EMBEDDING_EXPORT_EXAMPLE_FOR_PETER_PORTER___NOT_ZERO_POPULATION_COUNT")
  (param $input i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (i64.const 0)
          (local.get $input)
        )
      )
    )
  #endif

  (i64.popcnt
    (local.get $input)
  )
)
(func
  (export "WEBASSEMBLY_EMBEDDING_EXPORT_EXAMPLE_FOR_PETER_PORTER___UNCONDITIONALLY_TRAP_WITH_UNREACHABLE")
  unreachable
)
