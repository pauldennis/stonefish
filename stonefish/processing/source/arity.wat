

;; TODO how to properly handle the arity of rules?
;; TODO automate?
;; TODO very bad asymptotic time complexity?
;; TODO wrong place



(func
  $getArityFromRuleHeader
  (param $ruleFinger i64)

  (result i64)

  (local $ruleId i64)

  (local.set $ruleId
    (call $extract_RuleIdentification_fromRuleHeader
      (local.get $ruleFinger)
    )
  )

  (call $getArityFromRuleId
    (local.get $ruleId)
  )
)




(func
  $getArityFromRuleId
  (param $ruleId i64)

  (result i64)

  (block $result
    (result i64)

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Encapsulate)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Encapsulate)
        )
      )
    )
    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_IsWithinUnitBall)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_IsWithinUnitBall)
        )
      )
    )
    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_IsWithinUnitBall)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_IsWithinUnitBall)
        )
      )
    )
    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Float32_IsLessThan)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Float32_IsLessThan)
        )
      )
    )
    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Float32_Addition_RoundingDown)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Float32_Addition_RoundingDown)
        )
      )
    )
    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Float32_Square_RoundingDown)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Float32_Square_RoundingDown)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_If)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_If)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_DropLeftNullaryConstructorArgumentAndReturnRightArgument)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_DropLeftNullaryConstructorArgumentAndReturnRightArgument)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_WebSocketMessageTrigger)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_WebSocketMessageTrigger)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_MatchTwoPrivateMembers)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_MatchTwoPrivateMembers)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_MatchTwoCapsuleCallResult)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_MatchTwoCapsuleCallResult)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_InterfaceDispatch)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_InterfaceDispatch)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_MatchTwoPrivateMembers_forSetTrue)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_MatchTwoPrivateMembers_forSetTrue)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_MatchTwoPrivateMembers_forSetFalse)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_MatchTwoPrivateMembers_forSetFalse)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_BoolNeedsToBeSetTo)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_BoolNeedsToBeSetTo)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Add_floor)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Add_floor)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Add_floor_MatchSecondParameter)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Add_floor_MatchSecondParameter)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Negate)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Negate)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Subtract_floor)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Subtract_floor)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Float32_Negate)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Float32_Negate)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Halfe)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Halfe)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Midpoint_floor)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Midpoint_floor)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Float32_Multiplicate_RoundingDown)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Float32_Multiplicate_RoundingDown)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Length_floor)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Length_floor)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Float32_SquareRoot_RoundingDown)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Float32_SquareRoot_RoundingDown)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Distance_floor)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Distance_floor)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Scale_flipped)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Scale_flipped)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_Normalize_floor)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_Normalize_floor)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Float32_Reciprocal_RoundingDown)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Float32_Reciprocal_RoundingDown)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_CrossProduct_floor_MatchSecondParameter)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_CrossProduct_floor_MatchSecondParameter)
        )
      )
    )

    (if
      (call $i64.isEqual
        (local.get $ruleId)
        (call $MAGIC_RULE_ID_Vector3_CrossProduct_floor)
      )
      (then
        (br $result
          (call $MAGIC_RULE_ARITY_Vector3_CrossProduct_floor)
        )
      )
    )

    (call $unconditional_breakpoint
      (local.get $ruleId)
    )
    unreachable
  )

)
