#include macro_setup.wat

#include configure_semantics.wat



#define NEWLINE


;; hack up $assert after the fact
;; TODO is there a better way? possile with changing lots of places?
  ;; TODO just have some macro next to the assert. this is ok since the macro is some boilerplate anyway
;; TODO pre increment counter would be better
#define assert identified_assert (i64.const identified_assert_identication_counter) ;; __FILE__:__LINE__ #exec echo postIncremet_identified_assert_identication_counter __FILE__:__LINE__ >> BUILD_NAME.assert_index
#define conditional_breakpoint identified_conditional_breakpoint (i64.const identified_assert_identication_counter) ;; __FILE__:__LINE__ #exec echo postIncremet_identified_assert_identication_counter __FILE__:__LINE__ >> BUILD_NAME.assert_index





(module

  ;; module name
  $stonefish



  ;; imports need to go first
  #include imports/imports.wat
  #include imports/explicit_heap_is_full_error_for_debug_mode.wat
  #include imports/heapdump.wat
  #include imports/floating_point.wat
  #include imports/runtime_errors.wat
  #include imports/link.wat
  #include imports/events.wat

  ;; TODO move other files also into folder
  #include imports/import_dead_ends.wat
  #include imports/dead_ends/heapdump.wat
  #include imports/link_dead_ends.wat
  #include imports/events_deadEnd.wat


  (func
    $assertion_example_for_copy_and_paste

    #if STONEFISH_SHOULD_INCLUDE_ASSERT
      (call $assert
        (call $TRUE)
      )
    #endif
  )



  #include exports.wat



  #include arity.wat
  #include test_infrastructure/heapdump.wat
  #include test_infrastructure/Assert.wat
  #include commen_library/language_constants.wat
  #include commen_library/understanding_the_langauge.wat
  #include commen_library/renameExcruciatingNames.wat
  #include commen_library/commen_library.wat
  #include commen_library/overflow.wat
  #include commen_library/languageHelper.wat
  #include commen_library/bool.wat
  #include commen_library/divisible.wat
  #include commen_library/memory.wat
  #include commen_library/fakeIntegers.wat
  #include commen_library/inBetween.wat
  #include stack/stack_memory_operations.wat
  #include stack/stack.wat
  #include heap/heap.wat
  #include heap/heap_debug.wat
  #include runtime.wat
  #include runtime_store/initialize_runtime_store.wat
  #include runtime_store/RuntimeStore.wat
  #include runtime_store/FreeSlotsStack.wat
  #include runtime_store/Finger/Finger.wat
  #include runtime_store/Finger/leash.wat
  #include runtime_store/Finger/construct_variable_leash.wat
  #include runtime_store/Finger/construct_by_replacing_location.wat
  #include runtime_store/Finger/construct_IntrinsicNumber.wat
  #include runtime_store/Finger/construct_SuperpositionHeader.wat
  #include runtime_store/Finger/construct_TwinHeader.wat
  #include runtime_store/Finger/construct_SoloVariableHeader.wat
  #include runtime_store/Finger/construct_ApplicationHeader.wat
  #include runtime_store/Finger/construct_RuleCallHeader.wat
  #include runtime_store/Finger/construct_CapsuleApplicationResult_ConstructorHeader.wat
  #include runtime_store/Compose/Compose_Application.wat
  #include runtime_store/Compose/Compose_Constructor.wat
  #include runtime_store/Compose/Compose_ExportWrapper.wat
  #include runtime_store/Compose/Compose_Duplication.wat
  #include runtime_store/Finger/dupication_label.wat
  #include runtime_store/Layout/runtime_constants/runtime_constants.wat
  #include runtime_store/Layout/runtime_constants/finger_tags.wat
  #include runtime_store/Layout/node_anatomy.wat
  #include runtime_store/Layout/devisibleByElementSize.wat
  #include runtime_store/Layout/finger_offset_as_byte_offset.wat
  #include runtime_store/FingerNodesHeap/FingerNodesHeap_debug.wat
  #include runtime_store/FingerNodesHeap/FingerPlacements.wat
  #include runtime_store/HeapAsGraph/replace_solo_variables_and_twins.wat
  #include runtime_store/HeapAsGraph/associated_nodes.wat
  #include runtime_store/HeapAsGraph/leash.wat
  #include runtime_store/HeapAsGraph/graph.wat
  #include runtime_store/HeapAsGraph/check_consistency.wat
  #include runtime_store/HeapAsGraph/construct_new_nodes.wat
  #include runtime_store/HeapAsGraph/replace_operations.wat
  #include evaluate/force/force.wat
  #include evaluate/force/force_handle_constructor.wat
  #include evaluate/force/force_handle_lambda_case.wat
  #include evaluate/force/force_handle_capsule_case.wat
  #include evaluate/force/force_handle_superposition_case.wat
  #include evaluate/weak_head_normal_form/evaluate_IntrinsicBinaryOperator.wat
  #include evaluate/weak_head_normal_form/weak_head_normal_form.wat
  #include evaluate/weak_head_normal_form/evaluate_ruleCall.wat
  #include evaluate/magic-rules/rule.wat
  #include evaluate/magic-rules/rule_Encapsulate.wat
  #include evaluate/magic-rules/rule_If.wat
  #include evaluate/magic-rules/rule_Float32_IsLessThan.wat
  #include evaluate/magic-rules/rule_Float32_Addition_RoundingDown.wat
  #include evaluate/magic-rules/rule_Float32_Square_RoundingDown.wat
  #include evaluate/magic-rules/rule_Bits32_UnaryOperator.wat
  #include evaluate/magic-rules/rule_Bits32_BinaryOperator.wat
  #include evaluate/magic-rules/rule_DropLeftNullaryConstructorArgumentAndReturnRightArgument.wat
  #include evaluate/magic-rules/rule_IsWithinUnitBall.wat
  #include evaluate/magic-rules/rule_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule.wat
  #include evaluate/magic-rules/rule_Event_BoolNeedsToBeSetTo.wat
  #include evaluate/user-rule-handling/link_rule_fragment.wat
  #include evaluate/user-rule-handling/superposition_and_rule_call_distributive.wat
  #include evaluate/weak_head_normal_form/evaluate_twin.wat
  #include evaluate/weak_head_normal_form/handle_easy_inline_of_superposition_case.wat
  #include evaluate/maybe_repair_backreference.wat
  #include evaluate/weak_head_normal_form/shallow_inline_constructor.wat
  #include evaluate/weak_head_normal_form/shallow_inline_intrinsicNumber.wat
  #include evaluate/weak_head_normal_form/shallow_inline_Lambda.wat
  #include evaluate/weak_head_normal_form/evaluate_application.wat
  #include evaluate/weak_head_normal_form/shallow_inline_superposition.wat
  #include evaluate/weak_head_normal_form/apply_lambda_to_argument.wat
  #include evaluate/weak_head_normal_form/apply_capsule_to_argument.wat
  #include evaluate/weak_head_normal_form/apply_superposition_to_argument.wat
  #include evaluate/user_inflicted_error_case.wat
  #include evaluate/garbage_crawler/garbage_crawler.wat
  #include pattern_match/disaggregate_Superposition.wat
  #include pattern_match/parse_CapsuleCallResult.wat
  #include pattern_match/disaggregate_Vector3D.wat
  #include pattern_match/disaggregate_Constructor.wat
  #include pattern_match/disaggregate_IntrinsicNumber.wat
  #include pattern_match/disaggregate_ExportWrapper.wat
  #include pattern_match/disaggregate_Bool.wat
  #include pattern_match/disaggregate_ConstructorLeaf.wat
  #include pattern_match/disaggregate_GenieAndBool.wat
  #include test_infrastructure/TESTING.wat
  #include test_infrastructure/NOT_IMPLEMENTED.wat
  #include test_infrastructure/NEGATIVE_TESTS.wat
  #include test_infrastructure/TODO.wat
  #include test_infrastructure/BREAKPOINT.wat
  #include DEBUG.wat
  #include porting_ledder/ledderRungTests.wat
  #include porting_ledder/consume_expected_output.wat
  #include porting_ledder/consume_library.wat
  #include link/LINK.wat
  #include link/wrapper_stage/module_and_fragment.wat
  #include link/wrapper_stage/intrinsic_number_f32.wat
  #include link/embedder_wrapper.wat
  #include link/link_handle_ruleCall.wat
  #include link/link_handle_constructor.wat
  #include link/link_handle_lambda.wat
  #include link/link_handle_twins.wat
  #include link/link_handle_application.wat
  #include link/bookshelf.wat
  #include HOST_EVENT.wat
  #include desintegrate/DESINTEGRATE.wat
  #include RAW_APPLY.wat

  ;; auto-generated
  #include memoryInitialisation.wat
  #include combIdentifications.wat





;;;;;;;;;;;;;;
) ;; module ;;
;;;;;;;;;;;;;;



#exec echo -n GET_NEXT_COUNTER > number_of_performance_knobs
