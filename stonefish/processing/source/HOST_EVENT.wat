



(func
  $host_event
  (param $capsule_WrappedHeader i64)
  (param $eventPayload_WrappedHeader i64)

  (result i64) ;; result_capsule_WrappedHeader
  (result i64) ;; system_call_WrappedHeader

  (local $result_capsule_WrappedHeader i64)
  (local $resulting_system_call_WrappedHeader i64)

  (local $capsule_Header i64)
  (local $eventPayload_Header i64)
  (local $new_applicated_expression i64)

  (local $raw_result_modified_capsule i64)
  (local $raw_result_interface_call_result i64)

  (local $newCapsule_Export i64)
  (local $newSystemCall_Export i64)

  (local $freshly_reduced_term i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $capsule_WrappedHeader)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $eventPayload_WrappedHeader)
      )
    )
  #endif

  (local.set $capsule_Header
    (call $disaggregate_ExportWrapper
      (local.get $capsule_WrappedHeader)
    )
  )

  (local.set $eventPayload_Header
    (call $disaggregate_ExportWrapper
      (local.get $eventPayload_WrappedHeader)
    )
  )

  (local.set $new_applicated_expression
    (call $compose_application_node
      (local.get $capsule_Header)
      (local.get $eventPayload_Header)
    )
  )

  (local.set $freshly_reduced_term
    (call $evaluate_to_weak_head_normal_form
      (local.get $new_applicated_expression)
    )
  )

  ;; TODO can we actually know that we got a capsule?
  (local.set $raw_result_modified_capsule
    (local.set $raw_result_interface_call_result
      (call $disaggregate_CapsuleCallResult
        (local.get $freshly_reduced_term)
      )
    )
  )

  (local.set $newCapsule_Export
    (call $compose_wrappedTerm_Finger
      (local.get $raw_result_modified_capsule)
    )
  )
  (local.set $newSystemCall_Export
    (call $compose_wrappedTerm_Finger
      (local.get $raw_result_interface_call_result)
    )
  )

  (local.get $newCapsule_Export)
  (local.get $newSystemCall_Export)

)


