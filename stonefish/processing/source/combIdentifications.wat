
;; TODO automate

;; TODO are there other definitions lingering somewhere else?

;; TODO rename to constructir, rules are already automated

(func
  $CONSTRUCTOR_ID__Tuple0
  (result i64)
  (i64.const 0)
)

(func
  $CONSTRUCTOR_ID__ReturnNewPrivateDataAndSystemRequest
  (result i64)
  (i64.const 9)
)

(func
  $CONSTRUCTOR_ID__CapsuleCallResult
  (result i64)
  (i64.const 19)
)

(func
  $CONSTRUCTOR_ID__EmptyList
  (result i64)
  (i64.const 11)
)

(func
  $CONSTRUCTOR_ID__False
  (result i64)
  (i64.const 12)
)
(func
  $CONSTRUCTOR_ID__False_Arity
  (result i64)
  (i64.const 0)
)

(func
  $CONSTRUCTOR_ID__True
  (result i64)
  (i64.const 13)
)
(func
  $CONSTRUCTOR_ID__True_Arity
  (result i64)
  (i64.const 0)
)

;; ExportedTerm
(func
  $CONSTRUCTOR_ID__ExportedTerm_ID
  (result i64)
  (i64.const 29)
)
(func
  $CONSTRUCTOR_ID__ExportedTerm_Arity
  (result i64)
  (call $ARITY_OF_ONE)
)
(func
  $CONSTRUCTOR_ID__ExportedTerm__TheTerm
  (result i64)
  (i64.const 0)
)

;; Vector3D
(func
  $CONSTRUCTOR_Vector3D_ID
  (result i64)
  (i64.const 23)
)
(func
  $CONSTRUCTOR_Vector3D_Arity
  (result i64)
  (call $ARITY_OF_THREE)
)
(func
  $CONSTRUCTOR_Vector3D__x0
  (result i64)
  (i64.const 0)
)
(func
  $CONSTRUCTOR_Vector3D__x1
  (result i64)
  (i64.const 1)
)
(func
  $CONSTRUCTOR_Vector3D__x2
  (result i64)
  (i64.const 2)
)

;; GenieAndBool
(func
  $CONSTRUCTOR_GenieAndBool_ID
  (result i64)
  (i64.const 30)
)
(func
  $CONSTRUCTOR_GenieAndBool_Arity
  (result i64)
  (call $ARITY_OF_THREE)
)
(func
  $CONSTRUCTOR_GenieAndBool__genie
  (result i64)
  (i64.const 0)
)
(func
  $CONSTRUCTOR_GenieAndBool__bool
  (result i64)
  (i64.const 1)
)
