#define __FILE__ #file
#define __LINE__ #line
#define MACRO_CONCAT #1#2


#define LAST_COUNTER_VALUE 0

#define extract_LAST_COUNTER_VALUE LAST_COUNTER_VALUE #undef LAST_COUNTER_VALUE

#define GET_NEXT_COUNTER LAST_COUNTER_VALUE #defeval LAST_COUNTER_VALUE #eval extract_LAST_COUNTER_VALUE+1



;; ... | tee >(>&2 cat) | ...

#define GET_DETERMINISTIC_RANDOM_HEX_VALUE #exec echo STONEFISH_SEED GET_NEXT_COUNTER | sha512sum | head -c 1 | awk '{print "DIE_GOT_ROLLED_AND_IT_WAS_"$1}'

#define GET_PRECONFIGURED_BIT_VALUE_RAW #exec ./Inject_Profiling_Info GET_NEXT_COUNTER



;; TODO find better way to wrap hex to binary
#define DIE_GOT_ROLLED_AND_IT_WAS_0 1
#define DIE_GOT_ROLLED_AND_IT_WAS_1 1
#define DIE_GOT_ROLLED_AND_IT_WAS_2 1
#define DIE_GOT_ROLLED_AND_IT_WAS_3 1
#define DIE_GOT_ROLLED_AND_IT_WAS_4 1
#define DIE_GOT_ROLLED_AND_IT_WAS_5 1
#define DIE_GOT_ROLLED_AND_IT_WAS_6 1
#define DIE_GOT_ROLLED_AND_IT_WAS_7 1
#define DIE_GOT_ROLLED_AND_IT_WAS_8 0
#define DIE_GOT_ROLLED_AND_IT_WAS_9 0
#define DIE_GOT_ROLLED_AND_IT_WAS_a 0
#define DIE_GOT_ROLLED_AND_IT_WAS_b 0
#define DIE_GOT_ROLLED_AND_IT_WAS_c 0
#define DIE_GOT_ROLLED_AND_IT_WAS_d 0
#define DIE_GOT_ROLLED_AND_IT_WAS_e 0
#define DIE_GOT_ROLLED_AND_IT_WAS_f 0

#define GPP_TRUE 1
#define GPP_FALSE 0

#define STONEFISH_PROFILING_INCLUDE_HINT GPP_TRUE
#define STONEFISH_PROFILING_EXCLUDE_HINT GPP_FALSE





#defeval LAST_COIN_FLIP GET_DETERMINISTIC_RANDOM_HEX_VALUE

#define extract_LAST_COIN_FLIP LAST_COIN_FLIP #undef LAST_COIN_FLIP

#define GET_RANDOM_BIT_VALUE extract_LAST_COIN_FLIP #defeval LAST_COIN_FLIP GET_DETERMINISTIC_RANDOM_HEX_VALUE



#defeval LAST_COIN_FLIP2 GET_PRECONFIGURED_BIT_VALUE_RAW

#define extract_LAST_COIN_FLIP2 LAST_COIN_FLIP2 #undef LAST_COIN_FLIP2

#define GET_PRECONFIGURED_BIT_VALUE extract_LAST_COIN_FLIP2 #defeval LAST_COIN_FLIP2 GET_PRECONFIGURED_BIT_VALUE_RAW





;;;;
;; test deterministic macro
;; GET_PRECONFIGURED_BIT_VALUE
;; GET_PRECONFIGURED_BIT_VALUE
;; GET_PRECONFIGURED_BIT_VALUE
;;;;



;;;;
;; testing random output macro:
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; GET_RANDOM_BIT_VALUE
;; end test random macro


;;;;


;; TODO is there a way to do that better?
#define MACRO_DEBUG_OUTPUT(name, value) #warning name #eval defined(value) value


;;;;


;; begin with some big number inorder to have manuel used numbers
;; counter named next_identified_assert_identication_counter
#define identified_assert_identication_counter 10000
#define extract_identified_assert_identication_counter identified_assert_identication_counter #undef identified_assert_identication_counter
#define postIncremet_identified_assert_identication_counter identified_assert_identication_counter#defeval identified_assert_identication_counter #eval extract_identified_assert_identication_counter+1




