
;; TODO shouldnt that be derived from the haskell code for the layout?
(func
  $STACK_ELEMENT_SIZE_IN_BYTES
  (result i64)
  (i64.const 8)
)


(func
  $STACK_UNUSED_MARKING
  (result i64)
  (i64.const 6148914691236517205) ;; 1010101010101010101010101010101010101010101010101010101010101010
)

(func
  $initialize_stack
  (param $beginOfStack i64)
  (param $numberOfElements i64)

  (local $endOfStack i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (local.get $beginOfStack)
      )
    )
  #endif

  (local.set $endOfStack
    (call $i64.add
      (local.get $beginOfStack)
      (call $i64.multiplicate
        (call $STACK_ELEMENT_SIZE_IN_BYTES)
        (local.get $numberOfElements)
      )
    )
  )

  ;; do we have enouth memory for the stack?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $endOfStack)
        (call $i64.getLastAllocatedAddress_exclusive
        )
      )
    )
  #endif

  (global.set $arrayBegin_Word8Index_inclusive_FreeSlots
    (local.get $beginOfStack)
  )
  (global.set $arrayEnd___Word8Index_exclusive_FreeSlots
    (local.get $endOfStack)
  )
  (global.set $freeSlotsStack_NextUnusedElement
    (local.get $beginOfStack)
  )

  (call $debug_initialize_stack)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (local.get $endOfStack)
      )
    )
  #endif
)



(func
  $debug_initialize_stack

  #ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILSTACKELEMENTS
    (call $impure_assert_group.StackDebug
      (call $i64.memory.fill
        (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
        (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
        (call $i32.bitwiseAnd
          (call $i32.MAXIMUM_I8) ;; 11111111000000000000000000000000
          (i32.wrap_i64
            (call $STACK_UNUSED_MARKING)
          )
        )
      )
      (call $TRUE)
    )
  #endif
)


(func
  $stack_are_invariants_fullfilled
  (result i32)

  (if
    (call $i64.isLessOrEqual
      (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
      (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (if
    (call $isInBetween_closedIntervall
      (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
      (global.get $freeSlotsStack_NextUnusedElement)
      (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (call $TRUE)
)


(func
  $lengthOfStack
  (result i64)

  (call $i64.subtract
    (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
    (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
  )
)


(func
  $isStackTopPointerPossibleValid
  (param $theStackTopPointer i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 8) ;; sync down 6 lines, TODO refactor
        (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION)
      )
    )
  #endif

  (if
    (call $i64.isEqual
      (local.get $theStackTopPointer)
      (i64.const -8)
    )
    (then
      (return
        (call $TRUE)
      )
    )
  )

  (if
    (call $i64.isDevisibleBy8 ;; sync up 6 lines
      (local.get $theStackTopPointer)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )


  (if
    (call $isInBetween_closedIntervall
      (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
      (local.get $theStackTopPointer)
      (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (call $TRUE)
)

(func
  $isIntermediateInvalidButAllowedPointer
  (result i32)

  (call $i64.isEqual
    (call $i64.subtract
      (call $i64.MAXIMUM_I64)
      (call $i64.decrement
        (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION)
      )
    )
    (global.get $freeSlotsStack_NextUnusedElement)
  )
)


;; TODO wrong place?
(func
  $isStackFull_whichIsTheSameAs_isTheHeapEmpty
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  (call $i64.isEqual
    (global.get $freeSlotsStack_NextUnusedElement)
    (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
  )
)

(func
  $isStackEmpty_whichIsTheSameAs_isTheHeapFull
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  (call $i64.isEqual
    (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
    (global.get $freeSlotsStack_NextUnusedElement)
  )
)

(func
  $how_many_slots_are_left
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  (if
    (result i64)

    (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
    )

    (then
      ;; NOTE that currently there cannot be more requested than the right blank space 2da22c4b1b3b54ff219d3d5b005055f4c5363ec2c6098322fc03c617f5c4c8c4d93b4becf26d07573c77467ba918a5ce8328951439fed764167ca5eedd81b203
      (i64.const 0)
    )
    (else
      ;; TODO implement and publish this function for debugging the embedder
      unreachable
    )
  )
)



;;;


(func
  $IncrementStackTopPointer

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif


  (global.set $freeSlotsStack_NextUnusedElement
    (call $i64.add
      (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION)
      (global.get $freeSlotsStack_NextUnusedElement)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

)


(func
  $DecrementStackTopPointer

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  ;; STONEFISH_DEBUG_TOOLS__BREAKPOINT
  #ifdef STONEFISH_DEBUG_TOOLS__BREAKPOINT
    (call $conditional_breakpoint
      (call $not
        (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
        )
      )
    )
  #endif

  ;; heap might be full
  (call $conditional_breakpoint
    (call $i64.can_subtraction_be_represented
      (global.get $freeSlotsStack_NextUnusedElement)
      (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION) ;; last place
    )
  )

  (global.set $freeSlotsStack_NextUnusedElement
    (call $i64.subtract_wrapped
      (global.get $freeSlotsStack_NextUnusedElement)
      (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION) ;; last place
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $or
        (call $isStackTopPointerPossibleValid
          (global.get $freeSlotsStack_NextUnusedElement)
        )
        (call $isIntermediateInvalidButAllowedPointer
        )
      )
    )
  #endif

)



;;;

;; TODO convert asserts into bool returns
(func
  $assert_would_stack_push_be_valid
  (param $itemToBePushed i64)
;;   (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  ;; it is an invariant of the runtime layout, that the FreeSlotsStack cannot overflow.
  ;; That is because the Heap has the same element size as the Stack.
  ;; It is therefore enouth to just $assert that this case cannot happen
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
        )
      )
    )
  #endif

  ;; (it is an invariant, that the stack is not more than empty)
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  ;; the spot to be used for the new element is supposed to be unused
  #ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILSTACKELEMENTS
    (call $impure_assert_group.StackDebug
      (call $i64.isEqual
        (call $STACK_UNUSED_MARKING)
        (call $i64.stack_read64bitAligned
          (global.get $freeSlotsStack_NextUnusedElement)
        )
      )
    )
  #endif

)


(func
  $stack_push
  (param $itemToBePushed i64)

  (call $assert_would_stack_push_be_valid
    (local.get $itemToBePushed)
  )

  ;; first write at the new location
  (call $i64.stack_write64bitAligned
    (global.get $freeSlotsStack_NextUnusedElement)
    (local.get $itemToBePushed)
  )

  ;; then inrement the topPointer
  (call $IncrementStackTopPointer
  )
)


;;;;

(func
  $stack_pop
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackTopPointerPossibleValid
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  ;; STONEFISH_DEBUG_TOOLS__BREAKPOINT
  #ifdef STONEFISH_DEBUG_TOOLS__BREAKPOINT
    (call $conditional_breakpoint
      (call $not
        (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
        )
      )
    )
  #endif

  ;; the stack could be empty
  ;; in this case the heap is full
  ;; this means we ran out of memory
  ;; since we are interpreting code that is untrustworthy we must handle this case properly
  ;; an assertion with $assert wont do it since it can happen at runtime and it has defined behaviour.

  ;; here is a little trick (2da22c4b1b3b54ff219d3d5b005055f4c5363ec2c6098322fc03c617f5c4c8c4d93b4becf26d07573c77467ba918a5ce8328951439fed764167ca5eedd81b203) in webassembly that lets us trap without actual having code logic for it:
  ;; the stack begins at address 0 and shrinks towards address 0
  ;; poping from an empty stack would mean we would attempt to pop element at address -1.
  ;; Because of guaranteed wrapping behaviour the number -1 is the adress 4294967295. (4294967295 == maxBound::Word32)

  ;; webassembly guarantees that it traps should there be an memory access that is out of bounds.
  ;; the trick is now: We define the invariant, that the last webassembly page (page number 65536) is never allocated
  ;; that way we managed to offload the responsibility to check that we are not running out of memory to the embedder
  ;; that is better because the embedder knows more that we know here
  ;; it is important for the whole thing to be sound that no assertion is triggering that the stackSopPointer dropped below zero, otherwise a smart compiler might make invalid optimisations

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 0)
        (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION)
        (i64.extend_i32_u
          (call $PAGE_SIZE_IN_BYTES)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isLastMemoryPageAvailable
        )
      )
    )
  #endif

  ;; first decremnt
  (call $DecrementStackTopPointer
  )
  ;; from now on StackTopPointer might be maxBound::Word32

  ;; then readout the value:
  (if
    (result i64)

    (call $isIntermediateInvalidButAllowedPointer
    )
    ;; TODO: find out if the compiler figures out that both alternatives are equivalent
    (then
      ;; this operation must trap inorder for the runtime to be sound
      (i64.load
        (i32.wrap_i64
          (global.get $freeSlotsStack_NextUnusedElement)
        )
        (call $now_intentionally_invoking_a_trap)
      )
    )
    (else
      (call $i64.stack_read64bitAligned
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  )

  #ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILSTACKELEMENTS
    (call $impure_assert_group.StackDebug
      (call $i64.stack_write64bitAligned
        (global.get $freeSlotsStack_NextUnusedElement)
        (call $STACK_UNUSED_MARKING)
      )
      (call $TRUE)
    )
  #endif
)
(func $now_intentionally_invoking_a_trap)





(func
  $hasStackThisElement
  (param $theElement i64)
  (result i32)

  (local $atWhatElementAreWe i64)
  (local $isSuchAnElementHere i32)
  (local $result i32)

  (local.set $atWhatElementAreWe
    (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
  )

  (local.set $result
    (call $FALSE)
  )

  (block $terminateLoop
    ;; should we termiate
    (loop $goThroughStackElements
      (if
        (call $not
          (call $i64.isLessThan
            (local.get $atWhatElementAreWe)
            (global.get $freeSlotsStack_NextUnusedElement)
          )
        )
        (then
          (br $terminateLoop)
        )
      )

      (local.set $isSuchAnElementHere
        (call $i64.isEqual
          (local.get $theElement)
          (call $i64.stack_read64bitAligned
            (local.get $atWhatElementAreWe)
          )
        )
      )

      (local.set $result
        (call $or
          (local.get $result)
          (local.get $isSuchAnElementHere)
        )
      )

      ;; increment
      (local.set $atWhatElementAreWe
        (call $i64.add
          (local.get $atWhatElementAreWe)
          (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION)
        )
      )

      ;; go again
      (br $goThroughStackElements)
    )
  )

  (local.get $result)
)







(func
  $TEST_little_stack_test

  (call $TEST_assumtion_about_pointer_arithmetic)

  (call $TEST_test_zero_elements)
  (call $TEST_test_one_element)
  (call $TEST_test_two_elements)

  (call $TEST_test_pulsating_stack)
)

(func
  $TEST_assumtion_about_pointer_arithmetic

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i32.isEqual
        (i32.wrap_i64
          (call $i64.subtract
            (call $i64.MAXIMUM_I64)
            (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION)
          )
        )
        (i32.wrap_i64
          (call $i64.subtract
            (call $i64.MAXIMUM_I32)
            (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION)
          )
        )
      )
    )
  #endif
)

(func
  $TEST_test_zero_elements

  (call $initialize_stack
    (i64.const 0)
    (i64.const 0)
  )

  ;;

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
      )
    )
  #endif
)

(func
  $TEST_test_one_element

  (local $one i64)
  (local $one_after i64)

  (call $initialize_stack
    (i64.const 0)
    (i64.const 1)
  )

  (local.set $one
    (i64.const 10000)
  )


  ;;

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 0)
        (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 8)
        (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 0)
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
    )
  )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
      )
    )
  )
  #endif

  (call $stack_push
    (local.get $one)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
      )
    )
  #endif

  (local.set $one_after
    (call $stack_pop)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $one)
        (local.get $one_after)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 0)
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif
)




(func
  $TEST_test_two_elements

  (local $one i64)
  (local $one_after i64)

  (local $two i64)
  (local $two_after i64)

  (call $initialize_stack
    (i64.const 0)
    (i64.const 2)
  )

  (local.set $one
    (i64.const 10000)
  )
  (local.set $two
    (i64.const 20000)
  )

  ;;

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 0)
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
        )
      )
    )
  #endif

  (call $stack_push
    (local.get $one)
  )


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
        )
      )
    )
  #endif

  (call $stack_push
    (local.get $two)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
      )
    )
  #endif



  (local.set $two_after
    (call $stack_pop)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $two)
        (local.get $two_after)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
        )
      )
    )
  #endif

  (local.set $one_after
    (call $stack_pop)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $one)
        (local.get $one_after)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isStackFull_whichIsTheSameAs_isTheHeapEmpty
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 0)
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif
)


(func
  $TRIGGER_ASSERT_TEST_pop_from_empty_stack

  (call $initialize_stack
    (i64.const 0)
    (i64.const 2)
  )

  ;;

  (drop
    (call $stack_pop)
  )
)

(func
  $TEST_test_pulsating_stack

  (call $initialize_stack
    (i64.const 0)
    (i64.const 1)
  )

  (call $stack_push
    (i64.const 1)
  )
  (drop
    (call $stack_pop)
  )

  (call $stack_push
    (i64.const 2)
  )
  (drop
    (call $stack_pop)
  )

  (call $stack_push
    (i64.const 3)
  )
  (drop
    (call $stack_pop)
  )

)
