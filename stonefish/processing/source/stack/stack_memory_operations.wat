
(func
  $i64.stack_write64bitAligned
  (param $location i64)
  (param $word64 i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isInBetween_closedIntervall
        (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
        (local.get $location)
        (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
      )
    )
  #endif

  (call $i64.store_64bitAligned
    (local.get $location)
    (local.get $word64)
  )
)


(func
  $i64.stack_read64bitAligned
  (param $location i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isInBetween_closedIntervall
        (global.get $arrayBegin_Word8Index_inclusive_FreeSlots)
        (local.get $location)
        (global.get $arrayEnd___Word8Index_exclusive_FreeSlots)
      )
    )
  #endif

  (call $i64.load_64bitAligned
    (local.get $location)
  )
)
