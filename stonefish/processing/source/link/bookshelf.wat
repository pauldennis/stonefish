(func
  $allocate_duplication_identifications
  (param $to_be_allocated_count i64)

  (result i64)

  (local $old_unallocated_id i64)

  (local $wannabe_next_unallocated_id i64)
  (local $first_allocated_duplication_identification_number i64)


  (local.set $old_unallocated_id
    (global.get $nextFreeDuplicationLabel
    )
  )

  (if
    (call $i64.can_addition_be_represented
      (local.get $old_unallocated_id)
      (local.get $to_be_allocated_count)
    )
    (then
    )
    (else
      (call $runtime_errors.ran_out_of_duplication_identification_numbers
        (local.get $old_unallocated_id)
        (local.get $to_be_allocated_count)
      )
      unreachable
    )
  )

  (local.set $wannabe_next_unallocated_id
    (call $i64.add
      (local.get $old_unallocated_id)
      (local.get $to_be_allocated_count)
    )
  )

  (if
    (call $i64.isLessThan ;; could be isLessOrEqual but why not having some padding
      (local.get $wannabe_next_unallocated_id)
      ;; 3314e2c4f10be6588711cfa1aa5b4c83395c1c9c9ff285d46720fcfaf96940627266017476eff5e702306970b0490f91f248336e6fbf2928a9132bdcfb23df07
      (i64.const 134217728) ;; 134217728 == 2^27
    )
    (then
    )
    (else
      (call $runtime_errors.ran_out_of_duplication_identification_numbers
        (local.get $old_unallocated_id)
        (local.get $to_be_allocated_count)
      )
      unreachable
    )
  )


  (global.set $nextFreeDuplicationLabel
    (local.get $old_unallocated_id)
  )

  (local.get $old_unallocated_id)
)





(func $get_bookshelf
  (result externref)

  (global.get $bookshelf)
)




(func $add_book_to_bookshelf
  (param $rulebook externref)

  (result i64)

  (local $first_alloted_identification_number_for_EntryFragment i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (ref.is_null
          (local.get $rulebook)
        )
      )
    )
  #endif

  (global.set $bookshelf
    (local.get $rulebook)
  )

  (call $allocate_duplication_identifications
    (call $link.get_entry_fragment_duplication_identification_count
      (local.get $rulebook)
    )
  )
)
