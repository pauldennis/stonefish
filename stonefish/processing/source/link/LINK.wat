;; TODO rename module to link.wat
;; TODO rename to just link!


(func
  ;; this function is for somehow getting an external program from the embedder to be copied into the runtime
  ;; the easiest way i could think of is some recursive traversal of the ORIGINAL and simultanly allocating nodes in the runtime (COPY). lambdas and duplicatin nodes have a problem because when i encounter variables i have to somehow know where the COPY node lives. just looking at the ORIGINAL variable i only get the location of the ORIGINAL node. I use some embedder convenience helper code to just put the COPY location at this place for the twins and soloVariables to be found later.
  $raw_link

  (param $duplication_identification_mapping_offset i64)
  (param $rootFinger i64)
  (param $archetype externref)

  (result i64)

  (local $tag i64)

  (local.set $tag
    (call $extractTagFromFinger
      (local.get $rootFinger)
    )
  )

  (block $break
    (result i64)

    ;; TODO find out what is the fastest sorting

    (if
      (call $is_the_tag_a_RuleCall
        (local.get $tag)
      )
      (then
        (br $break
          (call $link_handle_ruleCall_case
            (local.get $duplication_identification_mapping_offset)
            (local.get $rootFinger)
            (local.get $archetype)
          )
        )
      )
    )

    (if
      (call $is_the_tag_a_Constructor
        (local.get $tag)
      )

      (then
        (br $break
          (call $link_handle_constructor_case
            (local.get $duplication_identification_mapping_offset)
            (local.get $rootFinger)
            (local.get $archetype)
          )
        )
      )
    )

    (if
      (call $is_the_tag_a_Lambda
        (local.get $tag)
      )

      (then
        (br $break
          (call $link_handle_lambda_case
            (local.get $duplication_identification_mapping_offset)
            (local.get $rootFinger)
            (local.get $archetype)
          )
        )
      )
    )

    (if
      (call $is_the_tag_an_SoloVariable
        (local.get $tag)
      )

      (then
        (br $break
          (call $link_handle_solo_variable_case
            (local.get $rootFinger)
            (local.get $archetype)
          )
        )
      )
    )

    (if
      (call $is_the_tag_an_IntrinsiscNumber
        (local.get $tag)
      )

      (then
        (br $break
          (local.get $rootFinger)
        )
      )
    )

    (if
      (call $is_the_tag_a_Twin
        (local.get $tag)
      )

      (then
        (br $break
          (call $link_handle_twin_case
            (local.get $duplication_identification_mapping_offset)
            (local.get $rootFinger)
            (local.get $archetype)
          )
        )
      )
    )

    (if
      (call $is_the_tag_an_Application
        (local.get $tag)
      )

      (then
        (br $break
          (call $link_handle_application_case
            (local.get $duplication_identification_mapping_offset)
            (local.get $rootFinger)
            (local.get $archetype)
          )
        )
      )
    )


    ;;TODO have unreachable call for triggerung undefined behaviour unconditionally
    #if STONEFISH_SHOULD_INCLUDE_ASSERT
      (call $assert
        (call $FALSE)
      )
    #endif

    unreachable
  )

)


