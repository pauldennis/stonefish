

(func $link_handle_application_case

  (param $duplication_identification_mapping_offset i64)
  (param $ORIGINAL_constructorFinger i64)
  (param $blueprint externref)

  (result i64)

  (local $arity i64)

  (local $atWhatChildAreWe i64)

  (local $COPY_newConstructor_Location i64)

  (local $COPY_childFinger i64)

  (local $ORIGINAL_subfinger_Location i64)
  (local $ORIGINAL_childFinger i64)

  (local $result_Header i64)


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnApplication
        (local.get $ORIGINAL_constructorFinger)
      )
    )
  #endif


  (local.set $arity
    (call $ARITY_OF_TWO)
  )

  (local.set $COPY_newConstructor_Location
    (call $request_Node__might_trap
    )
  )

  ;; TODO unroll loop with iteration count 2!
  (loop $goThroughChildren
    #if STONEFISH_SHOULD_INCLUDE_ASSERT
      (call $assert
        (call $isInBetween_closedIntervall
          (call $ARITY_OF_ZERO)
          (local.get $atWhatChildAreWe)
          (call $MAXIMUM_OF_ARITY)
        )
      )
    #endif
    ;;;

    (local.set $ORIGINAL_subfinger_Location
      (call $extractNodeLocationWithFingerOffset
        (local.get $atWhatChildAreWe)
        (local.get $ORIGINAL_constructorFinger)
      )
    )
    (local.set $ORIGINAL_childFinger
      (call $embedder_dereferenceSubNodeLocation
        (local.get $blueprint)
        (local.get $ORIGINAL_subfinger_Location)
      )
    )

    (local.set $COPY_childFinger
      (call $raw_link
        (local.get $duplication_identification_mapping_offset)
        (local.get $ORIGINAL_childFinger)
        (local.get $blueprint)
      )
    )

    (call $replace_at_NodeLocation_and_SubFingerOffset_with_Finger
      (local.get $COPY_newConstructor_Location)
      (local.get $atWhatChildAreWe)
      (local.get $COPY_childFinger)
    )
    (call $maybeInstallNewLeashIfVariable
      (call $construct_leash_from_nodeLocation_and_fingerOffset
        (local.get $COPY_newConstructor_Location)
        (local.get $atWhatChildAreWe)
      )
      (local.get $COPY_childFinger)
    )


    ;;;
    (local.set $atWhatChildAreWe
      (call $i64.increment
        (local.get $atWhatChildAreWe)
      )
    )
    (br_if $goThroughChildren
      (call $i64.isLessThan
        (local.get $atWhatChildAreWe)
        (local.get $arity)
      )
    )
  )

  ;; TODO use proper constructor?
  ;; TODO remove unessesary features by not using very generic $construct_Pointer_by_taking_Header_and_replacing_location_part
  (local.set $result_Header
    (call $construct_Pointer_by_taking_Header_and_replacing_location_part
      (local.get $ORIGINAL_constructorFinger)
      (local.get $COPY_newConstructor_Location)
    )
  )

  (local.get $result_Header)

)
