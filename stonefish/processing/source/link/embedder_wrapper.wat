;; TODO rename file to edgeRegister or the like

;; TODO Decide on a pattern for conditionally importing things or defined a function with undefined body?
;; current idea is to have dead imports wat files

(func
  $embedder_dereferenceSubNodeLocation
  (param $archetype externref)
  (param $location i64)

  (result i64)

  (local $fingerLocation i64)

  ;; TODO have interface with bytes

  (local.set $fingerLocation
    (call $i64.decreaseShift
      (local.get $location)
      (i64.const 3) ;; TODO divide by 8
    )
  )

  ;; TODO have proper dead ends
  #ifdef STONEFISH_EXPORT_LINKING
    (call $link.dereferenceSubNodeLocation
      (local.get $archetype)
      (local.get $fingerLocation)
    )
  #else
    (unreachable)
  #endif
)


(func
  $embedder_rememberCOPYLocation
  (param $blueprint externref)
  (param $ORIGINAL_location i64)
  (param $COPY_location i64)

  (local $ORIGINAL_FingerLocation i64)

  ;; TODO have interface with bytes?

  (local.set $ORIGINAL_FingerLocation
    (call $i64.decreaseShift
      (local.get $ORIGINAL_location)
      (i64.const 3) ;; TODO divide by 8
    )
  )

  ;; TODO have proper dead ends
  #ifdef STONEFISH_EXPORT_LINKING
    (call $link.memorizeCOPYLocation
      (local.get $blueprint)
      (local.get $ORIGINAL_FingerLocation)
      (local.get $COPY_location)
    )
  #else
    (unreachable)
  #endif
)


(func
  $embedder_isCOPYLocationSet
  (param $archetype externref)
  (param $ORIGINAL_location i64)
  (result i32)

  (local $result i32)

  (local $ORIGINAL_FingerLocation i64)



  ;; TODO have interface with bytes?

  (local.set $ORIGINAL_FingerLocation
    (call $i64.decreaseShift
      (local.get $ORIGINAL_location)
      (i64.const 3) ;; TODO divide by 8
    )
  )

  ;; TODO have proper dead ends
  #ifdef STONEFISH_EXPORT_LINKING
    (local.set $result
      (call $link.isCOPYLocationSet
        (local.get $archetype)
        (local.get $ORIGINAL_FingerLocation)
      )
    )
  #else
    (unreachable)
  #endif


  ;; lets be conservative since this is an interface to the outside world that cannot be changed easily later on
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isNormalizedBool
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)


(func
  $embedder_recallCOPYLocation
  (param $archetype externref)
  (param $ORIGINAL_location i64)
  (result i64)

  (local $ORIGINAL_FingerLocation i64)

  ;; TODO have interface with bytes?

  (local.set $ORIGINAL_FingerLocation
    (call $i64.decreaseShift
      (local.get $ORIGINAL_location)
      (i64.const 3) ;; TODO divide by 8
    )
  )

  ;; TODO have proper dead ends
  #ifdef STONEFISH_EXPORT_LINKING
    (call $link.recallCOPYLocation
      (local.get $archetype)
      (local.get $ORIGINAL_FingerLocation)
    )
  #else
    (unreachable)
  #endif
)


