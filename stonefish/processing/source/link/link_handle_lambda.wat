

(func
  $link_handle_lambda_case

  (param $duplication_identification_mapping_offset i64)
  (param $ORIGINAL_lambdaFinger i64)
  (param $archetype externref)

  (result i64)

  (local $COPY_newLambdaNode_Location i64)

  (local $ORIGINAL_soloVariableLeash_Location i64)
  (local $ORIGINAL_lambdaBodyFinger_Location i64)
  (local $ORIGINAL_lambda_body_finger i64)

  (local $COPY_lambdaBodyFinger i64)

  (local $result_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $ORIGINAL_lambdaFinger)
      )
    )
  #endif

  (local.set $COPY_newLambdaNode_Location
    (call $request_Node__might_trap
    )
  )

  (local.set $ORIGINAL_soloVariableLeash_Location
    (call $extractNodeLocationWithFingerOffset
      (call $LAMBDA_NODE__POSITION_OF_OCCURENCE_MARKER_POINTER)
      (local.get $ORIGINAL_lambdaFinger)
    )
  )

  (local.set $ORIGINAL_lambdaBodyFinger_Location
    (call $extractNodeLocationWithFingerOffset
      (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
      (local.get $ORIGINAL_lambdaFinger)
    )
  )

  (call $embedder_rememberCOPYLocation
    (local.get $archetype)
    (local.get $ORIGINAL_soloVariableLeash_Location)
    (local.get $COPY_newLambdaNode_Location)
  )

  (local.set $ORIGINAL_lambda_body_finger
    (call $embedder_dereferenceSubNodeLocation
      (local.get $archetype)
      (local.get $ORIGINAL_lambdaBodyFinger_Location)
    )
  )

  (local.set $COPY_lambdaBodyFinger
    (call $raw_link
      (local.get $duplication_identification_mapping_offset)
      (local.get $ORIGINAL_lambda_body_finger)
      (local.get $archetype)
    )
  )

  ;; the one that embeds the solo variable has to take care of repairing the backreference
  ;; while the lambda body is taken care of the COPY lambda node exists and the leash can be updated
  ;; for that to work the solo variable that gets returned has the correct COPY location in the pointing part of the finger

  (call $replace_at_NodeLocation_and_SubFingerOffset_with_Finger
    (local.get $COPY_newLambdaNode_Location)
    (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
    (local.get $COPY_lambdaBodyFinger)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $COPY_newLambdaNode_Location)
      (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
    )
    (local.get $COPY_lambdaBodyFinger)
  )

  (local.set $result_Header
    (call $construct_Pointer_by_taking_Header_and_replacing_location_part
      (local.get $ORIGINAL_lambdaFinger)
      (local.get $COPY_newLambdaNode_Location)
    )
  )

  (local.get $result_Header)
)


(func
  $link_handle_solo_variable_case

  (param $soloVariableFinger i64)
  (param $archetype externref)

  (result i64)

  (local $ORIGINAL_BoundingLambdaNode i64)

  (local $COPY_LambdaNode_Location i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASoloVariable
        (local.get $soloVariableFinger)
      )
    )
  #endif

  (local.set $ORIGINAL_BoundingLambdaNode
    (call $getAssociatedLambdaNodeLocation_of_SoloVariable
      (local.get $soloVariableFinger)
    )
  )

  (local.set $COPY_LambdaNode_Location
    (call $embedder_recallCOPYLocation
      (local.get $archetype)
      (local.get $ORIGINAL_BoundingLambdaNode)
    )
  )

  (call $construct_SoloVariableHeader
    (local.get $COPY_LambdaNode_Location)
  )
)

