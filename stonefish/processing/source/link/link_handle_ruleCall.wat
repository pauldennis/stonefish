

(func
  $link_handle_ruleCall_case
  (param $duplication_identification_mapping_offset i64)
  (param $ORIGINAL_ruleCallFinger i64)
  (param $array_to_be_linked externref)

  (result i64)

  (local $ORIGINAL_arity i64)
  (local $ORIGINAL_ruleID i64)
  (local $COPY_ruleID i64)

  (local $COPY_ruleCall_Location i64)
  (local $atWhatChildAreWe i64)
  (local $COPY_childFinger i64)
  (local $ORIGINAL_subfinger_Location i64)
  (local $ORIGINAL_childFinger i64)

  (local $result_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $ORIGINAL_ruleCallFinger)
      )
    )
  #endif

  (local.set $ORIGINAL_arity
    (call $getArityFromRuleHeader
      (local.get $ORIGINAL_ruleCallFinger)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (call $ARITY_OF_ZERO)
        (local.get $ORIGINAL_arity)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $ORIGINAL_arity)
        (call $MAXIMUM_OF_ARITY)
      )
    )
  #endif

  (local.set $ORIGINAL_ruleID
    (call $extract_RuleIdentification_fromRuleHeader
      (local.get $ORIGINAL_ruleCallFinger)
    )
  )

  ;; TODO implement
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (call $ARITY_OF_ZERO)
          (local.get $ORIGINAL_arity)
        )
      )
    )
  #endif

  (local.set $COPY_ruleCall_Location
    (call $request_Node__might_trap
    )
  )

  (local.set $atWhatChildAreWe
    (i64.const 0)
  )

  (loop $goThroughChildren
    ;; TODO use some clamp function
    #if STONEFISH_SHOULD_INCLUDE_ASSERT
      (call $assert
        (call $isInBetween_closedIntervall
          (call $ARITY_OF_ZERO)
          (local.get $atWhatChildAreWe)
          (call $MAXIMUM_OF_ARITY)
        )
      )
    #endif
    ;;;

    (local.set $ORIGINAL_subfinger_Location
      (call $extractNodeLocationWithFingerOffset
        (local.get $atWhatChildAreWe)
        (local.get $ORIGINAL_ruleCallFinger)
      )
    )

    (local.set $ORIGINAL_childFinger
      (call $embedder_dereferenceSubNodeLocation
        (local.get $array_to_be_linked)
        (local.get $ORIGINAL_subfinger_Location)
      )
    )

    (local.set $COPY_childFinger
      (call $raw_link
        (local.get $duplication_identification_mapping_offset)
        (local.get $ORIGINAL_childFinger)
        (local.get $array_to_be_linked)
      )
    )

    (call $replace_at_NodeLocation_and_SubFingerOffset_with_Finger
      (local.get $COPY_ruleCall_Location)
      (local.get $atWhatChildAreWe)
      (local.get $COPY_childFinger)
    )
    (call $maybeInstallNewLeashIfVariable
      (call $construct_leash_from_nodeLocation_and_fingerOffset
        (local.get $COPY_ruleCall_Location)
        (local.get $atWhatChildAreWe)
      )
      (local.get $COPY_childFinger)
    )

    ;;;
    (local.set $atWhatChildAreWe
      (call $i64.increment
        (local.get $atWhatChildAreWe)
      )
    )
    (br_if $goThroughChildren
      (call $i64.isLessThan
        (local.get $atWhatChildAreWe)
        (local.get $ORIGINAL_arity)
      )
    )

  )


  ;; TODO have proper rule id translation
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (local.get $ORIGINAL_ruleID)
          (call $MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule)
        )
      )
    )
  #endif


  ;; TODO have proper rule id translation given from outside
  (local.set $COPY_ruleID
    (if
      (result i64)
      (call $i64.isEqual
        (local.get $ORIGINAL_ruleID)
        (call $MAGIC_RULE_ID_WebSocketMessageTrigger)
      )
      (then
        (call $MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule)
      )
      (else
        (local.get $ORIGINAL_ruleID)
      )
    )
  )


  (local.set $result_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (local.get $COPY_ruleID)
      (local.get $COPY_ruleCall_Location)
    )
  )

  (local.get $result_Header)
)




