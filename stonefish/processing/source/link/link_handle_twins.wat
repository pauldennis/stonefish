

(func
  $link_handle_twin_case

  (param $duplication_identification_mapping_offset i64)
  (param $ORIGINAL_twinFinger i64)
  (param $archetype externref)

  (result i64) ;; the new root

  (local $ORIGINAL_duplicationNode_Location i64)

  (local $isDuplicationNodeAlreadyTakenCareOf i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $ORIGINAL_twinFinger)
      )
    )
  #endif

  (local.set $ORIGINAL_duplicationNode_Location
    (call $getAssociatedFloatingDuplicationNodeLocation
      (local.get $ORIGINAL_twinFinger)
    )
  )

  (local.set $isDuplicationNodeAlreadyTakenCareOf
    (call $embedder_isCOPYLocationSet
      (local.get $archetype)
      (local.get $ORIGINAL_duplicationNode_Location)
    )
  )

  (if
    (result i64)

    (local.get $isDuplicationNodeAlreadyTakenCareOf)

    (then
      (call $link_handle_twin_where_duplication_node_is_already_taken_care_of
        (local.get $duplication_identification_mapping_offset)
        (local.get $ORIGINAL_twinFinger)
        (local.get $archetype)
        (local.get $ORIGINAL_duplicationNode_Location)
      )
    )
    (else
      (call $link_handle_first_encounter_of_twin_of_some_duplication_node
        (local.get $duplication_identification_mapping_offset)
        (local.get $ORIGINAL_twinFinger)
        (local.get $archetype)
        (local.get $ORIGINAL_duplicationNode_Location)
      )
    )
  )
)





(func
  $link_handle_twin_where_duplication_node_is_already_taken_care_of

  (param $duplication_identification_mapping_offset i64)

  (param $ORIGINAL_twinFinger i64)
  (param $archetype externref)
  (param $remembered_COPY_duplication_Location i64)

  (result i64)

  (local $ORIGINAL_BoundingLambdaNode i64)

  (local $COPY_DuplicationNode_Location i64)

  (local $areWeHandlingALeftTwin i32)

  (local $ORIGINAL_duplicationLabel i64)
  (local $COPY_duplicationLabel i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $ORIGINAL_twinFinger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $embedder_isCOPYLocationSet
        (local.get $archetype)
        (local.get $remembered_COPY_duplication_Location)
      )
    )
  #endif


  (local.set $areWeHandlingALeftTwin
    (call $isFingerALeftTwin
      (local.get $ORIGINAL_twinFinger)
    )
  )
  (local.set $ORIGINAL_duplicationLabel
    (call $extract_DuplicationLabel_fromTwinFinger
      (local.get $ORIGINAL_twinFinger)
    )
  )

  (local.set $COPY_DuplicationNode_Location
    (call $embedder_recallCOPYLocation
      (local.get $archetype)
      (local.get $remembered_COPY_duplication_Location)
    )
  )


  ;; NOTE BEGIN SANDBOX BEHAVIOUR
  (local.set $COPY_duplicationLabel
    (call $i64.add
      (local.get $duplication_identification_mapping_offset)
      (local.get $ORIGINAL_duplicationLabel)
    )
  )
  ;; NOTE END SANDBOX BEHAVIOUR

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $COPY_duplicationLabel)
        ;; SYNC 3314e2c4f10be6588711cfa1aa5b4c83395c1c9c9ff285d46720fcfaf96940627266017476eff5e702306970b0490f91f248336e6fbf2928a9132bdcfb23df07
        (i64.const 134217728)
      )
    )
  #endif

  (call $construct_TwinHeader
    (local.get $areWeHandlingALeftTwin)
    (local.get $COPY_duplicationLabel)
    (local.get $COPY_DuplicationNode_Location)
  )
)



(func
  $link_handle_first_encounter_of_twin_of_some_duplication_node
  (param $duplication_identification_mapping_offset i64)
  (param $ORIGINAL_twinFinger i64)
  (param $archetype externref)
  (param $remembered_COPY_duplication_Location i64)

  (result i64)

  (local $COPY_newDuplicationNode_Location i64)

  (local $ORIGINAL_duplicationBodyFinger_Location i64)
  (local $ORIGINAL_duplication_body_Finger i64)

  (local $COPY_floatingDuplicationBody_Finger i64)

  (local.set $COPY_newDuplicationNode_Location
    (call $request_Node__might_trap
    )
  )

  (call $embedder_rememberCOPYLocation
    (local.get $archetype)
    (local.get $remembered_COPY_duplication_Location)
    (local.get $COPY_newDuplicationNode_Location)
  )

  (local.set $ORIGINAL_duplicationBodyFinger_Location
    (call $extractNodeLocationWithFingerOffset
      (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
      (local.get $ORIGINAL_twinFinger)
    )
  )

  (local.set $ORIGINAL_duplication_body_Finger
    (call $embedder_dereferenceSubNodeLocation
      (local.get $archetype)
      (local.get $ORIGINAL_duplicationBodyFinger_Location)
    )
  )

  (local.set $COPY_floatingDuplicationBody_Finger
    (call $raw_link
      (local.get $duplication_identification_mapping_offset)
      (local.get $ORIGINAL_duplication_body_Finger)
      (local.get $archetype)
    )
  )

  ;; the twin leashes are taken care of by the embedder of the twins
  ;; one of the leashes might already be taken care of by now
  ;; one or two are taken care after this function


  (call $replace_at_NodeLocation_and_SubFingerOffset_with_Finger
    (local.get $COPY_newDuplicationNode_Location)
    (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
    (local.get $COPY_floatingDuplicationBody_Finger)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $COPY_newDuplicationNode_Location)
      (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
    )
    (local.get $COPY_floatingDuplicationBody_Finger)
  )

  ;; TODO perhaps rename to something that
  (call $link_handle_twin_where_duplication_node_is_already_taken_care_of
    (local.get $duplication_identification_mapping_offset)
    (local.get $ORIGINAL_twinFinger)
    (local.get $archetype)
    (local.get $remembered_COPY_duplication_Location)
  )

)
