;; TODO rename to module_linking?

;; it appears to be convenient from a embedders perspective that variables are not floating when given outside of the runtime
  ;; TODO is this actually nessesary?
  ;; TODO it could be optimized that only variables (or maybe only twins) are placed into an wrapper
    ;; i think only as an more raw interface option it makes sense to have the no wrapper





;; TODO put into other folder since in this folder only functions concerning wrappes are supposed to go here?
(func
  $link_archetype_to_graph
  (param $duplication_identification_mapping_offset i64)
  (param $rootFinger i64)
  (param $archetype externref)

  (result i64)

  (local $raw_program i64)
  (local $wrapper_Node_Location i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (ref.is_null
          (local.get $archetype)
        )
      )
    )
  #endif

  (local.set $raw_program
    (call $raw_link
      (local.get $duplication_identification_mapping_offset)
      (local.get $rootFinger)
      (local.get $archetype)
    )
  )

  (local.get $raw_program)
)


(func
  $link_fragment_to_script
  (param $duplication_identification_mapping_offset i64)
  (param $rootFinger i64)
  (param $melee externref)
  (result i64)

  (local $archetype externref)
  (local $raw_program i64)
  (local $wrapper_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (ref.is_null
          (local.get $melee)
        )
      )
    )
  #endif

  (local.set $archetype
    (call $link.constructArchetype
      (local.get $melee)
    )
  )

  ;; hm, in c++ the empty array might be represented as a nullptr? what if some unboxed value is linked?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (ref.is_null
          (local.get $archetype)
        )
      )
    )
  #endif

  (local.set $raw_program
    (call $link_archetype_to_graph
      (local.get $duplication_identification_mapping_offset)
      (local.get $rootFinger)
      (local.get $archetype)
    )
  )

  (local.set $wrapper_Header
    (call $compose_wrappedTerm_Finger
      (local.get $raw_program)
    )
  )

  (local.get $wrapper_Header)
)





(func
  $link_module

  ;; TODO
    ;; the linking code needs to apply the translation while copying the fragment (and later the rules)
    ;; translate rule ids with internally allocated RuleMapping and translate rulebook

  ;; TODO for now leak identification numbers. so no cleanup of unused modules

  ;; TODO maybe i want to have some checking while linking that all rules are resolvable?
    ;; no since this was already checked by the assembler
    ;; but perhaps some assertions? Or maybe not because too complicated?
      ;; yes only do assertions at the very edge of operations

  ;; TODO or maybe i want to give the mapping into the runtime, that way i would not have to parse the blueprint in javascript which is a bad idea
    ;; the mapping of magic rules is still translated, but allways to the same reserved internal rule identifications
      ;; the compiler can still allocate some magic rule ids for itself
        ;; thats how it will go (probably): The compiler takes some hardcoded rule declaration. This is just passed ass additional assembly down. Later an more sophisticated frontend can generate these automatically or whatnot. But still the simple middle-end is the validation layer. It must check for every uniqueness for example
      ;; linking magic rules is opt-in!
    ;; the mapping of rules should be annodated to each rule in the bookshelf inorder to be used while linking?
      ;; perhaps splitting the rule identification space into two spaces just to be safe that i did not forget any translation
      ;; maybe some typesafty can be applied even in webassembly text?


  ;; TODO
    ;; the embedder gives a mapping (or perhaps just a whitelist) of magic rules that the module is allowed to use
      ;; Its not clear what that should be in praxis other than ALU access and internet access or perhaps some database access
        ;; within the runtime magic capabilities are trivially shared or restricted with the object capability model.
          ;; by having them passed EXPLICITLY the permissions becomne obvious.
          ;; restricting the permissions become obvious and natural
    ;; at the moment the embedder could check the whitelist itself but i think it is better to have the embedder require to explicitly list all magic used rules.
      ;; later perhaps there could be some callbacks that the embedder provided beforehand
        ;; these will be application specific, perhaps?

  ;; what about constructor identifications? Should they be namespaced? Currently I have no idea.

  (param $rootFinger i64)
  (param $melee externref)
  (param $rules externref)

  (result i64)

  (local $duplication_identification_mapping i64)
  (local $modele_entry_point i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (ref.is_null
          (local.get $melee)
        )
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (ref.is_null
          (local.get $rules)
        )
      )
    )
  #endif

  (local.set $duplication_identification_mapping
    (call $add_book_to_bookshelf
      (local.get $rules)
    )
  )

  (local.set $modele_entry_point
    (call $link_fragment_to_script
      (local.get $duplication_identification_mapping)
      (local.get $rootFinger)
      (local.get $melee)
    )
  )

  (local.get $modele_entry_point)
)
