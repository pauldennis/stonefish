
(func
  $link_intrinsic_number_f32
  (param $number f32)
  (result i64)

  (local $resultHeader i64)

  (local.set $resultHeader
    (call $construct_IntrinsicNumber_f32
      (local.get $number)
    )
  )

  (local.set $resultHeader
    (call $compose_wrappedTerm_Finger
      (local.get $resultHeader)
    )
  )

  (local.get $resultHeader)
)
