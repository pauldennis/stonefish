;; TODO renmae file to plain_heap

;; properly seperate the two heap files

(func
  $initialize_heap
  (param $addressBegin i64)
  (param $numberOfHeapElements i64)

  (local $endOfHeap i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleByNodeByteSize
        (local.get $addressBegin)
      )
    )
  #endif

  (local.set $endOfHeap
    (call $i64.add
      (local.get $addressBegin)
      (call $i64.multiplicate
        (call $NUMBER_OF_BYTES_PER_NODE)
        (local.get $numberOfHeapElements)
      )
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $endOfHeap)
        (call $i64.getLastAllocatedAddress_exclusive)
      )
    )
  #endif

  (global.set $arrayBegin_Word8Index_inclusive_NodeHeap
    (local.get $addressBegin)
  )
  (global.set $arrayEnd___Word8Index_exclusive_NodeHeap
    (local.get $endOfHeap)
  )

  (call $debug_initialize_heap)
)



(func
  $debug_initialize_heap

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $i64.memory.fill
        (global.get $arrayBegin_Word8Index_inclusive_NodeHeap)
        (global.get $arrayEnd___Word8Index_exclusive_NodeHeap)
        (call $heap_memory_fill_pattern)
      )
      (call $TRUE)
    )
  #endif
)



;; TODO remove Index?
(func
  $isValidNodeLocationIndex
  (param $nodeLocation i64)
  (result i32)

  (if
    (call $i64.isDevisibleByNodeByteSize
      (local.get $nodeLocation)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (if
    (call $isInBetween_rightOpenIntervall
      (global.get $arrayBegin_Word8Index_inclusive_NodeHeap)
      (local.get $nodeLocation)
      (global.get $arrayEnd___Word8Index_exclusive_NodeHeap)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (call $TRUE)
)



;;; Finger
  ;; TODO the concept of fingers does not belong here!


(func
  $isValidFingerLocationIndex
  (param $fingerLocation i64)
  (result i32)

  (if
    (call $i64.isDevisibleByFingerByteSize
      (local.get $fingerLocation)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (if
    (call $isInBetween_rightOpenIntervall
      (global.get $arrayBegin_Word8Index_inclusive_NodeHeap)
      (local.get $fingerLocation)
      (global.get $arrayEnd___Word8Index_exclusive_NodeHeap)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (call $TRUE)
)


(func
  $raw_dereferenceSubNodeLocation
  (param $location i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFingerLocationIndex
        (local.get $location)
      )
    )
  #endif

  (call $i32.cast_i64
    (local.get $location)
  )

  (i64.load
    (call $i32.leftover_1)
  )
)


;; TODO !!!!!!! introduce two versions: one that does not allow place SoloVariables, one that allows solovariables
;; TODO done by now: $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
(func
  $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
  (param $location i64)
  (param $replacement i64)

  (local $mmoryIndex i32)

;;   (if
;;     (call $i64.isEqual
;;       (local.get $replacement)
;;       (i64.const 10754598109187)
;;     )
;;     (then
;;       (call $breakpoint)
;;       (if
;;         (call $leash_leashes_to_a_SoloVariable
;;           (local.get $replacement)
;;         )
;;         (then
;;         )
;;         (else
;;           (call $breakpoint)
;;         )
;;       )
;;     )
;;     (else
;;     )
;;   )
;;   (if
;;     (call $i64.isEqual
;;       (local.get $replacement)
;;       (i64.const 10445360463874)
;;     )
;;     (then
;;       (call $unconditional_breakpoint
;;         (local.get $location)
;;       )
;;     )
;;     (else
;;     )
;;   )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFingerLocationIndex
        (local.get $location)
      )
    )
  #endif

  (local.set $mmoryIndex
    (call $i32.cast_i64
      (local.get $location)
    )
  )

  (i64.store
    (local.get $mmoryIndex)
    (local.get $replacement)
  )


  ;; TODO wrong location this belongs to the stack and heap combined files. not in the raw heap file
  ;; TODO first step: have wrapper for $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement and only call this wrapper in this wrapper
  ;; this debug magic happens here

  #ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILLEASHES_FOR_FORGOTTEN_BACKREFERENCES_DETECTION
    (call $impure_assert_group.SpoilLeashes_For_Forgotten_Backreference_detection
      (call $track_forgotten_backreference_repairments
        (local.get $location)
        (local.get $replacement)
      )
      (call $TRUE)
    )
  #endif

)


;; TODO move this functionality into the FingerPlacements.wat file
(func
  $track_forgotten_backreference_repairments
  (param $location i64)
  (param $replacement i64)

  (local $proper_leash i64)
  (local $leash_location i64)
  (local $current_value_at_leash_location i64)
  (local $spoil_pattern_with_index i64)

  (if
    (call $isFingerAVariable
      (local.get $replacement)
    )
    (then
    )
    (else
      (return)
    )
  )

  (local.set $proper_leash
    (call $construct_leash_from_fingerLocation
      (local.get $location)
    )
  )

  (local.set $leash_location
    (call $getLeashLocation_from_Variable
      (local.get $replacement)
    )
  )

  (local.set $current_value_at_leash_location
    (call $dereferenceSubNodeLocation
      (local.get $leash_location)
    )
  )

  (if
    (call $i64.isEqual
      (local.get $proper_leash)
      (local.get $current_value_at_leash_location)
    )
    (then
      ;; caller already took care of backreference, so do not destroy it
      (return)
    )
  )

  ;; spoil leash with spoil identification
  (if
    (call $TRUE)
    (then
      (local.set $spoil_pattern_with_index
        (call $i64.add
          (call $i64.multiplicate
            (i64.const 16) ;; 00001_2
            (i64.extend_i32_u
              (call $get_backreference_counter)
            )
          )
          (call $HEAP_SPOILED_LEASH)
        )
      )

      (if
        (call $catched_leash_number
        )

        (then
          #ifdef STONEFISH_DEBUG_TOOLS
            (call $unconditional_breakpoint
              (i64.const 200) ;; TODO
            )
          #else
            unreachable
          #endif
        )
      )

      (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
        (local.get $leash_location)
        (local.get $spoil_pattern_with_index)
      )

      (call $increment_backreference_counter
      )
    )
  )

  ;; inproper sanatizing, turn on to get a hint, that some backreference repairment is missing because it is working now
  (if
    (call $FALSE)
    (then
      (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
        (call $getLeashLocation_from_Variable
          (local.get $replacement)
        )
        (local.get $proper_leash)
      )
    )
  )
)
