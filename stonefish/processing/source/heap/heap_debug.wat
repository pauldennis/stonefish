;; TODO rename file to heap_with_debug_helpers


(func
  $HEAP_UNUSED_MARKING
  (result i64)
  (i64.const 14757395258967641292) ;; 0011001100110011001100110011001100110011001100110011001100110011
)

(func
  $HEAP_SPOILED_LEASH
  (result i64)
  (i64.const 13835058050987196431) ;; 1111000000000000000000000000000011111111111111111111111111111101_2
)

(func
  $HEAP__LEASH_NOT_YET_SET
  (result i64)
  (i64.const 9223372032559808527) ;; 1111000000000000000000000000000011111111111111111111111111111110_2
)

(func
  $heap_memory_fill_pattern
  (result i32)

  (call $i32.bitwiseAnd
    (call $i32.MAXIMUM_I8) ;; 11111111000000000000000000000000
    (i32.wrap_i64
      (call $HEAP_UNUSED_MARKING)
    )
  )
)

;; result depends on debug assert group
(func
  $isWholeHeapMarkedUnused
  (result i32)

  (local $atWhatNodeAreWe i64)

  (local $isTheNodeMarkedUnused i32)

  (local $result i32)

  (local.set $result
    (call $TRUE)
  )

  (local.set $atWhatNodeAreWe
    (global.get $arrayBegin_Word8Index_inclusive_NodeHeap)
  )

  (block $terminateLoop
    (loop $goingThroughAllNodes

      (br_if $terminateLoop
        (call $not
          (call $i64.isLessThan
            (local.get $atWhatNodeAreWe)
            (global.get $arrayEnd___Word8Index_exclusive_NodeHeap)
          )
        )
      )

      (local.set $isTheNodeMarkedUnused
        (call $checkIfNodeIsUnused
          (local.get $atWhatNodeAreWe)
        )
      )

      (local.set $result
        (call $and
          (local.get $result)
          (local.get $isTheNodeMarkedUnused)
        )
      )

      ;; increment
      (local.set $atWhatNodeAreWe
        (call $i64.add
          (local.get $atWhatNodeAreWe)
          (call $NUMBER_OF_BYTES_PER_NODE)
        )
      )

      (br $goingThroughAllNodes)
    )
  )

  (local.get $result)
)



;; TODO add some undersocres for better readability
(func
  $markAtFingerLocationFingerAsUnused
  (param $fingerLocation i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFingerLocationIndex
        (local.get $fingerLocation)
      )
    )
  #endif

  (call $i64.memory.fill
    (local.get $fingerLocation)
    (call $NUMBER_OF_BYTES_PER_FINGER)
    (call $heap_memory_fill_pattern)
  )
)


(func
  $overwriteNodeWithI8Pattern
  (param $nodeLocation i64)
  (param $heap_memory_fill_pattern i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $nodeLocation)
      )
    )
  #endif

  (call $i64.memory.fill
    (local.get $nodeLocation)
    (call $NUMBER_OF_BYTES_PER_NODE)
    (local.get $heap_memory_fill_pattern)
  )
)

(func
  $markNodeAsUnused
  (param $nodeLocation i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $nodeLocation)
      )
    )
  #endif

  (call $overwriteNodeWithI8Pattern
    (local.get $nodeLocation)
    (call $heap_memory_fill_pattern)
  )
)



(func
  $check_if_at_NodeLocation_the_Finger_x_is_unused
  (param $nodeLocation i64)
  (param $fingerIndex i64)
  (result i32)


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $nodeLocation)
      )
    )
  #endif

  ;; TODO extract this check out and find other locations where this test is done manually
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $fingerIndex)
        (call $MAXIMUM_NUMBER_OF_FINGERS_PER_NODE)
      )
    )
  #endif

  ;; TODO make a loop out of this
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (call $i64.multiplicate
          (local.get $fingerIndex)
          (call $NUMBER_OF_BYTES_PER_FINGER)
        )
        (local.get $nodeLocation)
      )
    )
  )
)

(func
  $check_if_at_FingerLocation_is_some_value
  (param $fingerLocation i64)
  (result i32)

  (call $not
    (call $check_if_at_FingerLocation_is_unused
      (local.get $fingerLocation)
    )
  )
)

(func
  $check_if_at_FingerLocation_is_unused
  (param $fingerLocation i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFingerLocationIndex
        (local.get $fingerLocation)
      )
    )
  #endif

  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (local.get $fingerLocation)
    )
  )
)


(func
  $checkIfNodeIsUnused
  (param $nodeLocation i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $nodeLocation)
      )
    )
  #endif

  ;; the code below assumes $MAXIMUM_NUMBER_OF_FINGERS_PER_NODE to be 8
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 8)
        (call $MAXIMUM_NUMBER_OF_FINGERS_PER_NODE)
      )
    )
  #endif

  ;; TODO make a loop out of this
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (i64.const 0)
        (local.get $nodeLocation)
      )
    )
  )
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (i64.const 8)
        (local.get $nodeLocation)
      )
    )
  )
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (i64.const 16)
        (local.get $nodeLocation)
      )
    )
  )
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (i64.const 24)
        (local.get $nodeLocation)
      )
    )
  )
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (i64.const 32)
        (local.get $nodeLocation)
      )
    )
  )
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (i64.const 40)
        (local.get $nodeLocation)
      )
    )
  )
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (i64.const 48)
        (local.get $nodeLocation)
      )
    )
  )
  (call $i64.isEqual
    (call $HEAP_UNUSED_MARKING)
    (call $raw_dereferenceSubNodeLocation
      (call $i64.add
        (i64.const 56)
        (local.get $nodeLocation)
      )
    )
  )

  (call $and)
  (call $and)
  (call $and)
  (call $and)
  (call $and)
  (call $and)
  (call $and)
)


(func
  $returnValueForgeryBug
  (param $location i64)
  (result i64)

  (call $i32.cast_i64
    (local.get $location)
  )

  (i64.load
    (call $i32.leftover_1)
  )
)


(func
  $dereferenceSubNodeLocation
  (param $location i64)
  (result i64)

  (local $castedLocation i32)
  (local $result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFingerLocationIndex
        (local.get $location)
      )
    )
  #endif

  (local.set $castedLocation
    (call $i32.cast_i64
      (local.get $location)
    )
  )

  ;; currently the unused pattern in the heap is forbidden.
  ;; Should it ever be nessesary to use this pattern then some check with the stack could be implemented
  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $check_if_at_FingerLocation_is_some_value
        (local.get $location)
      )
    )
  #endif

  (local.set $result
    (i64.load
      (local.get $castedLocation)
    )
  )

  (local.get $result)
)
