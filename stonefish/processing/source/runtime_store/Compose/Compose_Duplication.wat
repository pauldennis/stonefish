
;; you wanne use a term twice? This is the way to go
;; NOTE both twins need to embeded into the heap before one of them can be evaluated. If not the shallow inlining cannot happen.
(func
  $duplicate_header_into_twoTwins_with_DuplicationLabel

  (param $toBeDuplicated_Header i64)
  (param $usedDuplicationLabel i64)

  (result i64) ;; leftTwin
  (result i64) ;; rightTwin

  (local $new_duplicationNode_Location i64)

  (local.set $new_duplicationNode_Location
    (call $request_Node__might_trap
    )
  )

  (call $raw_place_something_in_FingerNodeHeap
    (local.get $new_duplicationNode_Location)
    (call $DUPLICATION_NODE__POSITION_OF_LEFT_TWIN_LEASH)
    (call $HEAP__LEASH_NOT_YET_SET)
  )
  (call $raw_place_something_in_FingerNodeHeap
    (local.get $new_duplicationNode_Location)
    (call $DUPLICATION_NODE__POSITION_OF_RIGHT_TWIN_LEASH)
    (call $HEAP__LEASH_NOT_YET_SET)
  )
  (call $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
    (local.get $new_duplicationNode_Location)
    (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
    (local.get $toBeDuplicated_Header)
  )


  (call $construct_twinFingers_from_label_and_duplicationNodeLocation
    (local.get $usedDuplicationLabel)
    (local.get $new_duplicationNode_Location)
  )

)
