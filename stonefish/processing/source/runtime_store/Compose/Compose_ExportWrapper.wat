(func
  $compose_wrappedTerm_Finger
  (param $the_term_Header i64)
  (result i64)

  (local $wrapper_Node_Location i64)
  (local $wrapper_Header i64)

  (local.set $wrapper_Node_Location
    (call $request_Node__might_trap
    )
  )

  (call $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
    (local.get $wrapper_Node_Location)
    (call $CONSTRUCTOR_ID__ExportedTerm__TheTerm)
    (local.get $the_term_Header)
  )

  ;; TODO do properly!
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_TAG)
        (i64.const 6) ;; "0110"
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID__ExportedTerm_Arity)
        (i64.const 1)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID__ExportedTerm_ID)
        (i64.const 29)
      )
    )
  #endif

  ;; TODO Is this a magic value? If so extract!
  (local.set $wrapper_Header
    (call $construct_Pointer_by_taking_Header_and_replacing_location_part
      (i64.const 7446) ;; 0110_1000_101110000000000000000000_2 = 7446
      (local.get $wrapper_Node_Location)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $wrapper_Header)
      )
    )
  #endif


  #ifdef STONEFISH_DEBUG_TOOLS_HEAPDUMP
    (call $debug.register_wrapper
      (local.get $wrapper_Header)
    )
  #endif

  (local.get $wrapper_Header)
)
