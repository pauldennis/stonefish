(func
  $compose_application_node
  (param $f_Header i64)
  (param $x_Header i64)

  (result i64)

  (local $resultin_Application_Header i64)

  (local $new_Application_Node_Location i64)

  ;; TODO check that f is applicable?
    ;; Is that even possible?
    ;; Maybe some type system could check it but should the runtime have types?

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $f_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $x_Header)
      )
    )
  #endif

  (local.set $new_Application_Node_Location
    (call $request_Node__might_trap)
  )

  (call $replace_at_NodeLocation_and_SubFingerOffset_with_Finger
    (local.get $new_Application_Node_Location)
    (call $APPLICATION_NODE__POSITION_OF_TOBEAPPLIED)
    (local.get $f_Header)
  )
  ;; TODO repairment is needed?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $f_Header)
        )
      )
    )
  #endif

  (call $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
    (local.get $new_Application_Node_Location)
    (call $APPLICATION_NODE__POSITION_OF_THEARGUMENT)
    (local.get $x_Header)
  )
  ;; TODO repairment is needed?
    ;; Yes! Why would that not be needed? Is there someone calling it, but can know for it not beeing neesesary?
;;   #if STONEFISH_SHOULD_INCLUDE_ASSERT
;;     (call $assert
;;       (call $not
;;         (call $isFingerAVariable
;;           (local.get $x_Header)
;;         )
;;       )
;;     )
;;   #endif

  (local.set $resultin_Application_Header
    (call $construct_ApplicationHeader
      (local.get $new_Application_Node_Location)
    )
  )

  (local.get $resultin_Application_Header)
)
