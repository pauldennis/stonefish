;; TODO rename
(func
  $followTwinLeashAnReplaceTwinByFinger
  (param $twinLeash i64)
  (param $fingerHeader i64)

  (local $twinLocation i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $hasValidFingerType
        (local.get $fingerHeader)
      )
    )
  #endif

  (local.set $twinLocation
    (call $extractTheLocationWhereTheTwinVariableHeaderResides
      (local.get $twinLeash)
    )
  )

  (call $overwrite_at_a_location_a_TwinFinger_with_some_new_Finger
    (local.get $twinLocation)
    (local.get $fingerHeader)
  )
)


;; TODO maybe use $replaceTwinByFinger instead of these two
(func
  $replaceTwinByLeafConstructor
  (param $twinLeash i64)
  (param $constructorHeader_to_be_inlined i64)

  (local $twinLocation i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $constructorHeader_to_be_inlined)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_ZERO)
        (call $extractArityFromConstructorFinger
          (local.get $constructorHeader_to_be_inlined)
        )
      )
    )
  #endif

  (local.set $twinLocation
    (call $extractTheLocationWhereTheTwinVariableHeaderResides
      (local.get $twinLeash)
    )
  )

  (call $overwrite_at_a_location_a_TwinFinger_with_some_new_Finger
    (local.get $twinLocation)
    (local.get $constructorHeader_to_be_inlined)
  )
)


;; TODO maybe use $replaceTwinByFinger instead of these two
(func
  $replaceTwinByConstructorWithChildren
  (param $twinLeash i64)
  (param $constructorHeader_to_be_inlined i64)

  (local $twinLocation i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $constructorHeader_to_be_inlined)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (call $ARITY_OF_ZERO)
          (call $extractArityFromConstructorFinger
            (local.get $constructorHeader_to_be_inlined)
          )
        )
      )
    )
  #endif

  (local.set $twinLocation
    (call $extractTheLocationWhereTheTwinVariableHeaderResides
      (local.get $twinLeash)
    )
  )

  (call $overwrite_at_a_location_a_TwinFinger_with_some_new_Finger
    (local.get $twinLocation)
    (local.get $constructorHeader_to_be_inlined)
  )
)



;;




(func
  $followSoloVariableLeash_And_replaceByFinger

  (param $soloVariableLeash i64)
  (param $finger_to_be_inlined i64)

  (local $locationOfVariableWithinTheLambdaBody i64)

  (local.set $locationOfVariableWithinTheLambdaBody
     ;; TODO rename marker to leash
    (call $extractTheLocationThatTheFingerMarkerIsPointingTo
      (local.get $soloVariableLeash)
    )
  )

  (call $overwrite_at_a_location_a_SoloVariable_with_some_new_Finger
    (local.get $locationOfVariableWithinTheLambdaBody)
    (local.get $finger_to_be_inlined)
  )
)

