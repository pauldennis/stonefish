(func
  $isValidVariableLeash
  (param $leash i64)

  (result i32)

  (if
    (call $hasValidMarkerFingerTag
      (local.get $leash)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (if
    (call $isValidFingerLocationIndex
      (call $extractTheLocationThatTheFingerMarkerIsPointingTo
        (local.get $leash)
      )
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (call $TRUE)
)


;; TODO place?
(func
  $leash_of_LambdaHeader_leashes_to_a_SoloVariable
  (param $lambdaHeader i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $lambdaHeader)
      )
    )
  #endif

  (call $isFingerASoloVariable
    (call $dereferenceSubNodeLocation
      (call $extractTheLocationThatTheFingerMarkerIsPointingTo
        (call $getSoloVariableLeash_from_LambdaHeader
          (local.get $lambdaHeader)
        )
      )
    )
  )
)

;; TODO reuse this in fucntion above
(func
  $leash_leashes_to_a_SoloVariable
  (param $leash i64)
  (result i32)

  (call $isFingerASoloVariable
    (call $dereferenceSubNodeLocation
      (call $extractTheLocationThatTheFingerMarkerIsPointingTo
        (local.get $leash)
      )
    )
  )
)


;; TODO do better with more object orienting!
;; TODO TODO TODO TODO TODO TODO TODO
(func
  $has_live_crossreference
  (param $beginning_near_Finger i64)
  (result i32)

  (local $far_Location i64)
  (local $far_Finger i64)

  (local $near_Location i64)
  (local $reoccurring_near_Finger i64)

  (local $result i32)

  ;; at least a little bit of checking
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAVariable
        (local.get $beginning_near_Finger)
      )
    )
  #endif

  (local.set $far_Location
    ;; TODO do properly
    (i64.extend_i32_u
      (call $extractPointingPartOfFinger
        (local.get $beginning_near_Finger)
      )
    )
  )

  (local.set $far_Finger
    (call $dereferenceSubNodeLocation
      (local.get $far_Location)
    )
  )

  (local.set $near_Location
    (i64.extend_i32_u
      (call $extractPointingPartOfFinger
        (local.get $far_Finger)
      )
    )
  )

  (local.set $reoccurring_near_Finger
    (call $dereferenceSubNodeLocation
      (local.get $near_Location)
    )
  )

  (local.set $result
    (call $i64.isEqual
      (local.get $beginning_near_Finger)
      (local.get $reoccurring_near_Finger)
    )
  )

  (call $breakpoint_on_false
    (i64.const 600)
    (local.get $result)
  )

  (if
    (local.get $result)
    (then
    )
    (else
;;       (call $breakpoint) ;; TODO remove?
      (call $heapdump) ;; TODO remove?
    )
  )

  (local.get $result)
)
