(func
  $getSubLocationAndSubFinger
  (param $childNumber i64)
  (param $finger i64)
  (result i64) ;; location
  (result i64)

  (local $location i64)
  (local $the_Finger_at_that_place i64)

  (local.set $location
    (call $extractNodeLocationWithFingerOffset
      (local.get $childNumber)
      (local.get $finger)
    )
  )

  (local.set $the_Finger_at_that_place
    (call $dereferenceSubNodeLocation
      (local.get $location)
    )
  )

  (local.get $location)
  (local.get $the_Finger_at_that_place)
)

(func
  $getSubFinger_from_birthnumber_and_finger
  (param $childNumber i64)
  (param $finger i64)
  (result i64)

  (local $result i64)

  (call $extractNodeLocationWithFingerOffset
    (local.get $childNumber)
    (local.get $finger)
  )

  (local.set $result
    (call $dereferenceSubNodeLocation
      (call $i64.leftover_1)
    )
  )

  (local.get $result)
)

;;

(func
  $getNthConstructorChild
  (param $childNumber i64)
  (param $finger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $isFingerAConstructor
      (local.get $finger)
    )
  )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (local.get $childNumber)
    (local.get $finger)
  )
)

(func
  $mark_Nth_Child_of_Finger_as_unused
  (param $childNumber i64)
  (param $finger i64)

  (local $subFingerLocation i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $isValidFinger
      (local.get $finger)
    )
  )
  #endif

  (local.set $subFingerLocation
    (call $extractNodeLocationWithFingerOffset
      (local.get $childNumber)
      (local.get $finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (local.get $subFingerLocation)
  )
)



(func
  $getDuplicationBody
  (param $twin i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $twin)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
    (local.get $twin)
  )
)

;; TODO rename to getLeftLeash
(func
  $getLeftTwinLeash
  (param $twin i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $twin)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $DUPLICATION_NODE__POSITION_OF_LEFT_TWIN_LEASH)
    (local.get $twin)
  )
)
;; TODO rename
(func
  $getRightTwinLeash
  (param $twin i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $twin)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $DUPLICATION_NODE__POSITION_OF_RIGHT_TWIN_LEASH)
    (local.get $twin)
  )
)
(func
  $getTwinLeash
  (param $twin i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $twin)
      )
    )
  #endif

  (if
    (result i64)
    (call $isFingerALeftTwin
      (local.get $twin)
    )
    (then
      (call $getLeftTwinLeash
        (local.get $twin)
      )
    )
    (else
      (call $getRightTwinLeash
        (local.get $twin)
      )
    )
  )
)


(func
  $extractTheLocationWhereTheTwinVariableHeaderResides
  (param $twin_leash i64)
  (result i64)

  (local $TwinVariableLocation i64)

  (local $The_Twin i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $theOccurenceExists
        (local.get $twin_leash)
      )
    )
  #endif

  (local.set $TwinVariableLocation
    (call $extractTheLocationThatTheFingerMarkerIsPointingTo
      (local.get $twin_leash)
    )
  )


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (local.set $The_Twin
      (call $dereferenceSubNodeLocation
        (local.get $TwinVariableLocation)
      )
    )
    (call $assert
      (call $isFingerATwin
        (local.get $The_Twin)
      )
    )
  #endif

  (local.get $TwinVariableLocation)
)


(func
  $getTheToBeAppliedPart
  (param $application i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnApplication
        (local.get $application)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $APPLICATION_NODE__POSITION_OF_TOBEAPPLIED)
    (local.get $application)
  )
)


;; TODO name? "To"?
(func
  $getTheToArgumentPart
  (param $application i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnApplication
        (local.get $application)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $APPLICATION_NODE__POSITION_OF_THEARGUMENT)
    (local.get $application)
  )
)


(func
  $getSoloVariableLeash_from_LambdaHeader
  (param $lambda_finger i64)
  (result i64)

  (local $leash i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $lambda_finger)
      )
    )
  #endif

  (local.set $leash
    (call $getSubFinger_from_birthnumber_and_finger
      (call $LAMBDA_NODE__POSITION_OF_OCCURENCE_MARKER_POINTER)
      (local.get $lambda_finger)
    )
  )

  ;; TODO other case not considered yet
      ;; What other case?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isThereAVariableOccurence
        (local.get $leash)
      )
    )
  #endif

  (local.get $leash)
)
(func
  $getSoloVariableLeashLocation_from_SoloVariable
  (param $soloVariableFinger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASoloVariable
        (local.get $soloVariableFinger)
      )
    )
  #endif

  (call $extractNodeLocationWithFingerOffset
    ;; fingeroffset
    (call $LAMBDA_NODE__POSITION_OF_OCCURENCE_MARKER_POINTER)
    ;; finger
    (local.get $soloVariableFinger)
  )
)
(func
  $getTwinLeashLocation_from_Twin
  (param $twinFinger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $twinFinger)
      )
    )
  #endif

  (if
    (result i64)
    (call $isFingerALeftTwin
      (local.get $twinFinger)
    )
    (then
      (call $extractNodeLocationWithFingerOffset
        ;; fingeroffset
        (call $DUPLICATION_NODE__POSITION_OF_LEFT_TWIN_LEASH)
        ;; finger
        (local.get $twinFinger)
      )
    )
    (else
      (call $extractNodeLocationWithFingerOffset
        ;; fingeroffset
        (call $DUPLICATION_NODE__POSITION_OF_RIGHT_TWIN_LEASH)
        ;; finger
        (local.get $twinFinger)
      )
    )
  )
)
(func
  $getLeashLocation_from_Variable
  (param $variableHeader i64)

  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAVariable
        (local.get $variableHeader)
      )
    )
  #endif

  (if
    (call $isFingerASoloVariable
      (local.get $variableHeader)
    )
    (then
      (return
        (call $getSoloVariableLeashLocation_from_SoloVariable
          (local.get $variableHeader)
        )
      )
    )
  )

  (if
    (call $isFingerATwin
      (local.get $variableHeader)
    )
    (then
      (return
        (call $getTwinLeashLocation_from_Twin
          (local.get $variableHeader)
        )
      )
    )
  )

  unreachable
)

;; TODO rename to $getLambdaBodyPart?
(func
  $getLambdaBody
  (param $lambda_finger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $lambda_finger)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
    (local.get $lambda_finger)
  )
)


(func
  $getLeftOperand
  (param $intrinsicOperationHeader i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscBinaryOperator
        (local.get $intrinsicOperationHeader)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $INTRINSIC_BINARY_OPERATION__POSITION_OF_LEFT_OPERANT)
    (local.get $intrinsicOperationHeader)
  )
)
(func
  $getRightOperand
  (param $intrinsicOperationHeader i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscBinaryOperator
        (local.get $intrinsicOperationHeader)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $INTRINSIC_BINARY_OPERATION__POSITION_OF_RIGHT_OPERANT)
    (local.get $intrinsicOperationHeader)
  )
)


(func
  $getLeftSuperimposedHeader
  (param $superpositionHeader i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $superpositionHeader)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $SUPERPOSITION__POSITION_OF_LEFT_SUPERIMPOSED_TERM)
    (local.get $superpositionHeader)
  )
)
(func
  $getRightSuperimposedHeader
  (param $superpositionHeader i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $superpositionHeader)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $SUPERPOSITION__POSITION_OF_RIGHT_SUPERIMPOSED_TERM)
    (local.get $superpositionHeader)
  )
)

(func
  $getPrivateDataPartOfCapsule
  (param $capsuleHeader i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerACapsule
        (local.get $capsuleHeader)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $CAPSULE__POSITION_OF_PRIVATE_DATA)
    (local.get $capsuleHeader)
  )
)
(func
  $getInterfacePartOfCapsule
  (param $capsuleHeader i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerACapsule
        (local.get $capsuleHeader)
      )
    )
  #endif

  (call $getSubFinger_from_birthnumber_and_finger
    (call $CAPSULE__POSITION_OF_INTERFACE)
    (local.get $capsuleHeader)
  )
)

