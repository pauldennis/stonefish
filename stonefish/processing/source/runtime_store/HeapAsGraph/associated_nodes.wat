

;; TODO rename to getAssociatedDuplicationNodeLocation ?
(func
  $getAssociatedFloatingDuplicationNodeLocation
  (param $twin i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $twin)
      )
    )
  #endif

  (call $extractNodeLocation
    (local.get $twin)
  )
)

(func
  $getAssociated_Lambda_NodeLocation
  (param $lambda_finger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $lambda_finger)
      )
    )
  #endif

  (call $extractNodeLocation
    (local.get $lambda_finger)
  )
)


(func
  $getAssociated_Application_NodeLocation
  (param $finger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnApplication
        (local.get $finger)
      )
    )
  #endif

  (call $extractNodeLocation
    (local.get $finger)
  )
)


(func
  $getAssociated_Constructor_NodeLocation
  (param $finger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $finger)
      )
    )
  #endif

  (call $extractNodeLocation
    (local.get $finger)
  )
)


(func
  $getAssociated_IntrinsicBinaryOperator_NodeLocation
  (param $finger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscBinaryOperator
        (local.get $finger)
      )
    )
  #endif

  (call $extractNodeLocation
    (local.get $finger)
  )
)


(func
  $getAssociated_Superposition_NodeLocation
  (param $finger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $finger)
      )
    )
  #endif

  (call $extractNodeLocation
    (local.get $finger)
  )
)

(func
  $getAssociatedLambdaNodeLocation_of_SoloVariable
  (param $soloVariable i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASoloVariable
        (local.get $soloVariable)
      )
    )
  #endif

  (call $extractNodeLocation
    (local.get $soloVariable)
  )
)
