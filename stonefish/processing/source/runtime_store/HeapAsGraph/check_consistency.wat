
(func
  $in_case_of_lambda_the_leash_points_to_solovariable
  (param $rootFinger i64)
  (result i32)
  (if
    (result i32)

    (call $isFingerALambda
      (local.get $rootFinger)
    )

    (then
      (call $leash_of_LambdaHeader_leashes_to_a_SoloVariable
        (local.get $rootFinger)
      )
    )
    (else
      (call $TRUE)
    )
  )
)

(func
  $isGraphConsistent
  (param $rootFinger i64)
  (result i32)

  (call $isGraphConsistent_intensive
    (local.get $rootFinger)
  )
)

(func
  $isGraphConsistent_skip
  (param $rootFinger i64)
  (result i32)

  (call $TRUE)
)


(func
  $isGraphConsistent_intensive
  (param $rootFinger i64)
  (result i32)

  (local $leashToIgnore i64)

  ;; TODO what is wrong in these cases?
  (if
    (call $isForceModeActivated
    )
    (then
      (return
        (call $TRUE)
      )
    )
  )

  ;; TODO how to encode no leash to compare to ?
  (if
    (call $isFingerATwin
      (local.get $rootFinger)
    )
    (then
      (local.set $leashToIgnore
        (call $getTwinLeash
          (local.get $rootFinger)
        )
      )
    )
  )

  (call $is_graph_consistent_dispatch
    (local.get $rootFinger)
    (local.get $leashToIgnore)
  )
)


(func
  $is_graph_consistent_dispatch
  (param $rootFinger i64)
  (param $leashToIgnore i64)
  (result i32)

  (local $result i32)

  (local.set $result
    (block $break

      (result i32)

      (if
        (call $isFingerALambda
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $is_lambda_consistent
              (local.get $rootFinger)
              (local.get $leashToIgnore)
            )
          )
        )
      )

      (if
        (call $isFingerAnApplication
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $is_application_consistent
              (local.get $rootFinger)
              (local.get $leashToIgnore)
            )
          )
        )
      )

      (if
        (call $isFingerARuleCall
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $is_rulecall_consistent
              (local.get $rootFinger)
              (local.get $leashToIgnore)
            )
          )
        )
      )

      (if
        (call $isFingerAConstructor
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $is_constructor_consistent
              (local.get $rootFinger)
              (local.get $leashToIgnore)
            )
          )
        )
      )

      (if
        (call $isFingerASoloVariable
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $has_live_crossreference
              (local.get $rootFinger)
            )
          )
        )
      )

      (if
        (call $isFingerACapsule
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $is_capsule_consistent
              (local.get $rootFinger)
              (local.get $leashToIgnore)
            )
          )
        )
      )

      (if
        (call $isFingerATwin
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $is_twin_consistent
              (local.get $rootFinger)
              (local.get $leashToIgnore)
            )
          )
        )
      )

      (if
        (call $isFingerASuperposition
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $is_superposition_consistent
              (local.get $rootFinger)
              (local.get $leashToIgnore)
            )
          )
        )
      )

      (if
        (call $isFingerAnIntrinsiscNumber
          (local.get $rootFinger)
        )
        (then
          (br $break
            (call $TRUE)
          )
        )
      )

      (call $unconditional_breakpoint
        (i64.const 300) ;; TODO
      ) ;; TODO ?
      (call $heapdump) ;; TODO remove?
      unreachable
    )
  )

  (if
    (local.get $result)
    (then
    )
    (else
      (call $unconditional_breakpoint
        (local.get $rootFinger)
      )
      ;; ?
    )
  )

  (local.get $result)
)


(func
  $is_lambda_consistent
  (param $lambda i64)
  (param $leashToIgnore i64)

  (result i32)

  (local $result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $lambda)
      )
    )
  #endif

  (if
    (call $leash_of_LambdaHeader_leashes_to_a_SoloVariable
      (local.get $lambda)
    )

    (then
    )

    (else
      (return
        (call $FALSE)
      )
    )
  )

  (local.set $result
    (call $is_graph_consistent_dispatch
      (call $getLambdaBody
        (local.get $lambda)
      )
      (local.get $leashToIgnore)
    )
  )

  (local.get $result)
)

(func
  $is_application_consistent
  (param $root i64)
  (param $leashToIgnore i64)

  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnApplication
        (local.get $root)
      )
    )
  #endif

  (call $is_graph_consistent_dispatch
    (call $getTheToBeAppliedPart
      (local.get $root)
    )
    (local.get $leashToIgnore)
  )
  (call $is_graph_consistent_dispatch
    (call $getTheToArgumentPart
      (local.get $root)
    )
    (local.get $leashToIgnore)
  )

  ;; TODO should optimize for bad case?
  ;; also in the other cases
  (call $and)
)

(func
  $is_rulecall_consistent
  (param $root i64)
  (param $leashToIgnore i64)

  (result i32)

  (local $ruleIdentification i64)
  (local $arity i64)

  (local $atWhatChildAreWe i64)

  (local $intermediateResult i32)
  (local $result i32)

  (local $childFinger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $root)
      )
    )
  #endif

  (local.set $ruleIdentification
    (call $extract_RuleIdentification_fromRuleHeader
      (local.get $root)
    )
  )
  (local.set $arity
    (call $getArityFromRuleHeader
      (local.get $root)
    )
  )

  (local.set $atWhatChildAreWe
    (i64.const 0)
  )

  (local.set $result
    (call $TRUE)
  )

  (block $abort (loop $nextChild
    (br_if $abort
      (call $not
        (call $i64.isLessThan
          (local.get $atWhatChildAreWe)
          (local.get $arity)
        )
      )
    )

    (local.set $childFinger
      (call $getSubFinger_from_birthnumber_and_finger
        (local.get $atWhatChildAreWe)
        (local.get $root)
      )
    )

    (local.set $intermediateResult
      (call $is_graph_consistent_dispatch
        (local.get $childFinger)
        (local.get $leashToIgnore)
      )
    )

    (local.set $result
      (call $and
        (local.get $result)
        (local.get $intermediateResult)
      )
    )

    (local.set $atWhatChildAreWe
      (call $i64.increment
        (local.get $atWhatChildAreWe)
      )
    )


    (br $nextChild)
  ))


  (local.get $result)
)


(func
  $is_constructor_consistent
  (param $root i64)
  (param $leashToIgnore i64)

  (result i32)

  (local $arity i64)

  (local $atWhatChildAreWe i64)

  (local $intermediateResult i32)
  (local $result i32)

  (local $child i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $root)
      )
    )
  #endif

  (local.set $arity
    (call $extractArityFromConstructorFinger
      (local.get $root)
    )
  )

  (local.set $atWhatChildAreWe
    (i64.const 0)
  )

  (local.set $result
    (call $TRUE)
  )

  (block $abort (loop $nextChild
    (br_if $abort
      (call $not
        (call $i64.isLessThan
          (local.get $atWhatChildAreWe)
          (local.get $arity)
        )
      )
    )

    (local.set $child
      (call $getSubFinger_from_birthnumber_and_finger
        (local.get $atWhatChildAreWe)
        (local.get $root)
      )
    )

    (local.set $intermediateResult
      (call $is_graph_consistent_dispatch
        (local.get $child)
        (local.get $leashToIgnore)
      )
    )

    (local.set $result
      (call $and
        (local.get $result)
        (local.get $intermediateResult)
      )
    )

    (local.set $atWhatChildAreWe
      (call $i64.increment
        (local.get $atWhatChildAreWe)
      )
    )


    (br $nextChild)
  ))


  (local.get $result)
)






(func
  $is_capsule_consistent
  (param $root i64)
  (param $leashToIgnore i64)

  (result i32)

  (local $arity i64)

  (local $atWhatChildAreWe i64)

  (local $intermediateResult i32)
  (local $result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerACapsule
        (local.get $root)
      )
    )
  #endif

  (local.set $arity
    (i64.const 2)
  )

  (local.set $atWhatChildAreWe
    (i64.const 0)
  )

  (local.set $result
    (call $TRUE)
  )

  (block $abort (loop $nextChild
    (br_if $abort
      (call $not
        (call $i64.isLessThan
          (local.get $atWhatChildAreWe)
          (local.get $arity)
        )
      )
    )

    (local.set $intermediateResult
      (call $is_graph_consistent_dispatch
        (call $getSubFinger_from_birthnumber_and_finger
          (local.get $atWhatChildAreWe)
          (local.get $root)
        )
        (local.get $leashToIgnore)
      )
    )

    (local.set $result
      (call $and
        (local.get $result)
        (local.get $intermediateResult)
      )
    )

    (local.set $atWhatChildAreWe
      (call $i64.increment
        (local.get $atWhatChildAreWe)
      )
    )


    (br $nextChild)
  ))


  (local.get $result)
)



;; TODO do better with more object orienting!
(func
  $is_twin_consistent
  (param $twin i64)
  (param $leash_to_ignore i64)
  (result i32)

  (local $twin_leash i64)

  (local $twin_Location i64)
  (local $reoccurring_twin_Finger i64)

  ;; at least a little bit of checking
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $twin)
      )
    )
  #endif

  (local.set $twin_leash
    (call $getTwinLeash
      (local.get $twin)
    )
  )

  (if
    (call $i64.isEqual
      (local.get $twin_leash)
      (local.get $leash_to_ignore)
    )
    (then
      (return
        (call $TRUE)
      )
    )
  )

  (local.set $twin_Location
    (i64.extend_i32_u
      (call $extractPointingPartOfFinger
        (local.get $twin_leash)
      )
    )
  )

  (local.set $reoccurring_twin_Finger
    (call $dereferenceSubNodeLocation
      (local.get $twin_Location)
    )
  )

  (call $i64.isEqual
    (local.get $twin)
    (local.get $reoccurring_twin_Finger)
  )

)





(func
  $is_superposition_consistent
  (param $root i64)
  (param $leashToIgnore i64)

  (result i32)

  (local $arity i64)

  (local $atWhatChildAreWe i64)

  (local $intermediateResult i32)
  (local $result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $root)
      )
    )
  #endif

  (local.set $arity
    (i64.const 2)
  )

  (local.set $atWhatChildAreWe
    (i64.const 0)
  )

  (local.set $result
    (call $TRUE)
  )

  (block $abort (loop $nextChild
    (br_if $abort
      (call $not
        (call $i64.isLessThan
          (local.get $atWhatChildAreWe)
          (local.get $arity)
        )
      )
    )

    (local.set $intermediateResult
      (call $is_graph_consistent_dispatch
        (call $getSubFinger_from_birthnumber_and_finger
          (local.get $atWhatChildAreWe)
          (local.get $root)
        )
        (local.get $leashToIgnore)
      )
    )

    (local.set $result
      (call $and
        (local.get $result)
        (local.get $intermediateResult)
      )
    )

    (local.set $atWhatChildAreWe
      (call $i64.increment
        (local.get $atWhatChildAreWe)
      )
    )


    (br $nextChild)
  ))


  (local.get $result)
)
