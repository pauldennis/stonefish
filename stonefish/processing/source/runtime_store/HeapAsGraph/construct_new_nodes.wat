(func
  $construct_at_location_new_duplication_node

  (param $node_Location i64)

  (param $leftLeash i64)
  (param $rightLeash i64)
  (param $duplication_body_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $node_Location)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $node_Location)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidVariableLeash
        (local.get $leftLeash)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidVariableLeash
        (local.get $rightLeash)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $duplication_body_Header)
      )
    )
  #endif

  ;; TODO dedicated functions for that?
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $DUPLICATION_NODE__POSITION_OF_LEFT_TWIN_LEASH)
    )
    (local.get $leftLeash)
  )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $DUPLICATION_NODE__POSITION_OF_RIGHT_TWIN_LEASH)
    )
    (local.get $rightLeash)
  )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
    )
    (local.get $duplication_body_Header)
  )

  ;; TODO setting of $duplication_body_Header: backreference not set?
)



(func
  $place_at_ConstructorNodeLocation_as_child_x_a_Finger
  (param $constructerNodeLocation i64)
  (param $child_number i64)
  (param $fingerToBePlaced i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $constructerNodeLocation)
      )
    )
  #endif

  ;; might be not true in the future should there be more optimisation
  ;; TODO this could be checked against the stack, since the stack knows if the space is allocated
  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $check_if_at_NodeLocation_the_Finger_x_is_unused
          (local.get $constructerNodeLocation)
          (local.get $child_number)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $fingerToBePlaced)
      )
    )
  #endif

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $constructerNodeLocation)
      (local.get $child_number)
    )
    (local.get $fingerToBePlaced)
  )
)






(func
  $construct_at_location_new_lambda_node
  (param $node_Location i64)

  (param $soloVariableLeash i64)
  (param $lambdaBodyHeader i64)


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $node_Location)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $node_Location)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidVariableLeash
        (local.get $soloVariableLeash)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $lambdaBodyHeader)
      )
    )
  #endif

  ;; TODO dedicated functions for that ?
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $LAMBDA_NODE__POSITION_OF_OCCURENCE_MARKER_POINTER)
    )
    (local.get $soloVariableLeash)
  )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
    )
    (local.get $lambdaBodyHeader)
  )
)



(func
  $construct_at_location_new_superposition_node
  (param $node_Location i64)

  (param $leftHeader i64)
  (param $rightHeader i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $node_Location)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $node_Location)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $leftHeader)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $rightHeader)
      )
    )
  #endif

  ;; TODO dedicated functions for that ?
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $SUPERPOSITION__POSITION_OF_LEFT_SUPERIMPOSED_TERM)
    )
    (local.get $leftHeader)
  )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $SUPERPOSITION__POSITION_OF_RIGHT_SUPERIMPOSED_TERM)
    )
    (local.get $rightHeader)
  )
)


(func
  $construct_at_location_new_application_node
  (param $node_Location i64)

  (param $toBeApplied_Header i64)
  (param $argument_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $node_Location)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $node_Location)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $toBeApplied_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $argument_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $mightFingerBeApplicable
        (local.get $toBeApplied_Header)
      )
    )
  #endif

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $APPLICATION_NODE__POSITION_OF_TOBEAPPLIED)
    )
    (local.get $toBeApplied_Header)
  )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $APPLICATION_NODE__POSITION_OF_THEARGUMENT)
    )
    (local.get $argument_Header)
  )
)
