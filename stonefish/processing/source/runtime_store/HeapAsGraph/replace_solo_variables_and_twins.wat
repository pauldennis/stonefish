;; TODO rename to FingerNodesHeap

(func
  $overwrite_at_a_location_a_TwinFinger_with_some_new_Finger
  (param $location i64)
  (param $newFinger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $isFingerATwin
      (call $dereferenceSubNodeLocation
        (local.get $location)
      )
    )
  )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $hasValidFingerType
        (local.get $newFinger)
      )
    )
  #endif

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (local.get $location)
    (local.get $newFinger)
  )
)


;;


(func
  $overwrite_at_a_location_a_SoloVariable_with_some_new_Finger
  (param $location i64)
  (param $newFinger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASoloVariable
        (call $dereferenceSubNodeLocation
          (local.get $location)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $hasValidFingerType
        (local.get $newFinger)
      )
    )
  #endif

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (local.get $location)
    (local.get $newFinger)
  )
)
