
(func
  $relinquish_Node_at_Location
  (param $oldNodeLocation i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $oldNodeLocation)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $markNodeAsUnused
        (local.get $oldNodeLocation)
      )
      (call $TRUE)
    )
  #endif

  (if
    #ifdef STONEFISH_DEBUG_SPOILTOOL_YOLOMODE
      (global.get $is_allocation_yolo_mode_activated
      )
    #else
      (call $FALSE)
    #endif
    (then
      #if STONEFISH_SHOULD_INCLUDE_ASSERT
        (call $assert
          (call $wouldPushBeAllowed
            (local.get $oldNodeLocation)
          )
        )
      #endif
    )
    (else
      (call $freeSlotsStack_push
        (local.get $oldNodeLocation)
      )
    )

  )
)

(func
  $relinquish_Node_behind_Finger
  (param $Finger i64)

  ;; TODO check that the finger actually has some node behind it!

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $Finger)
    )
  )
)

(func
  $request_Node__might_trap
  (result i64)

  (local $resultLocation i64)

  #ifdef STONEFISH_OPTIMIZATION_EXPLICIT_HEAP_IS_FULL_ERROR_MESSAGE
    ;; for the convienience of peter porter we can turn on an explicit error message that the heap is full
    ;; NOTE if performance is desired then we do not do this here but trap instead with an out of bounds error later
    (if
      (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
      )

      (then
        (call $optimization_versus_ergonomics.not_enough_space_left
          (call $how_many_slots_are_left
          )
          (i64.const 0)
        )
        unreachable
      )
    )
  #endif


  ;; breaking here means we will trap later
  (call $conditional_breakpoint
    (call $not
      (call $isStackEmpty_whichIsTheSameAs_isTheHeapFull
      )
    )
  )

  (local.set $resultLocation
    (call $freeSlotsStack_pop
    )
  )

;;   (if
;;     (call $i64.isEqual
;;       (local.get $resultLocation)
;;       (i64.const 17792)
;;     )
;;     (then
;;       (call $breakpoint)
;;     )
;;   )

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $checkIfNodeIsUnused
        (local.get $resultLocation)
      )
    )
  #endif

  ;; TODO testcase for this behaviour
  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $overwriteNodeWithI8Pattern
        (local.get $resultLocation)
        (i32.const 0) ;; 00000000
      )
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $resultLocation)
        )
      )
    )
  #endif

  (local.get $resultLocation)
)





;;;;






(func
  $relinquish_from_to_N_many
  (param $firstLocation i64)
  (param $numberOfNodes i64)

  (local $atWhatNodeAreWe i64)
  (local $lastLocationExclusive i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $firstLocation)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $firstLocation)
        (call $i64.getLastAllocatedAddress_exclusive)
      )
    )
  #endif

  (local.set $lastLocationExclusive
    (call $i64.add
      (local.get $firstLocation)
      (call $i64.multiplicate
        (call $NUMBER_OF_BYTES_PER_NODE)
        (local.get $numberOfNodes)
      )
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $lastLocationExclusive)
        (call $i64.getLastAllocatedAddress_exclusive)
      )
    )
  #endif

  (local.set $atWhatNodeAreWe
    (local.get $firstLocation)
  )

  (if
    (call $i64.isEqual
      (i64.const 0)
      (local.get $numberOfNodes)
    )
    (then
      ;; do nothing
    )
    (else

      (loop $goThroughNodes

        ;; do something with $atWhatNodeAreWe
        (call $relinquish_Node_at_Location
          (local.get $atWhatNodeAreWe)
        )

        ;; increment pointer
        (local.set $atWhatNodeAreWe
          (call $i64.add
            (local.get $atWhatNodeAreWe)
            (call $NUMBER_OF_BYTES_PER_NODE)
          )
        )

        ;; should we have to repeat?
        (br_if $goThroughNodes
          (call $i64.isLessThan
            (local.get $atWhatNodeAreWe)
            (local.get $lastLocationExclusive)
          )
        )

      )

    )
  )
)


(func
  $getNewDuplicationLabel
  (result i64)

  (local $result i64)

  (if
    (call $not
      (call $i64.isLessThan
        (global.get $nextFreeDuplicationLabel)
        (call $upperBoundDuplicationlabel_exclusive)
      )
    )
    (then
      (call $user-inflicted_error_case.ran_out_of_duplication_labels
      )
    )
  )

  (local.set $result
    (global.get $nextFreeDuplicationLabel)
  )

  (global.set $nextFreeDuplicationLabel
    (call $i64.increment
      (global.get $nextFreeDuplicationLabel)
    )
  )

  (local.get $result)
)



