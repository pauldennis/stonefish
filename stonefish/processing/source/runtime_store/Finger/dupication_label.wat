

(func
  $areDuplicationLabelsEqual
  (param $leftDuplicationLabel i64)
  (param $rightDuplicationLabel i64)

  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidDuplicationLabel
        (local.get $leftDuplicationLabel)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidDuplicationLabel
        (local.get $rightDuplicationLabel)
      )
    )
  #endif

  (call $i64.isEqual
    (local.get $leftDuplicationLabel)
    (local.get $rightDuplicationLabel)
  )
)



(func
  $isValidDuplicationLabel
  (param $duplicationLabel i64)
  (result i32)

  (call $i64.isLessOrEqual
    (local.get $duplicationLabel)
    (call $MAXIMUM_DUPLICATION_LABEL)
  )
)
