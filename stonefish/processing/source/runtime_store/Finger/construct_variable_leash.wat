
(func
  $construct_leash_from_nodeLocation_and_fingerOffset
  (param $node_location i64)
  (param $fingerOffset i64)

  (result i64)

  (local $fingerLocation i64)

  (local.set $fingerLocation
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_location)
      (local.get $fingerOffset)
    )
  )

  (call $i64.add
    ;; move location to upper 32 bits
    (call $i64.multiplicate
      (local.get $fingerLocation)
      (call $LEASH__FINGER_LOCATION__POSITION_BY_MULTIPLICATION)
    )
    (i64.extend_i32_u
      (call $MARKER_FINGER_THERE_IS_A_VARIABLE_OCCURENCE)
    )
  )
)


(func
  $construct_leash_from_fingerLocation
  (param $fingerLocation i64)

  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFingerLocationIndex
        (local.get $fingerLocation)
      )
    )
  #endif

  (call $i64.add
    ;; move location to upper 32 bits
    (call $i64.multiplicate
      (local.get $fingerLocation)
      (call $LEASH__FINGER_LOCATION__POSITION_BY_MULTIPLICATION)
    )
    (i64.extend_i32_u
      (call $MARKER_FINGER_THERE_IS_A_VARIABLE_OCCURENCE)
    )
  )
)
