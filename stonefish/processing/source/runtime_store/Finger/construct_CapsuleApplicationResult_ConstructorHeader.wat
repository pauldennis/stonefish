
(func
  $construct_CapsuleApplicationResult
  (param $associatedNode i64)
  (result i64)

  (local $lowerhalfbits i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $associatedNode)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_TAG)
        (i64.const 6) ;; 0110
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID__CapsuleCallResult)
        (i64.const 19) ;; 11001
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (i64.const 2) ;; 0100
      )
    )
  #endif

  ;; 0110 0100 110010000000000000000000
  ;; == 4902

  (local.set $lowerhalfbits
    (i64.const 4902)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $associatedNode)
        (i64.const 4294967295)
      )
    )
  #endif

  (call $i64.add
    ;; move location to upper 32 bits
    (call $i64.multiplicate
      (call $CONSTRUCTOR__LOCATION_PART_BY_MULTIPLY)
      (local.get $associatedNode)
    )
    (local.get $lowerhalfbits)
  )
)


