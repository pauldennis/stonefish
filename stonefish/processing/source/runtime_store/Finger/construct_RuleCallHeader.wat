;; TODO find correct place
(func
  $isValidRuleId
  (param $ruleID i64)
  (result i32)

  (call $i64.isLessOrEqual
    (local.get $ruleID)
    (call $RULE_ID_NUMBER_OF_BITS_UNARY)
  )
)

(func
  $construct_RuleCall_from_rule_id_and_Location
  (param $rule_id i64)
  (param $associatedArgumentBundleNode_Location i64)
  (result i64)

  (local $tag i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $associatedArgumentBundleNode_Location)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $associatedArgumentBundleNode_Location)
        (i64.const 4294967295)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $RULE_ID__BITPOSITION)
        (i64.const 4)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $RULE_ID_NUMBER_OF_BITS_UNARY)
        (i64.const 268435455)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidRuleId
        (local.get $rule_id)
      )
    )
  #endif

  (local.set $tag
    (call $RULECALL_TAG)
  )

  ;;

  (i64.const 0)

  (call $i64.add
    (local.get $tag)
  )

  (call $i64.add
    (call $i64.multiplicate
      (call $RULE_ID_POSITION_BY_MULTIPLICATION)
      (local.get $rule_id)
    )
  )

  (call $i64.add
    (call $i64.multiplicate
      (call $RULECALL__LOCATION_PART_BY_MULTIPLY)
      (local.get $associatedArgumentBundleNode_Location)
    )
  )

)


;; TODO use function from above
(func
  $construct_EncapsulateRuleCall
  (param $associatedArgumentBundleNode_Location i64)
  (result i64)

  (local $tag i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $associatedArgumentBundleNode_Location)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $associatedArgumentBundleNode_Location)
        (i64.const 4294967295)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $RULE_ID__BITPOSITION)
        (i64.const 4)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $RULE_ID_NUMBER_OF_BITS_UNARY)
        (i64.const 268435455)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidRuleId
        (call $MAGIC_RULE_ID_Encapsulate)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $MAGIC_RULE_ID_Encapsulate)
        (i64.const 0)
      )
    )
  #endif

  (local.set $tag
    (call $RULECALL_TAG)
  )

  (call $i64.add
    ;; move location to upper 32 bits
    (call $i64.multiplicate
      (call $RULECALL__LOCATION_PART_BY_MULTIPLY)
      (local.get $associatedArgumentBundleNode_Location)
    )
    (local.get $tag)
  )
)

