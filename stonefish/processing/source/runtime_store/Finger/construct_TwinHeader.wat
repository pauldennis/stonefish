
(func
  $leftTwinVariant
  (result i32)
  (call $TRUE)
)
(func
  $rightTwinVariant
  (result i32)
  (call $FALSE)
)

(func
  $construct_TwinHeader
  (param $isLeftTwin i32)
  (param $duplicationLabel i64)
  (param $associatedDuplicationNodeLocation i64)
  (result i64)

  (local $tag i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidDuplicationLabel
        (local.get $duplicationLabel)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $associatedDuplicationNodeLocation)
        (i64.const 4294967295)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $or
        (call $i32.isEqual
          (call $CANONICAL_TRUE)
          (local.get $isLeftTwin)
        )
        (call $i32.isEqual
          (call $FALSE)
          (local.get $isLeftTwin)
        )
      )
    )
  #endif

  (local.set $tag
    (if
      (result i64)

      (local.get $isLeftTwin)

      (then
        (call $LEFT_TWIN_TAG)
      )
      (else
        (call $RIGHT_TWIN_TAG)
      )
    )
  )

  (call $i64.add
    (call $i64.add
      ;; move location to upper 32 bits
      (call $i64.multiplicate
        (call $TWIN__LOCATION_PART_BY_MULTIPLY)
        (local.get $associatedDuplicationNodeLocation)
      )
      (local.get $tag)
    )
    (call $i64.multiplicate
      (call $TWIN_DUPLICATIONLABEL__POSITION_BY_MULTIPLICATION)
      (local.get $duplicationLabel)
    )
  )
)




(func
  $construct_twinFingers_from_label_and_duplicationNodeLocation
  (param $duplicationLabel i64)
  (param $duplicationNodeLocation i64)

  (result i64)
  (result i64)
  (local $result_leftTwin i64)
  (local $result_rightTwin i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidDuplicationLabel
        (local.get $duplicationLabel)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $duplicationNodeLocation)
      )
    )
  #endif


  ;; TODO reuse function above for that one
  (local.set $result_leftTwin
    (i64.const 0)

    (call $i64.add
      (call $LEFT_TWIN_TAG)
    )

    (call $i64.add
      ;; move duplication label to 0000111111111111111111111111111100000000000000000000000000000000
      (call $i64.multiplicate
        (local.get $duplicationLabel)
        (call $TWIN_DUPLICATIONLABEL__POSITION_BY_MULTIPLICATION)
      )
    )

    (call $i64.add
      ;; move location to upper 32 bits
      (call $i64.multiplicate
        (local.get $duplicationNodeLocation)
        (call $TWIN_ASSOCIATED_DUPLICATION_LOCATION__POSITION_BY_MULTIPLICATION)
      )
    )
  )
  (local.set $result_rightTwin
    (i64.const 0)

    (call $i64.add
      (call $RIGHT_TWIN_TAG)
    )

    (call $i64.add
      ;; move duplication label to 0000111111111111111111111111111100000000000000000000000000000000
      (call $i64.multiplicate
        (local.get $duplicationLabel)
        (call $TWIN_DUPLICATIONLABEL__POSITION_BY_MULTIPLICATION)
      )
    )

    (call $i64.add
      ;; move location to upper 32 bits
      (call $i64.multiplicate
        (local.get $duplicationNodeLocation)
        (call $TWIN_ASSOCIATED_DUPLICATION_LOCATION__POSITION_BY_MULTIPLICATION)
      )
    )
  )

  (local.get $result_leftTwin)
  (local.get $result_rightTwin)
)
