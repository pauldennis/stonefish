
(func
  $construct_IntrinsicNumber_i32
  (param $number i32)
  (result i64)

  (call $i64.add
    ;; move location to upper 32 bits
    (call $i64.multiplicate
      (call $INTRINSIC_NUMBER_PART_BY_MULTIPLY)
      (i64.extend_i32_u
        (local.get $number)
      )
    )
    (call $INTRINSIC_NUMBER_TAG)
  )
)

;; TODO this is actually quite expensive?
  ;; whatever
(func
  $construct_IntrinsicNumber_f32
  (param $number f32)
  (result i64)

  (call $construct_IntrinsicNumber_i32
    (i32.reinterpret_f32
      (local.get $number)
    )
  )
)

