
(func
  $construct_SuperpositionHeader
  (param $duplicationLabel i64)
  (param $superpositionNodeLocation i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidDuplicationLabel
        (local.get $duplicationLabel)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $superpositionNodeLocation)
        (i64.const 4294967295) ;; 1111111111111111111111111111111100000000000000000000000000000000_2
      )
    )
  #endif

  (call $i64.add
    (call $i64.add
      ;; move location to upper 32 bits
      (call $i64.multiplicate
        (call $SUPERPOSITON__LOCATION_PART_BY_MULTIPLY)
        (local.get $superpositionNodeLocation)
      )
      (call $SUPERPOSITION_TAG)
    )
    (call $i64.multiplicate
      (call $TWIN_DUPLICATIONLABEL__POSITION_BY_MULTIPLICATION) ;; TODO clean up usage of these guys
      (local.get $duplicationLabel)
    )
  )
)


