
;; TODO name?, purpose, clean up and make consistent with rest
(func
  $construct_Pointer_by_taking_Header_and_replacing_location_part
  (param $alreadyExistingConstructorHeader i64)
  (param $associatedNodeLocation i64)

  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $hasValidFingerType
        (local.get $alreadyExistingConstructorHeader)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $associatedNodeLocation)
      )
    )
  #endif

  (call $i64.add
    ;; move location to upper 32 bits
    (call $i64.multiplicate
      (local.get $associatedNodeLocation)
      (call $LOCATION_PART_BY_MULTIPLY)
    )
    (i64.extend_i32_u
      (i32.wrap_i64
        (local.get $alreadyExistingConstructorHeader)
      )
    )
  )
)
