;; TODO rename this file to something with leash

(func
  $getMarkerFingerTag
  (param $MarkerFinger i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $hasValidMarkerFingerTag
        (local.get $MarkerFinger)
      )
    )
  #endif

  (i32.wrap_i64
    (local.get $MarkerFinger)
  )
)

(func
  $isThereAVariableOccurence
  (param $leash i64)

  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i32.isLessOrEqual
        (call $i32.bitwiseAnd
          (i32.const 1)
          (i32.wrap_i64
            (local.get $leash)
          )
        )
        (i32.const 1)
      )
    )
  #endif

  (call $i32.isEqual
    (call $MARKER_FINGER_THERE_IS_A_VARIABLE_OCCURENCE)
    (call $getMarkerFingerTag
      (local.get $leash)
    )
  )
)

(func
  $hasValidMarkerFingerTag
  (param $leash_Finger i64)
  (result i32)

  (call $FALSE)

  (call $or
    (call $i32.isEqual
      (i32.wrap_i64
        (local.get $leash_Finger)
      )
      (call $MARKER_FINGER_THERE_IS_A_VARIABLE_OCCURENCE)
    )
  )

  (call $or
    (call $i32.isEqual
      (i32.wrap_i64
        (local.get $leash_Finger)
      )
      (call $MARKER_FINGER_THERE_IS_NO_VARIABLE_OCCURENCE)
    )
  )
)

(func
  $theOccurenceExists
  (param $leash_Finger i64)
  (result i32)

  (call $i32.isEqual
    (i32.wrap_i64
      (local.get $leash_Finger)
    )
    (call $MARKER_FINGER_THERE_IS_A_VARIABLE_OCCURENCE)
  )
)


(func
  $extractTheLocationOfFingerMarker
  (param $finger i64)
  (result i32)

  (local $result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $theOccurenceExists
        (local.get $finger)
      )
    )
  #endif

  (local.set $result
    (call $extractPointingPartOfFinger
      (local.get $finger)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i32.isDevisibleBy8
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)


(func
  $extractTheLocationThatTheFingerMarkerIsPointingTo
  (param $MarkerFinger i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $hasValidMarkerFingerTag
        (local.get $MarkerFinger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $theOccurenceExists
        (local.get $MarkerFinger)
      )
    )
  #endif

  (call $extractTheLocationOfFingerMarker
    (local.get $MarkerFinger)
  )

  (i64.extend_i32_u
    (call $i32.leftover_1)
  )
)
