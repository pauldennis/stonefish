
(func
  $construct_SoloVariableHeader
  (param $associatedLambdaNode i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $associatedLambdaNode)
        (i64.const 4294967295) ;;TODO what is this number?
      )
    )
  #endif

  (call $i64.add
    ;; move location to upper 32 bits
    (call $i64.multiplicate
      (call $SOLO_VARIABLE__LOCATION_PART_BY_MULTIPLY)
      (local.get $associatedLambdaNode)
    )
    (call $SOLO_VARIABLE_TAG)
  )
)

