
(func
  $construct_ApplicationHeader
  (param $applicationNodeLocation i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (local.get $applicationNodeLocation)
        (i64.const 4294967295) ;; 1111111111111111111111111111111100000000000000000000000000000000_2
      )
    )
  #endif

  (call $i64.add
    ;; move location to upper 32 bits
    (call $i64.multiplicate
      (call $APPLICATION__LOCATION_PART_BY_MULTIPLY)
      (local.get $applicationNodeLocation)
    )
    (call $APPLICATION_TAG)
  )
)

