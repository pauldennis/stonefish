;; TODO extract oop style grouping!

(func
  $isValidFingerOffset
  (param $fingerOffset i64)

  (result i32)

  (call $isInBetween_closedIntervall
    (i64.const 0) ;; TODO extract into name?
    (local.get $fingerOffset)
    (call $MAXIMUM_NUMBER_OF_FINGERS_PER_NODE)
  )
)

(func
  $fingerOffset_as_ByteOffset
  (param $fingerOffset i64)

  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFingerOffset
        (local.get $fingerOffset)
      )
    )
  #endif

  (call $i64.multiplicate
    (call $NUMBER_OF_BYTES_PER_FINGER)
    (local.get $fingerOffset)
  )
)

;; TODO rename to get_FingerLocation_from_nodeLocation_and_fingerOffset
(func
  $nodeLocation_and_fingerOffset_to_FingerLocation
  (param $nodeLocation i64)
  (param $fingerOffset i64)

  (result i64)

  (call $i64.add
    (local.get $nodeLocation)
    (call $fingerOffset_as_ByteOffset
      (local.get $fingerOffset)
    )
  )
)

