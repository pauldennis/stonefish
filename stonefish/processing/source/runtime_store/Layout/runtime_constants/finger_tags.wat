
;; TODO generate automatically
(func
  $CONSTRUCTOR_TAG
  (result i64)
  (i64.const 6) ;; "0110"
)
(func
  $LEFT_TWIN_TAG
  (result i64)
  (i64.const 0) ;; "0000"
)
(func
  $RIGHT_TWIN_TAG
  (result i64)
  (i64.const 1) ;; "1000"
)
(func
  $APPLICATION_TAG
  (result i64)
  (i64.const 10) ;; "0101"
)
(func
  $LAMBDA_TAG
  (result i64)
  (i64.const 4) ;; "0010"
)
(func
  $SOLO_VARIABLE_TAG
  (result i64)
  (i64.const 2) ;; "0100"
)
(func
  $INTRINSIC_BINARY_OPERATION_TAG
  (result i64)
  (i64.const 13) ;; "1011"
)
(func
  $INTRINSIC_NUMBER_TAG
  (result i64)
  (i64.const 14) ;; "0111"
)
(func
  $SUPERPOSITION_TAG
  (result i64)
  (i64.const 5) ;; "1010"
)
(func
  $RULECALL_TAG
  (result i64)
  (i64.const 8) ;; "0001"
)
(func
  $OPAQUE_OBJECT_TAG
  (result i64)
  (i64.const 9) ;; "1001"
)

