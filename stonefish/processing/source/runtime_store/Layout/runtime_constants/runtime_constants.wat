;; LayoutProperties

(func
  $NUMBER_OF_BYTES_PER_FINGER
  (result i64)
  (i64.const 8)
)

(func
  $NUMBER_OF_BYTES_PER_NODE
  (result i64)
  (i64.const 64) ;; 8Fingers * $NUMBER_OF_BYTES_PER_FINGER
)

(func
  $MAXIMUM_NUMBER_OF_FINGERS_PER_NODE
  (result i64)
  (i64.const 8)
)

(func
  $TAG_BITPOSITION
  (result i64)
  (i64.const 0)
)

(func
  $TAG_NUMBER_OF_BITS_UNARY
  (result i64)
  (i64.const 15) ;; 1111_2 == 15 == 2^4 - 1
)



;; VariableLeash
;; rename MarkerFinger to VariableLeashe
(func
  $MARKER_FINGER_THERE_IS_A_VARIABLE_OCCURENCE
  (result i32)
  (i32.const 3) ;; "1100****************************"
)
(func
  $MARKER_FINGER_THERE_IS_NO_VARIABLE_OCCURENCE
  (result i32)
  (i32.const 11) ;; "1101****************************"
)
(func
  $MARKER_TAG_UNARY_NUMBER_OF_BITS
  (result i32)
  (i32.const 15) ;; "1111"
)

(func
  $LEASH__FINGER_LOCATION__POSITION_BY_MULTIPLICATION
  (result i64)
  (i64.const 4294967296) ;; 0000000000000000000000000000000010000000000000000000000000000000
)

;; SUBTAGS (used for Arity in constructor id)


(func
  $SUBTAG_BITPOSITION
  (result i64)
  (i64.const 4)
)

(func
  $SUBTAG_NUMBER_OF_BITS_UNARY
  (result i64)
  (i64.const 15) ;; 1111_2 == 15 == 2^4 - 1
)


;; DuplicationLabel

(func
  $DUPLICATION_LABEL__BITPOSITION
  (result i64)
  (i64.const 4)
)

(func
  $DUPLICATION_LABEL__NUMBER_OF_BITS_UNARY
  (result i64)
  (i64.const 268435455) ;; 1111111111111111111111111111
)

(func
  $MAXIMUM_DUPLICATION_LABEL
  (result i64)
  (i64.const 268435455) ;; 1111111111111111111111111111
)

;; ConstrucorIdentification

(func
  $CONSTRUCTOR_ID_BITPOSITION
  (result i64)
  (i64.const 8) ;; 00000000_1
)

(func
  $CONSTRUCTOR_ID_NUMBER_OF_BITS_UNARY
  (result i64)
  (i64.const 16777215) ;; 111111111111111111111111_2 == 16777215 == 2^24 - 1
)


;; RuleIdentification

(func
  $RULE_ID__BITPOSITION
  (result i64)
  (i64.const 4)
)

(func
  $RULE_ID_POSITION_BY_MULTIPLICATION
  (result i64)
  (i64.const 16) ;; 00001_2
)

(func
  $RULE_ID_NUMBER_OF_BITS_UNARY
  (result i64)
  (i64.const 268435455) ;; 1111111111111111111111111111_2 == 268435455 == 2^28 - 1
)


;; Arity

;; TODO remove written out versions and replace by explicit ones
(func
  $ARITY_OF_ZERO
  (result i64)
  (i64.const 0)
)
(func
  $ARITY_OF_ONE
  (result i64)
  (i64.const 1)
)
(func
  $ARITY_OF_TWO
  (result i64)
  (i64.const 2)
)
(func
  $ARITY_OF_THREE
  (result i64)
  (i64.const 3)
)


(func
  $ARITY_OF_0
  (result i64)
  (i64.const 0)
)
(func
  $ARITY_OF_1
  (result i64)
  (i64.const 1)
)
(func
  $ARITY_OF_2
  (result i64)
  (i64.const 2)
)
(func
  $ARITY_OF_3
  (result i64)
  (i64.const 3)
)
(func
  $ARITY_OF_4
  (result i64)
  (i64.const 4)
)
(func
  $ARITY_OF_5
  (result i64)
  (i64.const 5)
)
(func
  $ARITY_OF_6
  (result i64)
  (i64.const 6)
)
(func
  $ARITY_OF_7
  (result i64)
  (i64.const 7)
)
(func
  $ARITY_OF_8
  (result i64)
  (i64.const 8)
)

(func
  $MINIMUM_OF_ARITY
  (result i64)
  (call $ARITY_OF_ZERO)
)
(func
  $MAXIMUM_OF_ARITY
  (result i64)
  (i64.const 8)
)






;; Stack

(func
  $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION
  (result i64)
  (i64.const 8)
)



;; duplicationLabel

(func
  $TWIN_DUPLICATIONLABEL__POSITION_BY_MULTIPLICATION
  (result i64)
  (i64.const 16) ;; 0000100000000000000000000000000000000000000000000000000000000000
)

(func
  $TWIN_ASSOCIATED_DUPLICATION_LOCATION__POSITION_BY_MULTIPLICATION
  (result i64)
  (i64.const 4294967296) ;; 0000000000000000000000000000000010000000000000000000000000000000
)


;; location part of various Headers

(func
  $LOCATION_PART_BY_MULTIPLY
  (result i64)
  (i64.const 4294967296)
)



(func
  $INTRINSIC_NUMBER_PART_BY_MULTIPLY
  (result i64)
  (i64.const 4294967296)
)
(func
  $SUPERPOSITON__LOCATION_PART_BY_MULTIPLY
  (result i64)
  (i64.const 4294967296)
)
(func
  $APPLICATION__LOCATION_PART_BY_MULTIPLY
  (result i64)
  (i64.const 4294967296)
)
(func
  $TWIN__LOCATION_PART_BY_MULTIPLY
  (result i64)
  (i64.const 4294967296)
)
(func
  $RULECALL__LOCATION_PART_BY_MULTIPLY
  (result i64)
  (i64.const 4294967296)
)
(func
  $SOLO_VARIABLE__LOCATION_PART_BY_MULTIPLY
  (result i64)
  (i64.const 4294967296)
)
(func
  $CONSTRUCTOR__LOCATION_PART_BY_MULTIPLY
  (result i64)
  (i64.const 4294967296)
)


;;


(func
  $upperBoundDuplicationlabel_exclusive
  (result i64)
  (i64.const 268435456) ;; 134217728 == 2^27
)
