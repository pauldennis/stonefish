(func
  $i64.isDevisibleByFingerByteSize
  (param $index i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $NUMBER_OF_BYTES_PER_FINGER)
        (i64.const 8)
      )
    )
  #endif

  (call $i64.isDevisibleBy8
    (local.get $index)
  )
)
(func
  $TEST_isDevisibleByFingerByteSize

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleByFingerByteSize
        (i64.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleByFingerByteSize
          (i64.const 4)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleByFingerByteSize
        (i64.const 8)
      )
    )
  #endif
)


(func
  $i64.isDevisibleByNodeByteSize
  (param $index i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $NUMBER_OF_BYTES_PER_NODE)
        (i64.const 64)
      )
    )
  #endif

  ;; TODO refactor isDevisibleByN out of isDevisibleBy64
  (call $i64.isDevisibleBy64
    (local.get $index)
  )
)
(func
  $TEST_isDevisibleByNodeByteSize

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleByNodeByteSize
        (i64.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleByNodeByteSize
          (i64.const 50)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleByNodeByteSize
        (i64.const 64)
      )
    )
  #endif
)

(func
  $TEST_isDevisibleByElementSize
  (call $TEST_isDevisibleByFingerByteSize)
  (call $TEST_isDevisibleByNodeByteSize)
)
