
;; TODO rename to twinLeash
(func
  $DUPLICATION_NODE__POSITION_OF_LEFT_TWIN_LEASH
  (result i64)
  (i64.const 0)
)
(func
  $DUPLICATION_NODE__POSITION_OF_RIGHT_TWIN_LEASH
  (result i64)
  (i64.const 1)
)
(func
  $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY
  (result i64)
  (i64.const 2)
)


(func
  $APPLICATION_NODE__POSITION_OF_TOBEAPPLIED
  (result i64)
  (i64.const 0)
)
(func
  $APPLICATION_NODE__POSITION_OF_THEARGUMENT
  (result i64)
  (i64.const 1)
)


;; TODO rename markerpointer to leash
(func
  $LAMBDA_NODE__POSITION_OF_OCCURENCE_MARKER_POINTER
  (result i64)
  (i64.const 0)
)
(func
  $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY
  (result i64)
  (i64.const 1)
)


(func
  $INTRINSIC_BINARY_OPERATION__POSITION_OF_LEFT_OPERANT
  (result i64)
  (i64.const 0)
)
(func
  $INTRINSIC_BINARY_OPERATION__POSITION_OF_RIGHT_OPERANT
  (result i64)
  (i64.const 1)
)


(func
  $SUPERPOSITION__POSITION_OF_LEFT_SUPERIMPOSED_TERM
  (result i64)
  (i64.const 0)
)
(func
  $SUPERPOSITION__POSITION_OF_RIGHT_SUPERIMPOSED_TERM
  (result i64)
  (i64.const 1)
)


(func
  $CAPSULE__POSITION_OF_PRIVATE_DATA
  (result i64)
  (i64.const 0)
)
(func
  $CAPSULE__POSITION_OF_INTERFACE
  (result i64)
  (i64.const 1)
)


;; TODO wrong place? better be autogenerated?
(func
  $ReturnNewPrivateDataAndSystemRequest__POSITION_OF_UPDATED_PRIVATE_DATA
  (result i64)
  (i64.const 0)
)
(func
  $ReturnNewPrivateDataAndSystemRequest__POSITION_OF_CAPSULE_CALL_RESULT
  (result i64)
  (i64.const 1)
)


;; TODO positions are in wrong order
;; TODO or is it not?
;; should probably same as Haskell State Monad.
;; What is better: (s -> (s, a)) or (s -> (a, s))
(func
  $CapsuleCallResult__POSITION_OF_MODIFIED_CAPSULE
  (result i64)
  (i64.const 0)
)
(func
  $CapsuleCallResult__POSITION_OF_CAPSULE_APPLICATION_RESULT
  (result i64)
  (i64.const 1)
)
