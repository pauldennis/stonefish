;; TODO rename file to FreeSlotsStack

(func
  $TEST_FreeSlotStack

  (call $TEST_STACK_UNUSED_MARKING_not_a_valid_heap_index)
;;   (call $TEST_free_slots_test)
)

(func
  $wouldPushBeAllowed
  (param $itemToBePushed i64)

  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $itemToBePushed)
      )
    )
  #endif

  ;; prevent double free
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $hasStackThisElement
          (local.get $itemToBePushed)
        )
      )
    )
  #endif

  (call $assert_would_stack_push_be_valid
    (local.get $itemToBePushed)
  )

  (call $TRUE)
)

(func
  $freeSlotsStack_push
  (param $itemToBePushed i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $wouldPushBeAllowed
        (local.get $itemToBePushed)
      )
    )
  #endif

  (call $stack_push
    (local.get $itemToBePushed)
  )
)


(func
  $freeSlotsStack_pop
  (result i64)

  (local $result i64)

  (local.set $result
     (call $stack_pop)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)


(func
  $TEST_STACK_UNUSED_MARKING_not_a_valid_heap_index

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isValidNodeLocationIndex
          (call $STACK_UNUSED_MARKING)
        )
      )
    )
  #endif
)


(func
  $TEST_free_slots_test

  (call $initialize_stack
    (i64.const 0)
    (i64.const 2)
  )
  (call $initialize_heap
    (i64.const 64)
    (i64.const 2)
  )

  (call $freeSlotsStack_push
    (i64.const 64)
  )
  (drop
    (call $freeSlotsStack_pop
    )
  )

  (call $freeSlotsStack_push
    (i64.const 64)
  )
  (call $freeSlotsStack_push
    (i64.const 128)
  )
  (drop
    (call $freeSlotsStack_pop
    )
  )
  (drop
    (call $freeSlotsStack_pop
    )
  )
)


(func
  $TRIGGER_ASSERT_TEST_no_double_free

  (call $initialize_stack
    (i64.const 0)
    (i64.const 2)
  )
  (call $initialize_heap
    (i64.const 64)
    (i64.const 2)
  )

  (call $freeSlotsStack_push
    (i64.const 64)
  )
  (drop
    (call $freeSlotsStack_pop
    )
  )

  (call $freeSlotsStack_push
    (i64.const 64)
  )

  (call $freeSlotsStack_push
    (i64.const 64)
  )
)

