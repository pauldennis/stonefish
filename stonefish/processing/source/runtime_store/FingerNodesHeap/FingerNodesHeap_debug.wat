
;; TODO different place, do with shadow bytes whem multiple memory comes available
(func
  $markUnusedFingersOfConstructorAsUnused
  (param $constructorHeader i64)

  (local $arity i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $constructorHeader)
      )
    )
  #endif

  (local.set $arity
    (call $extractArityFromConstructorFinger
      (local.get $constructorHeader)
    )
  )

  ;; TODO generalize this function for all arities
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (local.get $arity)
      )
    )
  #endif

  ;; TODO make a loop
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 2)
      (local.get $constructorHeader)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 3)
      (local.get $constructorHeader)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 4)
      (local.get $constructorHeader)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 5)
      (local.get $constructorHeader)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 6)
      (local.get $constructorHeader)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 7)
      (local.get $constructorHeader)
    )
  )

)
