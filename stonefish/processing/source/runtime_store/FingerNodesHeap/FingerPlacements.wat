
;; modificaions
;; TODO Where should this code be located?

;; TODO now there are two of these functions

(func
  $replace_at_NodeLocation_and_SubFingerOffset_with_Finger
  (param $nodeLocation i64)
  (param $subFingerIndex i64)
  (param $FingerReplacement i64)

  (local $subfinger_FingerLocation i64)

  (local.set $subfinger_FingerLocation
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $nodeLocation)
      (local.get $subFingerIndex)
    )
  )

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (local.get $subfinger_FingerLocation)
    (local.get $FingerReplacement)
  )
)




(func
  $raw_place_something_in_FingerNodeHeap
  (param $node_Location i64)
  (param $finger_POSITION_in_Node i64)

  (param $finger_to_be_placed i64)

  (local $fingerLocation i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
        (call $isValidFingerOffset
          (local.get $finger_POSITION_in_Node)
        )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
        (call $isValidNodeLocationIndex
          (local.get $node_Location)
        )
    )
  #endif

  (local.set $fingerLocation
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (local.get $finger_POSITION_in_Node)
    )
  )

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (local.get $fingerLocation)
    (local.get $finger_to_be_placed)
  )
)


(func
  $place_a_NON_VARIABLE_finger_in_FingerNodeHeap

  (param $node_Location i64)
  (param $finger_POSITION_in_Node i64)

  (param $finger_to_be_placed i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $finger_to_be_placed)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $finger_to_be_placed)
        )
      )
    )
  #endif

  (call $raw_place_something_in_FingerNodeHeap
    (local.get $node_Location)
    (local.get $finger_POSITION_in_Node)
    (local.get $finger_to_be_placed)
  )
)



;; this is the convinience function and when placing alien Headers
;; when the graph gets restructured by intrinsic rewrite rules it may be beneficial to do the backreference manually
;; TODO rename to correct argument order
;; TODO rename to place_finger_at_Node_Location_and_subFingerIndex_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
(func
  $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference

  (param $node_Location i64)
  (param $finger_POSITION_in_Node i64)

  (param $finger_to_be_placed i64)

  (local $fingerLocation i64)
  (local $constructed_leash i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
        (call $isValidFingerOffset
          (local.get $finger_POSITION_in_Node)
        )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
        (call $isValidNodeLocationIndex
          (local.get $node_Location)
        )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $finger_to_be_placed)
      )
    )
  #endif

  (local.set $fingerLocation
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (local.get $finger_POSITION_in_Node)
    )
  )

  (local.set $constructed_leash
    (call $construct_leash_from_fingerLocation
      (local.get $fingerLocation)
    )
  )


  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (local.get $fingerLocation)
    (local.get $finger_to_be_placed)
  )

  (call $maybeInstallNewLeashIfVariable
    (local.get $constructed_leash)
    (local.get $finger_to_be_placed)
  )

)
