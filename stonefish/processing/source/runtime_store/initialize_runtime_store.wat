
(func
  $initialize_runtime_store

  (local $item_count i64)

  (local $stack_begin i64)
  (local $heap_begin i64)

  ;; TODO SYNC 513d9d715c69ba152f9d289ea8e46eacf8c3b958bbed45a7aa5c39f480461a89e5590cd02de3022918515c8205565eb78bf76f96b432db9d9a57cbdd10516dfc
    ;; TODO where is this place?
  ;; TODO only have one place for it
  (local.set $item_count
;;     (i64.const 64)
;;     (i64.const 128)
;;     (i64.const 256)
    (i64.const 512)
  )


  ;; TODO keep first places of heap empty inorder to turn on this optimization in wasm-opt
  (local.set $stack_begin
    (i64.const 0)
  )

  (local.set $heap_begin
    (call $i64.multiplicate
      (call $NUMBER_OF_BYTES_FOR_HEAP_INDEX_THAT_IS_A_LOCATION)
      (local.get $item_count)
    )
  )
  ;; TODO calc next possible heap begin
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleByNodeByteSize
        (local.get $heap_begin)
      )
    )
  #endif


  ;; TODO compare store dimensions with memory size




  (call $initialize_stack
    (local.get $stack_begin)
    (local.get $item_count)
  )

  (call $initialize_heap
    (local.get $heap_begin)
    (local.get $item_count)
  )

  (call $relinquish_from_to_N_many
    (local.get $heap_begin)
    (local.get $item_count)
  )

)
