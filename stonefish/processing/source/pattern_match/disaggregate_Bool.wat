(func
  $isFingerABool
  (param $wanneBeBool i64)

  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID__False_Arity)
        (call $ARITY_OF_ZERO)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID__True_Arity)
        (call $ARITY_OF_ZERO)
      )
    )
  #endif

  (call $TRUE)

  (call $and
    (call $isFingerAConstructor
      (local.get $wanneBeBool)
    )
  )

  (call $and
    (call $i64.isEqual
      (call $extractArityFromConstructorFinger
        (local.get $wanneBeBool)
      )
      (call $ARITY_OF_ZERO)
    )
  )

  (call $and
    (call $or
      (call $i64.isEqual
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $wanneBeBool)
        )
        (call $CONSTRUCTOR_ID__False)
      )
      (call $i64.isEqual
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $wanneBeBool)
        )
        (call $CONSTRUCTOR_ID__True)
      )
    )
  )
)

(func
  $disaggregate_Bool
  (param $bool_finger i64)

  (result i32)

  (local $constructorId i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerABool
        (local.get $bool_finger)
      )
    )
  #endif

  (local.set $constructorId
    (call $extractConstructorId_from_ConstructorFinger
      (local.get $bool_finger)
    )
  )

  (call $i64.isEqual
    (call $CONSTRUCTOR_ID__True)
    (local.get $constructorId)
  )

  ;; no relinquish as there is no node
)

