(func
  $disaggregate_Constructor
  (param $ConstructorNode i64)

  (result i64)
  (result i64)

  (result i64)
  (result i64)
  (result i64)
  (result i64)
  (result i64)
  (result i64)
  (result i64)
  (result i64)

  (local $result_constructor_id i64)
  (local $result_constructor_arity i64)

  (local $result_x0 i64)
  (local $result_x1 i64)
  (local $result_x2 i64)
  (local $result_x3 i64)
  (local $result_x4 i64)
  (local $result_x5 i64)
  (local $result_x6 i64)
  (local $result_x7 i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $ConstructorNode)
      )
    )
  #endif

  (local.set $result_constructor_id
    (call $extractConstructorId_from_ConstructorFinger
      (local.get $ConstructorNode)
    )
  )

  (local.set $result_constructor_arity
    (call $extractArityFromConstructorFinger
      (local.get $ConstructorNode)
    )
  )

  ;; skip $n when arity is n
  (block $0
  (block $1
  (block $2
  (block $3
  (block $4
  (block $5
  (block $6
  (block $7
  (block $8

    (if (call $i64.isEqual (i64.const 8) (local.get $result_constructor_arity)) (then br $8))
    (local.set $result_x7 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 7) (local.get $result_constructor_arity)) (then br $7))
    (local.set $result_x6 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 6) (local.get $result_constructor_arity)) (then br $6))
    (local.set $result_x5 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 5) (local.get $result_constructor_arity)) (then br $5))
    (local.set $result_x4 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 4) (local.get $result_constructor_arity)) (then br $4))
    (local.set $result_x3 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 3) (local.get $result_constructor_arity)) (then br $3))
    (local.set $result_x2 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 2) (local.get $result_constructor_arity)) (then br $2))
    (local.set $result_x1 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 1) (local.get $result_constructor_arity)) (then br $1))
    (local.set $result_x0 (call $HEAP_UNUSED_MARKING)) ;; TODO only in DEBUG
    (if (call $i64.isEqual (i64.const 0) (local.get $result_constructor_arity)) (then br $0))

  )
    (local.set $result_x7
      (call $getSubFinger_from_birthnumber_and_finger
        (i64.const 7)
        (local.get $ConstructorNode)
      )
    )
  )
    (local.set $result_x6
      (call $getSubFinger_from_birthnumber_and_finger
        (i64.const 6)
        (local.get $ConstructorNode)
      )
    )
  )
    (local.set $result_x5
      (call $getSubFinger_from_birthnumber_and_finger
        (i64.const 5)
        (local.get $ConstructorNode)
      )
    )
  )
    (local.set $result_x4
      (call $getSubFinger_from_birthnumber_and_finger
        (i64.const 4)
        (local.get $ConstructorNode)
      )
    )
  )
    (local.set $result_x3
      (call $getSubFinger_from_birthnumber_and_finger
        (i64.const 3)
        (local.get $ConstructorNode)
      )
    )
  )
    (local.set $result_x2
      (call $getSubFinger_from_birthnumber_and_finger
        (i64.const 2)
        (local.get $ConstructorNode)
      )
    )
  )
    (local.set $result_x1
      (call $getSubFinger_from_birthnumber_and_finger
        (i64.const 1)
        (local.get $ConstructorNode)
      )
    )
  )
    (local.set $result_x0
      (call $getSubFinger_from_birthnumber_and_finger
        (i64.const 0)
        (local.get $ConstructorNode)
      )
    )
    #if STONEFISH_SHOULD_INCLUDE_ASSERT
      (call $assert
        (call $not
          (call $i64.isEqual
            (i64.const 0)
            (local.get $result_constructor_arity)
          )
        )
      )
    #endif
    (call $relinquish_Node_behind_Finger
      (local.get $ConstructorNode)
    )
  )

  ;; NOTE when the constructor is a leaf then there is no node that is to be relinquished!

  (local.get $result_constructor_id)
  (local.get $result_constructor_arity)
  (local.get $result_x0)
  (local.get $result_x1)
  (local.get $result_x2)
  (local.get $result_x3)
  (local.get $result_x4)
  (local.get $result_x5)
  (local.get $result_x6)
  (local.get $result_x7)
)

