

(func
  $disaggregate_ConstructorLeaf
  (param $constructor_leaf_finger i64)

  (result i64)

  (local $constructorId i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructorLeaf
        (local.get $constructor_leaf_finger)
      )
    )
  #endif

  (local.set $constructorId
    (call $extractConstructorId_from_ConstructorFinger
      (local.get $constructor_leaf_finger)
    )
  )

  ;; no relinquish as there is no node
  ;; no returned fingers as there is no node
  (local.get $constructorId)
)

