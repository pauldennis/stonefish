;; TODO rename filename from parse to disaggregate


(func
  $isType_CapsuleCallResult
  (param $Header i64)

  (result i32)

  (call $i64.isEqual
    (call $CONSTRUCTOR_ID__CapsuleCallResult)
    (call $extractConstructorId_from_ConstructorFinger
      (local.get $Header)
    )
  )
)


;; TODO type checking of usage of these functions?
(func
  $disaggregate_CapsuleCallResult
  (param $Header i64)

  (result i64)
  (result i64)
  (local $result_modified_capsule i64)
  (local $result_interface_call_result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isType_CapsuleCallResult
        (local.get $Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $Header)
        )
      )
    )
  #endif

  ;; TODO order of the positions is reversed
  ;; TODO or is it not?

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessThan
        (call $CapsuleCallResult__POSITION_OF_MODIFIED_CAPSULE)
        (call $CapsuleCallResult__POSITION_OF_CAPSULE_APPLICATION_RESULT)
      )
    )
  #endif

  (local.set $result_modified_capsule
    (call $getSubFinger_from_birthnumber_and_finger
      (call $CapsuleCallResult__POSITION_OF_MODIFIED_CAPSULE)
      (local.get $Header)
    )
  )
  (local.set $result_interface_call_result
    (call $getSubFinger_from_birthnumber_and_finger
      (call $CapsuleCallResult__POSITION_OF_CAPSULE_APPLICATION_RESULT)
      (local.get $Header)
    )
  )

  ;; TODO there needs to be a concept how these things should be done
  (local.set $result_interface_call_result
    (call $evaluate_to_weak_head_normal_form
      (local.get $result_interface_call_result)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $result_modified_capsule)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $result_interface_call_result)
        )
      )
    )
  #endif

  (call $relinquish_Node_behind_Finger
    (local.get $Header)
  )

  (local.get $result_modified_capsule)
  (local.get $result_interface_call_result)
)

