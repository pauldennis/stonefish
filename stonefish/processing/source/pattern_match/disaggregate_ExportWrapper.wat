(func
  $isFingerAnExportWrapper
  (param $wanneBeWrapper i64)

  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID__ExportedTerm_Arity)
        (i64.const 1)
      )
    )
  #endif

  (call $TRUE)

  (call $and
    (call $isFingerAConstructor
      (local.get $wanneBeWrapper)
    )
  )

  (call $and
    (call $i64.isEqual
      (call $extractArityFromConstructorFinger
        (local.get $wanneBeWrapper)
      )
      (call $CONSTRUCTOR_ID__ExportedTerm_Arity)
    )
  )

  (call $and
    (call $i64.isEqual
      (call $extractConstructorId_from_ConstructorFinger
        (local.get $wanneBeWrapper)
      )
      (call $CONSTRUCTOR_ID__ExportedTerm_ID)
    )
  )
)





(func
  $disaggregate_ExportWrapper
  (param $exportWrapper_Header i64)

  (result i64)

  (local $result_Finger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnExportWrapper
        (local.get $exportWrapper_Header)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_TOOLS_HEAPDUMP
    (call $debug.deregister_wrapper
      (local.get $exportWrapper_Header)
    )
  #endif

  (local.set $result_Finger
    (call $getSubFinger_from_birthnumber_and_finger
      (call $CONSTRUCTOR_ID__ExportedTerm__TheTerm)
      (local.get $exportWrapper_Header)
    )
  )

  (call $relinquish_Node_behind_Finger
    (local.get $exportWrapper_Header)
  )

  (local.get $result_Finger)
)
