
;; TODO this function is superfluous
(func
  $isType_IntrinsiscNumber
  (param $Finger i64)

  (result i32)

  (call $isFingerAnIntrinsiscNumber
    (local.get $Finger)
  )

)




(func
  $f32.disaggregate_IntrinsicNumber
  (param $Header i64)

  (result f32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isType_IntrinsiscNumber
        (local.get $Header)
      )
    )
  #endif

  (call $f32.extract_InFingerPayload_32bit
    (local.get $Header)
  )

)
