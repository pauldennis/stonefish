(func
  $disaggregate_Superposition
  (param $superposition_Header i64)

  (result i64)
  (result i64)
  (result i64)
  (local $duplicationLabel i64)
  (local $leftSuperImposed_Header i64)
  (local $rightSuperImposed_Header i64)

  ;; TODO extract into superpositionParser
  (local.set $duplicationLabel
    (call $extract_DuplicationLabel_fromSuperpositionHeader
      (local.get $superposition_Header)
    )
  )
  (local.set $leftSuperImposed_Header
    (call $getLeftSuperimposedHeader
      (local.get $superposition_Header)
    )
  )
  (local.set $rightSuperImposed_Header
    (call $getRightSuperimposedHeader
      (local.get $superposition_Header)
    )
  )

  ;; TODO correct here?
  (call $relinquish_Node_behind_Finger
    (local.get $superposition_Header)
  )

  (local.get $duplicationLabel)
  (local.get $leftSuperImposed_Header)
  (local.get $rightSuperImposed_Header)
)

