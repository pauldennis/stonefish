(func
  $isType_GenieAndBool
  (param $Header i64)

  (result i32)

  (call $TRUE)

  (call $and
    (call $isFingerAConstructor
      (local.get $Header)
    )
  )

  (call $and
    (call $isFingerAConstructor
      (local.get $Header)
    )
  )

  (call $and
    (call $i64.isEqual
      (call $CONSTRUCTOR_GenieAndBool_ID)
      (call $extractConstructorId_from_ConstructorFinger
        (local.get $Header)
      )
    )
  )

  (call $and
    (call $i64.isEqual
      (call $ARITY_OF_TWO)
      (call $extractArityFromConstructorFinger
        (local.get $Header)
      )
    )
  )
)




(func
  $disaggregate_GenieAndBool
  (param $Header i64)

  (result i64)
  (result i64)
  (local $result_x0 i64)
  (local $result_x1 i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isType_GenieAndBool
        (local.get $Header)
      )
    )
  #endif

  (local.set $result_x0
    (call $getSubFinger_from_birthnumber_and_finger
      (call $CONSTRUCTOR_GenieAndBool__genie)
      (local.get $Header)
    )
  )
  (local.set $result_x1
    (call $getSubFinger_from_birthnumber_and_finger
      (call $CONSTRUCTOR_GenieAndBool__bool)
      (local.get $Header)
    )
  )

  (call $relinquish_Node_behind_Finger
    (local.get $Header)
  )

  (local.get $result_x0)
  (local.get $result_x1)
)

