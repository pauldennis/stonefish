module ExtractLineNumberForUsabilityHack where

import System.IO
import System.Process
import Data.List

import Debug.Trace

main :: IO ()
main = do
  content <- readFile "wat2wasm_invokation_file_output"

  let output = extract content

  putStr $ output


extract
  chars
  = id
  $ takeWhile (/= ':')
  $ (\(':':xs) -> xs)
  $ dropWhile (/= ':')
  $ chars
