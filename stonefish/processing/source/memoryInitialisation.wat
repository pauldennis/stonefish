(func $MAGIC_RULE_ID_Encapsulate
  (result i64)
  (i64.const 0)
)
(func $MAGIC_RULE_ARITY_Encapsulate
  (result i64)
  (i64.const 2)
)

(func $MAGIC_RULE_ID_If
  (result i64)
  (i64.const 1)
)
(func $MAGIC_RULE_ARITY_If
  (result i64)
  (i64.const 3)
)

(func $MAGIC_RULE_ID_DropLeftNullaryConstructorArgumentAndReturnRightArgument
  (result i64)
  (i64.const 2)
)
(func $MAGIC_RULE_ARITY_DropLeftNullaryConstructorArgumentAndReturnRightArgument
  (result i64)
  (i64.const 2)
)

(func $MAGIC_RULE_ID_Float32_Square_RoundingDown
  (result i64)
  (i64.const 3)
)
(func $MAGIC_RULE_ARITY_Float32_Square_RoundingDown
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Float32_Addition_RoundingDown
  (result i64)
  (i64.const 4)
)
(func $MAGIC_RULE_ARITY_Float32_Addition_RoundingDown
  (result i64)
  (i64.const 2)
)

(func $MAGIC_RULE_ID_Float32_IsLessThan
  (result i64)
  (i64.const 5)
)
(func $MAGIC_RULE_ARITY_Float32_IsLessThan
  (result i64)
  (i64.const 2)
)

(func $MAGIC_RULE_ID_BoolNeedsToBeSetTo
  (result i64)
  (i64.const 6)
)
(func $MAGIC_RULE_ARITY_BoolNeedsToBeSetTo
  (result i64)
  (i64.const 2)
)

(func $MAGIC_RULE_ID_Float32_Multiplicate_RoundingDown
  (result i64)
  (i64.const 7)
)
(func $MAGIC_RULE_ARITY_Float32_Multiplicate_RoundingDown
  (result i64)
  (i64.const 2)
)

(func $MAGIC_RULE_ID_Float32_Negate
  (result i64)
  (i64.const 8)
)
(func $MAGIC_RULE_ARITY_Float32_Negate
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Float32_Reciprocal_RoundingDown
  (result i64)
  (i64.const 9)
)
(func $MAGIC_RULE_ARITY_Float32_Reciprocal_RoundingDown
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Float32_SquareRoot_RoundingDown
  (result i64)
  (i64.const 10)
)
(func $MAGIC_RULE_ARITY_Float32_SquareRoot_RoundingDown
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder5
  (result i64)
  (i64.const 11)
)
(func $MAGIC_RULE_ARITY_place_holder5
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder6
  (result i64)
  (i64.const 12)
)
(func $MAGIC_RULE_ARITY_place_holder6
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder7
  (result i64)
  (i64.const 13)
)
(func $MAGIC_RULE_ARITY_place_holder7
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder8
  (result i64)
  (i64.const 14)
)
(func $MAGIC_RULE_ARITY_place_holder8
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder9
  (result i64)
  (i64.const 15)
)
(func $MAGIC_RULE_ARITY_place_holder9
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder10
  (result i64)
  (i64.const 16)
)
(func $MAGIC_RULE_ARITY_place_holder10
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder11
  (result i64)
  (i64.const 17)
)
(func $MAGIC_RULE_ARITY_place_holder11
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder12
  (result i64)
  (i64.const 18)
)
(func $MAGIC_RULE_ARITY_place_holder12
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_place_holder13
  (result i64)
  (i64.const 19)
)
(func $MAGIC_RULE_ARITY_place_holder13
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_WebSocketMessageTrigger
  (result i64)
  (i64.const 20)
)
(func $MAGIC_RULE_ARITY_WebSocketMessageTrigger
  (result i64)
  (i64.const 2)
)

(func $MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule
  (result i64)
  (i64.const 21)
)
(func $MAGIC_RULE_ARITY_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule
  (result i64)
  (i64.const 2)
)

(func $MAGIC_RULE_ID_IsWithinUnitBall
  (result i64)
  (i64.const 22)
)
(func $MAGIC_RULE_ARITY_IsWithinUnitBall
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_MatchTwoPrivateMembers
  (result i64)
  (i64.const 23)
)
(func $MAGIC_RULE_ARITY_MatchTwoPrivateMembers
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_MatchTwoCapsuleCallResult
  (result i64)
  (i64.const 24)
)
(func $MAGIC_RULE_ARITY_MatchTwoCapsuleCallResult
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_InterfaceDispatch
  (result i64)
  (i64.const 25)
)
(func $MAGIC_RULE_ARITY_InterfaceDispatch
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_MatchTwoPrivateMembers_forSetTrue
  (result i64)
  (i64.const 26)
)
(func $MAGIC_RULE_ARITY_MatchTwoPrivateMembers_forSetTrue
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_MatchTwoPrivateMembers_forSetFalse
  (result i64)
  (i64.const 27)
)
(func $MAGIC_RULE_ARITY_MatchTwoPrivateMembers_forSetFalse
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Add_floor
  (result i64)
  (i64.const 28)
)
(func $MAGIC_RULE_ARITY_Vector3_Add_floor
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Add_floor_MatchSecondParameter
  (result i64)
  (i64.const 29)
)
(func $MAGIC_RULE_ARITY_Vector3_Add_floor_MatchSecondParameter
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Negate
  (result i64)
  (i64.const 30)
)
(func $MAGIC_RULE_ARITY_Vector3_Negate
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Subtract_floor
  (result i64)
  (i64.const 31)
)
(func $MAGIC_RULE_ARITY_Vector3_Subtract_floor
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Halfe
  (result i64)
  (i64.const 32)
)
(func $MAGIC_RULE_ARITY_Vector3_Halfe
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Midpoint_floor
  (result i64)
  (i64.const 33)
)
(func $MAGIC_RULE_ARITY_Vector3_Midpoint_floor
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Length_floor
  (result i64)
  (i64.const 34)
)
(func $MAGIC_RULE_ARITY_Vector3_Length_floor
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Distance_floor
  (result i64)
  (i64.const 35)
)
(func $MAGIC_RULE_ARITY_Vector3_Distance_floor
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Scale_flipped
  (result i64)
  (i64.const 36)
)
(func $MAGIC_RULE_ARITY_Vector3_Scale_flipped
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_Normalize_floor
  (result i64)
  (i64.const 37)
)
(func $MAGIC_RULE_ARITY_Vector3_Normalize_floor
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_CrossProduct_floor
  (result i64)
  (i64.const 38)
)
(func $MAGIC_RULE_ARITY_Vector3_CrossProduct_floor
  (result i64)
  (i64.const 1)
)

(func $MAGIC_RULE_ID_Vector3_CrossProduct_floor_MatchSecondParameter
  (result i64)
  (i64.const 39)
)
(func $MAGIC_RULE_ARITY_Vector3_CrossProduct_floor_MatchSecondParameter
  (result i64)
  (i64.const 1)
)


(func $init_FreeNodes_Stack
  (call $initialize_stack
    (i64.const 0)
    (i64.const 512)
  )
)

(func $init_NodeHeap_Array
  (call $initialize_heap
    (i64.const 4096)
    (i64.const 512)
  )
)

(func $overwrite_object_initialFragment
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044420)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 19791209299971) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 17867063951364) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 19516331393027) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18966575579139) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819765336) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 18691697672264) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 4575657221408423950) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 19241453486152) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 18966575579192) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4416) ;; position: (4416 == 64 + 544 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4424) ;; position: (4424 == 64 + 545 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4432) ;; position: (4432 == 64 + 546 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4440) ;; position: (4440 == 64 + 547 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4448) ;; position: (4448 == 64 + 548 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4456) ;; position: (4456 == 64 + 549 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4464) ;; position: (4464 == 64 + 550 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4472) ;; position: (4472 == 64 + 551 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4480) ;; position: (4480 == 64 + 552 * 8 + 0)
    (i64.const 19791209300024) ;; value
  )
  (i64.store
    (i32.const 4488) ;; position: (4488 == 64 + 553 * 8 + 0)
    (i64.const 19516331393080) ;; value
  )
  (i64.store
    (i32.const 4496) ;; position: (4496 == 64 + 554 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4504) ;; position: (4504 == 64 + 555 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4512) ;; position: (4512 == 64 + 556 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4520) ;; position: (4520 == 64 + 557 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4528) ;; position: (4528 == 64 + 558 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4536) ;; position: (4536 == 64 + 559 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4544) ;; position: (4544 == 64 + 560 * 8 + 0)
    (i64.const 17867063951362) ;; value
  )
  (i64.store
    (i32.const 4552) ;; position: (4552 == 64 + 561 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4560) ;; position: (4560 == 64 + 562 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4568) ;; position: (4568 == 64 + 563 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4576) ;; position: (4576 == 64 + 564 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4584) ;; position: (4584 == 64 + 565 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4592) ;; position: (4592 == 64 + 566 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4600) ;; position: (4600 == 64 + 567 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4608) ;; position: (4608 == 64 + 568 * 8 + 0)
    (i64.const 17592186044418) ;; value
  )
  (i64.store
    (i32.const 4616) ;; position: (4616 == 64 + 569 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4624) ;; position: (4624 == 64 + 570 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4632) ;; position: (4632 == 64 + 571 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4640) ;; position: (4640 == 64 + 572 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4648) ;; position: (4648 == 64 + 573 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4656) ;; position: (4656 == 64 + 574 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4664) ;; position: (4664 == 64 + 575 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4672)
    (i64.const 503)
  )
  
  (local.get $result)
)

(func $overwrite_object_LeafConstructor
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 2822)
  )
  
  
  (call $relinquish_from_to_N_many
    (i64.const 4096)
    (i64.const 512)
  )
  
  (local.get $result)
)

(func $overwrite_object_OneTimeDubplicate
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17867063951910)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 17867063951363) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 17901423689731) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 2822) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 17592186044416) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 17592186044417) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4224)
    (i64.const 510)
  )
  
  (local.get $result)
)

(func $overwrite_object_IdentityLambda
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044426)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 17867063951364) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 2822) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 17901423689731) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 17867063951362) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4224)
    (i64.const 510)
  )
  
  (local.get $result)
)

(func $overwrite_object_NestedLambda
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044426)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 17867063951370) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 3846) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 3590) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18691697672195) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819765252) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 18726057410563) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18691697672742) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 18416819765250) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4416)
    (i64.const 507)
  )
  
  (local.get $result)
)

(func $overwrite_object_MultipleNestedLambdas
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044426)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 17867063951370) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 3590) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 18141941858314) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 3846) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18416819765258) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 4102) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 18691697672202) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 4358) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 18966575579140) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 4614) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4416) ;; position: (4416 == 64 + 544 * 8 + 0)
    (i64.const 20340965113859) ;; value
  )
  (i64.store
    (i32.const 4424) ;; position: (4424 == 64 + 545 * 8 + 0)
    (i64.const 19241453486084) ;; value
  )
  (i64.store
    (i32.const 4432) ;; position: (4432 == 64 + 546 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4440) ;; position: (4440 == 64 + 547 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4448) ;; position: (4448 == 64 + 548 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4456) ;; position: (4456 == 64 + 549 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4464) ;; position: (4464 == 64 + 550 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4472) ;; position: (4472 == 64 + 551 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4480) ;; position: (4480 == 64 + 552 * 8 + 0)
    (i64.const 20375324852227) ;; value
  )
  (i64.store
    (i32.const 4488) ;; position: (4488 == 64 + 553 * 8 + 0)
    (i64.const 19516331393028) ;; value
  )
  (i64.store
    (i32.const 4496) ;; position: (4496 == 64 + 554 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4504) ;; position: (4504 == 64 + 555 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4512) ;; position: (4512 == 64 + 556 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4520) ;; position: (4520 == 64 + 557 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4528) ;; position: (4528 == 64 + 558 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4536) ;; position: (4536 == 64 + 559 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4544) ;; position: (4544 == 64 + 560 * 8 + 0)
    (i64.const 20409684590595) ;; value
  )
  (i64.store
    (i32.const 4552) ;; position: (4552 == 64 + 561 * 8 + 0)
    (i64.const 19791209299972) ;; value
  )
  (i64.store
    (i32.const 4560) ;; position: (4560 == 64 + 562 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4568) ;; position: (4568 == 64 + 563 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4576) ;; position: (4576 == 64 + 564 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4584) ;; position: (4584 == 64 + 565 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4592) ;; position: (4592 == 64 + 566 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4600) ;; position: (4600 == 64 + 567 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4608) ;; position: (4608 == 64 + 568 * 8 + 0)
    (i64.const 20444044328963) ;; value
  )
  (i64.store
    (i32.const 4616) ;; position: (4616 == 64 + 569 * 8 + 0)
    (i64.const 20066087206916) ;; value
  )
  (i64.store
    (i32.const 4624) ;; position: (4624 == 64 + 570 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4632) ;; position: (4632 == 64 + 571 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4640) ;; position: (4640 == 64 + 572 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4648) ;; position: (4648 == 64 + 573 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4656) ;; position: (4656 == 64 + 574 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4664) ;; position: (4664 == 64 + 575 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4672) ;; position: (4672 == 64 + 576 * 8 + 0)
    (i64.const 20478404067331) ;; value
  )
  (i64.store
    (i32.const 4680) ;; position: (4680 == 64 + 577 * 8 + 0)
    (i64.const 20340965115222) ;; value
  )
  (i64.store
    (i32.const 4688) ;; position: (4688 == 64 + 578 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4696) ;; position: (4696 == 64 + 579 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4704) ;; position: (4704 == 64 + 580 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4712) ;; position: (4712 == 64 + 581 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4720) ;; position: (4720 == 64 + 582 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4728) ;; position: (4728 == 64 + 583 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4736) ;; position: (4736 == 64 + 584 * 8 + 0)
    (i64.const 18966575579138) ;; value
  )
  (i64.store
    (i32.const 4744) ;; position: (4744 == 64 + 585 * 8 + 0)
    (i64.const 19241453486082) ;; value
  )
  (i64.store
    (i32.const 4752) ;; position: (4752 == 64 + 586 * 8 + 0)
    (i64.const 19516331393026) ;; value
  )
  (i64.store
    (i32.const 4760) ;; position: (4760 == 64 + 587 * 8 + 0)
    (i64.const 19791209299970) ;; value
  )
  (i64.store
    (i32.const 4768) ;; position: (4768 == 64 + 588 * 8 + 0)
    (i64.const 20066087206914) ;; value
  )
  (i64.store
    (i32.const 4776) ;; position: (4776 == 64 + 589 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4784) ;; position: (4784 == 64 + 590 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4792) ;; position: (4792 == 64 + 591 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4800)
    (i64.const 501)
  )
  
  (local.get $result)
)

(func $overwrite_object_MultipleTimeDuplicate
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 20615843021350)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 20615843020803) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 20650202759171) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 17867063953958) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 20340965113869) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 18141941860902) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 20066087206925) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819767846) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 19791209299981) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18691697674790) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 19516331393037) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 18966575581734) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4416) ;; position: (4416 == 64 + 544 * 8 + 0)
    (i64.const 19241453486093) ;; value
  )
  (i64.store
    (i32.const 4424) ;; position: (4424 == 64 + 545 * 8 + 0)
    (i64.const 2822) ;; value
  )
  (i64.store
    (i32.const 4432) ;; position: (4432 == 64 + 546 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4440) ;; position: (4440 == 64 + 547 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4448) ;; position: (4448 == 64 + 548 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4456) ;; position: (4456 == 64 + 549 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4464) ;; position: (4464 == 64 + 550 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4472) ;; position: (4472 == 64 + 551 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4480) ;; position: (4480 == 64 + 552 * 8 + 0)
    (i64.const 4294967310) ;; value
  )
  (i64.store
    (i32.const 4488) ;; position: (4488 == 64 + 553 * 8 + 0)
    (i64.const 4294967310) ;; value
  )
  (i64.store
    (i32.const 4496) ;; position: (4496 == 64 + 554 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4504) ;; position: (4504 == 64 + 555 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4512) ;; position: (4512 == 64 + 556 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4520) ;; position: (4520 == 64 + 557 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4528) ;; position: (4528 == 64 + 558 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4536) ;; position: (4536 == 64 + 559 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4544) ;; position: (4544 == 64 + 560 * 8 + 0)
    (i64.const 8589934606) ;; value
  )
  (i64.store
    (i32.const 4552) ;; position: (4552 == 64 + 561 * 8 + 0)
    (i64.const 8589934606) ;; value
  )
  (i64.store
    (i32.const 4560) ;; position: (4560 == 64 + 562 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4568) ;; position: (4568 == 64 + 563 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4576) ;; position: (4576 == 64 + 564 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4584) ;; position: (4584 == 64 + 565 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4592) ;; position: (4592 == 64 + 566 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4600) ;; position: (4600 == 64 + 567 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4608) ;; position: (4608 == 64 + 568 * 8 + 0)
    (i64.const 12884901902) ;; value
  )
  (i64.store
    (i32.const 4616) ;; position: (4616 == 64 + 569 * 8 + 0)
    (i64.const 12884901902) ;; value
  )
  (i64.store
    (i32.const 4624) ;; position: (4624 == 64 + 570 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4632) ;; position: (4632 == 64 + 571 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4640) ;; position: (4640 == 64 + 572 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4648) ;; position: (4648 == 64 + 573 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4656) ;; position: (4656 == 64 + 574 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4664) ;; position: (4664 == 64 + 575 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4672) ;; position: (4672 == 64 + 576 * 8 + 0)
    (i64.const 17179869198) ;; value
  )
  (i64.store
    (i32.const 4680) ;; position: (4680 == 64 + 577 * 8 + 0)
    (i64.const 17179869198) ;; value
  )
  (i64.store
    (i32.const 4688) ;; position: (4688 == 64 + 578 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4696) ;; position: (4696 == 64 + 579 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4704) ;; position: (4704 == 64 + 580 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4712) ;; position: (4712 == 64 + 581 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4720) ;; position: (4720 == 64 + 582 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4728) ;; position: (4728 == 64 + 583 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4736) ;; position: (4736 == 64 + 584 * 8 + 0)
    (i64.const 21474836494) ;; value
  )
  (i64.store
    (i32.const 4744) ;; position: (4744 == 64 + 585 * 8 + 0)
    (i64.const 21474836494) ;; value
  )
  (i64.store
    (i32.const 4752) ;; position: (4752 == 64 + 586 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4760) ;; position: (4760 == 64 + 587 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4768) ;; position: (4768 == 64 + 588 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4776) ;; position: (4776 == 64 + 589 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4784) ;; position: (4784 == 64 + 590 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4792) ;; position: (4792 == 64 + 591 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4800) ;; position: (4800 == 64 + 592 * 8 + 0)
    (i64.const 17592186044416) ;; value
  )
  (i64.store
    (i32.const 4808) ;; position: (4808 == 64 + 593 * 8 + 0)
    (i64.const 17592186044417) ;; value
  )
  (i64.store
    (i32.const 4816) ;; position: (4816 == 64 + 594 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4824) ;; position: (4824 == 64 + 595 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4832) ;; position: (4832 == 64 + 596 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4840) ;; position: (4840 == 64 + 597 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4848) ;; position: (4848 == 64 + 598 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4856) ;; position: (4856 == 64 + 599 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4864)
    (i64.const 500)
  )
  
  (local.get $result)
)

(func $overwrite_object_DuplicateIdentityLambda
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 18141941858854)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 18141941858307) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 18176301596675) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 17867063951364) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 17901423689731) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 17867063951362) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 17592186044416) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 17592186044417) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4288)
    (i64.const 509)
  )
  
  (local.get $result)
)

(func $overwrite_object_DubplicateNestedLambda
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 18691697672742)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 18691697672195) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 18726057410563) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 17867063951364) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 18416819765251) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18451179503619) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819765798) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 17867063951362) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 17592186044416) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 17592186044417) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4416)
    (i64.const 507)
  )
  
  (local.get $result)
)

(func $overwrite_object_DubplicateMultipleNestedLambda
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 20340965114406)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 20340965113859) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 20375324852227) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 17867063951364) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 20066087206915) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 20100446945283) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819765252) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 20134806683651) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18691697672196) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 20169166422019) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 18966575579140) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4416) ;; position: (4416 == 64 + 544 * 8 + 0)
    (i64.const 20203526160387) ;; value
  )
  (i64.store
    (i32.const 4424) ;; position: (4424 == 64 + 545 * 8 + 0)
    (i64.const 19241453486084) ;; value
  )
  (i64.store
    (i32.const 4432) ;; position: (4432 == 64 + 546 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4440) ;; position: (4440 == 64 + 547 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4448) ;; position: (4448 == 64 + 548 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4456) ;; position: (4456 == 64 + 549 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4464) ;; position: (4464 == 64 + 550 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4472) ;; position: (4472 == 64 + 551 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4480) ;; position: (4480 == 64 + 552 * 8 + 0)
    (i64.const 20237885898755) ;; value
  )
  (i64.store
    (i32.const 4488) ;; position: (4488 == 64 + 553 * 8 + 0)
    (i64.const 19516331393028) ;; value
  )
  (i64.store
    (i32.const 4496) ;; position: (4496 == 64 + 554 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4504) ;; position: (4504 == 64 + 555 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4512) ;; position: (4512 == 64 + 556 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4520) ;; position: (4520 == 64 + 557 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4528) ;; position: (4528 == 64 + 558 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4536) ;; position: (4536 == 64 + 559 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4544) ;; position: (4544 == 64 + 560 * 8 + 0)
    (i64.const 20272245637123) ;; value
  )
  (i64.store
    (i32.const 4552) ;; position: (4552 == 64 + 561 * 8 + 0)
    (i64.const 19791209299972) ;; value
  )
  (i64.store
    (i32.const 4560) ;; position: (4560 == 64 + 562 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4568) ;; position: (4568 == 64 + 563 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4576) ;; position: (4576 == 64 + 564 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4584) ;; position: (4584 == 64 + 565 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4592) ;; position: (4592 == 64 + 566 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4600) ;; position: (4600 == 64 + 567 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4608) ;; position: (4608 == 64 + 568 * 8 + 0)
    (i64.const 20306605375491) ;; value
  )
  (i64.store
    (i32.const 4616) ;; position: (4616 == 64 + 569 * 8 + 0)
    (i64.const 20066087209094) ;; value
  )
  (i64.store
    (i32.const 4624) ;; position: (4624 == 64 + 570 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4632) ;; position: (4632 == 64 + 571 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4640) ;; position: (4640 == 64 + 572 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4648) ;; position: (4648 == 64 + 573 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4656) ;; position: (4656 == 64 + 574 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4664) ;; position: (4664 == 64 + 575 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4672) ;; position: (4672 == 64 + 576 * 8 + 0)
    (i64.const 17867063951362) ;; value
  )
  (i64.store
    (i32.const 4680) ;; position: (4680 == 64 + 577 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4688) ;; position: (4688 == 64 + 578 * 8 + 0)
    (i64.const 18416819765250) ;; value
  )
  (i64.store
    (i32.const 4696) ;; position: (4696 == 64 + 579 * 8 + 0)
    (i64.const 18691697672194) ;; value
  )
  (i64.store
    (i32.const 4704) ;; position: (4704 == 64 + 580 * 8 + 0)
    (i64.const 18966575579138) ;; value
  )
  (i64.store
    (i32.const 4712) ;; position: (4712 == 64 + 581 * 8 + 0)
    (i64.const 19241453486082) ;; value
  )
  (i64.store
    (i32.const 4720) ;; position: (4720 == 64 + 582 * 8 + 0)
    (i64.const 19516331393026) ;; value
  )
  (i64.store
    (i32.const 4728) ;; position: (4728 == 64 + 583 * 8 + 0)
    (i64.const 19791209299970) ;; value
  )
  (i64.store
    (i32.const 4736) ;; position: (4736 == 64 + 584 * 8 + 0)
    (i64.const 17592186044416) ;; value
  )
  (i64.store
    (i32.const 4744) ;; position: (4744 == 64 + 585 * 8 + 0)
    (i64.const 17592186044417) ;; value
  )
  (i64.store
    (i32.const 4752) ;; position: (4752 == 64 + 586 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4760) ;; position: (4760 == 64 + 587 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4768) ;; position: (4768 == 64 + 588 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4776) ;; position: (4776 == 64 + 589 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4784) ;; position: (4784 == 64 + 590 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4792) ;; position: (4792 == 64 + 591 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4800)
    (i64.const 501)
  )
  
  (local.get $result)
)

(func $overwrite_object_DoublingFunction
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044426)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 17867063951364) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 12884901902) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 18210661335043) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 18416819765261) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18416819765251) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18451179503619) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 17867063951362) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 18141941858304) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18141941858305) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4352)
    (i64.const 508)
  )
  
  (local.get $result)
)

(func $overwrite_object_SharedComputationInLambda
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 19516331393574)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 20066087206915) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 19791209299971) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 17867063951370) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 8589934606) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 19035295055875) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819765252) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 18726057410563) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18691697672742) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 19241453486093) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 18416819765250) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4416) ;; position: (4416 == 64 + 544 * 8 + 0)
    (i64.const 19241453486083) ;; value
  )
  (i64.store
    (i32.const 4424) ;; position: (4424 == 64 + 545 * 8 + 0)
    (i64.const 19275813224451) ;; value
  )
  (i64.store
    (i32.const 4432) ;; position: (4432 == 64 + 546 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4440) ;; position: (4440 == 64 + 547 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4448) ;; position: (4448 == 64 + 548 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4456) ;; position: (4456 == 64 + 549 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4464) ;; position: (4464 == 64 + 550 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4472) ;; position: (4472 == 64 + 551 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4480) ;; position: (4480 == 64 + 552 * 8 + 0)
    (i64.const 18966575579152) ;; value
  )
  (i64.store
    (i32.const 4488) ;; position: (4488 == 64 + 553 * 8 + 0)
    (i64.const 18966575579153) ;; value
  )
  (i64.store
    (i32.const 4496) ;; position: (4496 == 64 + 554 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4504) ;; position: (4504 == 64 + 555 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4512) ;; position: (4512 == 64 + 556 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4520) ;; position: (4520 == 64 + 557 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4528) ;; position: (4528 == 64 + 558 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4536) ;; position: (4536 == 64 + 559 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4544) ;; position: (4544 == 64 + 560 * 8 + 0)
    (i64.const 20066087206922) ;; value
  )
  (i64.store
    (i32.const 4552) ;; position: (4552 == 64 + 561 * 8 + 0)
    (i64.const 19791209299978) ;; value
  )
  (i64.store
    (i32.const 4560) ;; position: (4560 == 64 + 562 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4568) ;; position: (4568 == 64 + 563 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4576) ;; position: (4576 == 64 + 564 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4584) ;; position: (4584 == 64 + 565 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4592) ;; position: (4592 == 64 + 566 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4600) ;; position: (4600 == 64 + 567 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4608) ;; position: (4608 == 64 + 568 * 8 + 0)
    (i64.const 17592186044417) ;; value
  )
  (i64.store
    (i32.const 4616) ;; position: (4616 == 64 + 569 * 8 + 0)
    (i64.const 85899345934) ;; value
  )
  (i64.store
    (i32.const 4624) ;; position: (4624 == 64 + 570 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4632) ;; position: (4632 == 64 + 571 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4640) ;; position: (4640 == 64 + 572 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4648) ;; position: (4648 == 64 + 573 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4656) ;; position: (4656 == 64 + 574 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4664) ;; position: (4664 == 64 + 575 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4672) ;; position: (4672 == 64 + 576 * 8 + 0)
    (i64.const 17592186044416) ;; value
  )
  (i64.store
    (i32.const 4680) ;; position: (4680 == 64 + 577 * 8 + 0)
    (i64.const 42949672974) ;; value
  )
  (i64.store
    (i32.const 4688) ;; position: (4688 == 64 + 578 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4696) ;; position: (4696 == 64 + 579 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4704) ;; position: (4704 == 64 + 580 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4712) ;; position: (4712 == 64 + 581 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4720) ;; position: (4720 == 64 + 582 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4728) ;; position: (4728 == 64 + 583 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4736)
    (i64.const 502)
  )
  
  (local.get $result)
)

(func $overwrite_object_CorrolaryApplicationAsChild
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044694)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 17867063951370) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 2822) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18176301596675) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4288)
    (i64.const 509)
  )
  
  (local.get $result)
)

(func $overwrite_object_IdentityLambdaWithUnusedState
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044426)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 17867063951368) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 6) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 3078) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18691697672195) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819765252) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 18726057410563) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18691697674534) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 18416819765250) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4416)
    (i64.const 507)
  )
  
  (local.get $result)
)

(func $overwrite_object_LatchingBool
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044424)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 3078) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 17867063951364) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 18726057410563) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18416819765251) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819765288) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18691697674534) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 3334) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 17867063951362) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4416)
    (i64.const 507)
  )
  
  (local.get $result)
)

(func $overwrite_object_LightSwitch
  (result i64)
  (local $result i64)
  
  (local.set $result
    (i64.const 17592186044424)
  )
  
  (i64.store
    (i32.const 4096) ;; position: (4096 == 64 + 504 * 8 + 0)
    (i64.const 3078) ;; value
  )
  (i64.store
    (i32.const 4104) ;; position: (4104 == 64 + 505 * 8 + 0)
    (i64.const 17867063951364) ;; value
  )
  (i64.store
    (i32.const 4112) ;; position: (4112 == 64 + 506 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4120) ;; position: (4120 == 64 + 507 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4128) ;; position: (4128 == 64 + 508 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4136) ;; position: (4136 == 64 + 509 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4144) ;; position: (4144 == 64 + 510 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4152) ;; position: (4152 == 64 + 511 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4160) ;; position: (4160 == 64 + 512 * 8 + 0)
    (i64.const 18760417148931) ;; value
  )
  (i64.store
    (i32.const 4168) ;; position: (4168 == 64 + 513 * 8 + 0)
    (i64.const 18141941858308) ;; value
  )
  (i64.store
    (i32.const 4176) ;; position: (4176 == 64 + 514 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4184) ;; position: (4184 == 64 + 515 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4192) ;; position: (4192 == 64 + 516 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4200) ;; position: (4200 == 64 + 517 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4208) ;; position: (4208 == 64 + 518 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4216) ;; position: (4216 == 64 + 519 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4224) ;; position: (4224 == 64 + 520 * 8 + 0)
    (i64.const 18416819765251) ;; value
  )
  (i64.store
    (i32.const 4232) ;; position: (4232 == 64 + 521 * 8 + 0)
    (i64.const 18416819765288) ;; value
  )
  (i64.store
    (i32.const 4240) ;; position: (4240 == 64 + 522 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4248) ;; position: (4248 == 64 + 523 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4256) ;; position: (4256 == 64 + 524 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4264) ;; position: (4264 == 64 + 525 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4272) ;; position: (4272 == 64 + 526 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4280) ;; position: (4280 == 64 + 527 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4288) ;; position: (4288 == 64 + 528 * 8 + 0)
    (i64.const 18141941858306) ;; value
  )
  (i64.store
    (i32.const 4296) ;; position: (4296 == 64 + 529 * 8 + 0)
    (i64.const 18966575581478) ;; value
  )
  (i64.store
    (i32.const 4304) ;; position: (4304 == 64 + 530 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4312) ;; position: (4312 == 64 + 531 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4320) ;; position: (4320 == 64 + 532 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4328) ;; position: (4328 == 64 + 533 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4336) ;; position: (4336 == 64 + 534 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4344) ;; position: (4344 == 64 + 535 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4352) ;; position: (4352 == 64 + 536 * 8 + 0)
    (i64.const 19241453486083) ;; value
  )
  (i64.store
    (i32.const 4360) ;; position: (4360 == 64 + 537 * 8 + 0)
    (i64.const 19000935317507) ;; value
  )
  (i64.store
    (i32.const 4368) ;; position: (4368 == 64 + 538 * 8 + 0)
    (i64.const 17867063951362) ;; value
  )
  (i64.store
    (i32.const 4376) ;; position: (4376 == 64 + 539 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4384) ;; position: (4384 == 64 + 540 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4392) ;; position: (4392 == 64 + 541 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4400) ;; position: (4400 == 64 + 542 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4408) ;; position: (4408 == 64 + 543 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4416) ;; position: (4416 == 64 + 544 * 8 + 0)
    (i64.const 19241453486104) ;; value
  )
  (i64.store
    (i32.const 4424) ;; position: (4424 == 64 + 545 * 8 + 0)
    (i64.const 18691697672193) ;; value
  )
  (i64.store
    (i32.const 4432) ;; position: (4432 == 64 + 546 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4440) ;; position: (4440 == 64 + 547 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4448) ;; position: (4448 == 64 + 548 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4456) ;; position: (4456 == 64 + 549 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4464) ;; position: (4464 == 64 + 550 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4472) ;; position: (4472 == 64 + 551 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4480) ;; position: (4480 == 64 + 552 * 8 + 0)
    (i64.const 18691697672192) ;; value
  )
  (i64.store
    (i32.const 4488) ;; position: (4488 == 64 + 553 * 8 + 0)
    (i64.const 3078) ;; value
  )
  (i64.store
    (i32.const 4496) ;; position: (4496 == 64 + 554 * 8 + 0)
    (i64.const 3334) ;; value
  )
  (i64.store
    (i32.const 4504) ;; position: (4504 == 64 + 555 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4512) ;; position: (4512 == 64 + 556 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4520) ;; position: (4520 == 64 + 557 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4528) ;; position: (4528 == 64 + 558 * 8 + 0)
    (i64.const 0) ;; value
  )
  (i64.store
    (i32.const 4536) ;; position: (4536 == 64 + 559 * 8 + 0)
    (i64.const 0) ;; value
  )
  
  (call $relinquish_from_to_N_many
    (i64.const 4544)
    (i64.const 505)
  )
  
  (local.get $result)
)

