module Inject_Profiling_Info where

import System.Environment
import System.IO (hPutStrLn, stderr)

import Numeric.Natural
import Data.List


at :: Eq a => Natural -> [a] -> Maybe a
at _ [] = Nothing
at 0 (x:xs) = Just x
at n (x:xs) = at (n-1) xs

meta_search_string :: String
meta_search_string = "cd69949d13af4508cf16da126ad79f2df085417ae30065f9724e55d4352554cff4ddba67251c247e35991cd70d6ecc224eecb1507fb0be35cab3c23aa107f8b2"

error_out :: String -> IO String
error_out errorNote = do
  hPutStrLn stderr errorNote
  return $ "#error " ++ errorNote

inject_profiling_info :: IO ()
inject_profiling_info = do
  [decision_number] <- getArgs

  let number = read decision_number :: Natural

  hints_file <- readFile "profiling_hints"

  let hints = lines hints_file

  let run_out_profiling_hints
        = "ran out profiling hints: index "
        ++ (show number)
        ++ " was requested but only got "
        ++ (show $ genericLength hints)
        ++ ". Provide longer file with hints. ("
        ++ meta_search_string
        ++ ")"

  let maybe_hint = at number hints

  outputString
    <- case maybe_hint of
        Just "i" -> return "STONEFISH_PROFILING_INCLUDE_HINT"
        Just "e" -> return "STONEFISH_PROFILING_EXCLUDE_HINT"
        Nothing -> error_out run_out_profiling_hints
        Just unkown_case -> error_out $ "unkown profiling hint case: " ++ show unkown_case

  let _ = outputString :: String

--   hPutStrLn stderr ("got asked for number: " ++ show number)
  putStrLn $ outputString
