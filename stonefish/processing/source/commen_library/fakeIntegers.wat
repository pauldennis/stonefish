

(func
  $i64.add
  (param $augend i64)
  (param $addend i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_addition_be_represented
        (local.get $augend)
        (local.get $addend)
      )
    )
  #endif

  (call $i64.add_wrapped
    (local.get $augend)
    (local.get $addend)
  )
)
(func
  $i32.add
  (param $augend i32)
  (param $addend i32)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i32.can_addition_be_represented
        (local.get $augend)
        (local.get $addend)
      )
    )
  #endif

  (call $i32.add_wrapped
    (local.get $augend)
    (local.get $addend)
  )
)


(func
  $i64.subtract
  (param $minuend i64)
  (param $subtrahend i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_subtraction_be_represented
        (local.get $minuend)
        (local.get $subtrahend)
      )
    )
  #endif

  (call $i64.subtract_wrapped
    (local.get $minuend)
    (local.get $subtrahend)
  )
)
(func
  $TEST_subtraction

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $i64.subtract
          (i64.const 10)
          (i64.const 3)
        )
        (i64.const 7)
      )
    )
  #endif
)


(func
  $i64.multiplicate
  (param $multiplier i64)
  (param $multiplicand i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_multiplication_be_represented
        (local.get $multiplier)
        (local.get $multiplicand)
      )
    )
  #endif

  (call $i64.multiplicate_wrapped
    (local.get $multiplier)
    (local.get $multiplicand)
  )
)

(func
  $i32.multiplicate
  (param $multiplier i32)
  (param $multiplicand i32)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i32.can_multiplication_be_represented
        (local.get $multiplier)
        (local.get $multiplicand)
      )
    )
  #endif

  (call $i32.multiplicate_wrapped
    (local.get $multiplier)
    (local.get $multiplicand)
  )
)

