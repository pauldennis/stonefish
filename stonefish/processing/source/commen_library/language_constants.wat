(func
  $PAGE_SIZE_IN_BYTES
  (result i32)
  (i32.const 65536)
)

(func
  $MAXIMUM_NUMBER_OF_PAGES_IN_ONE_MEMORY
  (result i32)
  (i32.const 65536)
)

;; TODO replace hardcoded occurences of these numbers
(func
  $i64.MAXIMUM_I32
  (result i64)
  (i64.const 4294967295) ;; maxBound::Word32
)
(func
  $i32.MAXIMUM_I32
  (result i32)
  (i32.const 4294967295) ;; maxBound::Word32
)

(func
  $i64.MAXIMUM_I64
  (result i64)
  (i64.const 18446744073709551615) ;; maxBound::Word32
)

(func
  $i32.MAXIMUM_I8
  (result i32)
  (i32.const 255) ;; maxBound::Word8
)

