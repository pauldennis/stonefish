(func
  $TEST_commen_library

  (call $TEST_and)
)


;; boolean

(func
  $or
  (param $left i32)
  (param $right i32)
  (result i32)

  (if
    (local.get $left)
    (then
      (return
        (call $TRUE)
      )
    )
  )

  (if
    (local.get $right)
    (then
      (return
        (call $TRUE) ;; Yes this could be optimised, but this more readable in this context
      )
    )
  )

  (call $FALSE)
)


;; (func
;;   $and
;;   (param $left i32)
;;   (param $right i32)
;;   (result i32)
;;   (call $and
;;     (local.get $left)
;;     (local.get $right)
;;   )
;; )


;; TODO rename to $and
(func
  $and
  (param $left i32)
  (param $right i32)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isNormalizedBool
        (local.get $left)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isNormalizedBool
        (local.get $right)
      )
    )
  #endif

  (if
    (result i32)

    (local.get $left)
    (then
      (local.get $right)
    )
    (else
      (call $FALSE)
    )
  )
)
(func
  $TEST_and

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $and
        (call $TRUE)
        (call $TRUE)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $and
          (call $FALSE)
          (call $TRUE)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $and
          (call $TRUE)
          (call $FALSE)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $and
          (call $FALSE)
          (call $FALSE)
        )
      )
    )
  #endif
)


;; integer

(func
  $i64.increment
  (param $number i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (i64.const 18446744073709551615) ;; maxBound::Word64
          (local.get $number)
        )
      )
    )
  #endif

  (call $i64.add_wrapped
    (i64.const 1)
    (local.get $number)
  )
)
(func
  $i32.increment
  (param $number i32)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i32.isEqual
          (i32.const 4294967295) ;; maxBound::Word32
          (local.get $number)
        )
      )
    )
  #endif

  (call $i32.add_wrapped
    (i32.const 1)
    (local.get $number)
  )
)


(func
  $i64.decrement
  (param $number i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (i64.const 0) ;; maxBound::Word64
          (local.get $number)
        )
      )
    )
  #endif

  (call $i64.subtract_wrapped
    (local.get $number)
    (i64.const 1) ;; second place
  )
)
(func
  $i32.decrement
  (param $number i32)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i32.isEqual
          (i32.const 0) ;; maxBound::Word32
          (local.get $number)
        )
      )
    )
  #endif

  (call $i32.subtract_wrapped
    (local.get $number)
    (i32.const 1) ;; second place
  )
)


(func
  $i64.swap
  (param $deep i64)
  (param $shallow i64)

  (result i64)
  (result i64)

  (local.get $shallow)
  (local.get $deep)
)
