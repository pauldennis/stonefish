;; the functions in overflow are not nessesarily implemented efficiently. They come to live for the use case of using them in $assert calls


;; https://stackoverflow.com/questions/8534107/detecting-multiplication-of-uint64-t-integers-overflow-with-c
(func
  $i64.can_addition_be_represented
  (param $left i64)
  (param $right i64)
  (result i32)

  (call $not
    (call $i64.isLessThan
      (call $i64.subtract_wrapped
        (i64.const 18446744073709551615) ;; maxBound :: Word64
        (local.get $left)
      )
      (local.get $right)
    )
  )
)
(func
  $TEST_addition_overflow

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_addition_be_represented
        (i64.const 18446744073709551615)
        (i64.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.can_addition_be_represented
          (i64.const 18446744073709551615)
          (i64.const 1)
        )
      )
    )
  #endif
)

(func
  $i32.can_addition_be_represented
  (param $left i32)
  (param $right i32)
  (result i32)

  (call $not
    (call $i32.isLessThan
      (call $i32.subtract_wrapped
        (i32.const 4294967295) ;; maxBound :: Word32
        (local.get $left)
      )
      (local.get $right)
    )
  )
)

;; TODO what about the signedness?
;; no source
;;     0 <= $left - $right
;; <=> $right <= $left
(func
  $i64.can_subtraction_be_represented
  (param $left i64)
  (param $right i64)
  (result i32)

  (call $i64.isLessOrEqual
    (local.get $right)
    (local.get $left)
  )
)
(func
  $TEST_subtraction_overflow

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_subtraction_be_represented
        (i64.const 18446744073709551615)
        (i64.const 18446744073709551615)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_subtraction_be_represented
        (i64.const 0)
        (i64.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_subtraction_be_represented
        (i64.const 1)
        (i64.const 1)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_subtraction_be_represented
        (i64.const 2)
        (i64.const 1)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.can_subtraction_be_represented
          (i64.const 18446744073709551614) ;; maxBound-1
          (i64.const 18446744073709551615)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.can_subtraction_be_represented
          (i64.const 0) ;; maxBound-1
          (i64.const 1)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.can_subtraction_be_represented
          (i64.const 1) ;; maxBound-1
          (i64.const 2)
        )
      )
    )
  #endif
)


;; https://stackoverflow.com/questions/8534107/detecting-multiplication-of-uint64-t-integers-overflow-with-c
(func
  $i64.can_multiplication_be_represented
  (param $left i64)
  (param $right i64)
  (result i32)

  (local $swap_helper_left i64)
  (local $swap_helper_right i64)

  (local $whatever_c i64)
  (local $whatever_d i64)
  (local $whatever_r i64)
  (local $whatever_s i64)

  (local $whatever_r_back i64)

  ;; swap arguments when not ordered
  ;; TODO find out why recursive call does not get optimized out (see git history of the state before)
  (if
    (call $i64.isLessThan
      (local.get $right)
      (local.get $left)
    )
    (then
      (local.set $swap_helper_left
        (local.get $left)
      )
      (local.set $swap_helper_right
        (local.get $right)
      )
      (local.set $left
        (local.get $swap_helper_right)
      )
      (local.set $right
        (local.get $swap_helper_left)
      )
    )
  )

  (if
    (call $i64.isLessThan
      (i64.const 4294967295) ;; maxBound :: Word32
      (local.get $left)
    )
    (then
      (return
        (call $FALSE)
      )
    )
  )

  (local.set $whatever_c
    (call $i64.decreaseShift
      (local.get $right)
      (i64.const 32)
    )
  )

  (local.set $whatever_d
    (call $i64.bitwiseAnd
      (i64.const 4294967295) ;; maxBound :: Word32
      (local.get $right)
    )
  )

  (local.set $whatever_r
    (call $i64.multiplicate_wrapped
      (local.get $left)
      (local.get $whatever_c)
    )
  )

  (local.set $whatever_s
    (call $i64.multiplicate_wrapped
      (local.get $left)
      (local.get $whatever_d)
    )
  )

  (if
    (call $i64.isLessThan
      (i64.const 4294967295) ;; maxBound :: Word32
      (local.get $whatever_r)
    )
    (then
      (return
        (call $FALSE)
      )
    )
  )

  (local.set $whatever_r_back
    (call $i64.increaseShift
      (local.get $whatever_r)
      (i64.const 32)
    )
  )

  ;; TODO is this still nessesary?
  (call $i64.can_addition_be_represented
    (local.get $whatever_s)
    (local.get $whatever_r_back)
  )
)
(func
  $TEST_multiplication_overflow

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_multiplication_be_represented
        (i64.const 0)
        (i64.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_multiplication_be_represented
        (i64.const 4294967295)
        (i64.const 4294967297)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.can_multiplication_be_represented
          (i64.const 4294967296)
          (i64.const 4294967296)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.can_multiplication_be_represented
          (i64.const 9223372036854775808)
          (i64.const 2)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.can_multiplication_be_represented
          (i64.const 18446744073709551615)
          (i64.const 18446744073709551615)
        )
      )
    )
  #endif
)

(func
  $i32.can_multiplication_be_represented
  (param $left i32)
  (param $right i32)
  (result i32)

  (local $left_extended i64)
  (local $right_extended i64)
  (local $wannebe_be_result i64)

  (local.set $left_extended
    (i64.extend_i32_u
      (local.get $left)
    )
  )
  (local.set $right_extended
    (i64.extend_i32_u
      (local.get $right)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.can_multiplication_be_represented
        (local.get $left_extended)
        (local.get $right_extended)
      )
    )
  #endif

  (local.set $wannebe_be_result
    (call $i64.add_wrapped
      (local.get $left_extended)
      (local.get $right_extended)
    )
  )

  (call $i64.isLessOrEqual
    (local.get $wannebe_be_result)
    (i64.const 4294967295) ;; maxBound :: Word32
  )
)


(func
  $TEST_overlows

  (call $TEST_addition_overflow)
  (call $TEST_subtraction_overflow)
  (call $TEST_multiplication_overflow)
)

