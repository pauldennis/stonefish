
;; i64.store and i64.load but with assertions

(func
  $i64.getLastAllocatedAddress_exclusive
  (result i64)

  (call $i64.multiplicate
    (i64.extend_i32_u
      (memory.size)
    )
    (i64.extend_i32_u
      (call $PAGE_SIZE_IN_BYTES)
    )
  )
)

(func
  $getAddressOfLast8BytesInCurrentMemory
  (result i64)

  (call $i64.subtract
    (call $i64.getLastAllocatedAddress_exclusive)
    (i64.const 8)
  )
)


;;;;



(func
  $i64.storeAtLocation
  (param $location i64)
  (param $word64 i64)

  (local $byteIndex i32)

  ;; currently only 4GB of data possible
  ;; Word64 has size of 8 bytes

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $location)
        (i64.const 4294967287) ;; 4294967287 == 4294967295-8
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $location)
        (call $getAddressOfLast8BytesInCurrentMemory)
      )
    )
  #endif

  (local.set $byteIndex
    (call $i32.cast_i64
      (local.get $location)
    )
  )

  (i64.store
    (local.get $byteIndex)
    (local.get $word64)
  )
)


(func
  $i64.loadAtLocation
  (param $location i64)
  (result i64)

  (local $byteIndex i32)

  ;; currently only 4GB of data possible
  ;; Word64 has size of 8 bytes

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $location)
        (i64.const 4294967287) ;; 4294967287 == 4294967295-8
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $location)
        (call $getAddressOfLast8BytesInCurrentMemory)
      )
    )
  #endif

  (local.set $byteIndex
    (call $i32.cast_i64
      (local.get $location)
    )
  )

  (i64.load
    (local.get $byteIndex)
  )
)



;;;


(func
  $i64.store_64bitAligned
  (param $location i64)
  (param $word64 i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleBy8
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  (call $i64.storeAtLocation
    (local.get $location)
    (local.get $word64)
  )
)



(func
  $i64.load_64bitAligned
  (param $location i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleBy8
        (global.get $freeSlotsStack_NextUnusedElement)
      )
    )
  #endif

  (call $i64.loadAtLocation
    (local.get $location)
  )
)


;;;;


(func
  $i64.memory.fill
  (param $firstAdress i64)
  (param $numberOfBytes i64)
  (param $theByteToBeUsed i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i32.isLessOrEqual
        (local.get $theByteToBeUsed)
        (call $i32.MAXIMUM_I8)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $firstAdress)
        (call $i64.getLastAllocatedAddress_exclusive)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (call $i64.add
          (local.get $firstAdress)
          (local.get $numberOfBytes)
        )
        (call $i64.getLastAllocatedAddress_exclusive)
      )
    )
  #endif

  (memory.fill
    ;; d
    (i32.wrap_i64
      (local.get $firstAdress)
    )

    ;; val
    (local.get $theByteToBeUsed)

    ;; n
    (i32.wrap_i64
      (local.get $numberOfBytes)
    )
  )
)

