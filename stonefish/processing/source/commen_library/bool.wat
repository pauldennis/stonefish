
(func
  $FALSE ;; 0
  (result i32)
  (i32.const 0)
)
(func
  $TRUE ;; 1, 2, 3, ...
  (result i32)
  (i32.const 1)
)
(func
  $CANONICAL_TRUE
  (result i32)
  (i32.const 1)
)

;; TODO rename to isCanonicalBool
(func
  $isNormalizedBool
  (param $bool i32)
  (result i32)

  (if
    (call $i32.isEqual
      (call $CANONICAL_TRUE)
      (local.get $bool)
    )
    (then
      (return
        (call $TRUE)
      )
    )
  )
  (if
    (call $i32.isEqual
      (call $FALSE)
      (local.get $bool)
    )
    (then
      (return
        (call $TRUE)
      )
    )
  )

  (call $FALSE)
)
