(func
  $isInBetween_rightOpenIntervall
  (param $left_limit_inclusive i64)
  (param $supposedToBeInBetween i64)
  (param $right_limit_exclusive i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $i64.isLessOrEqual
      (local.get $left_limit_inclusive)
      (local.get $right_limit_exclusive)
    )
  )
  #endif

  (if
    (call $i64.isLessOrEqual
      (local.get $left_limit_inclusive)
      (local.get $supposedToBeInBetween)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (if
    (call $i64.isLessThan
      (local.get $supposedToBeInBetween)
      (local.get $right_limit_exclusive)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (call $TRUE)
)
(func
  $TEST_isInBetween_rightOpenIntervall

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isInBetween_rightOpenIntervall
          (i64.const 0)
          (i64.const 0)
          (i64.const 0)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isInBetween_rightOpenIntervall
        (i64.const 0)
        (i64.const 0)
        (i64.const 1)
      )
    )
  #endif
)



(func
  $isInBetween_closedIntervall
  (param $left_limit_inclusive i64)
  (param $supposedToBeInBetween i64)
  (param $right_limit_exclusive i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $left_limit_inclusive)
        (local.get $right_limit_exclusive)
      )
    )
  #endif

  (if
    (call $i64.isLessOrEqual
      (local.get $left_limit_inclusive)
      (local.get $supposedToBeInBetween)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (if
    (call $i64.isLessOrEqual
      (local.get $supposedToBeInBetween)
      (local.get $right_limit_exclusive)
    )
    (then
    )
    (else
      (return
        (call $FALSE)
      )
    )
  )

  (call $TRUE)
)
(func
  $TEST_isInBetween_closedIntervall

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isInBetween_closedIntervall
        (i64.const 0)
        (i64.const 0)
        (i64.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isInBetween_closedIntervall
        (i64.const 0)
        (i64.const 1)
        (i64.const 1)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isInBetween_closedIntervall
          (i64.const 0)
          (i64.const 2)
          (i64.const 1)
        )
      )
    )
  #endif
)




(func
  $TEST_isInBetween
  (call $TEST_isInBetween_rightOpenIntervall)
  (call $TEST_isInBetween_closedIntervall)

)
