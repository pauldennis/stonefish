

(func
  $i32.isDevisibleBy64
  (param $value i32)
  (result i32)

  (call $i32.isZero
    (call $i32.bitwiseAnd
      (i32.const 63)
      (local.get $value)
    )
  )
)

(func
  $i64.isDevisibleBy64
  (param $value i64)
  (result i32)

  (call $i64.isZero
    (call $i64.bitwiseAnd
      (i64.const 63)
      (local.get $value)
    )
  )
)


(func
  $i32.isDevisibleBy16
  (param $value i32)
  (result i32)

  (call $i32.isZero
    (call $i32.bitwiseAnd
      (i32.const 15)
      (local.get $value)
    )
  )
)


(func
  $i64.isDevisibleBy8
  (param $value i64)
  (result i32)

  (call $i64.isZero
    (call $i64.bitwiseAnd
      (i64.const 7)
      (local.get $value)
    )
  )
)
(func
  $TEST_i64.devisibilityBy8

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleBy8
        (i64.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleBy8
        (i64.const 8)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isDevisibleBy8
        (i64.const 16)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleBy8
          (i64.const 1)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleBy8
          (i64.const 2)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleBy8
          (i64.const 3)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleBy8
          (i64.const 4)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleBy8
          (i64.const 5)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleBy8
          (i64.const 6)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleBy8
          (i64.const 7)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isDevisibleBy8
          (i64.const 9)
        )
      )
    )
  #endif
)




(func
  $i32.isDevisibleBy8
  (param $value i32)
  (result i32)

  (call $i32.isZero
    (call $i32.bitwiseAnd
      (i32.const 7)
      (local.get $value)
    )
  )
)
(func
  $TEST_i32.devisibilityBy8

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $i32.isDevisibleBy8
      (i32.const 0)
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $i32.isDevisibleBy8
      (i32.const 8)
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $i32.isDevisibleBy8
      (i32.const 16)
    )
  )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $i32.isDevisibleBy8
        (i32.const 1)
      )
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $i32.isDevisibleBy8
        (i32.const 2)
      )
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $i32.isDevisibleBy8
        (i32.const 3)
      )
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $i32.isDevisibleBy8
        (i32.const 4)
      )
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $i32.isDevisibleBy8
        (i32.const 5)
      )
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $i32.isDevisibleBy8
        (i32.const 6)
      )
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $i32.isDevisibleBy8
        (i32.const 7)
      )
    )
  )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
  (call $assert
    (call $not
      (call $i32.isDevisibleBy8
        (i32.const 9)
      )
    )
  )
  #endif
)



(func
  $TEST_devisibility

  (call $TEST_i32.devisibilityBy8)
  (call $TEST_i64.devisibilityBy8)
)
