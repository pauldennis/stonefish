
(func $TEST_languageHelpers
  (call $TEST_expectation_of_i64.isLessOrEqual)
  (call $TEST_i32.upperHalf)
)


;; tell the reader, that there is one variable still on the stack
(func
  $i64.leftover_1
  (param $value i64)
  (result i64)

  (local.get $value)
)

(func
  $i32.leftover_1
  (param $value i32)
  (result i32)

  (local.get $value)
)

;; these "hacky" functions trigger bugs in firefox
;; (func
;;   $i64.duplicateTop
;;   (param $element i64)
;;   (result i64)
;;   (result i64)
;;
;;   (local $workPlace i64)
;;
;;   (local.tee $workPlace
;;     (local.get $element)
;;   )
;;
;;   (local.get $workPlace)
;; )
;;
;; (func
;;   $i32.duplicateTop
;;   (param $element i32)
;;   (result i32)
;;   (result i32)
;;
;;   (local $workPlace i32)
;;
;;   (local.tee $workPlace
;;     (local.get $element)
;;   )
;;
;;   (local.get $workPlace)
;; )


(func
  $i32.upperHalf
  (param $word64 i64)
  (result i32)

  (call $i64.decreaseShift
    (local.get $word64)
    (i64.const 32) ;; 64/2 == 32
  )

  (call $i32.cast_i64)
)
(func
  $TEST_i32.upperHalf

  (local $subject i32)

  (local.set $subject
    (call $i32.upperHalf
      (i64.const 18446744069414584320) ;; 0000000000000000000000000000000011111111111111111111111111111111_2 == 18446744069414584320_10 ;; (little BitEndianess)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (i32.eq
        (local.get $subject)
        (i32.const 4294967295) ;; 11111111111111111111111111111111_2 == 4294967295;; (little BitEndianess)
      )
    )
  #endif
)




(func
  $i32.cast_i64
  (param $lessEnouth i64)
  (result i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (local.get $lessEnouth)
        (i64.const 4294967295) ;; 11111111111111111111111111111111_2 == 4294967295
      )
    )
  #endif

  (i32.wrap_i64
    (local.get $lessEnouth)
  )
)

(func
  $not
  (param $bool i32)
  (result i32)

  ;; 0 == 0 <=> True
  ;; 0 == 1 <=> False
  (call $i32.isZero
    (local.get $bool)
  )
)
(func
  $TEST_not

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (i32.const 1)
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (i32.const 2)
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (i32.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $not
          (i32.const 1)
        )
      )
    )
  #endif
)


;; memory tricks

(func
  $isLastMemoryPageAvailable
  (result i32)

  (call $i32.isEqual
    (call $MAXIMUM_NUMBER_OF_PAGES_IN_ONE_MEMORY)
    (memory.size)
  )
)
#ifdef STONEFISH_DEBUG_TOOLS
  (func
    $test_memsize
  ;;   (export "$test_memsize")
    (call $i32.print
      (memory.size)
    )
    (call $i32.print
      (call $isLastMemoryPageAvailable
      )
    )
  )
#endif



(func $i64.select
  (param $countinFromTop_number_1 i64)
  (param $countinFromTop_number_0 i64)
  (param $digNumber i32)

  (result i64)

  ;; according to the note in https://webassembly.github.io/spec/core/exec/instructions.html#exec-select it might be a good idea to be forward compatible
  ;; although that note does not make any sense, because that would be a breaking change, so i think a new opcode is more likely

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i32.isLessOrEqual
        (local.get $digNumber)
        (i32.const 1)
      )
    )
  #endif

  (select
    (local.get $countinFromTop_number_1)
    (local.get $countinFromTop_number_0)
    (local.get $digNumber)
  )
)


