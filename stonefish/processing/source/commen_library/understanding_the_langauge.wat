(func
  $TEST_understanding_the_language

  (call $TEST_understanding_multireturnValue)
  (call $TEST_understanding_select)
)

(func
  $TEST_understanding_multireturnValue

  (local $place1 i32)
  (local $place2 i64)

  (local.set $place1
    (local.set $place2
      (call $twoReturnValues)
    )
  )

  ;; ->

  (local.set $place2
    (call $twoReturnValues)
  )
  (local.set $place1)

  ;; ->

  (call $twoReturnValues)
  (local.set $place2)
  (local.set $place1)
)

(func
  $twoReturnValues
  (result i32)
  (result i64)

  (i32.const 1)
  (i64.const 2)
)

;;

(func
  $haveIfThenElseReturnSomething

  (if
    (result i64)

    (call $TRUE)
    (then
      (i64.const 0)
    )
    (else
      (i64.const 1)
    )
  )

  (drop)
)


;;



(func
  $TEST_understanding_select

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (select
        ;;(i64.const 8)
        ;;(i64.const 7)
        ;;(i64.const 6)
        ;;(i64.const 5)
        ;;(i64.const 4)
        ;;(i64.const 3)
        ;;(i64.const 2)
          (i64.const 1)
          (i64.const 0)

          (i32.const 0)
        )
        (i64.const 0)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (select
        ;;(i64.const 8)
        ;;(i64.const 7)
        ;;(i64.const 6)
        ;;(i64.const 5)
        ;;(i64.const 4)
        ;;(i64.const 3)
        ;;(i64.const 2)
          (i64.const 1)
          (i64.const 0)

          (i32.const 1)
        )
        (i64.const 1)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $i64.select
          (i64.const 1)
          (i64.const 0)

          (i32.const 1)
        )
        (i64.const 1)
      )
    )
  #endif

)
