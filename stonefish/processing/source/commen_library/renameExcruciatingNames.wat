

;; also known as right shift (>>)
(func
  $i64.decreaseShift
  (param $word64 i64)
  (param $offset i64)
  (result i64)

  (i64.shr_u
    (local.get $word64)
    (local.get $offset)
  )
)

;; also known as left shift (<<)
(func
  $i64.increaseShift
  (param $word64 i64)
  (param $offset i64)
  (result i64)

  (i64.shl
    (local.get $word64)
    (local.get $offset)
  )
)


(func
  $i64.bitwiseAnd
  (param $left i64)
  (param $right i64)
  (result i64)

  (i64.and
    (local.get $left)
    (local.get $right)
  )
)

(func
  $i32.bitwiseAnd
  (param $left i32)
  (param $right i32)
  (result i32)

  (i32.and
    (local.get $left)
    (local.get $right)
  )
)

(func
  $i64.isEqual
  (param $left i64)
  (param $right i64)
  (result i32)

  (i64.eq
    (local.get $left)
    (local.get $right)
  )
)
(func
  $TEST_i64.isEqual

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 100)
        (i64.const 100)
      )
    )
  #endif
)

(func
  $i32.isEqual
  (param $left i32)
  (param $right i32)
  (result i32)

  (i32.eq
    (local.get $left)
    (local.get $right)
  )
)



(func
  $i64.isLessOrEqual
  (param $left i64)
  (param $right i64)
  (result i32)

  (i64.le_u
    (local.get $left)
    (local.get $right)
  )
)
(func
  $TEST_expectation_of_i64.isLessOrEqual

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isLessOrEqual
        (i64.const 0)
        (i64.const 10)
      )
    )
  #endif
)


(func
  $i32.isLessOrEqual
  (param $left i32)
  (param $right i32)
  (result i32)

  (i32.le_u
    (local.get $left)
    (local.get $right)
  )
)


(func
  $i64.isLessThan
  (param $left i64)
  (param $right i64)
  (result i32)

  (i64.lt_u
    (local.get $left)
    (local.get $right)
  )
)
(func
  $i32.isLessThan
  (param $left i32)
  (param $right i32)
  (result i32)

  (i32.lt_u
    (local.get $left)
    (local.get $right)
  )
)
(func
  $f32.isLessThan
  (param $left f32)
  (param $right f32)
  (result i32)

  (f32.lt
    (local.get $left)
    (local.get $right)
  )
)



(func
  $i64.isZero
  (param $value i64)
  (result i32)

  (i64.eqz
    (local.get $value)
  )
)
(func
  $i32.isZero
  (param $value i32)
  (result i32)

  (i32.eqz
    (local.get $value)
  )
)





(func
  $i64.add_wrapped
  (param $augend i64)
  (param $addend i64)
  (result i64)

  (i64.add
    (local.get $augend)
    (local.get $addend)
  )
)
(func
  $i32.add_wrapped
  (param $augend i32)
  (param $addend i32)
  (result i32)

  (i32.add
    (local.get $augend)
    (local.get $addend)
  )
)

(func
  $i64.subtract_wrapped
  (param $minuend i64)
  (param $subtrahent i64)
  (result i64)

  (i64.sub
    (local.get $minuend)
    (local.get $subtrahent)
  )
)
(func
  $i32.subtract_wrapped
  (param $minuend i32)
  (param $subtrahent i32)
  (result i32)

  (i32.sub
    (local.get $minuend)
    (local.get $subtrahent)
  )
)

(func
  $i64.multiplicate_wrapped
  (param $multiplier i64)
  (param $multiplicand i64)
  (result i64)

  (i64.mul
    (local.get $multiplier)
    (local.get $multiplicand)
  )
)
(func
  $i32.multiplicate_wrapped
  (param $multiplier i32)
  (param $multiplicand i32)
  (result i32)

  (i32.mul
    (local.get $multiplier)
    (local.get $multiplicand)
  )
)



(func
  $f32.negate
  (param $unit f32)
  (result f32) ;; inverse
  (f32.neg
    (local.get $unit)
  )
)
