
(start $initialize_runtime_store)



;; it is an invariant of the runtime that the last page is never allocated therfore 65535 for max is ok. 65536 is not ok.
;; see also 2da22c4b1b3b54ff219d3d5b005055f4c5363ec2c6098322fc03c617f5c4c8c4d93b4becf26d07573c77467ba918a5ce8328951439fed764167ca5eedd81b203
;; $MAXIMUM_NUMBER_OF_PAGES_IN_ONE_MEMORY == 65536
;; SYNC 513d9d715c69ba152f9d289ea8e46eacf8c3b958bbed45a7aa5c39f480461a89e5590cd02de3022918515c8205565eb78bf76f96b432db9d9a57cbdd10516dfc
(memory $theOnlyMemory 1 1) ;; min, max

;; (memory $shadowBytes 1 1)

#ifdef STONEFISH_DEBUG_TOOLS_HEAPDUMP
  (export "memory" (memory $theOnlyMemory))
#endif

(func
  $TEST_last_page_is_not_available

    #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isLastMemoryPageAvailable
        )
      )
    )
  #endif
)


(global
  $arrayBegin_Word8Index_inclusive_NodeHeap

  #ifdef STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS
    (export "arrayBegin_Word8Index_inclusive_NodeHeap")
  #endif

  (mut i64)
  (i64.const 0)
)
(global
  $arrayEnd___Word8Index_exclusive_NodeHeap

  #ifdef STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS
    (export "arrayEnd___Word8Index_exclusive_NodeHeap")
  #endif

  (mut i64)
  (i64.const 0)
)


(global
  $arrayBegin_Word8Index_inclusive_FreeSlots

  #ifdef STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS
    (export "arrayBegin_Word8Index_inclusive_FreeSlots")
  #endif

  (mut i64)
  (i64.const 0)
)
(global
  $arrayEnd___Word8Index_exclusive_FreeSlots

  #ifdef STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS
    (export "arrayEnd___Word8Index_exclusive_FreeSlots")
  #endif

  (mut i64)
  (i64.const 0)
)
(global
  ;; TODO rename to $freeSlotsStack_IndexOfNextUnusedElement
  $freeSlotsStack_NextUnusedElement

  #ifdef STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS
    (export "freeSlotsStack_NextUnusedElement")
  #endif

  (mut i64)
  (i64.const 0)
)








(global
  $nextFreeDuplicationLabel

  #ifdef STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS
    (export "nextFreeDuplicationLabel")
  #endif

  (mut i64)

  ;; SYNC 3314e2c4f10be6588711cfa1aa5b4c83395c1c9c9ff285d46720fcfaf96940627266017476eff5e702306970b0490f91f248336e6fbf2928a9132bdcfb23df07
  (i64.const 134217728) ;; 134217728 == 2^27
)











;; TODO compile out of Release mode
;; TODO properly wrap with getters and setters
#ifdef STONEFISH_DEBUG_MODE_ALLOW_FORCING
  (global $allowForceMode (mut i32) (i32.const 0)) ;; (call $FALSE)
  #ifdef STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS
    (export "allowForceMode" (global $allowForceMode))
  #endif
#endif

(func $isForceModeActivated
  (result i32)
  #ifdef STONEFISH_DEBUG_MODE_ALLOW_FORCING
    (global.get $allowForceMode
    )
  #else
    (call $FALSE)
  #endif
)


#ifdef STONEFISH_DEBUG_SPOILTOOL_YOLOMODE
  (global $is_allocation_yolo_mode_activated (mut i32) (i32.const 0)) ;; TODO (call $FALSE)
  (func
    $activate_yolo_mode
    (export "activate_yolo_mode")

    (call $reentrence_detection_prolog
    )

    (global.set $is_allocation_yolo_mode_activated
      (call $TRUE)
    )

    (call $reentrence_detection_epilog
    )
  )
#endif



;; backreference counter
#ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILLEASHES_FOR_FORGOTTEN_BACKREFERENCES_DETECTION
  (global $backrefernces_counter (mut i32) (i32.const 1000000))
  (global $backrefernces_counter_catchNumber i32 (i32.const 0))
;;   (global $backrefernces_counter_catchNumber i32 (i32.const 1000017))
#endif
(func $get_backreference_counter
  (result i32)

  #ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILLEASHES_FOR_FORGOTTEN_BACKREFERENCES_DETECTION
    (global.get $backrefernces_counter)
  #else
    unreachable
  #endif
)
(func $increment_backreference_counter
  #ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILLEASHES_FOR_FORGOTTEN_BACKREFERENCES_DETECTION
    (global.set $backrefernces_counter
      (call $i32.increment
        (global.get $backrefernces_counter)
      )
    )
  #endif
)
(func $catched_leash_number
  (result i32)

  #ifdef STONEFISH_DEBUG_SPOILTOOL_SPOILLEASHES_FOR_FORGOTTEN_BACKREFERENCES_DETECTION
    (call $i32.isEqual
      (global.get $backrefernces_counter)
      (global.get $backrefernces_counter_catchNumber)
    )
  #else
    unreachable
  #endif
)




(global $bookshelf (mut externref) (ref.null extern))
(global
  $nextUnallocatedDuplicationLabel

  #ifdef STONEFISH_DEBUG_TOOLS_DEBUG_INSPECTION_EXPORTS
    (export "nextUnallocatedDuplicationLabel")
  #endif

  (mut i64)

  ;; SYNC 3314e2c4f10be6588711cfa1aa5b4c83395c1c9c9ff285d46720fcfaf96940627266017476eff5e702306970b0490f91f248336e6fbf2928a9132bdcfb23df07
    ;; (number needs to be less than)
  ;; NOTE It is considered to be of benficial to have some unallocated space in the front inorder to improve debug experience
#ifdef STONEFISH_DEBUG_WIGGLE_FIRST_ALLOCATED_DUPLICATION_IDENTIFICATION
  (i64.const 4096)
#else
  (i64.const 0)
#endif
)



#ifdef STONEFISH_DEBUG_DETECT_UNALLOWED_MULTITHREADING_USAGE
  (global $entrence_count (mut i64) (i64.const 0))
#endif
(func $reentrence_detection_prolog
  #ifdef STONEFISH_DEBUG_DETECT_UNALLOWED_MULTITHREADING_USAGE
;;     (call $host.print
;;       (global.get $entrence_count)
;;     )

    (if
      (call $i64.isEqual
        (i64.const 0)
        (global.get $entrence_count)
      )
      (then
        ;; all ok. we are the first time here
      )
      (else
        ;; hm. this is the second time we entered?
        ;; currently this is not considered to work
        ;; TODO?
        (call $unconditional_breakpoint
          (i64.const 201)
        )
        unreachable
      )
    )

    (global.set $entrence_count
      (call $i64.increment
        (global.get $entrence_count)
      )
    )
  #endif
)
(func $reentrence_detection_epilog
  #ifdef STONEFISH_DEBUG_DETECT_UNALLOWED_MULTITHREADING_USAGE
    (if
      (call $i64.isEqual
        (i64.const 1)
        (global.get $entrence_count)
      )
      (then
        ;; all ok. we are the first time here
      )
      (else
        ;; hm. this is is not the first time we entered?
        ;; currently this is not considered to work
        ;; TODO?
        (call $unconditional_breakpoint
          (i64.const 202) ;; TODO
        )
        unreachable
      )
    )

    (global.set $entrence_count
      (call $i64.decrement
        (global.get $entrence_count)
      )
    )
  #endif
)
