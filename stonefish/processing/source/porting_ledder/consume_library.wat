;; TODO these functionality might be replacable with later more advanced functionality


;; unary constructors

;; TODO check parts individually
;; $CONSTRUCTOR_ID_EmptyList....
;; TODO return the parsed Nullary Construcotr header
(func
  $consume_EmptyList
  (param $finger i64)

  ;; TODO introduce a error_if_not function?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $finger)
        (i64.const 2822)
      )
    )
  #endif
)

(func
  $consume_A
  (param $Header i64)

  ;; TODO check parts seperatly

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 3590)
        (local.get $Header)
      )
    )
  #endif
)
(func
  $consume_B
  (param $Header i64)

  ;; TODO check parts seperatly

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (i64.const 3846)
        (local.get $Header)
      )
    )
  #endif
)



(func
  $consume_Intrinsic_number
  (param $number i32)
  (param $finger i64)

  (local $expectedFinger i64)

  (local.set $expectedFinger
    (call $construct_IntrinsicNumber_i32
      (local.get $number)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $expectedFinger)
        (local.get $finger)
      )
    )
  #endif
)


(func $consumeIdentityLambda
  (param $identitiyLambdaFinger i64)

  (local $soloVariableLeash i64)
  (local $lambdaBody_that_is_the_variable i64)

  (local.set $soloVariableLeash
    (call $getSoloVariableLeash_from_LambdaHeader
      (local.get $identitiyLambdaFinger)
    )
  )

  (local.set $lambdaBody_that_is_the_variable
    (call $getLambdaBody
      (local.get $identitiyLambdaFinger)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $extractTheLocationThatTheFingerMarkerIsPointingTo
          (local.get $soloVariableLeash)
        )
        (call $extractNodeLocationWithFingerOffset
          (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
          (local.get $identitiyLambdaFinger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASoloVariable
        (local.get $lambdaBody_that_is_the_variable)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $getAssociatedLambdaNodeLocation_of_SoloVariable
          (local.get $lambdaBody_that_is_the_variable)
        )
        (call $getAssociated_Lambda_NodeLocation
          (local.get $identitiyLambdaFinger)
        )
      )
    )
  #endif

  (call $relinquish_Node_at_Location
    (call $getAssociated_Lambda_NodeLocation
      (local.get $identitiyLambdaFinger)
    )
  )
)


;; TODO remove these functions and replace by the growing desintegrate library
(func
  $consume_Boolean
  (param $root_finger i64)

  (result i32)

  (local $constructorId i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $root_finger)
      )
    )
  #endif

  (local.set $constructorId
    (call $extractConstructorId_from_ConstructorFinger
      (local.get $root_finger)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $FALSE)
      (call $or
        (call $i64.isEqual
          (call $CONSTRUCTOR_ID__False)
          (local.get $constructorId)
        )
      )
      (call $or
        (call $i64.isEqual
          (call $CONSTRUCTOR_ID__True)
          (local.get $constructorId)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_ZERO)
        (call $extractArityFromConstructorFinger
          (local.get $root_finger)
        )
      )
    )
  #endif

  (call $i64.isEqual
    (call $CONSTRUCTOR_ID__True)
    (local.get $constructorId)
  )
)

