

(func
  $consumeExpectedOutput_Rung1
  (param $root_finger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $root_finger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID_Tuple2)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $root_finger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $root_finger)
        )
      )
    )
  #endif

  (call $markUnusedFingersOfConstructorAsUnused
    (local.get $root_finger)
  )

  (call $consume_EmptyList
    (call $getSubFinger_from_birthnumber_and_finger
      (i64.const 0)
      (local.get $root_finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $root_finger)
    )
  )

  (call $consume_EmptyList
    (call $getSubFinger_from_birthnumber_and_finger
      (i64.const 1)
      (local.get $root_finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 1)
      (local.get $root_finger)
    )
  )

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $root_finger)
    )
  )
)



;;;;


(func
  $consumeExpectedOutput_Rung3
  (param $root_finger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $root_finger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID_Tuple2)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $root_finger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $root_finger)
        )
      )
    )
  #endif

  (call $markUnusedFingersOfConstructorAsUnused
    (local.get $root_finger)
  )
  (call $consume_A
    (call $getSubFinger_from_birthnumber_and_finger
      (i64.const 0)
      (local.get $root_finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $root_finger)
    )
  )

  (call $consume_B
    (call $getSubFinger_from_birthnumber_and_finger
      (i64.const 1)
      (local.get $root_finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 1)
      (local.get $root_finger)
    )
  )

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $root_finger)
    )
  )
)







(func
  $consumeExpectedOutput_Rung5
  (param $root_finger i64)

  (local $first_number i32)

  (local.set $first_number
    (call $i32.multiplicate
      (i32.const 2)
;;       (i32.const 2)
      (i32.const 5)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $root_finger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID_Tuple2)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $root_finger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $root_finger)
        )
      )
    )
  #endif

  (call $markUnusedFingersOfConstructorAsUnused
    (local.get $root_finger)
  )

  (call $consume_descending_list
    (local.get $first_number)
    (call $getSubFinger_from_birthnumber_and_finger
      (i64.const 0)
      (local.get $root_finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $root_finger)
    )
  )

  (call $consume_descending_list
    (local.get $first_number)
    (call $getSubFinger_from_birthnumber_and_finger
      (i64.const 1)
      (local.get $root_finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 1)
      (local.get $root_finger)
    )
  )

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $root_finger)
    )
  )
)



(func
  $consume_descending_list
  (param $first_number i32)
  (param $finger i64)

  (local $payload_finger i64)
  (local $restlist_finger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $finger)
      )
    )
  #endif

  (if
    (call $i32.isEqual
      (i32.const 0)
      (local.get $first_number)
    )
    (then
      ;; base case
      (return
        (call $consume_EmptyList
          (local.get $finger)
        )
      )
    )
  )

  ;; recursive case

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_ID_Consecution)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $finger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $finger)
        )
      )
    )
  #endif

  (local.set $payload_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (i64.const 0) ;; first comes payload
      (local.get $finger)
    )
  )
  (local.set $restlist_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (i64.const 1) ;; then comes rest list
      (local.get $finger)
    )
  )


  (call $consume_Intrinsic_number
    (local.get $first_number)
    (local.get $payload_finger)
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $finger)
    )
  )

  (call $consume_descending_list
    (call $i32.decrement
      (call $i32.decrement
        (local.get $first_number)
      )
    )
    (local.get $restlist_finger)
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $finger)
    )
  )

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $finger)
    )
  )

)






(func
  $consumeExpectedOutput_Rung6
  (param $Tuple2Finger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $Tuple2Finger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_TAG)
        (call $extractTagFromFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif

  ;; TODO auto generate constructr name to id table

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif

  (call $consumeIdentityLambda
    (call $getNthConstructorChild
      (i64.const 0)
      (local.get $Tuple2Finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $Tuple2Finger)
    )
  )

  (call $consumeIdentityLambda
    (call $getNthConstructorChild
      (i64.const 1)
      (local.get $Tuple2Finger)
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 1)
      (local.get $Tuple2Finger)
    )
  )

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $Tuple2Finger)
    )
  )
)




(func
  $consumeExpectedOutput_Rung7
  (param $Tuple2Finger i64)

  (local $lambdaFinger_1 i64)
  (local $lambdaFinger_2 i64)
  (local $supposed_lambdaNode_location_1 i64)
  (local $supposed_lambdaNode_location_2 i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $Tuple2Finger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_TAG)
        (call $extractTagFromFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif

  ;; TODO auto generate constructr name to id table

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif


  (local.set $lambdaFinger_1
    (call $getNthConstructorChild
      (i64.const 0)
      (local.get $Tuple2Finger)
    )
  )
  (local.set $supposed_lambdaNode_location_1
    (call $consume_applicable_Tuple2Constructor
      (local.get $lambdaFinger_1)
    )
  )
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $supposed_lambdaNode_location_1)
        (call $extractNodeLocation
          (local.get $lambdaFinger_1)
        )
      )
    )
  #endif
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $Tuple2Finger)
    )
  )

  (local.set $lambdaFinger_2
    (call $getNthConstructorChild
      (i64.const 1)
      (local.get $Tuple2Finger)
    )
  )
  (local.set $supposed_lambdaNode_location_2
    (call $consume_applicable_Tuple2Constructor
      (local.get $lambdaFinger_2)
    )
  )
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $supposed_lambdaNode_location_2)
        (call $extractNodeLocation
          (local.get $lambdaFinger_2)
        )
      )
    )
  #endif
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 1)
      (local.get $Tuple2Finger)
    )
  )

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $Tuple2Finger)
    )
  )
)



(func
  $consume_applicable_Tuple2Constructor
  (param $finger_OuterLambda i64)

  (result i64) ;; $supposed_to_be_lambdaLocation_1

  (local $soloVariableLeash_outer i64)
  (local $lambdaBody_that_is_the_inner_lambda i64)
  (local $location_of_inner_lambda_node i64)

  (local $outwards_LambdaNodeLocation_1 i64) ;; TODO rename to supposed_to_be_outer_lambda_node_location
  (local $outwards_LambdaNodeLocation_2 i64)
  (local $soloVariableLocation_1 i64)

  (local.set $soloVariableLeash_outer
    (call $getSoloVariableLeash_from_LambdaHeader
      (local.get $finger_OuterLambda)
    )
  )

  (local.set $lambdaBody_that_is_the_inner_lambda
    (call $getLambdaBody
      (local.get $finger_OuterLambda)
    )
  )

  (local.set $location_of_inner_lambda_node
    (call $extractNodeLocation
      (local.get $lambdaBody_that_is_the_inner_lambda)
    )
  )

  (local.set $outwards_LambdaNodeLocation_1
    (local.set $outwards_LambdaNodeLocation_2
      (local.set $soloVariableLocation_1
        (call $consume_inner_lambda
          (local.get $lambdaBody_that_is_the_inner_lambda)
        )
      )
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $location_of_inner_lambda_node)
        (local.get $outwards_LambdaNodeLocation_2)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $soloVariableLocation_1)
        (call $extractTheLocationThatTheFingerMarkerIsPointingTo
          (local.get $soloVariableLeash_outer)
        )
      )
    )
  #endif

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $finger_OuterLambda)
    )
  )

  (local.get $outwards_LambdaNodeLocation_1)
)


(func
  $consume_inner_lambda
  (param $innerLambdaFinger i64)

  (result i64) ;; outer lambda node location
  (result i64) ;; inner lambda node location
  (result i64) ;; location of outer SoloVariableOccurence

  (local $soloVariableLeash_2 i64)
  (local $lambdaBody_that_is_a_tuple_of_two_variables i64)

  (local $outwards_LambdaNodeLocation_1 i64)
  (local $outwards_LambdaNodeLocation_2 i64)
  (local $soloVariableLocation_1 i64)
  (local $soloVariableLocation_2 i64)

  (local.set $soloVariableLeash_2
    (call $getSoloVariableLeash_from_LambdaHeader
      (local.get $innerLambdaFinger)
    )
  )

  (local.set $lambdaBody_that_is_a_tuple_of_two_variables
    (call $getLambdaBody
      (local.get $innerLambdaFinger)
    )
  )

  ;; sonsume Tuple with Variables
  (local.set $outwards_LambdaNodeLocation_1
    (local.set $outwards_LambdaNodeLocation_2
      (local.set $soloVariableLocation_1
        (local.set $soloVariableLocation_2
          (call $consume_Tuple2Constructor_with_two_variables
            (local.get $lambdaBody_that_is_a_tuple_of_two_variables)
          )
        )
      )
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $soloVariableLocation_2)
        (call $extractTheLocationThatTheFingerMarkerIsPointingTo
          (local.get $soloVariableLeash_2)
        )
      )
    )
  #endif

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $innerLambdaFinger)
    )
  )

  (local.get $outwards_LambdaNodeLocation_1)
  (local.get $outwards_LambdaNodeLocation_2)
  (local.get $soloVariableLocation_1)
)

(func
  $consume_Tuple2Constructor_with_two_variables
  (param $TwoVariablesTuple i64)

  (result i64)
  (result i64)
  (result i64)
  (result i64)

  (local $lambda_node_of_first_variable i64)
  (local $lambda_node_of_second_variable i64)

  (local $soloVariableLocation_1 i64)
  (local $soloVariableLocation_2 i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_TAG)
        (call $extractTagFromFinger
          (local.get $TwoVariablesTuple)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $TwoVariablesTuple)
        )
      )
    )
  #endif

  ;; TODO auto generate constructr name to id table
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $TwoVariablesTuple)
        )
      )
    )
  #endif



  (local.set $soloVariableLocation_1
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $TwoVariablesTuple)
    )
  )
  (local.set $lambda_node_of_first_variable
    (call $consume_solo_Variable
      (call $getNthConstructorChild
        (i64.const 0)
        (local.get $TwoVariablesTuple)
      )
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 0)
      (local.get $TwoVariablesTuple)
    )
  )

  (local.set $soloVariableLocation_2
    (call $extractNodeLocationWithFingerOffset
      (i64.const 1)
      (local.get $TwoVariablesTuple)
    )
  )
  (local.set $lambda_node_of_second_variable
    (call $consume_solo_Variable
      (call $getNthConstructorChild
        (i64.const 1)
        (local.get $TwoVariablesTuple)
      )
    )
  )
  (call $markAtFingerLocationFingerAsUnused
    (call $extractNodeLocationWithFingerOffset
      (i64.const 1)
      (local.get $TwoVariablesTuple)
    )
  )



  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $TwoVariablesTuple)
    )
  )

  (local.get $lambda_node_of_first_variable)
  (local.get $lambda_node_of_second_variable)
  (local.get $soloVariableLocation_1)
  (local.get $soloVariableLocation_2)
)


(func
  $consume_solo_Variable
  (param $soloVariable i64)

  (result i64) ;; lambda location

  (local $associatedLambdaNode i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASoloVariable
        (local.get $soloVariable)
      )
    )
  #endif

  (local.set $associatedLambdaNode
    (call $getAssociatedLambdaNodeLocation_of_SoloVariable
      (local.get $soloVariable)
    )
  )

  (local.get $associatedLambdaNode)
)



;;


(func
  $consumeExpectedOutput_Rung10
  (param $ParentTuple2Finger i64)

  (local $leftTupleFinger i64)
  (local $rightTupleFinger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $ParentTuple2Finger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_TAG)
        (call $extractTagFromFinger
          (local.get $ParentTuple2Finger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $ParentTuple2Finger)
        )
      )
    )
  #endif

  ;; TODO auto generate constructr name to id table

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $ParentTuple2Finger)
        )
      )
    )
  #endif


  (call $consume_Tuple_with_two_numbers
    (call $getNthConstructorChild
      (i64.const 0)
      (local.get $ParentTuple2Finger)
    )
    (i32.const 4)
    (i32.const 10)
  )

  (call $consume_Tuple_with_two_numbers
    (call $getNthConstructorChild
      (i64.const 1)
      (local.get $ParentTuple2Finger)
    )
    (i32.const 4)
    (i32.const 20)
  )


  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $ParentTuple2Finger)
    )
  )
)


(func
  $consume_Tuple_with_two_numbers
  (param $Tuple2Finger i64)
  (param $left i32)
  (param $right i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $Tuple2Finger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_TAG)
        (call $extractTagFromFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractArityFromConstructorFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif

  ;; TODO auto generate constructr name to id table

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $Tuple2Finger)
        )
      )
    )
  #endif

  (call $consume_Intrinsic_number
    (local.get $left)
    (call $getNthConstructorChild
      (i64.const 0)
      (local.get $Tuple2Finger)
    )
  )

  (call $consume_Intrinsic_number
    (local.get $right)
    (call $getNthConstructorChild
      (i64.const 1)
      (local.get $Tuple2Finger)
    )
  )

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $Tuple2Finger)
    )
  )
)

(func
  $consumeWrappedEmptyList
  (param $wrapperFinger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $wrapperFinger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $CONSTRUCTOR_TAG)
        (call $extractTagFromFinger
          (local.get $wrapperFinger)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_ONE)
        (call $extractArityFromConstructorFinger
          (local.get $wrapperFinger)
        )
      )
    )
  #endif

  ;; TODO auto generate constructr name to id table
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_ONE)
        (call $extractConstructorId_from_ConstructorFinger
          (local.get $wrapperFinger)
        )
      )
    )
  #endif

  (call $consume_EmptyList
    (call $getNthConstructorChild
      (i64.const 0)
      (local.get $wrapperFinger)
    )
  )

  (call $relinquish_Node_at_Location
    (call $extractNodeLocation
      (local.get $wrapperFinger)
    )
  )
)
