module CheckTestCaseOutput where

import System.IO
import System.Process
import Data.List



testcase_prefix :: Testcase_prefix
testcase_prefix = Testcase_prefix "TRIGGER_ASSERT_TEST_"

trapping_testcase_prefix :: Testcase_prefix
trapping_testcase_prefix = Testcase_prefix "TRAPPING_TEST_"




normal_testsuit :: IO ()
normal_testsuit = do
  output <- run_testsuite (WasmFileToTest "stonefishRuntime_debug.wasm")
  check_positive_results output
  check_negtaive_results testcase_prefix output
  check_negtaive_results trapping_testcase_prefix output


  output_noBreakpointTriggered <- run_testsuite (WasmFileToTest "stonefishRuntime_noBreakpointTriggered.wasm")
  check_positive_results output_noBreakpointTriggered
  check_negtaive_results trapping_testcase_prefix output_noBreakpointTriggered


  output_noAssertions <- run_testsuite (WasmFileToTest "stonefishRuntime_noAssertions.wasm")
  check_positive_results output_noAssertions
  check_negtaive_results trapping_testcase_prefix output_noAssertions


  output_someAssertions <- run_testsuite (WasmFileToTest "stonefishRuntime_someAssertions.wasm")
  check_positive_results output_someAssertions
  check_negtaive_results trapping_testcase_prefix output_someAssertions

  output_someOtherAssertions <- run_testsuite (WasmFileToTest "stonefishRuntime_someOtherAssertions.wasm")
  check_positive_results output_someOtherAssertions
  check_negtaive_results trapping_testcase_prefix output_someOtherAssertions

  return ()




newtype WasmFileToTest
  = WasmFileToTest String


newtype RawTestResults
  = RawTestResults String

run_testsuite
  :: WasmFileToTest
  -> IO RawTestResults
run_testsuite
  (WasmFileToTest wasmFilename)
  = do
  putStrLn $ "run testsuite for: " ++ show wasmFilename
  hFlush stdout

  output <- readProcess
    "wasm-interp"
    [ wasmFilename
    , "--run-all-exports"
    , "--host-print"
    , "--dummy-import-func"
    , "--enable-multi-memory"
--     , "--trace"
    ]
    ""

  return $ RawTestResults output


check_negtaive_results
  :: Testcase_prefix
  -> RawTestResults
  -> IO ()
check_negtaive_results
  testcase_prefix
  (RawTestResults output)
  = do

  let negativetestcasesresult
        = didAllNegativeTestCasesTrap
            testcase_prefix
            output

  if fst negativetestcasesresult
    then do
      putStrLn $ " - negativ testsuit seemed to be successfull: " ++ show testcase_prefix
      hFlush stdout
      return ()
    else error $ ""
      ++ "\n\n"
      ++ "negative testsuite did not trap for these testcases: "
      ++ "\n"
      ++ enumeration (snd negativetestcasesresult)
      ++ "\n\n"
      ++ "See: "
      ++ "\n"
      ++ output


check_positive_results
  :: RawTestResults
  -> IO ()
check_positive_results
  (RawTestResults output) = do

  if didTestSuiteRunProperly output
    then do
      putStrLn " - positive tests seemed to be successfull"
      hFlush stdout
      return ()
    else error $ "testsuite did not run through: " ++ output


enumeration = unlines . (map (" - "++)) . (map fst)


prefix :: String
prefix = "TESTING() => i64:"

didTestSuiteRunProperly
  :: String
  -> Bool
didTestSuiteRunProperly
  standardOutput
  = result
  where

    invokations = lines standardOutput

    result
      = (== "0")
      $ interpretRelevantLines
      $ filter (isPrefixOf prefix)
      $ invokations

    interpretRelevantLines
      :: [String]
      -> String
    interpretRelevantLines
      relevant_Lines
      = case relevant_Lines of
          [line] -> drop (length prefix) line --TODO refactor isPrefixOf as a parser
          _ -> do
            error
              $ "not exactly one line with prefix: "
              ++ show prefix
              ++ "\n\nSee:\n"
              ++ (unlines $ invokations)


newtype Testcase_prefix
  = Testcase_prefix String
  deriving Show


didAllNegativeTestCasesTrap
  :: Testcase_prefix
  -> String
  -> (Bool, [(String, Bool)])
didAllNegativeTestCasesTrap
  (Testcase_prefix testcase_prefix)
  standardOutput
  = result
  where
    result
      = (overallBool, allFalingtests)

    allFalingtests = filter (\(name, bool)-> not bool) allNegativeTests

    -- we want at least one negative testcase, to test the negative testcase infrastrucutre
    overallBool = foldr1 (&&) (map snd allNegativeTests)

    allNegativeTests
      = id
      $ map parseNegativeTest
      $ filter beginsWith_NEGATIVE_TEST
      $ lines
      $ standardOutput

    beginsWith_NEGATIVE_TEST = isPrefixOf testcase_prefix

    parseNegativeTest inputStrings = result
      where
        result = (testname, isTestCaseSuccessfull)

        x1 = inputStrings
        x2 = drop (length testcase_prefix) x1
        (testname, x3) = span ('('/=) x2

        isTestCaseSuccessfull = isPrefixOf "() => error:" x3

