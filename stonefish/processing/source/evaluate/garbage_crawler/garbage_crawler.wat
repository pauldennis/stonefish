

(func
  $recursivly_relinquish_term__unbound_time_consuming

  (param $term_to_discarded i64)

  (local $fingerTagCase i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $term_to_discarded)
      )
    )
  #endif

  (local.set $fingerTagCase
    (call $extractTagFromFinger
      (local.get $term_to_discarded)
    )
  )

  ;; TODO find out most performant sorting
  (if
    (call $is_the_tag_a_Constructor
      (local.get $fingerTagCase)
    )

    (then
      (if
        (call $i64.isEqual
          (call $ARITY_OF_ZERO)
          (call $extractArityFromConstructorFinger
            (local.get $term_to_discarded)
          )
        )
        (then
          ;; nothing to do
          (return
          )
        )
        (else
          (call $TODO)
        )
      )
    )
  )

  (if
    (call $is_the_tag_a_leftTwin
      (local.get $fingerTagCase)
    )
    (then
      (return
        (call $deregister_twin_and_relinquish_duplicationNode
          (local.get $term_to_discarded)
        )
      )
    )
  )

  unreachable
)

(func
  $deregister_twin_and_relinquish_duplicationNode

  (param $TwinHeader i64)

  (local $the_other_twin_leash i64)
  (local $the_duplication_body i64)
  (local $DuplicationBody_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALeftTwin
        (local.get $TwinHeader)
      )
    )
  #endif

  (local.set $the_other_twin_leash
    (call $getRightTwinLeash
      (local.get $TwinHeader)
    )
  )

  (local.set $DuplicationBody_Header
    (call $getDuplicationBody
      (local.get $TwinHeader)
    )
  )

  (call $followTwinLeashAnReplaceTwinByFinger
    (local.get $the_other_twin_leash)
    (local.get $DuplicationBody_Header)
  )
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $DuplicationBody_Header)
        )
      )
    )
  #endif

  (call $relinquish_Node_at_Location
    (call $getAssociatedFloatingDuplicationNodeLocation
      (local.get $TwinHeader)
    )
  )

)
