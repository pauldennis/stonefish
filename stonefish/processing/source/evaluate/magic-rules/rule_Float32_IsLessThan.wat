(func
  $Float32_IsLessThan_Rule_LEFT_OPERATOR
  (result i64)
  (i64.const 0)
)
(func
  $Float32_IsLessThan_Rule_RIGHT_OPERATOR
  (result i64)
  (i64.const 1)
)


;; TODO refactor into binary operator prototyp
(func
  $implement_behaviour_of_rule_of_Float32_IsLessThan
  (param $comparision_Header i64)

  (result i64)
  (local $result i64)

  (local $left_argument i64)
  (local $right_argument i64)

  (local $left_argument_inWeakHeadNormalForm i64)
  (local $right_argument_inWeakHeadNormalForm i64)

  (local $type i64)
  (local $result_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $comparision_Header)
      )
    )
  #endif

  ;; first argument

  (local.set $left_argument
    (call $getSubFinger_from_birthnumber_and_finger
      (call $Float32_IsLessThan_Rule_LEFT_OPERATOR)
      (local.get $comparision_Header)
    )
  )
  ;; NOTE this evaluation might change the representation of the right argument
  (local.set $left_argument_inWeakHeadNormalForm
    (call $evaluate_to_weak_head_normal_form
      (local.get $left_argument)
    )
  )
  (block $type_checks
    (local.set $type
      (call $extractTagFromFinger
        (local.get $left_argument_inWeakHeadNormalForm)
      )
    )
    (if
      (call $is_the_tag_an_IntrinsiscNumber
        (local.get $type)
      )
      (then
        (br $type_checks)
      )
    )

    ;; TODO What happens in the case of a superposition?

    ;; TODO this error message lies about the superposition. Is this debilitating?
    (call $runtime_errors.expected_type_x_or_Superposition_but_got_x
      (call $INTRINSIC_NUMBER_TAG)
      (local.get $left_argument_inWeakHeadNormalForm)
    )
    unreachable
  )

  ;; second argument

  ;; NOTE the right argument might have changed by now in the corresponding node in the heap of the operation header
  (local.set $right_argument
    (call $getSubFinger_from_birthnumber_and_finger
      (call $Float32_IsLessThan_Rule_RIGHT_OPERATOR)
      (local.get $comparision_Header)
    )
  )
  (local.set $right_argument_inWeakHeadNormalForm
    (call $evaluate_to_weak_head_normal_form
      (local.get $right_argument)
    )
  )
  (block $type_checks
    (local.set $type
      (call $extractTagFromFinger
        (local.get $right_argument_inWeakHeadNormalForm)
      )
    )
    (if
      (call $is_the_tag_an_IntrinsiscNumber
        (local.get $type)
      )
      (then
        (br $type_checks)
      )
    )

    ;; TODO case of superposition?
    ;; TODO this error message lies about the superposition. Is this debilitating?
    (call $runtime_errors.expected_type_x_or_Superposition_but_got_x
      (call $INTRINSIC_NUMBER_TAG)
      (local.get $left_argument_inWeakHeadNormalForm)
    )
    unreachable
  )


  ;; only relinquish after both arguments are evaluated
  ;; this is because evaluating the first might update a twin as the second argument
  ;; TODO have proper testcase for that, that comments on this issue
  (call $relinquish_Node_behind_Finger
    (local.get $comparision_Header)
  )

  (local.set $result_Header
    (if
      (result i64)

      (call $f32.isLessThan
        (call $f32.disaggregate_IntrinsicNumber
          (local.get $left_argument_inWeakHeadNormalForm)
        )
        (call $f32.disaggregate_IntrinsicNumber
          (local.get $right_argument_inWeakHeadNormalForm)
        )
      )

      ;; TODO these constructor ids are magic?
      ;; TODO how to handle them?
      ;; TODO haskell returning an integer here, what should we be doing here?
      (then
        (call $compose_nullary_constructor_node
          (call $CONSTRUCTOR_ID__True)
        )
      )
      (else
        (call $compose_nullary_constructor_node
          (call $CONSTRUCTOR_ID__False)
        )
      )
    )
  )

  (local.get $result_Header)

)

