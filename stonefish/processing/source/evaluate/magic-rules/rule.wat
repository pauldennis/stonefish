


;; TODO place?
;; TODO name ? only for unary Rules?
;; TODO rename to: move_unaryRule_calling_into_superposition
(func
  $move_rule_calling_into_superposition
  (param $ruleId i64)
  (param $superimposed_Header i64)

  (result i64)

  (local $new_left_RuleCallNode_SuperimposedCall_Location i64)
  (local $new_right_RuleCallNode_SuperimposedCall_Location i64)
  (local $new_Superposition_Location i64)

  (local $spreading_duplicationLabel i64)
  (local $leftSuperImposed_InterfaceCallResult_Header i64)
  (local $rightSuperImposed_InterfaceCallResult_Header i64)

  (local $new_leftSuperimposedRuleCall_Header i64)
  (local $new_rightSuperimposedRuleCall_Header i64)

  (local $new_superposition_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $superimposed_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidRuleId
        (local.get $ruleId)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_ONE)
        (call $getArityFromRuleId
          (local.get $ruleId)
        )
      )
    )
  #endif

  ;; new locations
  (local.set $new_left_RuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_right_RuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_Superposition_Location
    (call $request_Node__might_trap
    )
  )

  (local.set $spreading_duplicationLabel
    (local.set $leftSuperImposed_InterfaceCallResult_Header
      (local.set $rightSuperImposed_InterfaceCallResult_Header
        (call $disaggregate_Superposition
          (local.get $superimposed_Header)
        )
      )
    )
  )

  ;; two Rules
  (call $construct_at_location_new_SingletonRuleCall
    (local.get $new_left_RuleCallNode_SuperimposedCall_Location)

    (local.get $leftSuperImposed_InterfaceCallResult_Header)
  )
  (call $construct_at_location_new_SingletonRuleCall
    (local.get $new_right_RuleCallNode_SuperimposedCall_Location)

    (local.get $rightSuperImposed_InterfaceCallResult_Header)
  )
  (local.set $new_leftSuperimposedRuleCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (local.get $ruleId)
      (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
    )
  )
  (local.set $new_rightSuperimposedRuleCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (local.get $ruleId)
      (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
    )
  )

  ;; new superposition Node
  (call $construct_at_location_new_superposition_node
    (local.get $new_Superposition_Location)
    (local.get $new_leftSuperimposedRuleCall_Header)
    (local.get $new_rightSuperimposedRuleCall_Header)
  )
  (local.set $new_superposition_Header
    (call $construct_SuperpositionHeader
      (local.get $spreading_duplicationLabel)
      (local.get $new_Superposition_Location)
    )
  )

  (local.get $new_superposition_Header)
)





;; TODO place as well?
(func
  $construct_at_location_new_SingletonRuleCall
  (param $node_Location i64)

  (param $singleArgument_Header i64)


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $node_Location)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $node_Location)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $singleArgument_Header)
      )
    )
  #endif


  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $RULE_ARGUMENT_INDEX_SINGLETON)
    )
    (local.get $singleArgument_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $node_Location)
        (call $RULE_ARGUMENT_INDEX_SINGLETON)
      )
    )
    (local.get $singleArgument_Header)
  )
)




;;;;




(func $RULE_ARGUMENT_INDEX_SINGLETON
  (result i64)
  (i64.const 0)
)


(func $RULE_ARGUMENT_INDEX_BINARY_LEFT
  (result i64)
  (i64.const 0)
)
(func $RULE_ARGUMENT_INDEX_BINARY_RIGHT
  (result i64)
  (i64.const 1)
)
