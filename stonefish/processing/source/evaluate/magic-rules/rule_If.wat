(func
  $If_Rule_PREDICATE_POSITION
  (result i64)
  (i64.const 0)
)
(func
  $If_Rule_TRUECASE_POSITION
  (result i64)
  (i64.const 1)
)
(func
  $If_Rule_FALSECASE_POSITION
  (result i64)
  (i64.const 2)
)

(func
  $construct_at_location_new_IfRuleCall
  (param $node_Location i64)

  (param $predicate_Header i64)
  (param $trueCase_Header i64)
  (param $falseCase_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $node_Location)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $node_Location)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $predicate_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $trueCase_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $falseCase_Header)
      )
    )
  #endif

  ;; TODO dedicated functions for that ?
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $If_Rule_PREDICATE_POSITION)
    )
    (local.get $predicate_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $node_Location)
        (call $If_Rule_PREDICATE_POSITION)
      )
    )
    (local.get $predicate_Header)
  )

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $If_Rule_TRUECASE_POSITION)
    )
    (local.get $trueCase_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $node_Location)
        (call $If_Rule_TRUECASE_POSITION)
      )
    )
    (local.get $trueCase_Header)
  )

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $node_Location)
      (call $If_Rule_FALSECASE_POSITION)
    )
    (local.get $falseCase_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $node_Location)
        (call $If_Rule_FALSECASE_POSITION)
      )
    )
    (local.get $falseCase_Header)
  )

)


(func
  $implement_behaviour_of_rule_of_if
  (param $IfCallHeader i64)

  (result i64)

  (local $predicate_Header_in_weak_head_normal_form i64)
  (local $true_case_Header i64)
  (local $false_case_Header i64)

  (local $predicate_tag i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $IfCallHeader)
      )
    )
  #endif

  (local.set $predicate_Header_in_weak_head_normal_form
    (call $evaluate_to_weak_head_normal_form
      (call $getSubFinger_from_birthnumber_and_finger
        (call $If_Rule_PREDICATE_POSITION)
        (local.get $IfCallHeader)
      )
    )
  )
  (local.set $true_case_Header
    (call $getSubFinger_from_birthnumber_and_finger
      (call $If_Rule_TRUECASE_POSITION)
      (local.get $IfCallHeader)
    )
  )
  (local.set $false_case_Header
    (call $getSubFinger_from_birthnumber_and_finger
      (call $If_Rule_FALSECASE_POSITION)
      (local.get $IfCallHeader)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $true_case_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $false_case_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $predicate_Header_in_weak_head_normal_form)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $gotTermReduced
        (local.get $predicate_Header_in_weak_head_normal_form)
      )
    )
  #endif
  ;; TODO DEEP_FORCE_SUPPORT
;;   (if
;;     (call $isFingerAWeakHead
;;       (local.get $predicate_Header_in_weak_head_normal_form)
;;     )
;;     (then
;;     )
;;     (else
;;       (return
;;         (local.get $IfCallHeader)
;;       )
;;     )
;;   )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $predicate_Header_in_weak_head_normal_form)
      )
    )
  #endif

  ;; TODO review memory call code
  ;; happens after the possible hacky return above
  (call $relinquish_Node_behind_Finger
    (local.get $IfCallHeader)
  )

  (local.set $predicate_tag
    (call $extractTagFromFinger
      (local.get $predicate_Header_in_weak_head_normal_form)
    )
  )

  (if
    (call $is_the_tag_a_Constructor
      (local.get $predicate_tag)
    )

    (then
      (return
        (call $rule_if_on_constructor
          (local.get $predicate_Header_in_weak_head_normal_form)
          (local.get $false_case_Header)
          (local.get $true_case_Header)
        )
      )
    )
  )

  (if
    (call $is_the_tag_a_Superposition
      (local.get $predicate_tag)
    )

    (then
      (return
        (call $rule_if_on_superposition
          (local.get $predicate_Header_in_weak_head_normal_form)
          (local.get $false_case_Header)
          (local.get $true_case_Header)
        )
      )
    )
  )

  unreachable
)


(func
  $rule_if_on_constructor
  (param $predicate_ConstructorHeader i64)
  (param $false_case_Header i64)
  (param $true_case_Header i64)

  (result i64)

  (local $predicate_as_webassembly_bool i32)
  (local $result_term i64)

  ;; TODO who is responsible for type safty?
  ;; depending on the answer there has to be a type error message or there might be an unreachable
  (local.set $predicate_as_webassembly_bool
    (call $consume_Boolean
      (local.get $predicate_ConstructorHeader)
    )
  )

  (local.set $result_term
    (if
      (result i64)

      (local.get $predicate_as_webassembly_bool)

      (then
        (call $recursivly_relinquish_term__unbound_time_consuming
          (local.get $false_case_Header)
        )
        (local.get $true_case_Header)
      )
      (else
        (call $recursivly_relinquish_term__unbound_time_consuming
          (local.get $true_case_Header)
        )
        (local.get $false_case_Header)
      )
    )
  )

  (call $evaluate_to_weak_head_normal_form
    (local.get $result_term)
  )
)

(func
  $rule_if_on_superposition
  (param $superimposedPredicate_Header i64)
  (param $false_case_Header i64)
  (param $true_case_Header i64)

  (result i64)

  (local $new_DuplicationNode_for_TrueCase_Location i64)
  (local $new_DuplicationNode_for_FalseCase_Location i64)
  (local $new_left_IfRuleCallNode_SuperimposedCall_Location i64)
  (local $new_right_IfRuleCallNode_SuperimposedCall_Location i64)
  (local $new_Superposition_Location i64)

  (local $spreading_duplicationLabel i64)
  (local $leftSuperImposed_Predicate_Header i64)
  (local $rightSuperImposed_Predicate_Header i64)

  (local $leftLeash_TrueCase i64)
  (local $rightLeash_TrueCase i64)
  (local $leftTwin_TrueCase i64)
  (local $rightTwin_TrueCase i64)

  (local $leftLeash_FalseCase i64)
  (local $rightLeash_FalseCase i64)
  (local $leftTwin_FalseCase i64)
  (local $rightTwin_FalseCase i64)

  (local $new_leftSuperimposedIfCall_Header i64)
  (local $new_rightSuperimposedIfCall_Header i64)

  (local $new_superposition_Header i64)

  ;; new locations
  (local.set $new_DuplicationNode_for_TrueCase_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_DuplicationNode_for_FalseCase_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_left_IfRuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_right_IfRuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_Superposition_Location
    (call $request_Node__might_trap
    )
  )

  ;; TODO should this stuff be combined?
    ;; Yes i thinks so!
  (local.set $spreading_duplicationLabel
    (call $extract_DuplicationLabel_fromSuperpositionHeader
      (local.get $superimposedPredicate_Header)
    )
  )
  (local.set $leftSuperImposed_Predicate_Header
    (call $getLeftSuperimposedHeader
      (local.get $superimposedPredicate_Header)
    )
  )
  (local.set $rightSuperImposed_Predicate_Header
    (call $getRightSuperimposedHeader
      (local.get $superimposedPredicate_Header)
    )
  )
  (call $relinquish_Node_behind_Finger
    (local.get $superimposedPredicate_Header)
  )


  ;; duplication TrueCase
  (local.set $leftLeash_TrueCase
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_left_IfRuleCallNode_SuperimposedCall_Location)
      (call $If_Rule_TRUECASE_POSITION)
    )
  )
  (local.set $rightLeash_TrueCase
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_right_IfRuleCallNode_SuperimposedCall_Location)
      (call $If_Rule_TRUECASE_POSITION)
    )
  )
  (local.set $leftTwin_TrueCase
    (call $construct_TwinHeader
      (call $leftTwinVariant)
      (local.get $spreading_duplicationLabel)
      (local.get $new_DuplicationNode_for_TrueCase_Location)
    )
  )
  (local.set $rightTwin_TrueCase
    (call $construct_TwinHeader
      (call $rightTwinVariant)
      (local.get $spreading_duplicationLabel)
      (local.get $new_DuplicationNode_for_TrueCase_Location)
    )
  )
  (call $construct_at_location_new_duplication_node
    (local.get $new_DuplicationNode_for_TrueCase_Location)
    (local.get $leftLeash_TrueCase)
    (local.get $rightLeash_TrueCase)
    (local.get $true_case_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $new_DuplicationNode_for_TrueCase_Location)
        (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
      )
    )
    (local.get $true_case_Header)
  )

  ;; duplication FalseCase
  (local.set $leftLeash_FalseCase
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_left_IfRuleCallNode_SuperimposedCall_Location)
      (call $If_Rule_FALSECASE_POSITION)
    )
  )
  (local.set $rightLeash_FalseCase
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_right_IfRuleCallNode_SuperimposedCall_Location)
      (call $If_Rule_FALSECASE_POSITION)
    )
  )
  (local.set $leftTwin_FalseCase
    (call $construct_TwinHeader
      (call $leftTwinVariant)
      (local.get $spreading_duplicationLabel)
      (local.get $new_DuplicationNode_for_FalseCase_Location)
    )
  )
  (local.set $rightTwin_FalseCase
    (call $construct_TwinHeader
      (call $rightTwinVariant)
      (local.get $spreading_duplicationLabel)
      (local.get $new_DuplicationNode_for_FalseCase_Location)
    )
  )
  (call $construct_at_location_new_duplication_node
    (local.get $new_DuplicationNode_for_FalseCase_Location)
    (local.get $leftLeash_FalseCase)
    (local.get $rightLeash_FalseCase)
    (local.get $false_case_Header)
  )
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $false_case_Header)
        )
      )
    )
  #endif

  ;; two If Rules
  (call $construct_at_location_new_IfRuleCall
    (local.get $new_left_IfRuleCallNode_SuperimposedCall_Location)

    (local.get $leftSuperImposed_Predicate_Header)
    (local.get $leftTwin_TrueCase)
    (local.get $leftTwin_FalseCase)
  )

  (call $construct_at_location_new_IfRuleCall
    (local.get $new_right_IfRuleCallNode_SuperimposedCall_Location)

    (local.get $rightSuperImposed_Predicate_Header)
    (local.get $rightTwin_TrueCase)
    (local.get $rightTwin_FalseCase)
  )

  (local.set $new_leftSuperimposedIfCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (call $MAGIC_RULE_ID_If)
      (local.get $new_left_IfRuleCallNode_SuperimposedCall_Location)
    )
  )
  (local.set $new_rightSuperimposedIfCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (call $MAGIC_RULE_ID_If)
      (local.get $new_right_IfRuleCallNode_SuperimposedCall_Location)
    )
  )

  ;; new superposition Node
  (call $construct_at_location_new_superposition_node
    (local.get $new_Superposition_Location)
    (local.get $new_leftSuperimposedIfCall_Header)
    (local.get $new_rightSuperimposedIfCall_Header)
  )
  (local.set $new_superposition_Header
    (call $construct_SuperpositionHeader
      (local.get $spreading_duplicationLabel)
      (local.get $new_Superposition_Location)
    )
  )

  (local.get $new_superposition_Header)
)
