
(func
  $implement_behaviour_of_rule_of_encapsulation
  (param $EncapsulationCallHeader i64)

  (result i64)

  (local $encapsulationHeader i64)


  ;; TODO do we have to check that the arity is correct? what if an argument is missing?

  ;; make sure we do not overflow the tag by incrementing

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (call $RULECALL_TAG)
          (i64.const 15) ;; 1111_2 == 2^4 - 1
        )
      )
    )
  #endif

  ;; by chance the tag where layout in a way that we can simply add one to get the desired tag
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $i64.increment
          (call $RULECALL_TAG)
        )
        (call $OPAQUE_OBJECT_TAG)
      )
    )
  #endif

  (local.set $encapsulationHeader
    (call $i64.increment
      (local.get $EncapsulationCallHeader)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerACapsule
        (local.get $encapsulationHeader)
      )
    )
  #endif

  (local.get $encapsulationHeader)
)
