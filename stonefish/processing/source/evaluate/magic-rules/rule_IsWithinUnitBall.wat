
;; TODO delete this function and replace with user provided rules


(func
  $IsWithinUnitBall_Rule_VECTOR3DPOSITION
  (result i64)
  (i64.const 0)
)

(func
  $implement_behaviour_of_rule_of_IsWithinUnitBall
  (param $FirstCustomRuleHeader i64)

  (result i64)
  (local $result i64)

  (local $vector3D_argument_finger i64)
  (local $vector3d_Header_in_weak_head_normal_form i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $FirstCustomRuleHeader)
      )
    )
  #endif

  (local.set $vector3D_argument_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (call $IsWithinUnitBall_Rule_VECTOR3DPOSITION)
      (local.get $FirstCustomRuleHeader)
    )
  )

  (call $relinquish_Node_behind_Finger
    (local.get $FirstCustomRuleHeader)
  )

  (local.set $vector3d_Header_in_weak_head_normal_form
    (call $evaluate_to_weak_head_normal_form
      (local.get $vector3D_argument_finger)
    )
  )

  (local.set $result
    (block $break
      (result i64)

      (if
        (call $isFingerASuperposition
          (local.get $vector3d_Header_in_weak_head_normal_form)
        )
        (then
          (br $break
            (call $move_rule_calling_into_superposition
              (call $MAGIC_RULE_ID_IsWithinUnitBall)
              (local.get $vector3d_Header_in_weak_head_normal_form)
            )
          )
        )
      )

      (if
        (call $isType_Vector3D
          (local.get $vector3d_Header_in_weak_head_normal_form)
        )
        (then
          (br $break
            (call $grow_graph_case_for_IsWithinUnitBall
              (local.get $vector3d_Header_in_weak_head_normal_form)
            )
          )
        )
      )

      (call $runtime_errors.expected_type_x_and_id_x_or_Superposition_but_got_x
        (call $CONSTRUCTOR_TAG)
        (call $CONSTRUCTOR_Vector3D_ID)
        (local.get $vector3d_Header_in_weak_head_normal_form)
      )
      unreachable
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)



(func
  $grow_graph_case_for_IsWithinUnitBall
  (param $vector_Header i64)

  (result i64)

  (local $x0 i64)
  (local $x1 i64)
  (local $x2 i64)

  (local $mapping i64)
  (local $the_root i64)
  (local $the_melee externref)
  (local $archetype externref)
  (local $ConstructorId i64)

  (local $rightSideLambda i64)
  (local $big_replacements i64)
  (local $further_reduced_result i64)

  ;; TODO this is too rigid? Currently there is no typesafty, thgat menas we would have to error out in case it is not a Vector3D.
  ;; we are in the grow case
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isType_Vector3D
        (local.get $vector_Header)
      )
    )
  #endif

  (local.set $ConstructorId
    (call $extractConstructorId_from_ConstructorFinger
      (local.get $vector_Header)
    )
  )

  (local.set $x0
    (local.set $x1
      (local.set $x2
        (call $disaggregate_Vector3D
          (local.get $vector_Header)
        )
      )
    )
  )

  (local.set $mapping
    (local.set $the_root
      (local.set $the_melee
        (call $install_fragments.extractMyRuleRightSide
          (call $get_bookshelf)
          (call $MAGIC_RULE_ID_IsWithinUnitBall)
          (local.get $ConstructorId)
        )
      )
    )
  )
  (local.set $archetype
    (call $link.constructArchetype
      (local.get $the_melee)
    )
  )

  (local.set $rightSideLambda
    (call $raw_link
      (local.get $mapping)
      (local.get $the_root)
      (local.get $archetype)
    )
  )

  (local.set $big_replacements
    (call $compose_application_node
      (call $compose_application_node
        (call $compose_application_node
          (local.get $rightSideLambda)
          (local.get $x0)
        )
        (local.get $x1)
      )
      (local.get $x2)
    )
  )

  (local.set $further_reduced_result
    (call $evaluate_to_weak_head_normal_form
      (local.get $big_replacements)
    )
  )

  (local.get $further_reduced_result)
)


