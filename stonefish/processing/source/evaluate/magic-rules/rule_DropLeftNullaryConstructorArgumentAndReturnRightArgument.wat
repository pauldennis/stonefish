(func
  $DropNewStateAndJustUseCapsuleCallResult_Rule_TO_BE_DROPED_Position
  (result i64)
  (i64.const 0)
)
(func
  $DropNewStateAndJustUseCapsuleCallResult_Rule_DROP_BEFORE_USING_THIS_Position
  (result i64)
  (i64.const 1)
)

(func
  $implement_behaviour_of_rule_of_DropLeftNullaryConstructorArgumentAndReturnRightArgument
  (param $rule_Header i64)

  (result i64)

  (local $argument_to_be_dropped i64)
  (local $dropping_happens_when_this_is_accesed i64)

  (local $tagCase i64)

  ;; the rule header

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $rule_Header)
      )
    )
  #endif

  ;; we implement the correct rule

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $MAGIC_RULE_ID_DropLeftNullaryConstructorArgumentAndReturnRightArgument)
        (call $extract_RuleIdentification_fromRuleHeader
          (local.get $rule_Header)
        )
      )
    )
  #endif

  ;; the two arguments
  (local.set $argument_to_be_dropped
    (call $getSubFinger_from_birthnumber_and_finger
      (call $DropNewStateAndJustUseCapsuleCallResult_Rule_TO_BE_DROPED_Position)
      (local.get $rule_Header)
    )
  )
  (local.set $dropping_happens_when_this_is_accesed
    (call $getSubFinger_from_birthnumber_and_finger
      (call $DropNewStateAndJustUseCapsuleCallResult_Rule_DROP_BEFORE_USING_THIS_Position)
      (local.get $rule_Header)
    )
  )

  ;; both arguments are strict
    ;; TODO are they? wouldnt that mean that the dropped expression is always evaluated?

  ;; the argument is supposed to be of a certain type

  ;; TODO is it realy enouth to have asserts here?
    ;; TODO No! I dont think so!
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $FALSE)
      (call $or
        (call $isFingerAConstructor
          (local.get $argument_to_be_dropped)
        )
      )
      (call $or
        (call $isFingerASuperposition
          (local.get $argument_to_be_dropped)
        )
      )
      (call $or
        (call $isFingerALeftTwin
          (local.get $argument_to_be_dropped)
        )
      )
      (call $or
        (call $isFingerAnIntrinsiscNumber
          (local.get $argument_to_be_dropped)
        )
      )
    )
  #endif

  (call $relinquish_Node_behind_Finger
    (local.get $rule_Header)
  )

  ;; trying out doing this very early
  (local.set $tagCase
    (call $extractTagFromFinger
      (local.get $argument_to_be_dropped)
    )
  )

  (if
    (call $is_the_tag_a_Superposition
      (local.get $tagCase)
    )

    (then
      (return
        (call $handle_SuperpositionCase_for_dropRule
          (local.get $argument_to_be_dropped)
          (local.get $dropping_happens_when_this_is_accesed)
        )
      )
    )
  )

  ;; TODO remove returns as they prevent bug detection
  (if
    (call $is_the_tag_a_Constructor
      (local.get $tagCase)
    )

    (then
      (return
        (call $handle_ConstructorCase_for_DropRule
          (local.get $argument_to_be_dropped)
          (local.get $dropping_happens_when_this_is_accesed)
        )
      )
    )
  )

  (if
    (call $is_the_tag_a_leftTwin
      (local.get $tagCase)
    )

    (then
      (return
        (call $handle_ConstructorCase_for_LeftTwin
          (local.get $argument_to_be_dropped)
          (local.get $dropping_happens_when_this_is_accesed)
        )
      )
    )
  )

  (if
    (call $is_the_tag_an_IntrinsiscNumber
      (local.get $tagCase)
    )

    (then
      (return
        (call $handle_IntrinsicNumber_for_DropRule
          (local.get $argument_to_be_dropped)
          (local.get $dropping_happens_when_this_is_accesed)
        )
      )
    )
  )

  ;; TODO refactor out continue evaluation

  unreachable
)

(func
  $handle_ConstructorCase_for_LeftTwin
  (param $leftTwin i64)
  (param $dropping_happens_when_this_is_accesed i64)
  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALeftTwin
        (local.get $leftTwin)
      )
    )
  #endif

  (call $deregister_twin_and_relinquish_duplicationNode
    (local.get $leftTwin)
  )

  (call $evaluate_to_weak_head_normal_form
    (local.get $dropping_happens_when_this_is_accesed)
  )
)


(func
  $handle_SuperpositionCase_for_dropRule

  (param $superimposed_Argument_to_be_dropped_Header i64)
  (param $dropping_happens_when_this_is_accesed i64)

  (result i64)

  (local $new_DuplicationNode_for_SecondArgument_Location i64)
  (local $new_left_RuleCallNode_SuperimposedCall_Location i64)
  (local $new_right_RuleCallNode_SuperimposedCall_Location i64)
  (local $new_Superposition_Location i64)

  (local $spreading_duplicationLabel i64)
  (local $leftSuperImposed_toBeDropped_Header i64)
  (local $rightSuperImposed_ToBeDropped_Header i64)

  (local $leftLeash_SecondArgument i64)
  (local $rightLeash_SecondArgument i64)
  (local $leftTwin_SecondArgument i64)
  (local $rightTwin_SecondArgument i64)

  (local $new_leftSuperimposedRuleCall_Header i64)
  (local $new_rightSuperimposedRuleCall_Header i64)

  (local $new_superposition_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $superimposed_Argument_to_be_dropped_Header)
      )
    )
  #endif

  ;; new locations
  (local.set $new_DuplicationNode_for_SecondArgument_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_left_RuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_right_RuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_Superposition_Location
    (call $request_Node__might_trap
    )
  )

  ;; TODO extract into superpositionParser
  (local.set $spreading_duplicationLabel
    (call $extract_DuplicationLabel_fromSuperpositionHeader
      (local.get $superimposed_Argument_to_be_dropped_Header)
    )
  )
  (local.set $leftSuperImposed_toBeDropped_Header
    (call $getLeftSuperimposedHeader
      (local.get $superimposed_Argument_to_be_dropped_Header)
    )
  )
  (local.set $rightSuperImposed_ToBeDropped_Header
    (call $getRightSuperimposedHeader
      (local.get $superimposed_Argument_to_be_dropped_Header)
    )
  )
  (call $relinquish_Node_behind_Finger
    (local.get $superimposed_Argument_to_be_dropped_Header)
  )

  ;; duplication of SecondArgument
  (local.set $leftLeash_SecondArgument
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
      (call $DropNewStateAndJustUseCapsuleCallResult_Rule_DROP_BEFORE_USING_THIS_Position)
    )
  )
  (local.set $rightLeash_SecondArgument
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
      (call $DropNewStateAndJustUseCapsuleCallResult_Rule_DROP_BEFORE_USING_THIS_Position)
    )
  )
  (local.set $leftTwin_SecondArgument
    (call $construct_TwinHeader
      (call $leftTwinVariant)
      (local.get $spreading_duplicationLabel)
      (local.get $new_DuplicationNode_for_SecondArgument_Location)
    )
  )
  (local.set $rightTwin_SecondArgument
    (call $construct_TwinHeader
      (call $rightTwinVariant)
      (local.get $spreading_duplicationLabel)
      (local.get $new_DuplicationNode_for_SecondArgument_Location)
    )
  )
  (call $construct_at_location_new_duplication_node
    (local.get $new_DuplicationNode_for_SecondArgument_Location)
    (local.get $leftLeash_SecondArgument)
    (local.get $rightLeash_SecondArgument)
    (local.get $dropping_happens_when_this_is_accesed)
  )
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $dropping_happens_when_this_is_accesed)
        )
      )
    )
  #endif

  ;; two spliced Rule
  (call $construct_at_location_new_DropLeftNullaryConstructorArgumentAndReturnRightArgument_RuleCall
    (local.get $new_left_RuleCallNode_SuperimposedCall_Location)

    (local.get $leftSuperImposed_toBeDropped_Header)
    (local.get $leftTwin_SecondArgument)
  )
  (call $construct_at_location_new_DropLeftNullaryConstructorArgumentAndReturnRightArgument_RuleCall
    (local.get $new_right_RuleCallNode_SuperimposedCall_Location)

    (local.get $rightSuperImposed_ToBeDropped_Header)
    (local.get $rightTwin_SecondArgument)
  )
  (local.set $new_leftSuperimposedRuleCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (call $MAGIC_RULE_ID_DropLeftNullaryConstructorArgumentAndReturnRightArgument)
      (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
    )
  )
  (local.set $new_rightSuperimposedRuleCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (call $MAGIC_RULE_ID_DropLeftNullaryConstructorArgumentAndReturnRightArgument)
      (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
    )
  )

  ;; new superposition Node
  (call $construct_at_location_new_superposition_node
    (local.get $new_Superposition_Location)
    (local.get $new_leftSuperimposedRuleCall_Header)
    (local.get $new_rightSuperimposedRuleCall_Header)
  )
  (local.set $new_superposition_Header
    (call $construct_SuperpositionHeader
      (local.get $spreading_duplicationLabel)
      (local.get $new_Superposition_Location)
    )
  )

  (local.get $new_superposition_Header)
)




(func
  $construct_at_location_new_DropLeftNullaryConstructorArgumentAndReturnRightArgument_RuleCall
  (param $node_Location i64)

  (param $to_be_dropped_Header i64)
  (param $dropping_when_this_is_evaluated_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $node_Location)
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $node_Location)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $to_be_dropped_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $dropping_when_this_is_evaluated_Header)
      )
    )
  #endif

  (call $replace_at_NodeLocation_and_SubFingerOffset_with_Finger
    (local.get $node_Location)
    (call $DropNewStateAndJustUseCapsuleCallResult_Rule_TO_BE_DROPED_Position)
    (local.get $to_be_dropped_Header)
  )
  ;; TODO this way of doing repairs is bogus!!!
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $node_Location)
        (call $DropNewStateAndJustUseCapsuleCallResult_Rule_TO_BE_DROPED_Position)
      )
    )
    (local.get $to_be_dropped_Header)
  )

  (call $replace_at_NodeLocation_and_SubFingerOffset_with_Finger
    (local.get $node_Location)
    (call $DropNewStateAndJustUseCapsuleCallResult_Rule_DROP_BEFORE_USING_THIS_Position)
    (local.get $dropping_when_this_is_evaluated_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $node_Location)
        (call $DropNewStateAndJustUseCapsuleCallResult_Rule_DROP_BEFORE_USING_THIS_Position)
      )
    )
    (local.get $dropping_when_this_is_evaluated_Header)
  )

)



(func
  $handle_ConstructorCase_for_DropRule

  (param $firstArgument i64)
  (param $secondArgument i64)

  (result i64)

  ;; TODO decide on naming convention NullaryConstructor or COnstructorLeaf
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerANullaryConstructor
        (local.get $firstArgument)
      )
    )
  #endif

  ;; nothing to do for leafs

  (call $evaluate_to_weak_head_normal_form
    (local.get $secondArgument)
  )
)


(func
  $handle_IntrinsicNumber_for_DropRule

  (param $firstArgument i64)
  (param $secondArgument i64)

  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscNumber
        (local.get $firstArgument)
      )
    )
  #endif

  ;; nothing to do for intrinsic numbers

  (call $evaluate_to_weak_head_normal_form
    (local.get $secondArgument)
  )
)
