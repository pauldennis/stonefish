
(func
  $WebSocketMessageTrigger_Rule_PASSED_BOOL_ARGUMENT
  (result i64)
  (i64.const 0)
)

;; TODO better name?
;; "BATON"
(func
  $WebSocketMessageTrigger_Rule_GIVEBACK_ARGUMENT
  (result i64)
  (i64.const 1)
)



(func
  $implement_behaviour_of_rule_of_MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule
  (param $MagicRuleHeader i64)

  (result i64)
  (local $result i64)

  (local $bool_message_finger i64)
  (local $bool_message_finger_in_weak_head_normal_form i64)
  (local $giveback_argument_finger i64)

  (local $giveback_Header_in_weak_head_normal_form i64)

  (local $intermediat_Node_Location i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $MagicRuleHeader)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $MAGIC_RULE_ARITY_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule)
      )
    )
  #endif

  (local.set $bool_message_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (call $WebSocketMessageTrigger_Rule_PASSED_BOOL_ARGUMENT)
      (local.get $MagicRuleHeader)
    )
  )
  (local.set $giveback_argument_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (call $WebSocketMessageTrigger_Rule_GIVEBACK_ARGUMENT)
      (local.get $MagicRuleHeader)
    )
  )
  (call $relinquish_Node_behind_Finger
    (local.get $MagicRuleHeader)
  )


;; TODO in what order should a rule be moved into a superpositions?



  (local.set $bool_message_finger_in_weak_head_normal_form
    (call $evaluate_to_weak_head_normal_form
      (local.get $bool_message_finger)
    )
  )

  (if
    (call $isFingerASuperposition
      (local.get $bool_message_finger_in_weak_head_normal_form)
    )
    (then
      (return
        (call $move_rule_calling_into_superposition_1_of_2
          (call $MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule)
          (local.get $bool_message_finger_in_weak_head_normal_form)
          (local.get $giveback_argument_finger)
        )
      )
    )
  )


  ;;;;;



  ;; TODO interesting: it appears that the result is first weak head normal formed before the side effekt can take place?
  ;; TODO because when the rule is applied to superposition it would seem be inappropriate to trigger some effect?
  (local.set $giveback_Header_in_weak_head_normal_form
    (call $evaluate_to_weak_head_normal_form
      (local.get $giveback_argument_finger)
    )
  )


  (local.set $result
    (block $break
      (result i64)

      (if
        (call $isFingerASuperposition
          (local.get $giveback_Header_in_weak_head_normal_form)
        )
        (then
          (br $break
            (call $move_rule_calling_into_superposition_2_of_2
              (call $MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule)
              (local.get $bool_message_finger_in_weak_head_normal_form)
              (local.get $giveback_Header_in_weak_head_normal_form)
            )
          )
        )
      )



      ;; the argument is not pattern matched
      ;; TODO that means the type does not matter?
;;       (if
;;         (call $isFingerASuperposition
;;           (local.get $bool_message_finger_in_weak_head_normal_form)
;;         )
;;         ;; TODO we would have to move the rule into the superposition and not make an side effect?
;;         (call $TODO)
;;       )
      (br $break
        (call $stonefish_import_passes.trigger_websocket_message
          (call $compose_wrappedTerm_Finger
            (local.get $bool_message_finger_in_weak_head_normal_form)
          )
        )

        (local.get $giveback_Header_in_weak_head_normal_form)
      )

      unreachable
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)


