
(func
  $BoolNeedsToBeSetTo_Rule_BOOL_ARGUMENT
  (result i64)
  (i64.const 0)
)

(func
  $BoolNeedsToBeSetTo_Rule_BATON
  (result i64)
  (i64.const 1)
)



(func
  $implement_behaviour_of_rule_of_BoolNeedsToBeSetTo
  (param $MagicRuleHeader i64)

  (result i64)
  (local $result i64)

  (local $bool_message_finger i64)
  (local $bool_message_finger_in_weak_head_normal_form i64)
  (local $giveback_argument_finger i64)

  (local $giveback_Header_in_weak_head_normal_form i64)

  (local $intermediat_Node_Location i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $MagicRuleHeader)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $MAGIC_RULE_ARITY_BoolNeedsToBeSetTo)
      )
    )
  #endif

  (local.set $bool_message_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (call $BoolNeedsToBeSetTo_Rule_BOOL_ARGUMENT)
      (local.get $MagicRuleHeader)
    )
  )
  (local.set $giveback_argument_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (call $BoolNeedsToBeSetTo_Rule_BATON)
      (local.get $MagicRuleHeader)
    )
  )
  (call $relinquish_Node_behind_Finger
    (local.get $MagicRuleHeader)
  )


  ;; it appears that the bool is strict
  ;; this makes kinda sense since the sideeffect differs for different bools
  (local.set $bool_message_finger_in_weak_head_normal_form
    (call $evaluate_to_weak_head_normal_form
      (local.get $bool_message_finger)
    )
  )

  (if
    (call $isFingerASuperposition
      (local.get $bool_message_finger_in_weak_head_normal_form)
    )
    (then
      (return
        (call $move_rule_calling_into_superposition_1_of_2
          (call $MAGIC_RULE_ID_BoolNeedsToBeSetTo)
          (local.get $bool_message_finger_in_weak_head_normal_form)
          (local.get $giveback_argument_finger)
        )
      )
    )
  )


  ;;;;;



  ;; TODO interesting: it appears that the result is first weak head normal formed before the side effekt can take place?
  ;; TODO because when the rule is applied to superposition it would seem be inappropriate to trigger some effect?
  (local.set $giveback_Header_in_weak_head_normal_form
    (call $evaluate_to_weak_head_normal_form
      (local.get $giveback_argument_finger)
    )
  )


  (local.set $result
    (block $break
      (result i64)

      (if
        (call $isFingerASuperposition
          (local.get $giveback_Header_in_weak_head_normal_form)
        )
        (then
          (br $break
            (call $move_rule_calling_into_superposition_2_of_2
              (call $MAGIC_RULE_ID_BoolNeedsToBeSetTo)
              (local.get $bool_message_finger_in_weak_head_normal_form)
              (local.get $giveback_Header_in_weak_head_normal_form)
            )
          )
        )
      )



      ;; the argument is not pattern matched
      ;; TODO that means the type does not matter?
      ;; TODO it is interesting that the argument is evaluated but not pattern matched
      (br $break
        (call $events.lightSwitchNeedsToBeSetToBool
          (call $compose_wrappedTerm_Finger
            (local.get $bool_message_finger_in_weak_head_normal_form)
          )
        )

        (local.get $giveback_Header_in_weak_head_normal_form)
      )

      unreachable
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)


