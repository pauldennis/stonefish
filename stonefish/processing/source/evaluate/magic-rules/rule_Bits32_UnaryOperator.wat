(func
  $THE_ONE_FIRST_ARGUMENT_OF_UNARY_OPERATOR
  (result i64)
  (i64.const 0)
)

(func
  $implement_behaviour_of_rule_of_Bits32_UnaryOperator
  (param $MagicRule_identification i64)
  (param $UnaryRuleHeader i64)

  (result i64)
  (local $result i64)

  (local $argument_finger i64)
  (local $argument_Header_in_weak_head_normal_form i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $MagicRule_identification)
        (call $extract_RuleIdentification_fromRuleHeader
          (local.get $UnaryRuleHeader)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $UnaryRuleHeader)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_1)
        (call $getArityFromRuleHeader
          (local.get $UnaryRuleHeader)
        )
      )
    )
  #endif

  (local.set $argument_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (call $THE_ONE_FIRST_ARGUMENT_OF_UNARY_OPERATOR)
      (local.get $UnaryRuleHeader)
    )
  )

  (call $relinquish_Node_behind_Finger
    (local.get $UnaryRuleHeader)
  )

  (local.set $argument_Header_in_weak_head_normal_form
    (call $evaluate_to_weak_head_normal_form
      (local.get $argument_finger)
    )
  )

  (local.set $result
    (block $break
      (result i64)

      (if
        (call $isFingerASuperposition
          (local.get $argument_Header_in_weak_head_normal_form)
        )
        (then
          (br $break
            (call $move_rule_calling_into_superposition
              (local.get $MagicRule_identification)
              (local.get $argument_Header_in_weak_head_normal_form)
            )
          )
        )
      )

      (if
        (call $isFingerAnIntrinsiscNumber
          (local.get $argument_Header_in_weak_head_normal_form)
        )
        (then
          (br $break
            (call $grow_graph_case_for_Bits32_UnaryOperator
              (local.get $MagicRule_identification)
              (local.get $argument_Header_in_weak_head_normal_form)
            )
          )
        )
      )

      (call $runtime_errors.expected_type_x_and_id_x_or_Superposition_but_got_x
        (call $INTRINSIC_NUMBER_TAG)
        (i64.const -1) ;; TODO have different error import?
        (local.get $argument_Header_in_weak_head_normal_form)
      )
      unreachable
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)



(func
  $grow_graph_case_for_Bits32_UnaryOperator
  (param $MagicRule_identification i64)
  (param $argument_Finger i64)

  (result i64)

  (local $the_argument_number f32)

  (local $result_number f32)

  (local $result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscNumber
        (local.get $argument_Finger)
      )
    )
  #endif

  (local.set $the_argument_number
    (call $f32.disaggregate_IntrinsicNumber
      (local.get $argument_Finger)
    )
  )

  (local.set $result_number
    (call $switch_unary_magic_operator
      (local.get $MagicRule_identification)
      (local.get $the_argument_number)
    )
  )

  (local.set $result
    (call $construct_IntrinsicNumber_f32
      (local.get $result_number)
    )
  )


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscNumber
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)


(func
  $switch_unary_magic_operator
  (param $MagicRule_identification i64)
  (param $argument f32)

  (result f32)

  (block $return (result f32)

    (block $Float32_Negate
    (block $Float32_Reciprocal_RoundingDown
    (block $Float32_SquareRoot_RoundingDown
      (if (call $i64.isEqual (local.get $MagicRule_identification) (call $MAGIC_RULE_ID_Float32_Negate)) (then br $Float32_Negate))
      (if (call $i64.isEqual (local.get $MagicRule_identification) (call $MAGIC_RULE_ID_Float32_Reciprocal_RoundingDown)) (then br $Float32_Reciprocal_RoundingDown))
      (if (call $i64.isEqual (local.get $MagicRule_identification) (call $MAGIC_RULE_ID_Float32_SquareRoot_RoundingDown)) (then br $Float32_SquareRoot_RoundingDown))

      unreachable
    )
    (br $return (call $f32.squareRoot_floor (local.get $argument)))
    )
    (br $return (call $f32.reciprocal_floor (local.get $argument)))
    )
    (br $return (call $f32.negate (local.get $argument)))

  )

)




;;;;;;




(func
  $f32.reciprocal_floor
  (param $x f32)
  (result f32)
  (call $f32.divide_floor
    (f32.const 1.0)
    (local.get $x)
  )
)
