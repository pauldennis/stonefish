(func
  $Float32_Square_RoundingDown_Rule_TheOneArgument
  (result i64)
  (i64.const 0)
)

(func
  $implement_behaviour_of_rule_of_Float32_Square_RoundingDown
  (param $FirstCustomRuleHeader i64)

  (result i64)
  (local $result i64)

  (local $argument_finger i64)
  (local $argument_Header_in_weak_head_normal_form i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $FirstCustomRuleHeader)
      )
    )
  #endif

  (local.set $argument_finger
    (call $getSubFinger_from_birthnumber_and_finger
      (call $Float32_Square_RoundingDown_Rule_TheOneArgument)
      (local.get $FirstCustomRuleHeader)
    )
  )

  (call $relinquish_Node_behind_Finger
    (local.get $FirstCustomRuleHeader)
  )

  (local.set $argument_Header_in_weak_head_normal_form
    (call $evaluate_to_weak_head_normal_form
      (local.get $argument_finger)
    )
  )

  (local.set $result
    (block $break
      (result i64)

      (if
        (call $isFingerASuperposition
          (local.get $argument_Header_in_weak_head_normal_form)
        )
        (then
          (br $break
            (call $move_rule_calling_into_superposition
              (call $MAGIC_RULE_ID_Float32_Square_RoundingDown)
              (local.get $argument_Header_in_weak_head_normal_form)
            )
          )
        )
      )

      (if
        (call $isFingerAnIntrinsiscNumber
          (local.get $argument_Header_in_weak_head_normal_form)
        )
        (then
          (br $break
            (call $grow_graph_case_for_Float32_Square_RoundingDown
              (local.get $argument_Header_in_weak_head_normal_form)
            )
          )
        )
      )

      (call $runtime_errors.expected_type_x_and_id_x_or_Superposition_but_got_x
        (call $INTRINSIC_NUMBER_TAG)
        (i64.const -1) ;; TODO have different error import?
        (local.get $argument_Header_in_weak_head_normal_form)
      )
      unreachable
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)



(func
  $grow_graph_case_for_Float32_Square_RoundingDown
  (param $argument_Finger i64)

  (result i64)

  (local $the_argument_number f32)

  (local $result_number f32)

  (local $result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscNumber
        (local.get $argument_Finger)
      )
    )
  #endif

  (local.set $the_argument_number
    (call $f32.disaggregate_IntrinsicNumber
      (local.get $argument_Finger)
    )
  )

  (local.set $result_number
    (call $f32.multiplicate_floor
      (local.get $the_argument_number)
      (local.get $the_argument_number)
    )
  )

  (local.set $result
    (call $construct_IntrinsicNumber_f32
      (local.get $result_number)
    )
  )


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscNumber
        (local.get $result)
      )
    )
  #endif

  (local.get $result)

)

