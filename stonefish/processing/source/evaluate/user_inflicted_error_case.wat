
(func
  $user-inflicted_error_case.cannot_apply_x
  (param $thing_that_cannot_be_applied i64)

  (call $runtime_errors.cannot_apply_x
    (local.get $thing_that_cannot_be_applied)
  )

  unreachable
)


(func
  $user-inflicted_error_case.left_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number
  (param $thing_that_cannot_be_applied i64)

  (call $runtime_errors.left_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number
    (local.get $thing_that_cannot_be_applied)
  )

  unreachable
)


(func
  $user-inflicted_error_case.right_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number
  (param $thing_that_cannot_be_applied i64)

  (call $runtime_errors.right_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number
    (local.get $thing_that_cannot_be_applied)
  )

  unreachable
)


(func
  $user-inflicted_error_case.addition_cannot_be_represented
  (param $left i32)
  (param $right i32)

  (call $runtime_errors.addition_cannot_be_represented
    (local.get $left)
    (local.get $right)
  )

  unreachable
)

(func
  $user-inflicted_error_case.ran_out_of_duplication_labels

  (call $runtime_errors.ran_out_of_duplication_labels
  )

  unreachable
)


(func
  $user-inflicted_error_case.expected_from_interface_to_return_a_constructor_but_got_x
  (param $offendingResultHeader i64)

  (call $runtime_errors.expected_from_interface_to_return_a_constructor_but_got_x
  )

  unreachable
)

(func
  $user-inflicted_error_case.expected_from_interface_to_return_a_constructor_called_ReturnNewPrivateDataAndSystemRequest_but_got_x
  (param $offendingResultHeader i64)

  (call $runtime_errors.expected_from_interface_to_return_a_constructor_called_ReturnNewPrivateDataAndSystemRequest_but_got_x
  )

  unreachable
)

(func
  $user-inflicted_error_case.expected_from_interface_to_return_a_constructor_with_arity_of_two_but_got_x
  (param $offendingResultHeader i64)

  (call $runtime_errors.expected_from_interface_to_return_a_constructor_with_arity_of_two_but_got_x
  )

  unreachable
)

