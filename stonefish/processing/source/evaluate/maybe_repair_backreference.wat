

(func
  $maybeInstallNewLeashIfVariable
  (param $backreferenceLeash i64)
  (param $placedHeader_that_might_have_been_a_Variable i64)

  ;; repair cases
  (if
    (call $isFingerASoloVariable
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
        (call $getSoloVariableLeashLocation_from_SoloVariable
          (local.get $placedHeader_that_might_have_been_a_Variable)
        )
        (local.get $backreferenceLeash)
      )

      (return)
    )
  )
  (if
    (call $isFingerATwin
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
        (call $getTwinLeashLocation_from_Twin
          (local.get $placedHeader_that_might_have_been_a_Variable)
        )
        (local.get $backreferenceLeash)
      )

      (return)
    )
  )

  ;; do nothing cases:
  (if
    (call $isFingerALambda
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (return)
    )
  )
  (if
    (call $isFingerAConstructor
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (return)
    )
  )
  (if
    (call $isFingerAnIntrinsiscNumber
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (return)
    )
  )
  (if
    (call $isFingerACapsule
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (return)
    )
  )
  (if
    (call $isFingerASuperposition
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (return)
    )
  )
  (if
    (call $isFingerARuleCall
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (return)
    )
  )
  (if
    (call $isFingerAnIntrinsiscBinaryOperator
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (return)
    )
  )
  (if
    (call $isFingerAnApplication
      (local.get $placedHeader_that_might_have_been_a_Variable)
    )
    (then
      (return)
    )
  )

  (call $TODO_not_implemented)
  unreachable
)
