(func
  $link_rule_fragment
  (param $rule_identification i64)
  (param $matchedConstructor i64)

  (result i64)

  (local $the_root i64)
  (local $the_melee externref)
  (local $archetype externref)

  (local $rightSideLambda i64)

  (local $duplication_identification_mapping_offset i64)

  (local.set $duplication_identification_mapping_offset
    (local.set $the_root
      (local.set $the_melee
        (call $install_fragments.extractMyRuleRightSide
          (call $get_bookshelf
          )
          (local.get $rule_identification)
          (local.get $matchedConstructor)
        )
      )
    )
  )
  (local.set $archetype
    (call $link.constructArchetype
      (local.get $the_melee)
    )
  )
  (local.set $rightSideLambda
    (call $raw_link
      (local.get $duplication_identification_mapping_offset)
      (local.get $the_root)
      (local.get $archetype)
    )
  )

  (local.get $rightSideLambda)
)


;; TODO maybe have better place for wrappers that assert parameters of imports?
(func
  $check_for_rule_existence
  (param $rule_identification i64)

  (result i32)

  (local $result i32)

  (local.set $result
    (call $install_fragments.check_for_rule_existence
      (call $get_bookshelf)
      (local.get $rule_identification)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isNormalizedBool
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)
