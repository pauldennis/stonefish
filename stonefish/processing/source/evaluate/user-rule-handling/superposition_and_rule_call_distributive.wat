

;; TODO ?
(func
  $move_rule_calling_into_superposition_2_of_2
  (param $ruleId i64)
  (param $left_argument_Header i64)
  (param $superimposed_Header i64)

  (result i64)

  (local $new_left_RuleCallNode_SuperimposedCall_Location i64)
  (local $new_right_RuleCallNode_SuperimposedCall_Location i64)
  (local $new_Superposition_Location i64)

  (local $spreading_duplicationLabel i64)
  (local $leftSuperImposed_InterfaceCallResult_Header i64)
  (local $rightSuperImposed_InterfaceCallResult_Header i64)

  (local $left_CopyOfBoolMessage i64)
  (local $right_CopyOfBoolMessage i64)

  (local $new_leftSuperimposedRuleCall_Header i64)
  (local $new_rightSuperimposedRuleCall_Header i64)

  (local $new_superposition_Header i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $superimposed_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidRuleId
        (local.get $ruleId)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $getArityFromRuleId
          (local.get $ruleId)
        )
      )
    )
  #endif

  ;; new locations
  (local.set $new_left_RuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_right_RuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_Superposition_Location
    (call $request_Node__might_trap
    )
  )

  (local.set $spreading_duplicationLabel
    (local.set $leftSuperImposed_InterfaceCallResult_Header
      (local.set $rightSuperImposed_InterfaceCallResult_Header
        (call $disaggregate_Superposition
          (local.get $superimposed_Header)
        )
      )
    )
  )

  (local.set $left_CopyOfBoolMessage
    (local.set $right_CopyOfBoolMessage
      (call $duplicate_header_into_twoTwins_with_DuplicationLabel
        (local.get $left_argument_Header)
        (local.get $spreading_duplicationLabel)
      )
    )
  )


  ;; two Rules
  ;; 0
;;   (call $construct_at_location_new_SingletonRuleCall
;;     (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
;;
;;     (local.get $leftSuperImposed_InterfaceCallResult_Header)
;;   )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
      (call $WebSocketMessageTrigger_Rule_GIVEBACK_ARGUMENT)
    )
    (local.get $leftSuperImposed_InterfaceCallResult_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
        (call $WebSocketMessageTrigger_Rule_GIVEBACK_ARGUMENT)
      )
    )
    (local.get $leftSuperImposed_InterfaceCallResult_Header)
  )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
      (call $WebSocketMessageTrigger_Rule_PASSED_BOOL_ARGUMENT)
    )

    (local.get $left_CopyOfBoolMessage)
  )

  ;; 1
;;   (call $construct_at_location_new_SingletonRuleCall
;;     (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
;;
;;     (local.get $rightSuperImposed_InterfaceCallResult_Header)
;;   )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
      (call $WebSocketMessageTrigger_Rule_GIVEBACK_ARGUMENT)
    )
    (local.get $rightSuperImposed_InterfaceCallResult_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
        (call $WebSocketMessageTrigger_Rule_GIVEBACK_ARGUMENT)
      )
    )
    (local.get $rightSuperImposed_InterfaceCallResult_Header)
  )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
      (call $WebSocketMessageTrigger_Rule_PASSED_BOOL_ARGUMENT)
    )
    (local.get $right_CopyOfBoolMessage)
  )





  (local.set $new_leftSuperimposedRuleCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (local.get $ruleId)
      (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
    )
  )
  (local.set $new_rightSuperimposedRuleCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (local.get $ruleId)
      (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
    )
  )

  ;; new superposition Node
  (call $construct_at_location_new_superposition_node
    (local.get $new_Superposition_Location)
    (local.get $new_leftSuperimposedRuleCall_Header)
    (local.get $new_rightSuperimposedRuleCall_Header)
  )
  (local.set $new_superposition_Header
    (call $construct_SuperpositionHeader
      (local.get $spreading_duplicationLabel)
      (local.get $new_Superposition_Location)
    )
  )

  (local.get $new_superposition_Header)
)







;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;








;; TODO generalize? Or maybe not?

;; F {x y} q
;; ---------
;; qL qR := q
;; {(F x qL) (F y qR)}

(func
  $move_rule_calling_into_superposition_1_of_2
  (param $ruleId i64)
  (param $superimposed_Header i64)
  (param $right_argument_Header i64)

  (result i64)

  (local $new_left_RuleCallNode_SuperimposedCall_Location i64)
  (local $new_right_RuleCallNode_SuperimposedCall_Location i64)
  (local $new_Superposition_Location i64)

  (local $spreading_duplicationLabel i64)
  (local $leftSuperImposed_Argument_Header i64)
  (local $rightSuperImposed_Argument_Header i64)

  (local $left_CopyOfBoolMessage i64)
  (local $right_CopyOfBoolMessage i64)

  (local $new_leftSuperimposedRuleCall_Header i64)
  (local $new_rightSuperimposedRuleCall_Header i64)

  (local $new_superposition_Header i64)

  ;; ?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isGraphConsistent
        (local.get $superimposed_Header)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isGraphConsistent
        (local.get $right_argument_Header)
      )
    )
  #endif



  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $superimposed_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidRuleId
        (local.get $ruleId)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_TWO)
        (call $getArityFromRuleId
          (local.get $ruleId)
        )
      )
    )
  #endif

  ;; new locations
  (local.set $new_left_RuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_right_RuleCallNode_SuperimposedCall_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_Superposition_Location
    (call $request_Node__might_trap
    )
  )

  (local.set $spreading_duplicationLabel
    (local.set $leftSuperImposed_Argument_Header
      (local.set $rightSuperImposed_Argument_Header
        (call $disaggregate_Superposition
          (local.get $superimposed_Header)
        )
      )
    )
  )

  (local.set $left_CopyOfBoolMessage
    (local.set $right_CopyOfBoolMessage
      (call $duplicate_header_into_twoTwins_with_DuplicationLabel
        (local.get $right_argument_Header)
        (local.get $spreading_duplicationLabel)
      )
    )
  )


  ;; two Rules
  ;; 0
  (call $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
    (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
    (call $RULE_ARGUMENT_INDEX_BINARY_LEFT)
    (local.get $leftSuperImposed_Argument_Header)
  )
  (call $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
    (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
    (call $RULE_ARGUMENT_INDEX_BINARY_RIGHT)
    (local.get $left_CopyOfBoolMessage)
  )

  ;; 1
  (call $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
    (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
    (call $RULE_ARGUMENT_INDEX_BINARY_LEFT)
    (local.get $rightSuperImposed_Argument_Header)
  )
  (call $place_finger_in_FingerNodeHeap__AND__maybe_repair_the_BackReference
    (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
    (call $RULE_ARGUMENT_INDEX_BINARY_RIGHT)
    (local.get $right_CopyOfBoolMessage)
  )





  (local.set $new_leftSuperimposedRuleCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (local.get $ruleId)
      (local.get $new_left_RuleCallNode_SuperimposedCall_Location)
    )
  )
  (local.set $new_rightSuperimposedRuleCall_Header
    (call $construct_RuleCall_from_rule_id_and_Location
      (local.get $ruleId)
      (local.get $new_right_RuleCallNode_SuperimposedCall_Location)
    )
  )

  ;; new superposition Node
  (call $construct_at_location_new_superposition_node
    (local.get $new_Superposition_Location)
    (local.get $new_leftSuperimposedRuleCall_Header)
    (local.get $new_rightSuperimposedRuleCall_Header)
  )
  (local.set $new_superposition_Header
    (call $construct_SuperpositionHeader
      (local.get $spreading_duplicationLabel)
      (local.get $new_Superposition_Location)
    )
  )

  (local.get $new_superposition_Header)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isGraphConsistent
        (local.get $new_superposition_Header)
      )
    )
  #endif
)

