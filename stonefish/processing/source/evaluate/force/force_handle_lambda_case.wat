
(func
  $handle_lambda_case
  (param $lambdaHeader i64)

  (result i64)

  (local $reduced_lambda_body i64)

  (local $location_of_lambda_body_header i64)

  (local.set $reduced_lambda_body
    (call $evaluate_to_weak_head_normal_form
      (call $getLambdaBody
        (local.get $lambdaHeader)
      )
    )
  )

  (local.set $location_of_lambda_body_header
    (call $extractNodeLocationWithFingerOffset
      (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
      (local.get $lambdaHeader)
    )
  )

  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (local.get $location_of_lambda_body_header)
    (local.get $reduced_lambda_body)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (local.get $location_of_lambda_body_header)
    )
    (local.get $reduced_lambda_body)
  )

  (call $force
    (local.get $reduced_lambda_body)
  )
)
