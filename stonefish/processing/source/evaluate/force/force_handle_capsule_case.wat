
;; TODO copied from constructor case
(func
  $handle_capsule_case
  (param $root_Finger_in_weakHeadNormal i64)
  (result i64)

  (local $arity i64)

  (local $atWhatChildAreWe i64) ;; TODO this local variable is out of place

  (local $current_child_location i64)
  (local $intermediate_unreduced_child_Finger i64)
  (local $intermediate_reduced_child_Finger i64)

  (local.set $atWhatChildAreWe
    (i64.const 0)
  )

  (local.set $arity
    (i64.const 2)
  )

  (loop $goThroughChildren
    (local.set $current_child_location
      (local.set $intermediate_unreduced_child_Finger
        (call $getSubLocationAndSubFinger
          (local.get $atWhatChildAreWe)
          (local.get $root_Finger_in_weakHeadNormal)
        )
      )
    )

    ;; TODO do we really need to reduce and place it before it is forced?
      ;; probably yes because we might have to repair some backreference
    (local.set $intermediate_reduced_child_Finger
      (call $evaluate_to_weak_head_normal_form
        (local.get $intermediate_unreduced_child_Finger)
      )
    )

    (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
      (local.get $current_child_location)
      (local.get $intermediate_reduced_child_Finger)
    )

    ;; TODO have an compilation flag for turning off this case since this is only needed when forcing lambdas but not when only reducing to weak head normal form
    (call $maybeInstallNewLeashIfVariable
      (call $construct_leash_from_fingerLocation
        (local.get $current_child_location)
      )
      (local.get $intermediate_reduced_child_Finger)
    )

    (call $force
      (local.get $intermediate_reduced_child_Finger)
    )

    drop
;;         (call $i64.print )


    ;; increase count
    (local.set $atWhatChildAreWe
      (call $i64.increment
        (local.get $atWhatChildAreWe)
      )
    )

    ;; should we repeat the loop body?
    (br_if $goThroughChildren
      (call $i64.isLessThan
        (local.get $atWhatChildAreWe)
        (local.get $arity)
      )
    )
  )

  (local.get $root_Finger_in_weakHeadNormal)
)
