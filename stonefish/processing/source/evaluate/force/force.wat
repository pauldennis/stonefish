#ifdef STONEFISH_DEBUG_MODE_ALLOW_FORCING
  (func $force_in_force_mode
    (param $originalRootReentrance i64)

    (result i64)

    (local $result i64)

    (global.set $allowForceMode
      (call $TRUE)
    )

    (local.set $result
      (call $force
        (local.get $originalRootReentrance)
      )
    )

    (global.set $allowForceMode
      (call $FALSE)
    )

    (local.get $result)
  )
#endif


(func $force
  (param $originalRootReentrance i64)

  (result i64)

  (local $weakHeadNormal_root_Finger i64)
  (local $fingerTagCase i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      #ifdef STONEFISH_DEBUG_MODE_ALLOW_FORCING
        (global.get $allowForceMode)
      #else
        (call $FALSE)
      #endif
    )
  #endif

  (local.set $weakHeadNormal_root_Finger
    (call $evaluate_to_weak_head_normal_form
      (local.get $originalRootReentrance)
    )
  )

  ;; extract the tag after root was reduced
  (local.set $fingerTagCase
    (call $extractTagFromFinger
      (local.get $weakHeadNormal_root_Finger)
    )
  )


  (block $break
    (result i64)

    ;; TODO find out what is the fastest sorting
    (if
      (call $is_the_tag_a_Constructor
        (local.get $fingerTagCase)
      )

      (then
        (br $break
          (call $force_handle_constructor_case
            (local.get $weakHeadNormal_root_Finger)
          )
        )
      )
    )

    (if
      (call $is_the_tag_an_IntrinsiscNumber
        (local.get $fingerTagCase)
      )

      (then
        (br $break
          (local.get $weakHeadNormal_root_Finger)
        )
      )
    )

    ;; TODO remove out of relase mode
    ;; bogus cases:
    (if
      (call $is_the_tag_a_Lambda
        (local.get $fingerTagCase)
      )

      (then
        (br $break
          (call $handle_lambda_case
            (local.get $weakHeadNormal_root_Finger)
          )
        )
      )
    )
    (if
      (call $is_the_tag_an_SoloVariable
        (local.get $fingerTagCase)
      )

      (then
        (br $break
          (local.get $weakHeadNormal_root_Finger)
        )
      )
    )
    (if
      (call $is_the_tag_a_capsule
        (local.get $fingerTagCase)
      )

      (then
        (br $break
          (call $handle_capsule_case
            (local.get $weakHeadNormal_root_Finger)
          )
        )
      )
    )
    (if
      (call $is_the_tag_a_Superposition
        (local.get $fingerTagCase)
      )

      (then
        (br $break
          (call $handle_superposition_case
            (local.get $weakHeadNormal_root_Finger)
          )
        )
      )
    )
    (if
      (call $is_the_tag_a_leftTwin
        (local.get $fingerTagCase)
      )

      (then
        (br $break
          (local.get $weakHeadNormal_root_Finger)
        )
      )
    )
    (if
      (call $is_the_tag_a_rightTwin
        (local.get $fingerTagCase)
      )

      (then
        (br $break
          (local.get $weakHeadNormal_root_Finger)
        )
      )
    )

    unreachable
  )

)

