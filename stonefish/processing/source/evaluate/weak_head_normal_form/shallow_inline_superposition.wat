
(func
  $shallow_inline_superposition
  (param $duplicationLabel_of_Twin i64)
  (param $theOtherTwinLeash i64)
  (param $superPositionHeader i64)
  (param $areWeReducingALeftTwin i32)

  (result i64)

  (local $duplicationLabel_of_Superposition i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidDuplicationLabel
        (local.get $duplicationLabel_of_Twin)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidVariableLeash
        (local.get $theOtherTwinLeash)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $superPositionHeader)
      )
    )
  #endif

  (local.set $duplicationLabel_of_Superposition
    (call $extract_DuplicationLabel_fromSuperpositionHeader
      (local.get $superPositionHeader)
    )
  )

  (if
    (result i64)

    (call $areDuplicationLabelsEqual
      (local.get $duplicationLabel_of_Twin)
      (local.get $duplicationLabel_of_Superposition)
    )
    (then
      (call $handle_easy_inline_of_superposition_case
        (local.get $superPositionHeader)
        (local.get $theOtherTwinLeash)
        (local.get $areWeReducingALeftTwin)
      )
    )
    (else
      (call $handle_swap_inline_of_superposition_case
        (local.get $superPositionHeader)
        (local.get $duplicationLabel_of_Twin)
        (local.get $duplicationLabel_of_Superposition)
        (local.get $theOtherTwinLeash)
        (local.get $areWeReducingALeftTwin)
      )
    )
  )
)

(func
  $handle_swap_inline_of_superposition_case
  (param $superpositionHeader_to_be_shallow_inlined i64)
  (param $duplicationLabel_of_Twin i64)
  (param $duplicationLabel_of_Superposition i64)
  (param $theOtherTwinLeash i64)
  (param $areWeReducingALeftTwin i32)

  (result i64)

  (local $new_shallowSuperpositionInline_for_LEFT_Twin_Location i64)
  (local $new_shallowSuperpositionInline_for_RIGHT_Twin_Location i64)
  (local $UP_new_DuplicationNode_for_formerLEFTSuperimposdHeader_Location i64)
  (local $DOWN_new_DuplicationNode_for_formerRIGHTSuperimposdHeader_Location i64)

  (local $formerLEFTSuperimposd_Header i64)
  (local $formerRIGHTSuperimposd_Header i64)

  (local $UP_leftLeash i64)
  (local $UP_rightLeash i64)
  (local $UP_leftTwin i64)
  (local $UP_rightTwin i64)

  (local $DOWN_leftLeash i64)
  (local $DOWN_rightLeash i64)
  (local $DOWN_leftTwin i64)
  (local $DOWN_rightTwin i64)

  (local $new_left_superposition_replacement_Header i64)
  (local $new_right_superposition_replacement_Header i64)

  (local $theOther_Superposition_Header i64)
  (local $the_Returned_Superposition_Header i64)



  ;; allocations
  (local.set $new_shallowSuperpositionInline_for_LEFT_Twin_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_shallowSuperpositionInline_for_RIGHT_Twin_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $UP_new_DuplicationNode_for_formerLEFTSuperimposdHeader_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $DOWN_new_DuplicationNode_for_formerRIGHTSuperimposdHeader_Location
    (call $request_Node__might_trap
    )
  )


  (local.set $formerLEFTSuperimposd_Header
    (call $getLeftSuperimposedHeader
      (local.get $superpositionHeader_to_be_shallow_inlined)
    )
  )
  (local.set $formerRIGHTSuperimposd_Header
    (call $getRightSuperimposedHeader
      (local.get $superpositionHeader_to_be_shallow_inlined)
    )
  )



  ;; UP duplication Node
  (local.set $UP_leftLeash
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_shallowSuperpositionInline_for_LEFT_Twin_Location)
      (call $SUPERPOSITION__POSITION_OF_LEFT_SUPERIMPOSED_TERM)
    )
  )
  (local.set $UP_rightLeash
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_shallowSuperpositionInline_for_RIGHT_Twin_Location)
      (call $SUPERPOSITION__POSITION_OF_LEFT_SUPERIMPOSED_TERM)
    )
  )
  (local.set $UP_leftTwin
    (call $construct_TwinHeader
      (call $leftTwinVariant)
      (local.get $duplicationLabel_of_Twin)
      (local.get $UP_new_DuplicationNode_for_formerLEFTSuperimposdHeader_Location)
    )
  )
  (local.set $UP_rightTwin
    (call $construct_TwinHeader
      (call $rightTwinVariant)
      (local.get $duplicationLabel_of_Twin)
      (local.get $UP_new_DuplicationNode_for_formerLEFTSuperimposdHeader_Location)
    )
  )
  (call $construct_at_location_new_duplication_node
    (local.get $UP_new_DuplicationNode_for_formerLEFTSuperimposdHeader_Location)
    (local.get $UP_leftLeash)
    (local.get $UP_rightLeash)
    (local.get $formerLEFTSuperimposd_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $UP_new_DuplicationNode_for_formerLEFTSuperimposdHeader_Location)
        (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
      )
    )
    (local.get $formerLEFTSuperimposd_Header)
  )


  ;; DOWN duplication Node
  (local.set $DOWN_leftLeash
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_shallowSuperpositionInline_for_LEFT_Twin_Location)
      (call $SUPERPOSITION__POSITION_OF_RIGHT_SUPERIMPOSED_TERM)
    )
  )
  (local.set $DOWN_rightLeash
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_shallowSuperpositionInline_for_RIGHT_Twin_Location)
      (call $SUPERPOSITION__POSITION_OF_RIGHT_SUPERIMPOSED_TERM)
    )
  )
  (local.set $DOWN_leftTwin
    (call $construct_TwinHeader
      (call $leftTwinVariant)
      (local.get $duplicationLabel_of_Twin)
      (local.get $DOWN_new_DuplicationNode_for_formerRIGHTSuperimposdHeader_Location)
    )
  )
  (local.set $DOWN_rightTwin
    (call $construct_TwinHeader
      (call $rightTwinVariant)
      (local.get $duplicationLabel_of_Twin)
      (local.get $DOWN_new_DuplicationNode_for_formerRIGHTSuperimposdHeader_Location)
    )
  )
  (call $construct_at_location_new_duplication_node
    (local.get $DOWN_new_DuplicationNode_for_formerRIGHTSuperimposdHeader_Location)
    (local.get $DOWN_leftLeash)
    (local.get $DOWN_rightLeash)
    (local.get $formerRIGHTSuperimposd_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $DOWN_new_DuplicationNode_for_formerRIGHTSuperimposdHeader_Location)
        (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY)
      )
    )
    (local.get $formerRIGHTSuperimposd_Header)
  )

  ;; shallow left copy of superposition node
  (call $construct_at_location_new_superposition_node
    (local.get $new_shallowSuperpositionInline_for_LEFT_Twin_Location)
    (local.get $UP_leftTwin)
    (local.get $DOWN_leftTwin)
  )
  (local.set $new_left_superposition_replacement_Header
    (call $construct_SuperpositionHeader
      (local.get $duplicationLabel_of_Superposition)
      (local.get $new_shallowSuperpositionInline_for_LEFT_Twin_Location)
    )
  )
  (call $construct_at_location_new_superposition_node
    (local.get $new_shallowSuperpositionInline_for_RIGHT_Twin_Location)
    (local.get $UP_rightTwin)
    (local.get $DOWN_rightTwin)
  )
  (local.set $new_right_superposition_replacement_Header
    (call $construct_SuperpositionHeader
      (local.get $duplicationLabel_of_Superposition)
      (local.get $new_shallowSuperpositionInline_for_RIGHT_Twin_Location)
    )
  )

  ;; TODO does this actually make a difference what header is returned?
  ;; TODO try out what is more performant
  (local.set $theOther_Superposition_Header
    (if
      (result i64)

      (local.get $areWeReducingALeftTwin)

      (then
        (local.get $new_right_superposition_replacement_Header)
      )
      (else
        (local.get $new_left_superposition_replacement_Header)
      )
    )
  )

  (local.set $the_Returned_Superposition_Header
    (if
      (result i64)

      (local.get $areWeReducingALeftTwin)

      (then
        (local.get $new_left_superposition_replacement_Header)
      )
      (else
        (local.get $new_right_superposition_replacement_Header)
      )
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (local.get $theOther_Superposition_Header)
          (local.get $the_Returned_Superposition_Header)
        )
      )
    )
  #endif

  ;; replace the other twin
  (call $followTwinLeashAnReplaceTwinByFinger
    (local.get $theOtherTwinLeash)
    (local.get $theOther_Superposition_Header)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $theOther_Superposition_Header)
        )
      )
      ;;   (call $maybeInstallNewLeashIfVariable
      ;;     (local.get $reeusedTwinLeashAsSoloVariableOccurenceLeash)
      ;;     (local.get $theOtherSuperimposedHeader)
      ;;   )
    )
  #endif

  (call $relinquish_Node_behind_Finger
    (local.get $superpositionHeader_to_be_shallow_inlined)
  )

  (local.get $the_Returned_Superposition_Header)
)

