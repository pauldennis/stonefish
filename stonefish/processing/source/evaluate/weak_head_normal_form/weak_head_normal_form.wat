;; TODO have the name dispatch in the file name?



(func
  $gotTermReduced
  (param $Header i64)
  (result i32)

  (if
    (result i32)

    #ifdef STONEFISH_DEBUG_MODE_ALLOW_FORCING
      (global.get $allowForceMode)
    #else
      (call $FALSE)
    #endif

    (then
      ;; in force mode the reduction property is too complicated for me right now
      ;; TODO should there be some sophisticated reduced predicate?
      (call $TRUE)
    )

    (else
      (call $isFingerAWeakHead
        (local.get $Header)
      )
    )
  )

)


;; NOTE this is the evaluation function in case you where searching for it
;; "reduce" for short
;; TODO perhaps the name should be "reduce" to emphasize the importance of this function by beeing short?
(func
  $evaluate_to_weak_head_normal_form
  (param $rootFinger i64)
  (result i64)

  (local $result i64)

  #ifdef STONEFISH_DEBUG_TOOLS_HEAPDUMP
;;     (call $debug.heapdump
;;     )
  #endif

;;   (if
;;     (call $i64.isEqual
;;       (i64.const 18691697672200)
;;       (local.get $result)
;;     )
;;     (then
;;       (call $breakpoint)
;;     )
;;     (else
;;     )
;;   )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isGraphConsistent
        (local.get $rootFinger)
      )
    )
  #endif

  (local.set $result
    (call $evaluate_to_weak_head_normal_form_dispatch
      (local.get $rootFinger)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isGraphConsistent
        (local.get $result)
      )
    )
  #endif



  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $gotTermReduced
        (local.get $result)
      )
    )
  #endif



  (local.get $result)
)

(func
  $evaluate_to_weak_head_normal_form_dispatch
  (param $rootFinger i64)
  (result i64)

  (local $fingerTagCase i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $rootFinger)
      )
    )
  #endif

  (local.set $fingerTagCase
    (call $extractTagFromFinger
      (local.get $rootFinger)
    )
  )

  ;; TODO find out most performant sorting
  (if
    (call $is_the_tag_a_Constructor
      (local.get $fingerTagCase)
    )

    (then
      #if STONEFISH_SHOULD_INCLUDE_ASSERT
        (call $assert
          (call $isFingerAWeakHead
            (local.get $rootFinger)
          )
        )
      #endif

      (return
        (local.get $rootFinger)
      )
    )
  )


  (if
    (call $is_the_tag_a_Lambda
      (local.get $fingerTagCase)
    )

    (then
      #if STONEFISH_SHOULD_INCLUDE_ASSERT
        (call $assert
          (call $isFingerAWeakHead
            (local.get $rootFinger)
          )
        )
      #endif

      (return
        (local.get $rootFinger)
      )
    )
  )


  (if
    (call $is_the_tag_a_leftTwin
      (local.get $fingerTagCase)
    )

    (then
      (return
        (call $evaluate_Twin
          (local.get $rootFinger)
        )
      )
    )
  )


  (if
    (call $is_the_tag_a_rightTwin
      (local.get $fingerTagCase)
    )

    (then
      (return
        (call $evaluate_Twin
          (local.get $rootFinger)
        )
      )
    )
  )


  (if
    (call $is_the_tag_an_Application
      (local.get $fingerTagCase)
    )

    (then
      (return
        (call $evaluate_Application
          (local.get $rootFinger)
        )
      )
    )
  )


  (if
    (call $is_the_tag_an_IntrinsiscBinaryOperator
      (local.get $fingerTagCase)
    )

    (then
      (return
        (call $evaluate_IntrinsiscBinaryOperator
          (local.get $rootFinger)
        )
      )
    )
  )


  (if
    (call $is_the_tag_an_IntrinsiscNumber
      (local.get $fingerTagCase)
    )

    (then
      #if STONEFISH_SHOULD_INCLUDE_ASSERT
        (call $assert
          (call $isFingerAWeakHead
            (local.get $rootFinger)
          )
        )
      #endif

      (return
        (local.get $rootFinger)
      )
    )
  )


  (if
    (call $is_the_tag_a_Superposition
      (local.get $fingerTagCase)
    )

    (then
      #if STONEFISH_SHOULD_INCLUDE_ASSERT
        (call $assert
          (call $isFingerAWeakHead
            (local.get $rootFinger)
          )
        )
      #endif

      (return
        (local.get $rootFinger)
      )
    )
  )


  (if
    (call $is_the_tag_a_RuleCall
      (local.get $fingerTagCase)
    )

    (then
      (return
        (call $evaluate_ruleCall
          (local.get $rootFinger)
        )
      )
    )
  )

  (if
    (call $is_the_tag_a_capsule
      (local.get $fingerTagCase)
    )

    (then
      #if STONEFISH_SHOULD_INCLUDE_ASSERT
        (call $assert
          (call $isFingerAWeakHead
            (local.get $rootFinger)
          )
        )
      #endif
      (return
        (local.get $rootFinger)
      )
    )
  )

  ;; bogus cases:

  (if
    (call $is_the_tag_an_SoloVariable
      (local.get $fingerTagCase)
    )

    (then
      #if STONEFISH_SHOULD_INCLUDE_ASSERT
        (call $assert
          ;; TODO have speciual force flavored variant of this function
          ;; TODO fix sandbox behaviour 563d99d090ca4c78fa90af0c302ccea725b74158dca38c61c2a3f7aa70523558
          (call $gotTermReduced
            (local.get $rootFinger)
          )
        )
      #endif

      (return
        (local.get $rootFinger)
      )
    )
  )

  unreachable
)




