
(func
  $apply_superposition_to_argument

  (param $original_superposition_Header i64)
  (param $argument_Header i64)

  (result i64)

  (local $left_superimposed_Header i64)
  (local $right_superimposed_Header i64)

  (local $new_duplicationNode_Location i64)
  (local $new_superpositionNode_Location i64)
  (local $new_left_applicationNode_Location i64)
  (local $new_right_applicationNode_Location i64)

  (local $leftLeash_to_leftTwinCopy_for_Argument i64)
  (local $rightLeash_to_rightTwinCopy_for_Argument i64)
  (local $duplicationLabel i64)
  (local $leftTwin_that_is_left_Copy_of_Argument i64)
  (local $rightTwin_that_is_right_Copy_of_Argument i64)

  (local $new_left_Application_Header i64)
  (local $new_right_Application_Header i64)

  (local $new_result_Superposition_Header i64)


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerASuperposition
        (local.get $original_superposition_Header)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $argument_Header)
        )
      )
    )
  #endif

  (local.set $left_superimposed_Header
    (call $getLeftSuperimposedHeader
      (local.get $original_superposition_Header)
    )
  )
  (local.set $right_superimposed_Header
    (call $getRightSuperimposedHeader
      (local.get $original_superposition_Header)
    )
  )

  (call $relinquish_Node_behind_Finger
    (local.get $original_superposition_Header)
  )

  (local.set $new_duplicationNode_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_superpositionNode_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_left_applicationNode_Location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_right_applicationNode_Location
    (call $request_Node__might_trap
    )
  )

  ;; suplication node
  (local.set $leftLeash_to_leftTwinCopy_for_Argument
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_left_applicationNode_Location)
      (call $APPLICATION_NODE__POSITION_OF_THEARGUMENT)
    )
  )
  (local.set $rightLeash_to_rightTwinCopy_for_Argument
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_right_applicationNode_Location)
      (call $APPLICATION_NODE__POSITION_OF_THEARGUMENT)
    )
  )
  (local.set $duplicationLabel
    (call $extract_DuplicationLabel_fromSuperpositionHeader
      (local.get $original_superposition_Header)
    )
  )
  (local.set $leftTwin_that_is_left_Copy_of_Argument
    (call $construct_TwinHeader
      (call $leftTwinVariant)
      (local.get $duplicationLabel)
      (local.get $new_duplicationNode_Location)
    )
  )
  (local.set $rightTwin_that_is_right_Copy_of_Argument
    (call $construct_TwinHeader
      (call $rightTwinVariant)
      (local.get $duplicationLabel)
      (local.get $new_duplicationNode_Location)
    )
  )
  (call $construct_at_location_new_duplication_node
    (local.get $new_duplicationNode_Location)
    (local.get $leftLeash_to_leftTwinCopy_for_Argument)
    (local.get $rightLeash_to_rightTwinCopy_for_Argument)
    (local.get $argument_Header)
  )
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $argument_Header)
        )
      )
    )
  #endif

  ;; the two new frayed applications
  (call $construct_at_location_new_application_node
    (local.get $new_left_applicationNode_Location)
    (local.get $left_superimposed_Header)
    (local.get $leftTwin_that_is_left_Copy_of_Argument)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $new_left_applicationNode_Location)
        (call $APPLICATION_NODE__POSITION_OF_TOBEAPPLIED)
      )
    )
    (local.get $left_superimposed_Header)
  )
  ;; no repair for $leftTwin_that_is_left_Copy_of_Argument as we did that manually earlier on
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALeftTwin
        (local.get $leftTwin_that_is_left_Copy_of_Argument)
      )
    )
  #endif

  (call $construct_at_location_new_application_node
    (local.get $new_right_applicationNode_Location)
    (local.get $right_superimposed_Header)
    (local.get $rightTwin_that_is_right_Copy_of_Argument)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $new_right_applicationNode_Location)
        (call $APPLICATION_NODE__POSITION_OF_TOBEAPPLIED)
      )
    )
    (local.get $right_superimposed_Header)
  )
  ;; no repair for $leftTwin_that_is_left_Copy_of_Argument as we did that manually earlier on
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARightTwin
        (local.get $rightTwin_that_is_right_Copy_of_Argument)
      )
    )
  #endif

  (local.set $new_left_Application_Header
    (call $construct_ApplicationHeader
      (local.get $new_left_applicationNode_Location)
    )
  )
  (local.set $new_right_Application_Header
    (call $construct_ApplicationHeader
      (local.get $new_right_applicationNode_Location)
    )
  )

  ;; new superpostion node

  (call $construct_at_location_new_superposition_node
    (local.get $new_superpositionNode_Location)
    (local.get $new_left_Application_Header)
    (local.get $new_right_Application_Header)
  )

  (local.set $new_result_Superposition_Header
    (call $construct_SuperpositionHeader
      (local.get $duplicationLabel)
      (local.get $new_superpositionNode_Location)
    )
  )

  (local.get $new_result_Superposition_Header)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $new_result_Superposition_Header)
      )
    )
  #endif
)
