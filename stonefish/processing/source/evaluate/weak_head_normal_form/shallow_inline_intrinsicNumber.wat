(func
  $shallow_inline_IntrinsicNumber
  (param $theOtherTwinLeash i64)
  (param $IntrinsiscNumberHeader_to_be_inlined i64)

  (result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscNumber
        (local.get $IntrinsiscNumberHeader_to_be_inlined)
      )
    )
  #endif

  (call $followTwinLeashAnReplaceTwinByFinger
    (local.get $theOtherTwinLeash)
    (local.get $IntrinsiscNumberHeader_to_be_inlined)
  )
  ;; since the finger is not a variable, we do not have to repair backlinks
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $IntrinsiscNumberHeader_to_be_inlined)
        )
      )
    )
  #endif

  (local.get $IntrinsiscNumberHeader_to_be_inlined)
)
