
(func
  $evaluate_ruleCall
  (param $ruleHeader i64)

  (result i64)

  (local $rule_identification i64)
  (local $result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $ruleHeader)
      )
    )
  #endif

  (local.set $rule_identification
    (call $extract_RuleIdentification_fromRuleHeader
      (local.get $ruleHeader)
    )
  )

  (local.set $result
    (block $break
      (result i64)

      ;; first we try out to find if we know the id our self
      ;; in that case we implement the logic our self

      ;; magic or intrinsic rules

      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_Encapsulate)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_encapsulation
              (local.get $ruleHeader)
            )
          )
        )
      )
      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_DropLeftNullaryConstructorArgumentAndReturnRightArgument)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_DropLeftNullaryConstructorArgumentAndReturnRightArgument
              (local.get $ruleHeader)
            )
          )
        )
      )


      ;; pure and one step math functions

      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_Float32_IsLessThan)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_Float32_IsLessThan
              (local.get $ruleHeader)
            )
          )
        )
      )

      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_Float32_Addition_RoundingDown)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_Float32_Addition_RoundingDown
              (local.get $ruleHeader)
            )
          )
        )
      )

      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_Float32_Square_RoundingDown)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_Float32_Square_RoundingDown
              (local.get $ruleHeader)
            )
          )
        )
      )

      (if
        (call $isInBetween_closedIntervall
          ;; TODO magic rule identificatin 4e1556de4aa9da14bcd1ec814df3756d95820fc87906bd9236e65323eed77bc4
          (call $MAGIC_RULE_ID_Float32_Negate)
          (local.get $rule_identification)
          ;; TODO magic rule identificatin 4e1556de4aa9da14bcd1ec814df3756d95820fc87906bd9236e65323eed77bc4
          (call $MAGIC_RULE_ID_Float32_SquareRoot_RoundingDown)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_Bits32_UnaryOperator
              (local.get $rule_identification)
              (local.get $ruleHeader)
            )
          )
        )
      )

      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_Float32_Multiplicate_RoundingDown)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_Bits32_BinaryOperator
              (local.get $rule_identification)
              (local.get $ruleHeader)
            )
          )
        )
      )


      ;; we are the embedder for the capsules. we provided "imports" for the capsule via magic functions
      ;; now it happens that we need to pass functionality to the capsule that we imported ourself from our embedder

      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_MAGIC_RULE_ID_WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule
              (local.get $ruleHeader)
            )
          )
        )
      )

      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_BoolNeedsToBeSetTo)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_BoolNeedsToBeSetTo
              (local.get $ruleHeader)
            )
          )
        )
      )



      ;; magic rules to be disposed of in the future?

      (if
        (call $i64.isEqual
          (call $MAGIC_RULE_ID_If)
          (local.get $rule_identification)
        )

        (then
          (br $break
            (call $implement_behaviour_of_rule_of_if
              (local.get $ruleHeader)
            )
          )
        )
      )



      ;; try to find a user provided rule

      (br $break
        (call $try_apply_user_provided_rule
          (local.get $ruleHeader)
        )
      )

      unreachable
    )
  )

  ;; the rule specific logic got invoked soly by weak-head-normal-form-evaluation of the RuleCall.
  ;; Therefore the thing returned is to be evauluated to weak head normal form when returned
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $gotTermReduced
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)




(func
  $THE_ONE_RULE_ARGUMENT_THAT_IS_TO_BE_PATTERN_MATCHED
  (result i64)
  (i64.const 0)
)

(func
  $try_apply_user_provided_rule
  (param $ruleHeader i64)

  (result i64)
  (local $result i64)


  (local $RULE_ID i64)
  (local $the_header_to_be_pattern_matched_unevaluated i64)
  (local $argument_to_be_pattern_matched i64)


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARuleCall
        (local.get $ruleHeader)
      )
    )
  #endif

  (local.set $RULE_ID
    (call $extract_RuleIdentification_fromRuleHeader
      (local.get $ruleHeader)
    )
  )


  ;; SECURITY CONCEPT
  ;; we assume that the rule we encounter is safe to invoke.
  ;; also we assume the rule actually exists
  ;; the embedding code is responsible for understanding the stonefish API documentation
  ;; the embedder is responsible for managing what code is allowed to call what rules
  ;; the rules might trigger events for the embedder to invoke effekts


  ;; TODO what macro comes before what macro?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $check_for_rule_existence
        (local.get $RULE_ID)
      )
    )
  #endif


  ;; QUESTION
  ;; currently (2023) there are no default branches. That means the (currently first and only) argument is strict
  ;; should there be a default branch later on then the currently one argument is still strict
  ;; only with one default branch the argument could be non strict. Should this trivial case be forbidden?
  ;; what about the identity rule?


  (local.set $the_header_to_be_pattern_matched_unevaluated
    (call $getSubFinger_from_birthnumber_and_finger
      (call $THE_ONE_RULE_ARGUMENT_THAT_IS_TO_BE_PATTERN_MATCHED)
      (local.get $ruleHeader)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_1)
        (call $getArityFromRuleHeader
          (local.get $ruleHeader)
        )
      )
    )
  #endif

  (call $relinquish_Node_behind_Finger
    (local.get $ruleHeader)
  )

  (local.set $argument_to_be_pattern_matched
    (call $evaluate_to_weak_head_normal_form
      (local.get $the_header_to_be_pattern_matched_unevaluated)
    )
  )


  (local.set $result
    (block $break
      (result i64)

      (if
        (call $isFingerASuperposition
          (local.get $argument_to_be_pattern_matched)
        )
        (then
          (br $break
            (call $move_rule_calling_into_superposition
              (local.get $RULE_ID)
              (local.get $argument_to_be_pattern_matched)
            )
          )
        )
      )

      (if
        (call $isFingerAConstructor
          (local.get $argument_to_be_pattern_matched)
        )
        (then
          (br $break
            (call $grow_graph_case_for_user_provided_rule
              (local.get $RULE_ID)
              (local.get $argument_to_be_pattern_matched)
            )
          )
        )
      )

      ;; TODO is it enouth to assert this?
      (call $runtime_errors.expected_constructor_for_pattern_matching_or_Superposition_but_got_x
        (local.get $argument_to_be_pattern_matched)
      )
      unreachable
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)

;; TODO remove all user provided rules that are only magicical because it was faster in the beginning to do it manually

(func $grow_graph_case_for_user_provided_rule
  (param $rule_identification i64)
  (param $constructorHeader i64)


  (result i64)
  (local $result i64)

  (local $matchedConstructorIdentification i64)
  (local $rightSide_zero_to_eight_Lambdas i64)

  (local $index_of_next_child_to_handle__beginning_from_one i64)
  (local $child_of_matched_constructor i64)
  (local $arity i64)
  (local $case_boundings i64)

  ;; NOTE currently the verficator verfied that user provided rules have only arity of 1
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (call $ARITY_OF_1)
        (call $getArityFromRuleId
          (local.get $rule_identification)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $constructorHeader)
      )
    )
  #endif

  (local.set $matchedConstructorIdentification
    (call $extractConstructorId_from_ConstructorFinger
      (local.get $constructorHeader)
    )
  )

  (local.set $rightSide_zero_to_eight_Lambdas
    (call $link_rule_fragment
      (local.get $rule_identification)
      (local.get $matchedConstructorIdentification)
    )
  )

  (local.set $arity
    (call $extractArityFromConstructorFinger
      (local.get $constructorHeader)
    )
  )

  (local.set $index_of_next_child_to_handle__beginning_from_one
    (i64.const 0)
  )

  (local.set $case_boundings
    (local.get $rightSide_zero_to_eight_Lambdas)
  )

  ;; TODO check that this loop is properly optimized
  (block $break
    (loop $goThroughNodes
      (if
        (call $i64.isEqual
          (local.get $arity)
          (local.get $index_of_next_child_to_handle__beginning_from_one)
        )

        (then
          (br $break)
        )
      )

      (local.set $child_of_matched_constructor
        (call $getSubFinger_from_birthnumber_and_finger
          (local.get $index_of_next_child_to_handle__beginning_from_one)
          (local.get $constructorHeader)
        )
      )

      (local.set $case_boundings
        (call $compose_application_node
          (local.get $case_boundings)
          (local.get $child_of_matched_constructor)
        )
      )

      (local.set $index_of_next_child_to_handle__beginning_from_one
        (call $i64.increment
          (local.get $index_of_next_child_to_handle__beginning_from_one)
        )
      )

      (br $goThroughNodes)
    )
  )

  (if
    (call $not
      (call $i64.isEqual
        (call $ARITY_OF_0)
        (local.get $arity)
      )
    )

    (then
      (call $relinquish_Node_behind_Finger
        (local.get $constructorHeader)
      )
    )
  )



  (local.set $result
    (call $evaluate_to_weak_head_normal_form
      (local.get $case_boundings)
    )
  )

  (local.get $result)
)

