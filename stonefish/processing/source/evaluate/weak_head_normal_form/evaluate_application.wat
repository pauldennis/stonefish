
(func
  $evaluate_Application
  (param $applicationHeader i64)
  (result i64)

  (local $finger_ofTheToBeAppliedExpression i64)
  (local $finger_theArgument i64)

  (local $final_result i64)

  (local $wannebe_applicable_thing i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnApplication
        (local.get $applicationHeader)
      )
    )
  #endif

  (local.set $finger_ofTheToBeAppliedExpression
    (call $getTheToBeAppliedPart
      (local.get $applicationHeader)
    )
  )

  ;; first we gota reduce the to be applied expression to weak head normal form inorder to actually know how to feed the argument to it
  (local.set $wannebe_applicable_thing
    (call $evaluate_to_weak_head_normal_form
      (local.get $finger_ofTheToBeAppliedExpression)
    )
  )

  ;; only access the argument after (as it could have been updated)
  ;; TODO testcase for this?
  (local.set $finger_theArgument
    (call $getTheToArgumentPart ;; TODO remove To in the name?
      (local.get $applicationHeader)
    )
  )

  ;; TODO this check is to be removed, because it doesnt matter what the argument is?
  ;; TODO What cases are missing?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $identified_assert (local.get $finger_theArgument)
      (call $FALSE)
      (call $or
        (call $isFingerAConstructor
          (local.get $finger_theArgument)
        )
      )
      (call $or
        (call $isFingerAnIntrinsiscNumber
          (local.get $finger_theArgument)
        )
      )
      (call $or
        (call $isFingerARuleCall
          (local.get $finger_theArgument)
        )
      )
      (call $or
        (call $isFingerATwin
          (local.get $finger_theArgument)
        )
      )
      (call $or
        (call $isFingerACapsule
          (local.get $finger_theArgument)
        )
      )
      (call $or
        (call $isFingerASuperposition
          (local.get $finger_theArgument)
        )
      )
      (call $or
        (call $isFingerALambda
          (local.get $finger_theArgument)
        )
      )
      (call $or
        (call $isFingerAnApplication
          (local.get $finger_theArgument)
        )
      )
    )
  #endif

  ;; we dont need the application node anymore
  ;; the application node bundled two fingers, the applicant and the argument
  ;; we dont need them anymore because the argument finger was included into the applicant and the applicant Finger got somehow returned
  ;; TODO try out to relinquish node before calling subroutine
  (call $relinquish_Node_at_Location
    (call $getAssociated_Application_NodeLocation
      (local.get $applicationHeader)
    )
  )

  (block $break_switch_case
    (if
      (call $isFingerALambda
        (local.get $wannebe_applicable_thing)
      )
      (then
        ;; all good. we know how to apply a lambda to an argument
        (local.set $final_result
          (call $apply_lambda_to_argument
            (local.get $wannebe_applicable_thing)
            (local.get $finger_theArgument)
          )
        )
        (br $break_switch_case)
      )
    )

    (if
      (call $isFingerACapsule
        (local.get $wannebe_applicable_thing)
      )
      (then
        ;; all good. i hope i know how apply a capsule to an argument
        (local.set $final_result
          (call $apply_capsule_to_argument
            (local.get $wannebe_applicable_thing)
            (local.get $finger_theArgument)
          )
        )
        (br $break_switch_case)
      )
    )

    (if
      (call $isFingerASuperposition
        (local.get $wannebe_applicable_thing)
      )
      (then
        ;; probably good as we can move the application into the superposition
        (local.set $final_result
          (call $apply_superposition_to_argument
            (local.get $wannebe_applicable_thing)
            (local.get $finger_theArgument)
          )
        )
        (br $break_switch_case)
      )
    )

    ;; TODO turn off in reease mode DEEP_FORCE_SUPPORT
    (if
      (call $isFingerASoloVariable
        (local.get $wannebe_applicable_thing)
      )
      (then
        ;; return appicatin unchanged
        (return
          (local.get $applicationHeader)
        )
      )
    )

    ;; TODO is it possible to statically render all these cases impossible?
    (call $user-inflicted_error_case.cannot_apply_x
      (local.get $wannebe_applicable_thing)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $final_result)
      )
    )
  #endif

  (local.get $final_result)
)


