(func
  $apply_capsule_to_argument
  (param $oldCapsuleHeader i64)
  (param $theArgument_Header i64)

  (result i64)

  ;; pseudocode:

  ;; Applicate (OldCapsule privateData interface) argument
  ;;   = dup interface1, interface2
  ;;           := interface
  ;;     let ReturnNewPrivateDataAndSystemRequest newPrivateData result
  ;;           := (interface1 privateData) argument
  ;;     in
  ;;       CapsuleCallResult
  ;;         (NewCapsule newPrivateData interface2)
  ;;         result

  (local $oldPrivateDataOfCapsule_Header i64)
  (local $originalInterface_Header i64)

  (local $newCapsule_NodeLocation i64)
  (local $duplicationNodeForCopyOfInterface_NodeLocation i64)
  (local $innerApply_NodeLocation i64)
  (local $outerApply_NodeLocation i64)
  (local $CapsuleApplicationResult_NodeLocation i64)

  (local $newDuplicationLabel i64)

  (local $leftLeash__InterfaceInvokation i64)
  (local $rightLeash__preserveInterfaceForNewCapsule i64)
  (local $leftTwin_ThatIsCopyOfOldInterface_forActuallCalling i64)
  (local $rightTwin_ThatIsCopyOfOldInterface_forReEncapsulating i64)

  (local $innerApplication_Header i64)
  (local $outerApplication_Header i64)

  (local $supposedlyCapsuleCallResult_inWeakHeadNormalForm i64)
  (local $updatedPrivateData_SubHeader i64)
  (local $resultFromCapsuleInterfaceCall_SubHeader i64)

  (local $newCapsule_Header i64)
  (local $newCapsuleApplicationResult_Header i64)


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerACapsule
        (local.get $oldCapsuleHeader)
      )
    )
  #endif

  ;; TODO parse_and_pattern_match
  (local.set $oldPrivateDataOfCapsule_Header
    (call $getPrivateDataPartOfCapsule
      (local.get $oldCapsuleHeader)
    )
  )
  (local.set $originalInterface_Header
    (call $getInterfacePartOfCapsule
      (local.get $oldCapsuleHeader)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $in_case_of_lambda_the_leash_points_to_solovariable
        (local.get $originalInterface_Header)
      )
    )
  #endif

  ;; NOTE be aware that this might be a problem when the the two capsule members (private data and interface) are variables
  ;; but it seems to be ok since they are both used in the interface invokation application nodes that are composed before
  ;; evaluate gets called
  (call $relinquish_Node_behind_Finger
    (local.get $oldCapsuleHeader)
  )

  ;; TODO find out fastest sorting of these allocations
  (local.set $outerApply_NodeLocation
    (call $request_Node__might_trap
    )
  )
  (local.set $duplicationNodeForCopyOfInterface_NodeLocation
    (call $request_Node__might_trap
    )
  )
  (local.set $innerApply_NodeLocation
    (call $request_Node__might_trap
    )
  )
  (local.set $newCapsule_NodeLocation
    (call $request_Node__might_trap
    )
  )
  (local.set $CapsuleApplicationResult_NodeLocation
    (call $request_Node__might_trap
    )
  )

  ;; introduced duplication node
  ;; because interface is used twice
  ;; (one time as copy for new capsule and
  ;; one time for invoking the interface of the old capsule)
  (local.set $leftLeash__InterfaceInvokation
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $innerApply_NodeLocation)
      (call $APPLICATION_NODE__POSITION_OF_TOBEAPPLIED)
    )
  )
  (local.set $rightLeash__preserveInterfaceForNewCapsule
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $newCapsule_NodeLocation)
      (call $CAPSULE__POSITION_OF_INTERFACE)
    )
  )
  (local.set $newDuplicationLabel
    (call $getNewDuplicationLabel
    )
  )
  (local.set $leftTwin_ThatIsCopyOfOldInterface_forActuallCalling
    (call $construct_TwinHeader
      (call $leftTwinVariant)
      (local.get $newDuplicationLabel)
      (local.get $duplicationNodeForCopyOfInterface_NodeLocation)
    )
  )
  (local.set $rightTwin_ThatIsCopyOfOldInterface_forReEncapsulating
    (call $construct_TwinHeader
      (call $rightTwinVariant)
      (local.get $newDuplicationLabel)
      (local.get $duplicationNodeForCopyOfInterface_NodeLocation)
    )
  )
  (call $construct_at_location_new_duplication_node
    (local.get $duplicationNodeForCopyOfInterface_NodeLocation)
    (local.get $leftLeash__InterfaceInvokation)
    (local.get $rightLeash__preserveInterfaceForNewCapsule)
    (local.get $originalInterface_Header)
  )

  ;; inner application node
  (call $construct_at_location_new_application_node
    (local.get $innerApply_NodeLocation)
    (local.get $leftTwin_ThatIsCopyOfOldInterface_forActuallCalling)
    (local.get $oldPrivateDataOfCapsule_Header)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $innerApply_NodeLocation)
        (call $APPLICATION_NODE__POSITION_OF_THEARGUMENT)
      )
    )
    (local.get $oldPrivateDataOfCapsule_Header)
  )
  (local.set $innerApplication_Header
    (call $construct_ApplicationHeader
      (local.get $innerApply_NodeLocation)
    )
  )

  ;; outer application
  (call $construct_at_location_new_application_node
    (local.get $outerApply_NodeLocation)
    (local.get $innerApplication_Header)
    (local.get $theArgument_Header)
  )
  ;; no repair for $innerApplication_Header as we now it is not a variable
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $innerApplication_Header)
        )
      )
    )
  #endif
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $outerApply_NodeLocation)
        (call $APPLICATION_NODE__POSITION_OF_THEARGUMENT)
      )
    )
    (local.get $theArgument_Header)
  )
  (local.set $outerApplication_Header
    (call $construct_ApplicationHeader
      (local.get $outerApply_NodeLocation)
    )
  )

  ;; currently we are not as lazy as we cloud get. If this is good or bad is yet to be found out. For now i am doing it this way because that way i dont have to implement another Rule for fst and snd of the ReturnNewPrivateDataAndSystemRequest tuple -- using it twice and having an another duplication node.
  ;; It might be because of strictness analysis this is actually a good thing to do. Lets wait and see. TODO

  ;; partially populate the new capsule
  ;; place the rightTwin at the interface fingerlocation even though the firstborn sibling is not known yet
  ;; this is done inorder to allow the twin be replaced by the interface shallow copy when the other twin is evaluated
  ;; both twins need to be placed on a proper memory location inorder to be updated when the other twin is evaluated
  (call $place_at_ConstructorNodeLocation_as_child_x_a_Finger
    (local.get $newCapsule_NodeLocation)
    (call $CAPSULE__POSITION_OF_INTERFACE)
    (local.get $rightTwin_ThatIsCopyOfOldInterface_forReEncapsulating)
  )
  ;; we want to get the updated private Data and the interface call result
  (local.set $supposedlyCapsuleCallResult_inWeakHeadNormalForm
    (call $evaluate_to_weak_head_normal_form
      (local.get $outerApplication_Header)
    )
  )

  ;; extract to consume_ReturnNewPrivateDataAndSystemRequest?
  ;; TODO put into its own function
  ;; TODO various error cases, that might be possible to make unuterable by some kind of type checker?
  (if
    (call $isFingerAConstructor
      (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
    )
    (then
    )
    (else
      (call $user-inflicted_error_case.expected_from_interface_to_return_a_constructor_but_got_x
        (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
      )
    )
  )
  (if
    (call $i64.isEqual
      (call $CONSTRUCTOR_ID__ReturnNewPrivateDataAndSystemRequest) ;; TODO automate generation of identifications
      (call $extractConstructorId_from_ConstructorFinger
        (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
      )
    )
    (then
    )
    (else
      (call $user-inflicted_error_case.expected_from_interface_to_return_a_constructor_called_ReturnNewPrivateDataAndSystemRequest_but_got_x
        (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
      )
    )
  )
  (if
    (call $i64.isEqual
      (call $ARITY_OF_TWO)
      (call $extractArityFromConstructorFinger
        (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
      )
    )
    (then
    )
    (else
      (call $user-inflicted_error_case.expected_from_interface_to_return_a_constructor_with_arity_of_two_but_got_x
        (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
      )
    )
  )

  ;; now that we checked the returned data it is save to get the first child

  (local.set $updatedPrivateData_SubHeader
    (call $getNthConstructorChild
      (call $ReturnNewPrivateDataAndSystemRequest__POSITION_OF_UPDATED_PRIVATE_DATA)
      (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
    )
  )
  ;; TODO maybe move this debug marking into the extractor? Are there cases where we want only to peek but not mark as unused again
  ;; TODO is this side effecto ok to have in a \STONEFISH_SHOULD_INCLUDE_ASSERT? Or should it be included into an assertGroup?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $mark_Nth_Child_of_Finger_as_unused
      (call $ReturnNewPrivateDataAndSystemRequest__POSITION_OF_UPDATED_PRIVATE_DATA)
      (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
    )
  #endif

  ;; aparently we need to somehow prevent a space leak of untouched private data of a capsule used in a loop
  ;; TODO there could be another scheme:
  ;;   - have a rule with two parameters
  ;;       - updated private data
  ;;       - api twin variable
  ;;   - when the rule is evaluated  it weak-heads the private data
  ;;   - that way the user can decide when the private data gets evaluated.
  ;; would such a scheme make sense?
  ;; NOTE $resultFromCapsuleInterfaceCall_SubHeader might be a variable that gets updated by this evaluation. This means $supposedlyCapsuleCallResult_inWeakHeadNormalForm may not be relinquished yet
  (local.set $updatedPrivateData_SubHeader
    (call $evaluate_to_weak_head_normal_form
      (local.get $updatedPrivateData_SubHeader)
    )
  )

  ;; resultFromCapsuleInterfaceCall_SubHeader might be variable
  ;; this means we can only relinquish $supposedlyCapsuleCallResult_inWeakHeadNormalForm only after
    ;; TODO after what?


  ;; only the private data needs to be updated as the interface is taken care of earlier by the twin
  ;; now we have the updated private data so we can put it into the place the user can find it
  (call $place_at_ConstructorNodeLocation_as_child_x_a_Finger
    (local.get $newCapsule_NodeLocation)
    (call $CAPSULE__POSITION_OF_PRIVATE_DATA)
    (local.get $updatedPrivateData_SubHeader)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $newCapsule_NodeLocation)
        (call $CAPSULE__POSITION_OF_PRIVATE_DATA)
      )
    )
    (local.get $updatedPrivateData_SubHeader)
  )
  ;; capsulate thing again. use the same mechanism that the user is using as well
  ;; TODO currently the evaluate_to_weak_head_normal_form should only increment the tag by one. So it would make sense for this function call to be inlined
  (local.set $newCapsule_Header
    (call $evaluate_to_weak_head_normal_form
      (call $construct_EncapsulateRuleCall
        (local.get $newCapsule_NodeLocation)
      )
    )
  )

  ;; NOTE relinquish happens after one-step encapsulationCall with $evaluate_to_weak_head_normal_form inorder to not trigger consistency checks
  (local.set $resultFromCapsuleInterfaceCall_SubHeader
    (call $getNthConstructorChild
      (call $ReturnNewPrivateDataAndSystemRequest__POSITION_OF_CAPSULE_CALL_RESULT)
      (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
    )
  )
  (call $relinquish_Node_behind_Finger
    (local.get $supposedlyCapsuleCallResult_inWeakHeadNormalForm)
  )






  ;; one needs to somehow return the new capsule and the interface call result
  (call $place_at_ConstructorNodeLocation_as_child_x_a_Finger
    (local.get $CapsuleApplicationResult_NodeLocation)
    (call $CapsuleCallResult__POSITION_OF_MODIFIED_CAPSULE)
    (local.get $newCapsule_Header)
  )
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $newCapsule_Header)
        )
      )
    )
  #endif
  (call $place_at_ConstructorNodeLocation_as_child_x_a_Finger
    (local.get $CapsuleApplicationResult_NodeLocation)
    (call $CapsuleCallResult__POSITION_OF_CAPSULE_APPLICATION_RESULT)
    (local.get $resultFromCapsuleInterfaceCall_SubHeader)
  )
  (call $maybeInstallNewLeashIfVariable
    (call $construct_leash_from_fingerLocation
      (call $nodeLocation_and_fingerOffset_to_FingerLocation
        (local.get $CapsuleApplicationResult_NodeLocation)
        (call $CapsuleCallResult__POSITION_OF_CAPSULE_APPLICATION_RESULT)
      )
    )
    (local.get $resultFromCapsuleInterfaceCall_SubHeader)
  )

  (local.set $newCapsuleApplicationResult_Header
    (call $construct_CapsuleApplicationResult
      (local.get $CapsuleApplicationResult_NodeLocation)
    )
  )

  (local.get $newCapsuleApplicationResult_Header)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $newCapsuleApplicationResult_Header)
      )
    )
  #endif

)
