
(func
  $shallow_inline_Lambda
  (param $duplicationLabel i64)
  (param $theOtherTwinLeash i64)
  (param $lambdaHeader_to_be_inlined i64)
  (param $areWeReducingALeftTwin i32)

  (result i64)

  (local $new_body_duplication_node_location i64)
  (local $new_left_shallow_copy_of_lambda_node_location i64)
  (local $new_right_shallow_copy_of_lambda_node_location i64)
  (local $new_superposition_node_location i64)

  (local $leash_to_deep_buried_solo_variable i64)
  (local $new_left_lambdaCopy_header i64)
  (local $new_right_lambdaCopy_header i64)
  (local $new_superpositionHeader i64)

  ;; ingredients for the two new lambda nodes
  (local $left_leash_to_superimposed_solo_variable i64)
  (local $right_leash_to_superimposed_solo_variable i64)
  (local $leftTwin_for_deepLambdaBody i64)
  (local $rightTwin_for_deepLambdaBody i64)

  ;; ingredients for the new superposition node
  (local $leftSuperimposed_SoloVariable i64)
  (local $rightSuperimposed_SoloVariable i64)

  ;; ingredients for new duplication node
  (local $deepLambdaBodyHeader i64)
  (local $left_TwinLeash i64)
  (local $right_TwinLeash i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $lambdaHeader_to_be_inlined)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $leash_of_LambdaHeader_leashes_to_a_SoloVariable
        (local.get $lambdaHeader_to_be_inlined)
      )
    )
  #endif


  ;; new locations
  (local.set $new_body_duplication_node_location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_left_shallow_copy_of_lambda_node_location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_right_shallow_copy_of_lambda_node_location
    (call $request_Node__might_trap
    )
  )
  (local.set $new_superposition_node_location
    (call $request_Node__might_trap
    )
  )

  (local.set $leash_to_deep_buried_solo_variable
    (call $getSoloVariableLeash_from_LambdaHeader
      (local.get $lambdaHeader_to_be_inlined)
    )
  )

  (local.set $new_left_lambdaCopy_header
    (call $construct_Pointer_by_taking_Header_and_replacing_location_part
      (local.get $lambdaHeader_to_be_inlined)
      (local.get $new_left_shallow_copy_of_lambda_node_location)
    )
  )
  (local.set $new_right_lambdaCopy_header
    (call $construct_Pointer_by_taking_Header_and_replacing_location_part
      (local.get $lambdaHeader_to_be_inlined)
      (local.get $new_right_shallow_copy_of_lambda_node_location)
    )
  )

  ;; repair external edge

  ;; TODO i think this is wrong?!?!?!
  ;; TODO refactor to be less hacky
  ;; TODO order matters because of superpositions!
  ;; TODO is this needed?
  (if
    (local.get $areWeReducingALeftTwin
    )
    (then
    )
    (else
      (local.set $new_left_lambdaCopy_header
      (local.set $new_right_lambdaCopy_header
        (call $i64.swap
          (local.get $new_left_lambdaCopy_header)
          (local.get $new_right_lambdaCopy_header)
        )
      )
      )
    )
  )

  (call $followTwinLeashAnReplaceTwinByFinger
    (local.get $theOtherTwinLeash)
    (local.get $new_right_lambdaCopy_header)
  )

  (local.set $new_superpositionHeader
    (call $construct_SuperpositionHeader
      (local.get $duplicationLabel)
      (local.get $new_superposition_node_location)
    )
  )

  ;; repair external edge
  ;; NOTE: this replacement must be done before the other location of (eb45874a706afa43a897)
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $new_superpositionHeader)
        )
      )
    )
  #endif
  (call $followSoloVariableLeash_And_replaceByFinger
    (local.get $leash_to_deep_buried_solo_variable)
    (local.get $new_superpositionHeader)
  )


  ;; set ingredients for lambda nodes

  (local.set $leftTwin_for_deepLambdaBody
    (call $construct_TwinHeader
      (call $leftTwinVariant)
      (local.get $duplicationLabel)
      (local.get $new_body_duplication_node_location)
    )
  )
  (local.set $rightTwin_for_deepLambdaBody
    (call $construct_TwinHeader
      (call $rightTwinVariant)
      (local.get $duplicationLabel)
      (local.get $new_body_duplication_node_location)
    )
  )

  (local.set $left_leash_to_superimposed_solo_variable
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_superposition_node_location)
      (call $SUPERPOSITION__POSITION_OF_LEFT_SUPERIMPOSED_TERM)
    )
  )
  (local.set $right_leash_to_superimposed_solo_variable
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_superposition_node_location)
      (call $SUPERPOSITION__POSITION_OF_RIGHT_SUPERIMPOSED_TERM)
    )
  )

  ;; set ingredients for new superpositionNode

  (local.set $leftSuperimposed_SoloVariable
    (call $construct_SoloVariableHeader
      (local.get $new_left_shallow_copy_of_lambda_node_location)
    )
  )
  (local.set $rightSuperimposed_SoloVariable
    (call $construct_SoloVariableHeader
      (local.get $new_right_shallow_copy_of_lambda_node_location)
    )
  )

  ;; set ingredients for new duplication

  ;; NOTE: this access to the body must be done after the other location of (eb45874a706afa43a897) since the body could have been the solo variable (Header) of the original lambda that got replaced by the new superpositionHeader
  (local.set $deepLambdaBodyHeader
    (call $getLambdaBody
      (local.get $lambdaHeader_to_be_inlined)
    )
  )

  (local.set $left_TwinLeash
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_left_shallow_copy_of_lambda_node_location)
      (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
    )
  )
  (local.set $right_TwinLeash
    (call $construct_leash_from_nodeLocation_and_fingerOffset
      (local.get $new_right_shallow_copy_of_lambda_node_location)
      (call $LAMBDA_NODE__POSITION_OF_LAMBDA_BODY)
    )
  )

  (call $construct_at_location_new_duplication_node
    (local.get $new_body_duplication_node_location)
    (local.get $left_TwinLeash)
    (local.get $right_TwinLeash)
    (local.get $deepLambdaBodyHeader)
  )
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $isFingerAVariable
          (local.get $deepLambdaBodyHeader)
        )
      )
    )
  #endif

  (call $construct_at_location_new_lambda_node
    (local.get $new_left_shallow_copy_of_lambda_node_location)
    (local.get $left_leash_to_superimposed_solo_variable)
    (local.get $leftTwin_for_deepLambdaBody)
  )
  (call $construct_at_location_new_lambda_node
    (local.get $new_right_shallow_copy_of_lambda_node_location)
    (local.get $right_leash_to_superimposed_solo_variable)
    (local.get $rightTwin_for_deepLambdaBody)
  )

  (call $construct_at_location_new_superposition_node
    (local.get $new_superposition_node_location)
    (local.get $leftSuperimposed_SoloVariable)
    (local.get $rightSuperimposed_SoloVariable)
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $leash_of_LambdaHeader_leashes_to_a_SoloVariable
        (local.get $new_left_lambdaCopy_header)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $leash_of_LambdaHeader_leashes_to_a_SoloVariable
        (local.get $new_right_lambdaCopy_header)
      )
    )
  #endif

  (call $relinquish_Node_at_Location
    (call $getAssociated_Lambda_NodeLocation
      (local.get $lambdaHeader_to_be_inlined)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $leash_of_LambdaHeader_leashes_to_a_SoloVariable
        (local.get $new_left_lambdaCopy_header)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $leash_of_LambdaHeader_leashes_to_a_SoloVariable
        (local.get $new_right_lambdaCopy_header)
      )
    )
  #endif

  ;; the right one was the replacement of the original right twin
  (local.get $new_left_lambdaCopy_header)

)

