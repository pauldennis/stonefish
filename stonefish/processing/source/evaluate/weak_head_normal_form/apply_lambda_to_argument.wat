
(func
  $apply_lambda_to_argument
  (param $lambdaHeader i64)
  (param $argumentFinger_that_is_the_replacement_for_the_variable i64)
  (result i64)

  (local $soloVariableLeash i64)
  (local $updatedBody i64)
  (local $updatedBody_inWeakHeadNormalForm i64)

  (local $final_result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $hasValidFingerType
        (local.get $argumentFinger_that_is_the_replacement_for_the_variable)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALambda
        (local.get $lambdaHeader)
      )
    )
  #endif

  (local.set $soloVariableLeash
    (call $getSoloVariableLeash_from_LambdaHeader
      (local.get $lambdaHeader)
    )
  )


  (call $followSoloVariableLeash_And_replaceByFinger
    (local.get $soloVariableLeash)
    (local.get $argumentFinger_that_is_the_replacement_for_the_variable)
  )
  ;; when we replace the solo variable with a variable we need to repair the backreference
  (call $maybeInstallNewLeashIfVariable
    (local.get $soloVariableLeash)
    (local.get $argumentFinger_that_is_the_replacement_for_the_variable)
  )


  (local.set $updatedBody
    (call $getLambdaBody
      (local.get $lambdaHeader)
    )
  )


  ;; we cant relinquish the node any earlier, because the lambda body might be the solo variable
  ;; TODO would it be worth to handle identitiy lambdas seperately?
  (call $relinquish_Node_at_Location
    (call $getAssociated_Lambda_NodeLocation
      (local.get $lambdaHeader)
    )
  )

  ;; the body result might not be in weak head normal form
  (local.set $final_result
    (call $evaluate_to_weak_head_normal_form
      (local.get $updatedBody)
    )
  )

  (local.get $final_result)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $final_result)
      )
    )
  #endif
)

