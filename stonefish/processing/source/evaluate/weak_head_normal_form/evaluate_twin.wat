
(func
  $evaluate_Twin
  (param $rootTwinFinger i64)
  (result i64)

  (local $areWeReducingALeftTwin i32)

  (local $duplicationLabel i64)

  (local $leftTwinUsageMarker i64)
  (local $rightTwinUsageMarker i64)

  (local $theOtherTwinLeash_that_is_to_replaced i64)

  (local $reducedDuplicationBody i64)

  (local $result i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerATwin
        (local.get $rootTwinFinger)
      )
    )
  #endif

  (local.set $areWeReducingALeftTwin
    (call $isFingerALeftTwin
      (local.get $rootTwinFinger)
    )
  )

  (local.set $duplicationLabel
    (call $extract_DuplicationLabel_fromTwinFinger
      (local.get $rootTwinFinger)
    )
  )

  (local.set $leftTwinUsageMarker
    (call $getLeftTwinLeash
      (local.get $rootTwinFinger)
    )
  )
  (local.set $rightTwinUsageMarker
    (call $getRightTwinLeash
      (local.get $rootTwinFinger)
    )
  )

  ;; NOTE for example we cannot shallow copy an application because we ought to return our result in weakhead normal form
  (local.set $reducedDuplicationBody
    (call $evaluate_to_weak_head_normal_form
      (call $getDuplicationBody
        (local.get $rootTwinFinger)
      )
    )
  )

  (local.set $theOtherTwinLeash_that_is_to_replaced
    (call $chooseTheOtherTwinLeash
      (local.get $areWeReducingALeftTwin)
      (local.get $leftTwinUsageMarker)
      (local.get $rightTwinUsageMarker)
    )
  )

  ;; check that both labels are the same
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $i64.isEqual
        (local.get $duplicationLabel)
        (call $extract_DuplicationLabel_fromTwinFinger
          (call $dereferenceSubNodeLocation
            (call $extractTheLocationWhereTheTwinVariableHeaderResides
              (local.get $theOtherTwinLeash_that_is_to_replaced)
            )
          )
        )
      )
    )
  #endif

  ;; TODO clean up this hack!!!
  ;; TODO IN CASE OF FORCING SUPPORT
  ;; aparently it is not needed now?
;;   (if
;;     (call $isFingerAWeakHead
;;       (local.get $reducedDuplicationBody)
;;     )
;;     (then
;;       ;; normal case
;;     )
;;     ;; bogus case
;;     (else
;;       (return
;;         (local.get $rootTwinFinger)
;;       )
;;     )
;;   )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $reducedDuplicationBody)
      )
    )
  #endif

  (local.set $result
    (call $shallow_inline_the_duplication_body
      (local.get $duplicationLabel)
      (local.get $theOtherTwinLeash_that_is_to_replaced)
      (local.get $reducedDuplicationBody)
      (local.get $areWeReducingALeftTwin)
    )
  )

  ;; TODO try to move this one before recursing
  ;; the old duplication node is now garbage
  (call $relinquish_Node_at_Location
    (call $getAssociatedFloatingDuplicationNodeLocation
      (local.get $rootTwinFinger)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $gotTermReduced
        (local.get $result)
      )
    )
  #endif

  (local.get $result)
)


(func
  $chooseTheOtherTwinLeash
  (param $isALeftTwin i32)
  (param $leftTwinUsageMarker i64) ;; TODO rename to leash
  (param $rightTwinUsageMarker i64)
  (result i64)

  (if
    (result i64)

    (local.get $isALeftTwin)

    (then
      (local.get $rightTwinUsageMarker)
    )

    (else
      (local.get $leftTwinUsageMarker)
    )
  )
)



(func
  $shallow_inline_the_duplication_body
  (param $duplicationLabel i64)
  (param $theOtherTwinLeash i64)
  (param $duplication_body i64)
  (param $areWeReducingALeftTwin i32)
  (result i64)

  (local $FingerTypeToBeDuplicated i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $gotTermReduced
        (local.get $duplication_body)
      )
    )
  #endif

  (local.set $FingerTypeToBeDuplicated
    (call $extractTagFromFinger
      (local.get $duplication_body)
    )
  )

  ;; TODO find out fastest sorting

  (if
    (call $is_the_tag_a_Constructor
      (local.get $FingerTypeToBeDuplicated)
    )

    (then
      (return
        (call $shallow_inline_constructor
          (local.get $duplicationLabel)
          (local.get $theOtherTwinLeash)
          (local.get $duplication_body)
          (local.get $areWeReducingALeftTwin)
        )
      )
    )
  )

  (if
    (call $is_the_tag_an_IntrinsiscNumber
      (local.get $FingerTypeToBeDuplicated)
    )

    (then
      (return
        (call $shallow_inline_IntrinsicNumber
          (local.get $theOtherTwinLeash)
          (local.get $duplication_body)
        )
      )
    )
  )

  (if
    (call $is_the_tag_a_Lambda
      (local.get $FingerTypeToBeDuplicated)
    )

    (then
      (return
        (call $shallow_inline_Lambda
          (local.get $duplicationLabel)
          (local.get $theOtherTwinLeash)
          (local.get $duplication_body)
          (local.get $areWeReducingALeftTwin)
        )
      )
    )
  )

  (if
    (call $is_the_tag_a_Superposition
      (local.get $FingerTypeToBeDuplicated)
    )

    (then
      (return
        (call $shallow_inline_superposition
          (local.get $duplicationLabel)
          (local.get $theOtherTwinLeash)
          (local.get $duplication_body)
          (local.get $areWeReducingALeftTwin)
        )
      )
    )
  )

  unreachable
)

