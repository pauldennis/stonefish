
(func
  $shallow_inline_constructor
  (param $duplicationLabel i64)
  (param $theOtherTwinLeash i64)
  (param $constructorHeader_to_be_inlined i64)
  (param $areWeReducingALeftTwin i32)

  (result i64)

  (local $arity i64)

  (local $newConstructorHeader_left i64)
  (local $newConstructorHeader_right i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAConstructor
        (local.get $constructorHeader_to_be_inlined)
      )
    )
  #endif


  (local.set $arity
    (call $extractArityFromConstructorFinger
      (local.get $constructorHeader_to_be_inlined)
    )
  )

  (if
    (result i64)

    (call $i64.isEqual
      (call $ARITY_OF_ZERO)
      (local.get $arity)
    )

    (then
      (call $replaceTwinByLeafConstructor
        (local.get $theOtherTwinLeash)
        (local.get $constructorHeader_to_be_inlined)
      )

      ;; TODO garbage collection seems to be done later?
      (local.get $constructorHeader_to_be_inlined)
    )

    (else
      ;; we need more complicated things to do, similar to the spinless taglass g-mashine (underlying calculus of haskell)
      (local.set $newConstructorHeader_left
        (local.set $newConstructorHeader_right
          (call $shallow_Inline_NonTrivial_Constructor
            (local.get $duplicationLabel)
            (local.get $arity)
            (local.get $constructorHeader_to_be_inlined)
          )
        )
      )

      ;; TODO: one could reuse the old constructor for the left constructor. in that case left twins keep there body data in the same place in the memory when read. a consumer of the right twin would have his data be copied out of the body structure which would make sense because he wants to consume the data anyway.
      ;; another possibility would be to allways replace the left twin with the left constructor


      ;; NOTE: It is essential replace the former left twin with the constructor that has the left twins as children
      ;; TODO do it less hacky
      (if
        (local.get $areWeReducingALeftTwin)

        (then
        )

        (else
          (local.set $newConstructorHeader_left
            (local.set $newConstructorHeader_right
              (call $i64.swap
                (local.get $newConstructorHeader_left)
                (local.get $newConstructorHeader_right)
              )
            )
          )
        )
      )

      (call $replaceTwinByConstructorWithChildren
        (local.get $theOtherTwinLeash)
        (local.get $newConstructorHeader_right)
      )

      (local.get $newConstructorHeader_left)
    )
  )

)


(func
  $shallow_Inline_NonTrivial_Constructor
  (param $duplicationLabel i64)
  (param $arity i64)
  (param $constructorHeader_to_be_inlined i64)

  (result i64) ;; left copy of constructor
  (result i64) ;; right copy of constructor

  ;; from that one we have always two
  (local $newConstructorLocation_left i64)
  (local $newConstructorLocation_right i64)

  (local $newConstructorHeader_left i64)
  (local $newConstructorHeader_right i64)

  ;; duplication nodes indexed from 0 to 7
  (local $newDuplicationNode_Location_for_Child_0 i64)
  (local $newDuplicationNode_Location_for_Child_1 i64)
  (local $newDuplicationNode_Location_for_Child_2 i64)
  (local $newDuplicationNode_Location_for_Child_3 i64)
  (local $newDuplicationNode_Location_for_Child_4 i64)
  (local $newDuplicationNode_Location_for_Child_5 i64)
  (local $newDuplicationNode_Location_for_Child_6 i64)
  (local $newDuplicationNode_Location_for_Child_7 i64)

  (local $deepChild i64)


  ;; TODO separate arity checking into its own function?
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isInBetween_closedIntervall
        (call $MINIMUM_OF_ARITY)
        (local.get $arity)
        (call $MAXIMUM_OF_ARITY)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $not
        (call $i64.isEqual
          (call $ARITY_OF_ZERO)
          (local.get $arity)
        )
      )
    )
  #endif




  (local.set $newConstructorLocation_left
    (call $request_Node__might_trap)
  )
  (local.set $newConstructorLocation_right
    (call $request_Node__might_trap)
  )




  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



  ;;;;
  ;; allocate space for the 1 to 8 new duplication nodes
  ;; LOOP
  (block $treat_child_index_0_to_7
  (block $treat_child_index_1_to_7
  (block $treat_child_index_2_to_7
  (block $treat_child_index_3_to_7
  (block $treat_child_index_4_to_7
  (block $treat_child_index_5_to_7
  (block $treat_child_index_6_to_7
  (block $treat_child_index_7_to_7

    ;; NOTE arity of zero is forbideen
    (if (call $i64.isEqual (call $ARITY_OF_1) (local.get $arity)) (then (br $treat_child_index_1_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_2) (local.get $arity)) (then (br $treat_child_index_2_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_3) (local.get $arity)) (then (br $treat_child_index_3_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_4) (local.get $arity)) (then (br $treat_child_index_4_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_5) (local.get $arity)) (then (br $treat_child_index_5_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_6) (local.get $arity)) (then (br $treat_child_index_6_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_7) (local.get $arity)) (then (br $treat_child_index_7_to_7)))

    ;; NOTE nothing to skip for arity 8
    (local.set $newDuplicationNode_Location_for_Child_7 ;; N
      (call $request_Node__might_trap)
    )
  )
    (local.set $newDuplicationNode_Location_for_Child_6 ;; N
      (call $request_Node__might_trap)
    )
  )
    (local.set $newDuplicationNode_Location_for_Child_5 ;; N
      (call $request_Node__might_trap)
    )
  )
    (local.set $newDuplicationNode_Location_for_Child_4 ;; N
      (call $request_Node__might_trap)
    )
  )
    (local.set $newDuplicationNode_Location_for_Child_3 ;; N
      (call $request_Node__might_trap)
    )
  )
    (local.set $newDuplicationNode_Location_for_Child_2 ;; N
      (call $request_Node__might_trap)
    )
  )
    (local.set $newDuplicationNode_Location_for_Child_1 ;; N
      (call $request_Node__might_trap)
    )
  )
    (local.set $newDuplicationNode_Location_for_Child_0 ;; N
      (call $request_Node__might_trap)
    )
  )




  ;;;;
  ;; LOOP through children and create a duplication node for each deep child
  ;; n=0
  ;; LOOP
  #define STONEFISH_CREATE_DUPLICATION_NODE(N) \
    (local.set $deepChild \
      (call $getNthConstructorChild \
        (i64.const N) ;; \N \
        (local.get $constructorHeader_to_be_inlined) \
      ) \
    ) \
    (call $construct_at_location_new_duplication_node \
      ;; location of new duplication node \
      (local.get $MACRO_CONCAT(newDuplicationNode_Location_for_Child_,N)) ;; \N \
      ;; left leash \
      (call $construct_leash_from_nodeLocation_and_fingerOffset \
        (local.get $newConstructorLocation_left) \
        (i64.const N) ;; \N \
      ) \
      ;; right leash \
      (call $construct_leash_from_nodeLocation_and_fingerOffset \
        (local.get $newConstructorLocation_right) \
        (i64.const N) ;; \N \
      ) \
      ;; deep child \
      (local.get $deepChild) \
    ) \
    ;; TODO too much boiler plate? \
    (call $maybeInstallNewLeashIfVariable \
      (call $construct_leash_from_fingerLocation \
        (call $nodeLocation_and_fingerOffset_to_FingerLocation \
          (local.get $MACRO_CONCAT(newDuplicationNode_Location_for_Child_,N)) ;; \N \
          (call $DUPLICATION_NODE__POSITION_OF_DUPLICATION_BODY) \
        ) \
      ) \
      (local.get $deepChild) \
    )

  (block $treat_child_index_0_to_7
  (block $treat_child_index_1_to_7
  (block $treat_child_index_2_to_7
  (block $treat_child_index_3_to_7
  (block $treat_child_index_4_to_7
  (block $treat_child_index_5_to_7
  (block $treat_child_index_6_to_7
  (block $treat_child_index_7_to_7

    ;; NOTE arity of zero is forbideen
    (if (call $i64.isEqual (call $ARITY_OF_1) (local.get $arity)) (then (br $treat_child_index_1_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_2) (local.get $arity)) (then (br $treat_child_index_2_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_3) (local.get $arity)) (then (br $treat_child_index_3_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_4) (local.get $arity)) (then (br $treat_child_index_4_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_5) (local.get $arity)) (then (br $treat_child_index_5_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_6) (local.get $arity)) (then (br $treat_child_index_6_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_7) (local.get $arity)) (then (br $treat_child_index_7_to_7)))

    ;; NOTE nothing to skip for arity 8
    STONEFISH_CREATE_DUPLICATION_NODE(7)
  )
    STONEFISH_CREATE_DUPLICATION_NODE(6)
  )
    STONEFISH_CREATE_DUPLICATION_NODE(5)
  )
    STONEFISH_CREATE_DUPLICATION_NODE(4)
  )
    STONEFISH_CREATE_DUPLICATION_NODE(3)
  )
    STONEFISH_CREATE_DUPLICATION_NODE(2)
  )
    STONEFISH_CREATE_DUPLICATION_NODE(1)
  )
    STONEFISH_CREATE_DUPLICATION_NODE(0)
  )







  ;;;;
  ;; bundle the left and right twins in the two new ConstructorNodes
  ;; LOOP:
  ;; n=0
  #define STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(N) \
    (call $place_at_locations_of_twoSplicedConstructerNodes_twins_as_child_x \
      ;; locations of new constructors \
      (local.get $newConstructorLocation_left) \
      (local.get $newConstructorLocation_right) \
      ;; location of the new duplication node (for some deep child n) \
      (local.get $MACRO_CONCAT(newDuplicationNode_Location_for_Child_,N)) ;; \N \
      ;; spread label from orginal duplication node \
      (local.get $duplicationLabel) \
      ;; the birth number \
      (i64.const N) ;; \N \
    )

  (block $treat_child_index_0_to_7
  (block $treat_child_index_1_to_7
  (block $treat_child_index_2_to_7
  (block $treat_child_index_3_to_7
  (block $treat_child_index_4_to_7
  (block $treat_child_index_5_to_7
  (block $treat_child_index_6_to_7
  (block $treat_child_index_7_to_7

    ;; NOTE arity of zero is forbideen
    (if (call $i64.isEqual (call $ARITY_OF_1) (local.get $arity)) (then (br $treat_child_index_1_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_2) (local.get $arity)) (then (br $treat_child_index_2_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_3) (local.get $arity)) (then (br $treat_child_index_3_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_4) (local.get $arity)) (then (br $treat_child_index_4_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_5) (local.get $arity)) (then (br $treat_child_index_5_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_6) (local.get $arity)) (then (br $treat_child_index_6_to_7)))
    (if (call $i64.isEqual (call $ARITY_OF_7) (local.get $arity)) (then (br $treat_child_index_7_to_7)))

    ;; NOTE nothing to skip for arity 8
    STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(7)
  )
    STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(6)
  )
    STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(5)
  )
    STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(4)
  )
    STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(3)
  )
    STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(2)
  )
    STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(1)
  )
    STONEFISH_BUNDLE_TWINS_IN_NEW_CONSTRUCTOR_NODES(0)
  )





  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





  (local.set $newConstructorHeader_left
    (call $construct_Pointer_by_taking_Header_and_replacing_location_part
      (local.get $constructorHeader_to_be_inlined)
      (local.get $newConstructorLocation_left)
    )
  )
  (local.set $newConstructorHeader_right
    (call $construct_Pointer_by_taking_Header_and_replacing_location_part
      (local.get $constructorHeader_to_be_inlined)
      (local.get $newConstructorLocation_right)
    )
  )



  ;; throw away the old constructor, that got spliced up into two copies
  (call $relinquish_Node_at_Location
    (call $getAssociated_Constructor_NodeLocation
      (local.get $constructorHeader_to_be_inlined)
    )
  )

  (local.get $newConstructorHeader_left)
  (local.get $newConstructorHeader_right)
)










(func
  $place_at_locations_of_twoSplicedConstructerNodes_twins_as_child_x
  (param $constructerNodeLocation_left i64)
  (param $constructerNodeLocation_right i64)

  (param $the_one_duplicationNodeLocation i64)

  (param $duplicationLabel i64)

  (param $childOffset i64)

  (local $leftTwinFinger i64)
  (local $rightTwinFinger i64)

  (local.set $leftTwinFinger
    (local.set $rightTwinFinger
      (call $construct_twinFingers_from_label_and_duplicationNodeLocation
        (local.get $duplicationLabel)
        (local.get $the_one_duplicationNodeLocation)
      )
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $leftTwinFinger)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerALeftTwin
        (local.get $leftTwinFinger)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFinger
        (local.get $rightTwinFinger)
      )
    )
  #endif
  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerARightTwin
        (local.get $rightTwinFinger)
      )
    )
  #endif


  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $constructerNodeLocation_left)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $constructerNodeLocation_right)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidNodeLocationIndex
        (local.get $the_one_duplicationNodeLocation)
      )
    )
  #endif

  ;; might be not true in the future should there be more optimisation
  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $constructerNodeLocation_left)
        )
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $constructerNodeLocation_right)
        )
      )
    )
  #endif

  #ifdef STONEFISH_DEBUG_SPOILTOOL_HEAPSPOILS
    (call $impure_assert_group.HeapDebug
      (call $not
        (call $checkIfNodeIsUnused
          (local.get $the_one_duplicationNodeLocation)
        )
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isValidFingerOffset
        (local.get $childOffset)
      )
    )
  #endif


  ;; TODO refactor out the inner call, because it is used everywhere in this file
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $constructerNodeLocation_left)
      (local.get $childOffset)
    )
    (local.get $leftTwinFinger)
  )
  (call $overwrite_in_fingerNodeHeap_at_SubFingerLocation_with_some_replacement
    (call $nodeLocation_and_fingerOffset_to_FingerLocation
      (local.get $constructerNodeLocation_right)
      (local.get $childOffset)
    )
    (local.get $rightTwinFinger)
  )
)



