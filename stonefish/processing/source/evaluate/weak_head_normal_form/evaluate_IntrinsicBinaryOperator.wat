
(func
  $evaluate_IntrinsiscBinaryOperator
  (param $IntrinsiscBinaryOperator_Header i64)
  (result i64)

  (local $left_wanne_be_number i64)
  (local $right_wanne_be_number i64)

  (local $operationResultFinger i64)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscBinaryOperator
        (local.get $IntrinsiscBinaryOperator_Header)
      )
    )
  #endif

  (local.set $left_wanne_be_number
    (call $evaluate_to_weak_head_normal_form
      (call $getLeftOperand
        (local.get $IntrinsiscBinaryOperator_Header)
      )
    )
  )

  (if
    (call $isFingerAnIntrinsiscNumber
      (local.get $left_wanne_be_number)
    )
    (then
      ;; all good, as we know how to add intrinsic numbers
    )
    (else
      (call $user-inflicted_error_case.left_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number
        (local.get $left_wanne_be_number)
      )
    )
  )

  ;; NOTE: the right operand might have changed after evaluating the left operand
  (local.set $right_wanne_be_number
    (call $evaluate_to_weak_head_normal_form
      (call $getRightOperand
        (local.get $IntrinsiscBinaryOperator_Header)
      )
    )
  )

  (if
    (call $isFingerAnIntrinsiscNumber
      (local.get $right_wanne_be_number)
    )
    (then
      ;; all good, as we know how to add intrinsic numbers
    )
    (else
      (call $user-inflicted_error_case.right_argument_of_intrinsic_binary_operator_is_not_an_intrinsic_number
        (local.get $right_wanne_be_number)
      )
    )
  )

  (local.set $operationResultFinger
    (call $addTwoIntrinsicNumberFingers
      (local.get $left_wanne_be_number)
      (local.get $right_wanne_be_number)
    )
  )

  (call $relinquish_Node_at_Location
    (call $getAssociated_IntrinsicBinaryOperator_NodeLocation
      (local.get $IntrinsiscBinaryOperator_Header)
    )
  )

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAWeakHead
        (local.get $operationResultFinger)
      )
    )
  #endif

  (local.get $operationResultFinger)
)


(func
  $addTwoIntrinsicNumberFingers
  (param $leftFinger i64)
  (param $rightFinger i64)
  (result i64)

  (local $left_number i32)
  (local $right_number i32)
  (local $result_number i32)

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscNumber
        (local.get $leftFinger)
      )
    )
  #endif

  #if STONEFISH_SHOULD_INCLUDE_ASSERT
    (call $assert
      (call $isFingerAnIntrinsiscNumber
        (local.get $rightFinger)
      )
    )
  #endif

  ;; TODO replace by named function
  (local.set $left_number
    (call $i32.extract_InFingerPayload_32bit
      (local.get $leftFinger)
    )
  )
  (local.set $right_number
    (call $i32.extract_InFingerPayload_32bit
      (local.get $rightFinger)
    )
  )

  (if
    (call $i32.can_addition_be_represented
      (local.get $left_number)
      (local.get $right_number)
    )
    (then
      ;; all good result can be saved
    )
    (else
      (call $user-inflicted_error_case.addition_cannot_be_represented
        (local.get $left_number)
        (local.get $right_number)
      )
    )
  )

  (local.set $result_number
    (call $i32.add
      (local.get $left_number)
      (local.get $right_number)
    )
  )

  (call $construct_IntrinsicNumber_i32
    (local.get $result_number)
  )
)
