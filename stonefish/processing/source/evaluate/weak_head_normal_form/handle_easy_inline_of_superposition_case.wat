

;; this is the resolves superpositions that got introduced by duplicating a lamda
(func
  $handle_easy_inline_of_superposition_case
  (param $superPositionHeader_to_be_inlined i64)
  (param $theOtherTwinLeash i64)
  (param $areWeReducingALeftTwin i32)

  (result i64)

  (local $leftSuperimposedHeader i64)
  (local $rightSuperimposedHeader i64)

  (local $theReturnedSuperimposedHeader i64)
  (local $theOtherSuperimposedHeader i64)
  (local $reeusedTwinLeashAsVariableOccurenceLeash i64)

  (local $theReturnedSuperimposedHeader_inWeakHeadNormalForm i64)


  (local.set $leftSuperimposedHeader
    (call $getLeftSuperimposedHeader
      (local.get $superPositionHeader_to_be_inlined)
    )
  )
  (local.set $rightSuperimposedHeader
    (call $getRightSuperimposedHeader
      (local.get $superPositionHeader_to_be_inlined)
    )
  )

  (local.set $theOtherSuperimposedHeader
    (if
      (result i64)

      (local.get $areWeReducingALeftTwin)

      (then
        (local.get $rightSuperimposedHeader)
      )
      (else
        (local.get $leftSuperimposedHeader)
      )
    )
  )

  (local.set $theReturnedSuperimposedHeader
    (if
      (result i64)

      (local.get $areWeReducingALeftTwin)

      (then
        (local.get $leftSuperimposedHeader)
      )
      (else
        (local.get $rightSuperimposedHeader)
      )
    )
  )

  (local.set $reeusedTwinLeashAsVariableOccurenceLeash
    (local.get $theOtherTwinLeash)
  )

  (call $followTwinLeashAnReplaceTwinByFinger
    (local.get $theOtherTwinLeash)
    (local.get $theOtherSuperimposedHeader)
  )
  (call $maybeInstallNewLeashIfVariable
    (local.get $reeusedTwinLeashAsVariableOccurenceLeash)
    (local.get $theOtherSuperimposedHeader)
  )

  (call $relinquish_Node_behind_Finger
    (local.get $superPositionHeader_to_be_inlined)
  )

  (local.set $theReturnedSuperimposedHeader_inWeakHeadNormalForm
    (call $evaluate_to_weak_head_normal_form
      (local.get $theReturnedSuperimposedHeader)
    )
  )

  (local.get $theReturnedSuperimposedHeader_inWeakHeadNormalForm)
)

