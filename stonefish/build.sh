SHOULD_DO_DEPENDECY_UPDATE=$1
NO_INTERNET_NIX_FUCKUP=$2
  #https://github.com/NixOS/nix/issues/1985

set -e

if [[ "$1" == "no-dependency-update" ]] ; then
  echo "skip dependency update"
else
  ./fetch-dependencies.sh
fi

rsync --partial --progress --recursive --delete --checksum input-channels/niv-sources/nix processing/

pushd processing

  ./build.sh $NO_INTERNET_NIX_FUCKUP

popd
